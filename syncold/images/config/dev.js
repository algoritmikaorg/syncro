const DOMAIN = 'synchronize.ru';
const PROJECT_ID = 'cjd67we9401sw0155segppqze';

module.exports = {
  PROJECT_ID,
  SIMPLE_API_ENDPOINT: `http://api-dev.${DOMAIN}/simple/v1`,
  FILE_API_ENDPOINT: `http://file-dev.${DOMAIN}/file`,
  DIR: 'public/files',
  CONTENT_LENGTH: 25 * 1024 * 1024,
  LIMIT_OF_FILES_IN_DIR: 10,
};
