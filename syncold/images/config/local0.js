const DOMAIN = 'localhost:60000';
const PROJECT_ID = 'cjdagzahz001w0190w199ns7j';

module.exports = {
  PROJECT_ID,
  SIMPLE_API_ENDPOINT: `http://${DOMAIN}/simple/v1`,
  FILE_API_ENDPOINT: 'http://localhost:60001/file',
  DIR: 'public/files',
  CONTENT_LENGTH: 25 * 1024 * 1024,
  LIMIT_OF_FILES_IN_DIR: 10,
};
