const express = require('express');

const serve = express.static('public/files', { index: false });

const proxy = require('./../middlewares/proxy');
const batchImageTransform = require('./../middlewares/batchImageTransform');

const router = express.Router();


const checkParamForTransform = param => /\d{1,}x\d{1,}!?|\d{1,}x!?|x\d{1,}!?/g.test(param);

const checkParam1ResizeOrFilename = nextProxy => (req, res, next) => {
  const { params: { param1 } } = req;

  if (checkParamForTransform(param1)) {
    next();
  } else {
    nextProxy(req, res, next);
  }
};

/* GET home page. */
router.get('/', (req, res, next) => {
  res.send('OK');
});

router.get('/images/:secret', proxy);
router.get('/images/:secret/:param1', checkParam1ResizeOrFilename(proxy), ...batchImageTransform, serve);

router.get('/images/:secret/:param1/:param2', ...batchImageTransform, serve);
router.get('/images/:secret/:param1/:param2/param3', ...batchImageTransform, serve);


module.exports = router;
