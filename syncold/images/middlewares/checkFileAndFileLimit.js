const fs = require('fs-extra');
const async = require('async');

const {
  PROJECT_ID,
  DIR,
  LIMIT_OF_FILES_IN_DIR,
} = require('./../config');

module.exports = (req, res, next) => {
  const { params: { secret } } = req;
  const { filenameForSave } = res.locals;

  const { files } = req.app.locals;

  if (files[PROJECT_ID] && files[PROJECT_ID][secret] && files[PROJECT_ID][secret][filenameForSave]) {
    res.locals.fileAlreadyCreated = true;
  } else {
    res.locals.fileAlreadyCreated = false;
    res.locals.filesLimitReached = false;
    if (files[PROJECT_ID]
      && files[PROJECT_ID][secret]
      && Object.keys(files[PROJECT_ID][secret]).length >= LIMIT_OF_FILES_IN_DIR
    ) {
      res.locals.filesLimitReached = true;
    }
  }
  next();
};
