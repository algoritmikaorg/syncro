module.exports = {
  apps: [
    {
      name: 'site',
      script: './bin/www',
      watch: false,
      env: {
        PORT: 3000,
        NODE_ENV: 'development',
        LOCATION_ENV: 'dev',
      },
      env_production: {
        PORT: 3000,
        NODE_ENV: 'production',
        LOCATION_ENV: 'prod',
      },
    },
  ],
};
