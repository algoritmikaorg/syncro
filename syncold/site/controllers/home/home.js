const moment = require('moment');

const redis = require('./../../db/redis');
const { ROOT_TOKEN } = require('./../../config');

const { graphQlFetch } = require('./../../graphql');

const groupSheduleOfDate = (events) => {
  const output = [];
  const obj = {};
  events.forEach((event) => {
    const key = moment(event.date).format('YYYY-MM-DD');
    if (!obj[key]) {
      obj[key] = [];
    }
    obj[key].push(event);
  });
  return Object.keys(obj).map(key => ({ date: new Date(key), events: obj[key] }));
};

const mainPageQuery = (req) => {
  const date_gte = new Date(moment().format());
  const date_lte = new Date(moment().add(7, 'days').endOf('day').format());

  return graphQlFetch({ req, query: 'mainPageQuery', variables: { date_lte, date_gte } })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};

module.exports = (req, alias) => {
  return Promise.all([
    redis.batch([
      ['get', 'template'],
      ['get', 'howWork:mainpage'],
      ['get', 'gallery:maingallery'],
    ]).execAsync(),
    mainPageQuery(req),
  ])
    .then((result) => {
      const [[template, howWork, gallery], data] = result;
      const images = gallery ? JSON.parse(gallery).images : [];
      return Object.assign(
        {},
        data,
        data.allMainPages[0],
        {
          mainGallery: images,
          templateData: JSON.parse(template),
          groupSheduleOfDate: groupSheduleOfDate(data.allEvents),
        },
      );
    });
};
