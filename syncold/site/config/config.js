const dev = require('./dev');
const prod = require('./prod');
const local = require('./local');
const local0 = require('./local0');

console.log('NODE_ENV: ', process.env.NODE_ENV);
console.log('LOCATION_ENV: ', process.env.LOCATION_ENV);

let config = prod;

switch (process.env.LOCATION_ENV) {
  case 'local': {
    config = local;
    break;
  }
  case 'local0': {
    config = local0;
    break;
  }
  case 'dev': {
    config = dev;
    break;
  }
  default: {
    config = prod;
  }
}

module.exports = config;
