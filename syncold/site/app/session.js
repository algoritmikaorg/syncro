const session = require('express-session');
const RedisStore = require('connect-redis')(session);

const { REDIS: { HOST, PORT, PASS } } = require('./../config');

module.exports = (app) => {
  const options = {
    host: HOST,
    port: PORT,
    pass: PASS,
  };
  app.use(session({
    store: new RedisStore(options),
    secret: 'df ee888nb',
    resave: false,
    saveUninitialized: false,
  }));
};
