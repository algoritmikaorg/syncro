const logger = require('./../lib/logger');
const controllerAsync = require('./../controllers/errors');

module.exports = (err, req, res, next) => {
  // error handler
  console.error(err);
  logger.error(err);
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);

  return controllerAsync(req)
    .then((result) => {
      if (err.status === 404) return res.render('error404', { currentPageData: Object.assign({}, result, { user: req.user }) });
      return res.render('error', { currentPageData: Object.assign({}, result, { user: req.user }), title: 'Ошибка' });
    })
    .catch(error => res.render('fatal'));
};
