const crypto = require('crypto');
const { promisify } = require('util');

module.exports = crypto;
module.exports.randomBytesAsync = promisify(crypto.randomBytes).bind(crypto);
