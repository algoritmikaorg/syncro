const express = require('express');
const router = express.Router();

const controllerPagesAsync = require('./../controllers/pages');

router.get('/:page', (req, res, next) => {
  const { page } = req.params;
  if (!page || !(controllerPagesAsync[page] instanceof Function)) return next();

  controllerPagesAsync[page](req)
    .then((data) => {
      res.render('pages/simple', {
        currentPageData: Object.assign(
          {},
          data,
          {
            activeLink: `/${page}`,
            page,
          },
        ),
      });
    })
    .catch(next);
});

module.exports = router;
