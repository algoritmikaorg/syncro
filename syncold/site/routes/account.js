const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/account');

/* GET home page. */
router.get('/', (req, res, next) => {
  return controllerAsync(req)
    .then((result) => {
      const currentPageData = Object.assign(result, { user: req.user });
      return res.render('pages/account', { currentPageData });
    })
    .catch(next);
});

module.exports = router;
