const express = require('express');
const router = express.Router();

const redis = require('./../db/redis');
const { graphQlFetch } = require('./../graphql');
// const templateQuery = require('./../graphql/templateQuery');

const tagsWithActiveLectures = (req) => {
  const date_gte = new Date();
  return graphQlFetch({ req, query: 'allTagsLecturesActiveQuery', variables: { date_gte } })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return {
        allTagsWithActiveLecture: data ? data.allTags : [],
      };
    });
};

const tagsWithArchiveLectures = (req) => {
  const date_lt = new Date();
  return graphQlFetch({ req, query: 'allTagsLecturesArchiveQuery', variables: { date_lt } })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return {
        allTagsWithArchiveLecture: data ? data.allTags : [],
      };
    });
};

const allTagsQuery = (req) => {
  return graphQlFetch({ req, query: 'allTagsQuery' })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);

      data.allTags.unshift({
        title: 'Все темы',
        alias: {
          alias: 'all',
        },
      });
      return data;
    });
};

router.get('/', (req, res, next) => {

  Promise.all([
    tagsWithActiveLectures(req),
    tagsWithArchiveLectures(req),
    allTagsQuery(req),
    redis.getAsync('template'),
  ])
    .then((result) => {
      const [data1, data2, data3, template] = result;

      const currentPageData = Object.assign(
        {
          user: req.user,
          templateData: JSON.parse(template),
        },
        data1,
        data2,
        data3,
      );

      res.render('pages/catalog', { currentPageData });
    })
    .catch((error) => {
      next(error);
    });
});

module.exports = router;
