const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('pages/course', { title: 'Course' });
});

router.get('/:id', (req, res, next) => {
  res.render('pages/course', { title: 'Course' });
});

module.exports = router;
