const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('pages/cycle', { title: 'Cycle' });
});

router.get('/:id', (req, res, next) => {
  res.render('pages/cycle', { title: 'Cycle' });
});

module.exports = router;
