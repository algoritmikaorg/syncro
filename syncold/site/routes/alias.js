const express = require('express');
const router = express.Router();
const jsonfile = require('jsonfile');

const controllerAliasAsync = require('./../controllers/alias');

/* GET home page. */
router.get('/:alias', (req, res, next) => {
  const { alias } = req.params;
  if (!alias) {
    next();
    return null;
  }

  return controllerAliasAsync(req, alias)
    .then((result) => {
      // jsonfile.writeFile('currentPageData.json', currentPageData, {spaces: 2, EOL: '\r\n'}, function(err) {
      //   console.error(err)
      // });
      if (!result) return next();

      const currentPageData = Object.assign(result, { user: req.user });

      switch (currentPageData.___typename) {
        case 'Lecture': {
          return res.render('pages/lecture', { currentPageData });
        }
        case 'Course': {
          return res.render('pages/course', { currentPageData });
        }
        case 'Cycle': {
          return res.render('pages/cycle', { currentPageData });
        }
        case 'Lecturer': {
          return res.render('pages/lecturer', { currentPageData });
        }
        default: {
          next();
        }
      }
      return result;
    })
    .catch(next);
});

module.exports = router;
