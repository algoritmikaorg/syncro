const express = require('express');
const request = require('request');
const async = require('async');
const router = express.Router();

const signupUserMutation = require('./../graphql/signupUserMutation');
const signinUserMutation = require('./../graphql/signinUserMutation');
const signupUserSocialNetworks = require('./../graphql/signupUserSocialNetworks');

const instagramAPIKey = 'a9661a585aba4ab2865d58ac7d94e7bd';
const instagramSecretKey = '9470d3417f4647aea0aeaef2a66c671e';
const instagramCallback = '/instagram/callback';

const genInstagramURI = host => `https://api.instagram.com/oauth/authorize/?client_id=${instagramAPIKey}&redirect_uri=${host}/users${instagramCallback}&response_type=code&scope=basic`;

const genInstagramCodeURI = (host, code) => ({
  url: 'https://api.instagram.com/oauth/access_token',
  form: {
    client_id: instagramAPIKey,
    client_secret: instagramSecretKey,
    grant_type: 'authorization_code',
    redirect_uri: `${host}/users${instagramCallback}`,
    code,
  },
});

const gebInstagramUserGraphURI = (userId, token) => `https://api.instagram.com/v1/users/${userId}/?access_token=${token}&scope=basic`;

const vkAppId = '6307787';
const vkSecretKey = 'Qh4vMRpFqlyykx4Rr4q0';
const vkServiceKey = '0491c9f20491c9f20491c9f2f904f1f639004910491c9f25eacd7eddc499cb04f33ec7c';
const vkCallback = '/vk/callback';

const genVkURI = host => `https://oauth.vk.com/authorize?client_id=${vkAppId}&display=page&redirect_uri=${host}/users${vkCallback}&scope=email,first_name,last_name&response_type=code&v=5.69`

const genVkCodeURI = (host, code) => `https://oauth.vk.com/access_token?client_id=${vkAppId}&client_secret=${vkSecretKey}&redirect_uri=${host}/users${vkCallback}&code=${code}`;


const genVkUserGraphURI = (userId, token) => `https://api.vk.com/method/users.get?access_token=${token}&user_ids=${userId}&fields=email,first_name,last_name`;

const facebookAPIKey = '262384627627343';
const facebookCallback = '/facebook/callback';
const facebookClientSecret = '815db799cd4d167c2baf06f88c7714d1';

const genFacebookURI = host => `https://www.facebook.com/v2.11/dialog/oauth?auth_type=rerequest&client_id=${facebookAPIKey}&redirect_uri=${host}/users${facebookCallback}&scope=email,public_profile`;

const genFacebookCodeURI = (host, code) => `https://graph.facebook.com/v2.11/oauth/access_token?client_id=${facebookAPIKey}&redirect_uri=${host}/users${facebookCallback}&client_secret=${facebookClientSecret}&code=${code}`;

const genFacebookCheckTokenURI = token => `https://graph.facebook.com/debug_token?input_token=${token}&access_token=${facebookAPIKey}|${facebookClientSecret}`;

const genFaceboorUserGraphURI = (userId, token) => `https://graph.facebook.com/v2.11/${userId}?access_token=${facebookAPIKey}|${facebookClientSecret}&fields=first_name,last_name,email`;

router.get('/logout', (req, res, next) => {
  res.clearCookie('dtcToken');
  res.redirect('/')
});

router.post('/sign-in', (req, res, next) => {
  signinUserMutation(req)
    .then(({ data, errors, error, extensions }) => {
      res.cookie('dtcToken', data.authenticateUser.token, { expires: new Date(Date.now() + 900000) });
      res.json({ data, errors, extensions })
    })
    .catch((err) => {
      console.log('error', err);
      next(err);
    });
});

router.post('/sign-up', (req, res, next) => {
  signupUserMutation(req)
    .then(({ data, errors, extensions }) => {
      res.cookie('dtcToken', data.signupUser.token, { expires: new Date(Date.now() + 900000) });
      res.json({ data, errors, extensions })
    })
    .catch((err) => {
      console.log('error', err);
      next(err);
    });
});

router.get('/social', (req, res, next) => {
  res.send('respond with a resource');
});

router.get('/reset-pass/:token', (req, res, next) => {
  res.send('respond with a resource');
});

router.get('/confirm-email', (req, res, next) => {
  res.send('respond with a resource');
});

router.get('/facebook', (req, res, next) => {
  const { hostname, protocol } = req;
  const port = hostname === 'localhost' ? ':3000' : '';
  res.redirect(genFacebookURI(`${protocol}://${hostname}${port}`));
});

router.get(facebookCallback, (req, res, next) => {
  const { hostname, protocol } = req;
  const port = hostname === 'localhost' ? ':3000' : '';
  const code = req.query.code;
  if (code) {
    let token = null;
    let userId = null;
    let user = {};
    async.waterfall([
      (callback) => {
        request(genFacebookCodeURI(`${protocol}://${hostname}${port}`, code), callback);
      },
      (response, body, callback) => {
        const result = JSON.parse(body);
        if (result.error) {
          callback(result.error);
          return null;
        }
        token = result.access_token;
        request(genFacebookCheckTokenURI(token), callback);
      },
      (response, body, callback) => {
        const result = JSON.parse(body);
        if (result.error) {
          callback(result.error);
          return null;
        }
        userId = result.data.user_id;
        request(genFaceboorUserGraphURI(userId, token), callback);
      },
      (response, body, callback) => {
        const result = JSON.parse(body);
        if (result.error) {
          callback(result.error);
          return null;
        }
        user.socToken = token;
        user.socUserId = userId;
        user.firstName = result.first_name;
        user.lastName = result.last_name;
        user.email = result.email;
        user.socialNetwork = 'FACEBOOK';

        signupUserSocialNetworks(req, user)
          .then(({ data, errors }) => {
            res.cookie('dtcToken', data.signupUserSocialNetworks.token, { expires: new Date(Date.now() + 900000) });
            res.redirect('/');
          })
          .catch((err) => {
            console.log('error', err);
            next(err);
          })
      },
    ], (err, result) => {
      if (err) {
        next();
      }
    });
  } else {
    next();
  }
  // res.send('ok');
});

router.get('/vk', (req, res, next) => {
  const { hostname, protocol } = req;
  const port = hostname === 'localhost' ? ':3000' : '';
  res.redirect(genVkURI(`${protocol}://${hostname}${port}`));
});

router.get(vkCallback, (req, res, next) => {
  const { hostname, protocol } = req;
  const port = hostname === 'localhost' ? ':3000' : '';
  const code = req.query.code;
  if (code) {
    let token = null;
    let userId = null;
    let user = {};
    async.waterfall([
      (callback) => {
        request(genVkCodeURI(`${protocol}://${hostname}${port}`, code), callback);
      },
      (response, body, callback) => {
        const result = JSON.parse(body);
        if (result.error) {
          callback(result.error);
          return null;
        }
        token = result.access_token;
        userId = result.user_id;
        user.socToken = token;
        user.socUserId = userId.toString();
        user.email = result.email;
        request(genVkUserGraphURI(userId, token), callback);
      },
      (response, body, callback) => {
        const result = JSON.parse(body).response[0];
        if (result.error) {
          callback(result.error);
          return null;
        }
        user.firstName = result.first_name;
        user.lastName = result.last_name;
        user.socialNetwork = 'VKONTAKTE';

        signupUserSocialNetworks(req, user)
          .then(({ data, errors }) => {
            res.cookie('dtcToken', data.signupUserSocialNetworks.token, { expires: new Date(Date.now() + 900000) });
            res.redirect('/');
          })
          .catch((err) => {
            console.log('error', err);
            next(err);
          })
      },
    ], (err, result) => {
      if (err) {
        next();
      }
    });
  } else {
    next();
  }
});

router.get('/instagram', (req, res, next) => {
  const { hostname, protocol } = req;
  const port = hostname === 'localhost' ? ':3000' : '';
  res.redirect(genInstagramURI(`${protocol}://${hostname}${port}`));
});

router.get(instagramCallback, (req, res, next) => {
  const { hostname, protocol } = req;
  const port = hostname === 'localhost' ? ':3000' : '';
  const code = req.query.code;

  if (code) {
    let token = null;
    let userId = null;
    let user = {};
    async.waterfall([
      (callback) => {
        request.post(genInstagramCodeURI(`${protocol}://${hostname}${port}`, code), callback);
      },
      (response, body, callback) => {
        const result = JSON.parse(body);
        if (result.error_message || result.error_type) {
          callback(result.error_message);
          return null;
        }
        token = result.access_token;
        userId = result.user.id;
        user.socToken = token;
        user.socUserId = userId;
        request(gebInstagramUserGraphURI(userId, token), callback);
      },
      (response, body, callback) => {
        const result = JSON.parse(body).data;
        if (result.error_message || result.error_type) {
          callback(result.error_message);
          return null;
        }
        const fullName = result.full_name.split(' ');
        user.firstName = fullName[0];
        user.lastName = fullName[1];
        user.email = result.email;
        user.socialNetwork = 'INSTAGRAM';

      },
    ], (err, result) => {
      console.log(err, result);
      if (err) {
        next();
      }
    });
  } else {
    next();
  }
});

module.exports = router;
