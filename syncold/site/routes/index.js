const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/home');

/* GET home page. */
router.get('/', (req, res, next) => {

  return controllerAsync(req)
    .then((result) => {
      if (!result) return next();

      const currentPageData = Object.assign(result, { user: req.user });

      return res.render('pages/index', {
        currentPageData,
      });
    })
    .catch(next);
});

module.exports = router;
