const { graphQlFetch } = require('./graphql');

const acquiringSettingQuery = (req, users) => {
  return graphQlFetch({
    req,
    query: 'checkAvailabilityOfUserMutation',
    variables: {
      users: users.map(user => JSON.stringify(user)),
    }
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.checkAvailabilityOfUser;
    });
};

module.exports = acquiringSettingQuery;
