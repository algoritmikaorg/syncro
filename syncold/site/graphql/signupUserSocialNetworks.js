const graphQlFetch = require('./graphql').graphQlFetch;

const signupUserSocialNetworks = (req, user) => {
  return graphQlFetch({ req, query: 'signupUserSocialNetworks', variables: user })
    .then(({ data, errors, extensions }) => {
      return {
        data,
        errors,
        extensions,
      };
    });
};

module.exports = signupUserSocialNetworks;
