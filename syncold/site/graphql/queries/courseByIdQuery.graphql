query ($id: ID!, $lecturesIds: [ID!]!, $tagsIds: [ID!]!, $date: DateTime!) {
  Course (id: $id) {
    ...CoursePayload
    __typename
  }
  # лекторы, даты, цены
  allEvents (
    filter: {
      lecture: {
        id_in: $lecturesIds
      }
    }
  ) {
    ...EventPayload
    lecture {
      id
    }
  }
  # рекомендованые лекции по тегам
  allLectures (
    filter: {
      public: true,
      course_every: {
        id_not: $id,
      }
      tags_some: {
        id_in: $tagsIds
      }
    },
    first: 3,
  ) {
    ...LectureShortExtPayload
  }
  # отзывы
  allReviews (
    filter: {
      public: true,
      lecture: {
        id_in: $lecturesIds,
      }
    }
  ) {
    ...ReviewPayload
  }
  # скидки
  allDiscounts (
    filter: {
      public: true,
      promocode: "",
      unused_gt: 0,
      validityPeriodFrom_lte: $date,
      validityPeriodTo_gt: $date,
      OR: [
        {
          allProducts: true,
        },
        {
          courses_some: {
            id: $id,
          }
        },
        {
          tags_some: {
            id_in: $tagsIds,
          },
        }
      ]
    }
  ) {
    ...DiscountPayload
  }
}

fragment CoursePayload on Course {
  id
  title
  price
  anons
  buyFull
  description
  img340x192
  img1240x349
  tags {
    ...TagPayload
  }
  lectures (
    filter: {
      public: true,
    }
  ) {
    ...LecturePayload
  }
  recommendedLectures (
    filter: {
      public: true
    }
  ) {
    ...LectureShortExtPayload
  }
  metaDescription
  metaKeywords
  metaTags {
    metaKeywords
    metaDescription
  }
}

fragment LecturePayload on Lecture {
  id
  title
  subTitle
  anons
  anonsForCourse
  anonsForCycle
  description
  themes
  duration
  img1240x349
  img340x192
  img340x340

  discounts (
    filter: {
      public: true
    }
  ) {
    ...DiscountPayload
  }
  alias {
    ...AliasPayload
  }
  events (
    filter: {
      date_gte: $date
      quantityOfTicketsAvailable_gt: 0
    }
    orderBy: date_ASC
  ) {
    ...EventPayload
  }
}

fragment LectureShortPayload on Lecture {
  id
  title
  anons
  anonsForCourse
  anonsForCycle
  img340x340
  alias {
    ...AliasPayload
  }
  events (
    filter: {
      date_gte: $date
      quantityOfTicketsAvailable_gt: 0
    }
    orderBy: date_ASC
  ) {
    ...EventPayload
  }
}

fragment LectureShortExtPayload on Lecture {
  ...LectureShortPayload
  course {
    ...CourseShortPayload
  }
  cycle {
    ...CycleShortPayload
  }
  tags {
    ...TagPayload
  }
}

fragment ReviewPayload on Review {
  anons
  description
  user {
    firstName
    lastName
    avatar
  }
  lecture {
    title
    alias {
      ...AliasPayload
    }
    course {
      title
      alias {
        ...AliasPayload
      }
    }
    cycle {
      title
      alias {
        ...AliasPayload
      }
    }
  }
}

fragment DiscountPayload on Discount {
  id
  title
  rate
  buyingLecturesCount
  participantsCount
}

fragment AliasPayload on Alias {
  alias
}

fragment EventPayload on Event {
  id
  date
  price
  quantityOfTicketsAvailable
  lecturers {
    ...LecturerPayload
  }
  location {
    ...LocationPayload
  }
}

fragment LecturerPayload on Lecturer {
  id
  skills
  firstName
  lastName
  alias {
    ...AliasPayload
  }
}

fragment LocationPayload on Location {
  id
  title
  address
  metro
}

fragment CourseShortPayload on Course {
  id
  title
  buyFull
  alias {
    ...AliasPayload
  }
}

fragment CycleShortPayload on Cycle {
  id
  title
  buyFull
  alias {
    ...AliasPayload
  }
}

fragment TagPayload on Tag {
  id
  title
  textColor
  color
  alias {
    ...AliasPayload
  }
}
