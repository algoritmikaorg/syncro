module.exports = require('./mailchimp');

// const MailChimpApi = require('./libs/mailChimpApi');
//
// const mailChimpApi = new MailChimpApi();
//
// module.exports = MailChimpApi;
// mailChimpApi.updateSubscribeAnAddress(listId, 'cool.finanfree10@yandex.ru', {
//   email: 'cool.finanfree10@yandex.ru',
//   firstName: 'Slava',
//   lastName: 'Viktorov',
//   userId: 'dsac323423dxsa11',
// })
//   .then((result) => {
//     console.log(result);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });

// mailChimpApi.deleteSubscribeAnAddress(listId, 'cool.finanfree1@yandex.ru')
//   .then((json) => {
//     console.log(json);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });

// mailChimpApi.lists()
//   .then((lists) => {
//     listId = lists[0].id;
//     return mailChimpApi.subscribeAnAddress(listId, {
//       email: 'cool.finanfree2@yandex.ru',
//       firstName: 'Slava',
//       lastName: 'Viktorov',
//     });
//   })
//   .then((json) => {
//     console.log(json);
//     return mailChimpApi.deleteSubscribeAnAddress(listId, 'cool.finanfree2@yandex.ru');
//   })
//   .then((json) => {
//     console.log('>>', json);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });

// mailChimpApi.addNewStore(listId, {
//   id: 'synchronize-dev.ru',
//   name: 'synchronize-dev.ru',
//   domain: 'http://synchronize-dev.ru/',
//   isSyncing: true,
//   email: 'info@synchronize-dev.ru',
//   currencyCode: 'RUB',
//   moneyFormat: '₽',
//   timezone: 'MSK',
// })
//   .then((res) => {
//     console.log(res);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });

// mailChimpApi.stores()
//   .then((stores) => {
//     console.log(stores);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });

const product1 = {
  id: 'lecture1',
  title: 'Лекция1',
  variants: [
    {
      id: 'lecture1',
      title: 'Лекция1',
    },
  ],
};
const product2 = {
  id: 'lecture2',
  title: 'Лекция2',
  variants: [
    {
      id: 'lecture2',
      title: 'Лекция2',
    },
  ],
};
const product3 = {
  id: 'lecture3',
  title: 'Лекция3',
  variants: [
    {
      id: 'lecture3',
      title: 'Лекция3',
    },
  ],
};

// mailChimpApi.addNewProductToStore(storeId, product1)
//   .then((result) => {
//     console.log('result product1', result);
//     return mailChimpApi.addNewProductToStore(storeId, product2);
//   })
//   .then((result) => {
//     console.log('result product2', result);
//     return mailChimpApi.addNewProductToStore(storeId, product3);
//   })
//   .then((result) => {
//     console.log('result product3', result);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   })

// mailChimpApi.products(storeId)
//   .then((products) => {
//     console.log(products);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });

// const cart = {
//   id: 'orderid16',
//   customer: {
//     id: '6',
//     email: 'cool.finanfree5@yandex.ru',
//     firstName: 'Slava',
//     lastName: 'Viktorov',
//     userId: '4454545',
//   },
//   campaignId: '791b8acca6',
//   currencyCode: 'RUB',
//   checkoutUrl: 'http://localhost:3000/mc/cart',
//   products: [
//     {
//       id: 'eventid01',
//       product_id: 'lecture1',
//       product_title: 'Лекция "Название" - даты ("даты")',
//       product_variant_id: 'lecture1',
//       product_variant_title: 'Лекция "Название" - даты ("даты")',
//       quantity: 2,
//       price: 1000,
//     },
//     {
//       id: 'eventid02',
//       product_id: 'lecture2',
//       product_title: 'Лекция "Название" - даты ("даты")',
//       product_variant_id: 'lecture2',
//       product_variant_title: 'Лекция "Название" - даты ("даты")',
//       quantity: 2,
//       price: 1000,
//     },
//     {
//       id: 'eventid03',
//       product_id: 'lecture3',
//       product_title: 'Лекция "Название" - даты ("даты")',
//       product_variant_id: 'lecture3',
//       product_variant_title: 'Лекция "Название" - даты ("даты")',
//       quantity: 2,
//       price: 1000,
//     },
//   ],
//   orderTotal: 3000,
// };
//
// mailChimpApi.addNewCart(storeId, cart)
//   .then((result) => {
//     console.log(result);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });

// mailChimpApi.carts(storeId)
//   .then((carts) => {
//     console.log(carts);
//   })
//   .catch((err) => {
//     console.log('error', err)
//   });

// mailChimpApi.campaignFolders()
//   .then((folders) => {
//     console.log(folders);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });

// mailChimpApi.campaigns()
//   .then((campaigns) => {
//     console.log(campaigns);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });
// mailChimpApi.automations()
//   .then((automations) => {
//     console.log(automations);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });


// mailChimpApi.storeSyncingOff(storeId)
//   .then((result) => {
//     console.log(result);
//   })
//   .catch((err) => {
//     console.log('error', err);
//   });
