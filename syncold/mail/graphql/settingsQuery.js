const { graphQlFetch } = require('./graphql');

module.exports = (input, template) => {
  const { name: templateName } = template;
  return graphQlFetch({ query: 'allSettingsQuery' })
    .then(({ data, errors, error }) => {
      if (!data || data.allSettings.length === 0) return Promise.reject(new Error('No settings found'));
      const { allSettings: [settings] } = data;
      if (!settings[templateName]) return Promise.reject(new Error('Template not found'));
      if (!settings.senderEmail || !settings.senderName || !settings.mandrillAPIKey) return Promise.reject(new Error('settings param not found'));

      return settings;
    });
};
