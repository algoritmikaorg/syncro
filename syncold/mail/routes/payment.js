const express = require('express');
const router = express.Router();
const moment = require('moment');

const mandrill = require('./../mandrill');

router.post('/', (req, res, next) => {
  const { data: { Payment: { node } } } = req.body;

  const {
    status,
    certificate,
    subscription,
    order,
    orderPrice,
    discountPrice,
    totalPrice,
    user,
  } = node;

  let templateName = null;
  let tags = null;
  let subject = null;
  const content = [
    {
      name: 'CERTIFICATE',
      content: certificate ? certificate.title : null,
    },
    {
      name: 'SUBSCRIPTION',
      content: subscription ? subscription.title : null,
    },
    {
      name: 'ORDER',
      content: order ? order.events.map(({ lecture: { title }, date }) => `${title} - ${date}`).join(', ') : null,
    },
    {
      name: 'ORDER_PRICE',
      content: orderPrice,
    },
    {
      name: 'DISCOUNT_PRICE',
      content: discountPrice,
    },
    {
      name: 'TOTAL_PRICE',
      content: totalPrice,
    },
  ];

  switch (status) {
    case 'PENDING': {
      subject = 'Ваш заказ ожидает оплаты';
      tags = ['payment_panding'];
      templateName = 'mandrillTemplateSlugOrderWithoutPayment';
      break;
    }
    default: {
      subject = 'Ваш заказ оплачен';
      tags = ['payment_paid'];
      templateName = 'mandrillTemplateSlugPayment';
    }
  }

  mandrill(user, {
    name: templateName,
    tags,
    subject,
    content,
  })
    .then((result) => {
      res.json(result);
      // res.json(req.body);
    })
    .catch((err) => {
      console.error(err);
      // res.json(req.body);
      next(err);
    });
});

module.exports = router;
