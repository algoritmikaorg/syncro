const express = require('express');
const router = express.Router();

const mandrill = require('./../mandrill');

router.post('/', (req, res, next) => {
  const { data: { User: { node, mutation, previousValues } } } = req.body;

  switch (mutation) {
    case 'CREATED': {
      mandrill(node, {
        name: 'mandrillTemplateSlugRegistration',
        tags: ['registration'],
        subject: 'Регистрация',
        content: [],
      })
        .then((result) => {
          res.json(result);
        })
        .catch(next);

      break;
    }
    case 'UPDATED': {
      const { blocked } = node;

      let templateName = null;
      let tags = null;
      let subject = null;

      if (blocked !== previousValues.blocked) {
        if (blocked) {
          templateName = 'mandrillTemplateSlugUserBlock';
          tags = ['user-block'];
          subject = 'Ваш аккаунт заблокирован';
        } else {
          templateName = 'mandrillTemplateSlugUserUnblock';
          tags = ['user-unblock'];
          subject = 'Ваш аккаунт разблокирован';
        }

        mandrill(node, {
          name: templateName,
          tags,
          subject,
          content: [],
        })
          .then((result) => {
            // res.json(req.body);
            res.json(result);
          })
          .catch(next);
      }

      break;
    }
    default: {
      res.json({});
    }
  }
});

router.post('/reset-pass', (req, res, next) => {
  const { data } = req.body;
  const { link, expires } = data;

  const templateName = 'mandrillTemplateSlugResetPass';
  const tags = ['user-reset-pass'];
  const subject = 'Сброс пароля';

  mandrill(data, {
    name: templateName,
    tags,
    subject,
    content: [
      {
        name: 'LINK',
        content: link,
      },
      {
        name: 'EXPIRES',
        content: expires,
      },
    ],
  })
    .then((result) => {
      res.json(result);
    })
    .catch(next);
});

router.post('/confirm-email', (req, res, next) => {
  const { data } = req.body;

  const { link } = data;
  const templateName = 'mandrillTemplateSlugConfirmEmail';
  const tags = ['user-confirm-email'];
  const subject = 'Подтверждение E-mail';

  mandrill(data, {
    name: templateName,
    tags,
    subject,
    content: [
      {
        name: 'LINK',
        content: link,
      },
    ],
  })
    .then((result) => {
      res.json(result);
    })
    .catch(next);
});

module.exports = router;
