const express = require('express');
const router = express.Router();

const mailchimp = require('./../mailchimp');

router.post('/common', (req, res, next) => {
  const { data } = req.body;
  if (!data || !data.email) return Promise.reject(new Error('Wrong params'));

  const {
    email,
  } = data;

  mailchimp('common', {
    email,
  })
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      next(err);
    });
});

router.post('/schedule', (req, res, next) => {
  const { data } = req.body;
  if (!data || !data.email) return Promise.reject(new Error('Wrong params'));

  const {
    email,
  } = data;
  
  mailchimp('schedule', {
    email,
  })
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      next(err);
    });
});

module.exports = router;
