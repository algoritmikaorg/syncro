module.exports = {
  apps: [
    {
      name: 'mail',
      script: './bin/www',
      watch: false,
      env: {
        PORT: 60003,
        NODE_ENV: 'development',
        LOCATION_ENV: 'dev',
      },
      env_production: {
        PORT: 60003,
        NODE_ENV: 'production',
        LOCATION_ENV: 'prod',
      },
    },
  ],
};
