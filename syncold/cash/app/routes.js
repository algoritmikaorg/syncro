const createError = require('http-errors');

const index = require('./../routes/index');
const alias = require('./../routes/alias');
const course = require('./../routes/course');
const cycle = require('./../routes/cycle');
const lecture = require('./../routes/lecture');
const lecturer = require('./../routes/lecturer');
const tag = require('./../routes/tag');
const howWorkPage = require('./../routes/howWorkPage');
const template = require('./../routes/template');
const image = require('./../routes/image');
const gallery = require('./../routes/gallery');
const settings = require('./../routes/settings');
const payment = require('./../routes/payment');
const event = require('./../routes/event');
const acquiring = require('./../routes/acquiring');
const status = require('./../routes/status');

module.exports = (app) => {
  app.use('/', index);
  app.use('/alias', alias);
  app.use('/course', course);
  app.use('/cycle', cycle);
  app.use('/lecture', lecture);
  app.use('/lecturer', lecturer);
  app.use('/tag', tag);
  app.use('/how-work-page', howWorkPage);
  app.use('/template', template);
  app.use('/settings', settings);
  app.use('/status', status);
  app.use('/payment', payment);
  app.use('/acquiring', acquiring);
  app.use('/event', event);
  app.use('/gallery', gallery);
  app.use('/image', image);
  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    next(createError(404));
  });

  // error handler
  app.use((err, req, res, next) => {
    // set locals, only providing error in development
    console.error(err);
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({ error: res.locals.error });
  });
};
