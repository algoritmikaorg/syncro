const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/acquiring');

/* GET home page. */
router.post('/', (req, res, next) => {
  if (!req.body.data || !req.body.data.Acquiring) return next(new Error('Wrond Acquiring data'));
  const { data: { Acquiring: { node, mutation, previousValues } } } = req.body;

  controllerAsync(mutation, previousValues, node)
    .then((result) => {
      return res.json({ data: { status: 'OK' }, error: null });
    })
    .catch(next);
});

module.exports = router;
