const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/course');

/* GET home page. */
router.post('/', (req, res, next) => {
  if (!req.body.data || !req.body.data.Course) return next(new Error('Wrond Course data'));
  const { data: { Course: { node, mutation, previousValues } } } = req.body;

  controllerAsync(mutation, previousValues, node)
    .then((result) => {
      return res.json({ data: { status: 'OK' }, error: null });
    })
    .catch(next);
});

module.exports = router;
