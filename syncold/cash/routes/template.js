const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/template');

/* GET home page. */
router.post('/', (req, res, next) => {
  if (!req.body.data || !req.body.data.Template) return next(new Error('Wrond Template data'));
  const { data: { Template: { node, mutation, previousValues } } } = req.body;

  controllerAsync(mutation, previousValues, node)
    .then((result) => {
      return res.json({ data: { status: 'OK' }, error: null });
    })
    .catch(next);
});

module.exports = router;
