const redis = require('./../../db/redis');

const redisItemUpdate = (data) => {
  const { id, secret, description, gallery: { type, id: galleryId, title } } = data;
  const gallery = `gallery:${type}`;

  return redis.getAsync(gallery)
    .then((reply) => {
      let obj = null;
      if (!reply) {
        obj = {
          id: galleryId,
          type,
          title,
          images: [{ id, secret, description }],
        };
      } else {
        obj = JSON.parse(reply);
        obj.images = obj.images.map((image) => {
          if (image.id === id) {
            return { id, secret, description };
          }
          return image;
        });
      }

      return redis.setAsync(gallery, JSON.stringify(obj));
    });
};

const redisItemSet = (data) => {
  const { id, secret, description, gallery: { type, id: galleryId, title } } = data;
  const gallery = `gallery:${type}`;

  return redis.getAsync(gallery)
    .then((reply) => {
      let obj = null;
      if (!reply) {
        obj = {
          id: galleryId,
          type,
          title,
          images: [],
        };
      } else {
        obj = JSON.parse(reply);
      }
      obj.images.push({ id, secret, description });

      return redis.setAsync(gallery, JSON.stringify(obj));
    });
};

const redisItemDelete = (previousValues) => {
  const { id } = previousValues;
  let galleryKeys = null;

  return redis.keysAsync('gallery:*')
    .then((reply) => {
      if (!reply) return null;
      galleryKeys = reply;

      return redis.batch(galleryKeys.map(key => ['get', key])).execAsync();
    })
    .then((results) => {
      if (!results) return null;

      const commands = results.map((reply, index) => {
        if (!reply) return [];
        const obj = JSON.parse(reply);
        obj.images = obj.images.filter(image => image.id !== id);
        const key = galleryKeys[index];
        return ['set', key, JSON.stringify(obj)];
      });

      return redis.batch(commands).execAsync();
    });
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisItemSet(node);
    }
    case 'UPDATED': {
      return redisItemUpdate(node);
    }
    case 'DELETED': {
      return redisItemDelete(previousValues);
    }
    default: {
      return redisItemSet(node);
    }
  }
};
