const redis = require('./../../db/redis');

const redisBatchUpdate = (data) => {
  const { id, page } = data;

  const commands = [];
  commands.push(
    ['set', 'template', JSON.stringify(data)],
  );

  return redis.batch(commands).execAsync();
};

const redisBatchSet = (data) => {
  const { page } = data;
  return redis.batch([
    ['set', 'template', JSON.stringify(data)],
  ]).execAsync();
};

const redisBatchDelete = (previousValues) => {
  const { page } = previousValues;
  return redis.batch([
    ['del', 'template'],
  ]).execAsync();
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchSet(node);
    }
    case 'UPDATED': {
      return redisBatchUpdate(node);
    }
    case 'DELETED': {
      return redisBatchDelete(previousValues);
    }
    default: {
      return redisBatchSet(node);
    }
  }
};
