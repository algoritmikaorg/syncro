const { GraphQLClient } = require('graphql-request');
const {
  PROJECT_ID,
  SIMPLE_API_ENDPOINT,
} = require('./../config');


const fileQuery = `
  query FileQuery(
    $secret: String!
  ) {
    File(secret: $secret) {
      id
      contentType
      name
      size
      url
      secret
    }
  }
`;

module.exports = (req, res, next) => {
  const { params: { secret, filename } } = req;

  const token = req.get('Authorization');
  const client = new GraphQLClient(`${SIMPLE_API_ENDPOINT}/${PROJECT_ID}`, {
    headers: {
      Authorization: token,
    },
  });
  client.request(fileQuery, { secret })
    .then((result) => {
      const file = result.File;
      if (!file) {
        res.status(404).send('Not Found');
        return;
      }
      const {
        name,
        size,
        contentType,
      } = file;

      res.set({
        'Content-Disposition': `filename="${encodeURIComponent(filename || name)}"; filename*="UTF-8''${encodeURIComponent(filename || name)}"`,
        'Content-Length': size,
        'Content-Type': contentType,
      });
      res.locals.filename = name;
      next();
    })
    .catch((err) => {
      console.error(err);
      res.status(500);
      next(new Error('Internal Error'));
    });
};
