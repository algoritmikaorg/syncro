const async = require('async');
const request = require('request');
const cuid = require('scuid');
const path = require('path');
const fs = require('fs-extra');

const graphqlClient = require('./../lib/graphqlClient');

const {
  PROJECT_ID,
  DIR,
  SIMPLE_API_ENDPOINT,
} = require('./../config');

const createFileMutation = `
  mutation CreateFileMutation(
    $contentType: String!
    $name: String!
    $size: Int!
    $url: String!
    $secret: String!
  ){
    createFile(
      contentType: $contentType
      name: $name
      size: $size
      url: $url
      secret: $secret
    ) {
      id
      contentType
      name
      size
      url
      secret
    }
  }
`;

module.exports = (req, res, next) => {
  const {
    files,
    params,
  } = req;
  const {
    data,
  } = files;
  const {
    name: filename,
    mimetype,
  } = data;

  const token = req.get('Authorization');

  if (!data) return res.json({ errors: [{ message: 'No files were uploaded.' }] });

  const secret = cuid();
  const extname = path.extname(filename);

  // create dir
  async.series([
    (callback) => { fs.ensureDir(`${DIR}/${PROJECT_ID}`, callback); },
    (callback) => { data.mv(`${DIR}/${PROJECT_ID}/${secret}_${filename}`, callback); },
    (callback) => {
      const variables = {
        contentType: mimetype,
        name: filename,
        size: data.data.length,
        secret,
        url: `${req.originalUrl}/${secret}`,
      };

      const client = graphqlClient(token);
      client.request(createFileMutation, variables)
        .then((result) => { callback(null, result); })
        .catch(callback);
    },
  ], (err, result) => {
    if (err) return res.status(500).json({ errors: err.response.errors });
    res.status(200).json(result[result.length - 1].createFile);
  });
};
