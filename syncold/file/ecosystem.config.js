module.exports = {
  apps: [
    {
      name: 'file',
      script: './bin/www',
      watch: false,
      env: {
        PORT: 60001,
        NODE_ENV: 'development',
        LOCATION_ENV: 'dev',
      },
      env_production: {
        PORT: 60001,
        NODE_ENV: 'production',
        LOCATION_ENV: 'prod',
      },
    },
  ],
};
