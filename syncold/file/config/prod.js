const PROJECT_ID = 'cjd67we9401sw0155segppqze';
const DOMAIN = 'synchronize.ru';

module.exports = {
  PROJECT_ID,
  SIMPLE_API_ENDPOINT: `http://api-dev.${DOMAIN}/simple/v1`,
  DIR: 'public/files',
  WHITE_LIST: ['http://synchronize.ru', 'http://api-dev.synchronize.ru'],
  CONTENT_LENGTH: 300 * 1024 * 1024,
};
