const express = require('express');
const async = require('async');
const request = require('request');
const cuid = require('scuid');
const path = require('path');
const cors = require('cors');

const replaceReqUrl = require('./../middlewares/replaceReqUrl');
const fetchFileQuery = require('./../middlewares/fetchFileQuery');
const createFile = require('./../middlewares/createFile');

const {
  WHITE_LIST,
} = require('./../config');

const router = express.Router();
const serve = express.static('public/files', { index: false });

const corsOptions = {
  origin: (origin, callback) => {
    if (WHITE_LIST.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
};

/* GET home page. */
router.get('/', (req, res, next) => {
  res.send('OK');
});

router.get('/file/:secret', fetchFileQuery, replaceReqUrl, serve);

router.get('/file/:secret/:filename', fetchFileQuery, replaceReqUrl, serve);

router.options('/file', cors(corsOptions));
router.post('/file', cors(corsOptions), createFile);

module.exports = router;
