const { graphQlFetch } = require('./../../graphql');
const fetcPostJson = require('./../../lib/fetcPostJson');
const { MAIL_HOST } = require('./../../config');

exports.onTick = (id) => {
  console.log(`tick-${id}`, new Date());
  return graphQlFetch({
    query: 'EventByIdQuery',
    variables: {
      id,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return fetcPostJson(
        `${MAIL_HOST}/cron/after`,
        {
          data: {
            Event: data.Event,
          },
        },
      );
    })
    .then((result) => {
      console.log(result);
    })
    .catch((err) => {
      console.error(err);
    });
};

exports.onComplete = () => {

};
