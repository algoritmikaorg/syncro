const createError = require('http-errors');

const indexRoutes = require('./../routes/index');

module.exports = (app) => {
  app.use('/', indexRoutes);

  app.use((req, res, next) => {
    next(createError(404));
  });

  app.use((err, req, res, next) => {
    console.error(err);
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.json({ error: res.locals.error });
  });
};
