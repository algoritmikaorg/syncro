function pug_attr(t,e,n,f){return e!==!1&&null!=e&&(e||"class"!==t&&"style"!==t)?e===!0?" "+(f?t:t+'="'+t+'"'):("function"==typeof e.toJSON&&(e=e.toJSON()),"string"==typeof e||(e=JSON.stringify(e),n||e.indexOf('"')===-1)?(n&&(e=pug_escape(e))," "+t+'="'+e+'"'):" "+t+"='"+e.replace(/'/g,"&#39;")+"'"):""}
function pug_classes(s,r){return Array.isArray(s)?pug_classes_array(s,r):s&&"object"==typeof s?pug_classes_object(s):s||""}
function pug_classes_array(r,a){for(var s,e="",u="",c=Array.isArray(a),g=0;g<r.length;g++)s=pug_classes(r[g]),s&&(c&&a[g]&&(s=pug_escape(s)),e=e+u+s,u=" ");return e}
function pug_classes_object(r){var a="",n="";for(var o in r)o&&r[o]&&pug_has_own_property.call(r,o)&&(a=a+n+o,n=" ");return a}
function pug_escape(e){var a=""+e,t=pug_match_html.exec(a);if(!t)return e;var r,c,n,s="";for(r=t.index,c=0;r<a.length;r++){switch(a.charCodeAt(r)){case 34:n="&quot;";break;case 38:n="&amp;";break;case 60:n="&lt;";break;case 62:n="&gt;";break;default:continue}c!==r&&(s+=a.substring(c,r)),c=r+1,s+=n}return c!==r?s+a.substring(c,r):s}
var pug_has_own_property=Object.prototype.hasOwnProperty;
var pug_match_html=/["&<>]/;function template_search(locals) {var pug_html = "", pug_mixins = {}, pug_interp;;var locals_for_with = (locals || {});(function (Date, IMAGE_HOST, events, moment) {function checkHoliday(date) { const day = new Date(date).getDay(); return day === 0 || day === 6 ? true : false }
if ((!events || events.length === 0)) {
pug_html = pug_html + "\u003Cdiv class=\"text-center no-found\"\u003EНичего не найдено\u003C\u002Fdiv\u003E";
}
else {
// iterate events
;(function(){
  var $$obj = events;
  if ('number' == typeof $$obj.length) {
      for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
        var event = $$obj[pug_index0];
var lecture = event.lecture;
pug_html = pug_html + "\u003Cdiv class=\"item\"\u003E\u003Cdiv class=\"grid-container\"\u003E\u003Cdiv class=\"grid-x grid-margin-x\"\u003E\u003Cdiv class=\"large-3 medium-4 small-6 cell\"\u003E\u003Ca" + (pug_attr("href", lecture.alias.alias, true, false)) + "\u003E\u003Cdiv class=\"image\"\u003E";
if (lecture.img340x192 && lecture.img340x192 !== 'no-photo') {
pug_html = pug_html + "\u003Cimg" + (pug_attr("src", `${IMAGE_HOST}/${lecture.img340x192}`, true, false)+pug_attr("alt", lecture.title, true, false)) + "\u002F\u003E";
}
else {
pug_html = pug_html + "\u003Cimg" + (" src=\"\u002Fassets\u002Fimg\u002Fno-photo-340x192.png\""+pug_attr("alt", lecture.title, true, false)) + "\u002F\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"large-13 medium-12 small-10 cell\"\u003E\u003Cdiv class=\"date-item\"\u003E\u003Cspan\u003E" + (pug_escape(null == (pug_interp = moment(event.date).format('D MMMM')) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E\u003Cspan\u003E, \u003C\u002Fspan\u003E\u003Cdiv" + (pug_attr("class", pug_classes(["day",checkHoliday(event.date) ? 'holiday' : ''], [false,true]), false, false)) + "\u003E\u003Cspan\u003E" + (pug_escape(null == (pug_interp = moment(event.date).format('dddd')) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"title\"\u003E\u003Ca" + (pug_attr("href", lecture.alias.alias, true, false)) + "\u003E\u003Cdiv class=\"lecture-title\"\u003E" + (pug_escape(null == (pug_interp = `Лекция “${lecture.title}”`) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
if (lecture.course.length > 0) {
pug_html = pug_html + "\u003Ca" + (pug_attr("href", lecture.course[0].alias.alias, true, false)) + "\u003E\u003Cdiv class=\"course-title\"\u003E" + (pug_escape(null == (pug_interp = 'Курс' + '“' + lecture.course[0].title + '”') ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
}
if (lecture.cycle.length > 0) {
pug_html = pug_html + "\u003Ca" + (pug_attr("href", lecture.cycle[0].alias.alias, true, false)) + "\u003E\u003Cdiv class=\"course-title\"\u003E" + (pug_escape(null == (pug_interp = 'Цикл' + '“' + lecture.cycle[0].title + '”') ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003Cdiv class=\"lecturers-list hide-for-small-only\"\u003E";
// iterate event.lecturers
;(function(){
  var $$obj = event.lecturers;
  if ('number' == typeof $$obj.length) {
      for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
        var lecturer = $$obj[pug_index1];
pug_html = pug_html + "\u003Ca" + (pug_attr("href", lecturer.alias.alias, true, false)) + "\u003E\u003Cdiv class=\"lecturer-item\"\u003E\u003Cdiv class=\"icon\"\u003E\u003Cimg src=\"assets\u002Fimg\u002Flecturer.png\"\u002F\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"title\"\u003E";
let fullName = `${lecturer.firstName} ${lecturer.lastName}`
pug_html = pug_html + (pug_escape(null == (pug_interp = fullName) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index1 in $$obj) {
      $$l++;
      var lecturer = $$obj[pug_index1];
pug_html = pug_html + "\u003Ca" + (pug_attr("href", lecturer.alias.alias, true, false)) + "\u003E\u003Cdiv class=\"lecturer-item\"\u003E\u003Cdiv class=\"icon\"\u003E\u003Cimg src=\"assets\u002Fimg\u002Flecturer.png\"\u002F\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"title\"\u003E";
let fullName = `${lecturer.firstName} ${lecturer.lastName}`
pug_html = pug_html + (pug_escape(null == (pug_interp = fullName) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
    }
  }
}).call(this);

pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003Cdiv class=\"locations-list hide-for-small-only\"\u003E \u003Cdiv class=\"location-item\"\u003E\u003Cdiv class=\"icon\"\u003E\u003Cimg" + (" src=\"assets\u002Fimg\u002Flocation.png\""+pug_attr("alt", event.location.title, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"title\"\u003E" + (pug_escape(null == (pug_interp = event.location.title) ? "" : pug_interp)) + ", " + (pug_escape(null == (pug_interp = event.location.address) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index0 in $$obj) {
      $$l++;
      var event = $$obj[pug_index0];
var lecture = event.lecture;
pug_html = pug_html + "\u003Cdiv class=\"item\"\u003E\u003Cdiv class=\"grid-container\"\u003E\u003Cdiv class=\"grid-x grid-margin-x\"\u003E\u003Cdiv class=\"large-3 medium-4 small-6 cell\"\u003E\u003Ca" + (pug_attr("href", lecture.alias.alias, true, false)) + "\u003E\u003Cdiv class=\"image\"\u003E";
if (lecture.img340x192 && lecture.img340x192 !== 'no-photo') {
pug_html = pug_html + "\u003Cimg" + (pug_attr("src", `${IMAGE_HOST}/${lecture.img340x192}`, true, false)+pug_attr("alt", lecture.title, true, false)) + "\u002F\u003E";
}
else {
pug_html = pug_html + "\u003Cimg" + (" src=\"\u002Fassets\u002Fimg\u002Fno-photo-340x192.png\""+pug_attr("alt", lecture.title, true, false)) + "\u002F\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"large-13 medium-12 small-10 cell\"\u003E\u003Cdiv class=\"date-item\"\u003E\u003Cspan\u003E" + (pug_escape(null == (pug_interp = moment(event.date).format('D MMMM')) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E\u003Cspan\u003E, \u003C\u002Fspan\u003E\u003Cdiv" + (pug_attr("class", pug_classes(["day",checkHoliday(event.date) ? 'holiday' : ''], [false,true]), false, false)) + "\u003E\u003Cspan\u003E" + (pug_escape(null == (pug_interp = moment(event.date).format('dddd')) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"title\"\u003E\u003Ca" + (pug_attr("href", lecture.alias.alias, true, false)) + "\u003E\u003Cdiv class=\"lecture-title\"\u003E" + (pug_escape(null == (pug_interp = `Лекция “${lecture.title}”`) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
if (lecture.course.length > 0) {
pug_html = pug_html + "\u003Ca" + (pug_attr("href", lecture.course[0].alias.alias, true, false)) + "\u003E\u003Cdiv class=\"course-title\"\u003E" + (pug_escape(null == (pug_interp = 'Курс' + '“' + lecture.course[0].title + '”') ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
}
if (lecture.cycle.length > 0) {
pug_html = pug_html + "\u003Ca" + (pug_attr("href", lecture.cycle[0].alias.alias, true, false)) + "\u003E\u003Cdiv class=\"course-title\"\u003E" + (pug_escape(null == (pug_interp = 'Цикл' + '“' + lecture.cycle[0].title + '”') ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003Cdiv class=\"lecturers-list hide-for-small-only\"\u003E";
// iterate event.lecturers
;(function(){
  var $$obj = event.lecturers;
  if ('number' == typeof $$obj.length) {
      for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
        var lecturer = $$obj[pug_index2];
pug_html = pug_html + "\u003Ca" + (pug_attr("href", lecturer.alias.alias, true, false)) + "\u003E\u003Cdiv class=\"lecturer-item\"\u003E\u003Cdiv class=\"icon\"\u003E\u003Cimg src=\"assets\u002Fimg\u002Flecturer.png\"\u002F\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"title\"\u003E";
let fullName = `${lecturer.firstName} ${lecturer.lastName}`
pug_html = pug_html + (pug_escape(null == (pug_interp = fullName) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index2 in $$obj) {
      $$l++;
      var lecturer = $$obj[pug_index2];
pug_html = pug_html + "\u003Ca" + (pug_attr("href", lecturer.alias.alias, true, false)) + "\u003E\u003Cdiv class=\"lecturer-item\"\u003E\u003Cdiv class=\"icon\"\u003E\u003Cimg src=\"assets\u002Fimg\u002Flecturer.png\"\u002F\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"title\"\u003E";
let fullName = `${lecturer.firstName} ${lecturer.lastName}`
pug_html = pug_html + (pug_escape(null == (pug_interp = fullName) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fa\u003E";
    }
  }
}).call(this);

pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003Cdiv class=\"locations-list hide-for-small-only\"\u003E \u003Cdiv class=\"location-item\"\u003E\u003Cdiv class=\"icon\"\u003E\u003Cimg" + (" src=\"assets\u002Fimg\u002Flocation.png\""+pug_attr("alt", event.location.title, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003Cdiv class=\"title\"\u003E" + (pug_escape(null == (pug_interp = event.location.title) ? "" : pug_interp)) + ", " + (pug_escape(null == (pug_interp = event.location.address) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
    }
  }
}).call(this);

}}.call(this,"Date" in locals_for_with?locals_for_with.Date:typeof Date!=="undefined"?Date:undefined,"IMAGE_HOST" in locals_for_with?locals_for_with.IMAGE_HOST:typeof IMAGE_HOST!=="undefined"?IMAGE_HOST:undefined,"events" in locals_for_with?locals_for_with.events:typeof events!=="undefined"?events:undefined,"moment" in locals_for_with?locals_for_with.moment:typeof moment!=="undefined"?moment:undefined));;return pug_html;}