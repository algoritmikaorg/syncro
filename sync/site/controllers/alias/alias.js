const redis = require('./../../db/redis');
const { ROOT_TOKEN } = require('./../../config');

const { graphQlFetch } = require('./../../graphql');

const controllerLectureAsync = require('./../lecture');
const controllerCycleAsync = require('./../cycle');
const controllerCourseAsync = require('./../course');
const controllerLecturerAsync = require('./../lecturer');
const controllerTagAsync = require('./../tag');

const allAliasesByAliasShortQuery = (req, alias) => {
  return graphQlFetch({
    req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
    query: 'allAliasesByAliasShortQuery',
    variables: {
      alias,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.allAliases[0];
      // return null;
    });
};

module.exports = (req, alias) => {
  return redis.getAsync(`alias:${alias}`)
    .then((reply) => {
      if (!reply) return allAliasesByAliasShortQuery(req, alias);
      return JSON.parse(reply);
    })
    .then((result) => {
      if (!result) return Promise.reject();

      const {
        lecture,
        cycle,
        course,
        lecturer,
        tag,
      } = result;

      switch (true) {
        case lecture instanceof Object: {
          if (!lecture.public) return Promise.reject();
          return controllerLectureAsync(req, lecture);
        }
        case cycle instanceof Object: {
          if (!cycle.public) return Promise.reject();
          return controllerCycleAsync(req, cycle);
        }
        case course instanceof Object: {
          if (!course.public) return Promise.reject();
          return controllerCourseAsync(req, course);
        }
        case lecturer instanceof Object: {
          return controllerLecturerAsync(req, lecturer);
        }
        case tag instanceof Object: {
          return controllerTagAsync(req, tag);
        }
        default: {
          return Promise.reject();
        }
      }
    })
    .then((result) => {
      return result;
    });
};
