const moment = require('moment');

const redis = require('./../../db/redis');
const { ROOT_TOKEN } = require('./../../config');

const { graphQlFetch } = require('./../../graphql');

module.exports = (req, alias) => {
  return Promise.all([
    redis.batch([
      ['get', 'template'],
    ]).execAsync(),
  ])
    .then((result) => {
      const [[template]] = result;
      return Object.assign(
        {},
        {
          templateData: JSON.parse(template),
        },
      );
    });
};
