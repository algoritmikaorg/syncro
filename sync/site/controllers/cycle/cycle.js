const redis = require('./../../db/redis');
const { graphQlFetch } = require('./../../graphql');
const { ROOT_TOKEN } = require('./../../config');

const {
  sortForUniq,
  additionalDataOfEventsForLectures,
  productParams,
  checkCourseAvailableForBuy,
  additionalDataOfEvents,
} = require('./../course/functions');

const cycleByIdQuery = (req, cycle) => {
  const { id, lectures, tags } = cycle;

  return graphQlFetch({
    req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
    query: 'cycleByIdQuery',
    variables: {
      id,
      lecturesIds: lectures.map(lecture => lecture.id),
      tagsIds: tags ? tags.map(tag => tag.id) : [],
      date: new Date(),
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      const { Cycle, allEvents, allLectures } = data;
      const { lectures: lecturesOfCycle, recommendedLectures } = Cycle;
      const additionalDataForCycle = additionalDataOfEvents(allEvents);
      const additionalDataForLectures = additionalDataOfEventsForLectures(allEvents);
      const lecturesWithAdditionalData = lecturesOfCycle
        .map(lecture => Object.assign({}, lecture, additionalDataForLectures[lecture.id]));

      const CycleWithAdditionalData = Object.assign(
        {},
        Cycle,
        {
          discounts: data.allDiscounts.slice(),
          lectures: lecturesWithAdditionalData,
        },
        additionalDataForCycle,
      );
      const dataWithAdditionalData = Object.assign(
        {},
        data,
        {
          Cycle: Object.assign(
            CycleWithAdditionalData,
            {
              isAvailableForBuy: checkCourseAvailableForBuy(CycleWithAdditionalData),
              product: productParams(CycleWithAdditionalData),
            },
          ),
          recomendation: recommendedLectures.concat(allLectures).slice(0, 3),
        },
      );
      const currentPageData = Object.assign(
        {},
        dataWithAdditionalData,
      );
      return currentPageData;
      // return null;
    });
};

const redisBatchCycle = (cycle) => {
  const output = Object.assign({}, cycle);
  const { id, lectures, recommendedLectures, tags } = cycle;
  const commandsGetLectures = []
    .concat(lectures
      .map(({ id: lectureId }) => ['get', `lecture:${lectureId}`]));
  const commanndsRecommendedLectures = []
    .concat(recommendedLectures
      .map(({ id: lectureId }) => ['get', `lecture:${lectureId}`]));
  const commandsGetRecomendedLecturesOfTags = []
    .concat(tags
      .map(({ id: tagId }) => ['get', `recomendedLecturesOfTag:${tagId}`]));

  return redis.batch(commandsGetLectures).execAsync()
    .then((result) => {
      output.lectures = result
        .map(item => JSON.parse(item))
        .filter(item => item.public);
      if (commandsGetRecomendedLectures.length === 0) {
        return redis.batch(commandsGetRecomendedLecturesOfTags).execAsync()
      }
      return redis.batch(commandsGetRecomendedLectures).execAsync();
    })
    .then((result) => {
      output.recommendedLectures = result
        .map(item => JSON.parse(item))
        .filter(item => item.public);
      return output;
    });
};

module.exports = (req, cycle) => {
  return Promise.all([
    redis.batch([
      ['get', 'template'],
      ['get', 'howWork:course'],
    ]).execAsync(),
    cycleByIdQuery(req, cycle),
  ])
    .then((result) => {
      const [[template, howWork], data] = result;
      return Object.assign(
        {},
        data,
        {
          HowWorkPage: JSON.parse(howWork),
          templateData: JSON.parse(template),
        },
        { ___typename: 'Cycle' },
      );
    });
};
