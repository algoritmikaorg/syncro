const { graphQlFetch } = require('./../../graphql');
const acquiringSettingQuery = require('./../../graphql/acquiringSettingQuery');
const checkAvailabilityOfUserMutation = require('./../../graphql/checkAvailabilityOfUserMutation');
const validator = require('./../../lib/validator');
const redis = require('./../../db/redis');
const { randomBytesAsync } = require('./../../lib/crypto');
const clearPhone = require('./../../lib/clearPhone');

const { ROOT_TOKEN } = require('./../../config');

const allSubscriptionProductsQuery = (req) => {
  return graphQlFetch({ req: { user: { token: ROOT_TOKEN } }, query: 'allSubscriptionProductsQuery' })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};

module.exports = (req) => {
  return Promise.all([
    redis.getAsync('template'),
    allSubscriptionProductsQuery(req),
  ])
    .then((result) => {
      const [template, data] = result;
      return Object.assign(
        {},
        data,
        {
          templateData: JSON.parse(template),
        },
      );
    });
};

module.exports.setDraft = (req) => {
  let userAnOrder = null;
  let userAnGift = null;
  let certificate = null;
  let order = null;

  const {
    firstName,
    lastName,
    email,
    phone,
  } = req.body;

  userAnOrder = {
    firstName,
    lastName,
    email,
    phone: clearPhone(phone),
  };

  const id = Object.keys(req.body)
    .filter(key => /^id-/.test(key))
    .map(key => key.replace(/^id-/, ''))[0];

  if (!id) return Promise.reject(new Error('Wrong params'));

  const errUserAnOrder = validator(userAnOrder);

  if (errUserAnOrder.length > 0) return Promise.reject(new Error('Wrong user params'));

  let hash = null;

  return randomBytesAsync(12)
    .then((buff) => {
      hash = buff.toString('hex');
      return graphQlFetch({
        req: { user: { token: ROOT_TOKEN } },
        query: 'subscriptionProductByIdQuery',
        variables: {
          id,
        },
      });
    })
    .then(({ data, errors, error }) => {
      if (
        errors
        || error
        || !data
        || !data.SubscriptionProduct
        || !data.SubscriptionProduct.public
      ) return Promise.reject(errors || error);
      subscription = data.SubscriptionProduct;
      order = {
        totalPrice: subscription.price,
        orderPrice: subscription.price,
        discountPrice: 0,
        discountsRate: 0,
      };
      return redis.setAsync(
        `subscription-draft:${hash}`,
        JSON.stringify({
          userAnOrder,
          subscription,
          order,
        }),
      );
    })
    .then(() => ({ hash }));
};

module.exports.draft = (req) => {
  const { id } = req.params;
  return Promise.all([
    redis.getAsync(`subscription-draft:${id}`),
    acquiringSettingQuery(req),
  ])
    .then((result) => {
      const [reply, setting] = result;
      if (!reply) return Promise.reject();
      const data = JSON.parse(reply);

      return Object.assign(
        {},
        data,
        {
          id,
          acquiring: setting.acquiring || {},
        },
      );
    });
};

module.exports.order = (req) => {
  const { id } = req.body;

  let userAnOrder = null;
  let userAnGift = null;
  let order = null;
  let product = null;

  let subscription = null;
  let draft = null;

  return redis.getAsync(`subscription-draft:${id}`)
    .then((reply) => {
      if (!reply) return Promise.reject();
      const obj = JSON.parse(reply);
      draft = JSON.parse(reply);
      userAnOrder = obj.userAnOrder;
      order = obj.order;
      product = obj.subscription;

      return Promise.all([
        acquiringSettingQuery(req),
        checkAvailabilityOfUserMutation(req, [userAnOrder]),
      ]);
    })
    .then((result) => {
      const [setting, userIds] = result;
      const [userAnOrderIds] = userIds.ids;

      const payment = {
        acquiringId: setting.acquiring.id,
        commission: setting.acquiring.commission,
        orderPrice: order.totalPrice,
        discountPrice: 0,
        totalPrice: order.totalPrice,
        type: 'INTERNET_ACQUIRING',
        userId: userAnOrderIds,
        status: 'PENDING',
        referer: 'SITE',
        data: draft,
      }
      return graphQlFetch({
        req: { user: { token: ROOT_TOKEN } },
        query: 'createSubscriptionMutation',
        variables: {
          userId: userAnOrderIds,
          productId: product.id,
          payment,
        }
      })
    })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.createSubscription;
    })
    .then((result) => {
      subscription = result;
      draft.paymentId = subscription.payment.id;

      const commands = [
        ['set', `payment-data:${subscription.payment.id}`, JSON.stringify(draft)],
        ['del', `subscription-draft:${id}`],
      ];
      return redis.batch(commands).execAsync();
    })
    .then(() => {
      subscription.data = draft;
      return subscription;
    });
};

module.exports.payment = (req) => {
  const { id } = req.params;

  return Promise.all([
    redis.getAsync(`payment-data:${id}`),
    acquiringSettingQuery(req),
  ])
    .then((result) => {
      const [reply, setting] = result;
      if (!reply) return Promise.reject();
      const data = JSON.parse(reply);
      if (!data) return Promise.reject();

      return Object.assign(
        {},
        data,
        {
          acquiring: setting.acquiring || {},
        },
      );
    });
};

module.exports.cancel = (req) => {
  const { id } = req.params;
  const { id: userId } = req.user;

  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'deleteSubscriptionWithPaymentMutation',
    variables: {
      id,
      userId,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};
