const { graphQlFetch } = require('./../../graphql');
const { ROOT_TOKEN } = require('./../../config');

module.exports = (req) => {
  let { review } = req.body;
  const { lectureId, eventId } = req.body;
  const { id: userId } = req.user;
  review = review.trim();

  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'createReviewMutation',
    variables: {
      description: review,
      lectureId,
      eventId,
      userId,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.Review;
    });
};
