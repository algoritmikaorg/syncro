const sortForUniq = (obj) => {
  Object.keys(obj).forEach((key) => {
    Object.keys(obj[key]).forEach((key1) => {
      if (!Array.isArray(obj[key][key1])) return undefined;
      const tmpIds = [];
      obj[key][key1] = obj[key][key1].filter(({ id }) => {
        if (tmpIds.includes(id)) return false;
        tmpIds.push(id);
        return true;
      });
    })
  });
};

const additionalDataOfEventsForLectures = (events) => {
  const lecturesAddationalData = {};
  events.forEach((event) => {
    const {
      lecture: { id },
      lecturers,
      location,
      date,
      price,
    } = event;
    if (!lecturesAddationalData[id]) lecturesAddationalData[id] = {};
    if (!lecturesAddationalData[id].lecturers) lecturesAddationalData[id].lecturers = [];
    if (!lecturesAddationalData[id].locations) lecturesAddationalData[id].locations = [];
    if (!lecturesAddationalData[id].dates) lecturesAddationalData[id].dates = [];
    if (!lecturesAddationalData[id].prices) lecturesAddationalData[id].prices = [];
    lecturesAddationalData[id].lecturers = lecturesAddationalData[id].lecturers.concat(lecturers);
    lecturesAddationalData[id].locations.push(location);
    lecturesAddationalData[id].dates.push(date);
    lecturesAddationalData[id].prices.push(price);
  });

  sortForUniq(lecturesAddationalData);
  return lecturesAddationalData;
};

const productParams = (item) => {
  const product = {};
  product.__typename = item.__typename;
  product.id = item.id;
  product.title = item.title;
  product.tags = item ? item.tags.map(({ id }) => id) : null;
  product.price = item.price;
  product.lectures = [];
  product.discounts = item.discounts
    .map(({
      id,
      rate,
      unused,
      title,
      buyingLecturesCount,
      participantsCount,
      allProducts,
    }) => ({
      id,
      rate,
      unused,
      title,
      buyingLecturesCount,
      participantsCount,
      allProducts,
    }));

  product.discountsOfLectures = {};
  product.events = {};
  if (item.lectures) {
    item.lectures.forEach(({ id: lectureId, discounts, events }) => {
      product.lectures.push(lectureId);
      product.discountsOfLectures[lectureId] = discounts.map(({
        id,
        rate,
        title,
        unused,
        buyingLecturesCount,
        participantsCount,
      }) => ({
        id,
        rate,
        title,
        unused,
        buyingLecturesCount,
        participantsCount,
      }));

      events.forEach(({ id, price, date }) => {
        product.events[id] = { price, date };
      });
    });
  } else {
    item.events.forEach(({ id, price, date }) => {
      product.events[id] = { price, date };
    });
  }
  if (item.buyFull) {
    product.buyFull = item.buyFull;
  }

  return product;
};

const checkCourseAvailableForBuy = ({ buyFull, lectures }) => {
  const lecturesAvailableForBuy = lectures.filter(({ events }) => Boolean(events.length));
  if (
    lecturesAvailableForBuy.length === 0
    || (buyFull && lecturesAvailableForBuy.length !== lectures.length)
  ) return false;

  return true;
};

const additionalDataOfEvents = (events) => {
  const lecturersOflecture = {};
  const lecturers = [];
  const locations = [];
  const dates = [];
  let prices = [];
  const lecturersIds = [];
  const locationsIds = [];

  events.forEach((event) => {
    const {
      location,
      lecturers: lecturersOfEvent,
      price,
      date,
    } = event;

    lecturersOfEvent.forEach((lecturer) => {
      const { id } = lecturer;
      if (!lecturersIds.includes(id)) {
        lecturersIds.push(id);
        lecturers.push(lecturer);
      }
    });

    if (!locationsIds.includes(location.id)) {
      locations.push(event.location);
      locationsIds.push(location.id);
    }

    if (!prices.includes(price)) {
      prices.push(price);
    }
    if (!dates.includes(date)) {
      dates.push(date);
    }
  });

  prices = prices.sort((a, b) => a - b);
  let formattedPrice = '';
  const min = prices[0];
  const max = prices[prices.length - 1];
  if (!max || min === max) {
    formattedPrice = min;
  } else {
    formattedPrice = `${min} - ${max}`;
  }

  return {
    lecturers,
    locations,
    dates,
    prices,
    formattedPrice,
  };
};

module.exports = {
  sortForUniq,
  additionalDataOfEventsForLectures,
  productParams,
  checkCourseAvailableForBuy,
  additionalDataOfEvents,
};
