const PROJECT_ID = 'cjd5n24x100080161u9vrln2c';
// const PROJECT_ID = 'cjdagzahz001w0190w199ns7j';
const DOMAIN = 'localhost:60000';

module.exports = {
  SIMPLE_API_ENDPOINT: `http://${DOMAIN}/simple/v1/${PROJECT_ID}`,
  // FILE_API_ENDPOINT: `http://${DOMAIN}/file/v1/${PROJECT_ID}`,
  FILE_API_ENDPOINT: 'http://localhost:60001/file',
  SUBSCRIPTIONS__API_ENDPOINT: `ws://${DOMAIN}/subscriptions/v1/${PROJECT_ID}`,
  // IMAGE_HOST: `http://${DOMAIN}/images/v1/${PROJECT_ID}`,
  FILE_HOST: 'http://localhost:60001/file',
  IMAGE_HOST: 'http://localhost:60002/images',

  REDIS: {
    HOST: '127.0.0.1',
    PORT: '6379',
    PASS: '28b51f7a0b573fc6a71b79de5635c97484e67a8fee8dd430a9b1b60f8475c9f9',
  },
  COOKIE_MAX_AGE: 30 * 24 * 60 * 60 * 1000,
  ROOT_TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTc1NTgzNzIsImNsaWVudElkIjoiY2pkNW4xaDFnMDAwMDAxNjFkdGVveWNvNSIsInByb2plY3RJZCI6ImNqZDVuMjR4MTAwMDgwMTYxdTl2cmxuMmMiLCJwZXJtYW5lbnRBdXRoVG9rZW5JZCI6ImNqZDVuMm1jZzAwdG4wMTYxOHg4ZjU4djkifQ.VPozKoLZ3jEHVtw949ooXKdN3abPhLMGNTYFrik4mgs',
  // ROOT_TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTc4NTA0OTEsImNsaWVudElkIjoiY2pkYWdydGUwMDAwMDAxOTBnMGw2eDY0MCIsInByb2plY3RJZCI6ImNqZGFnemFoejAwMXcwMTkwdzE5OW5zN2oiLCJwZXJtYW5lbnRBdXRoVG9rZW5JZCI6ImNqZGFnenFzejAwdmUwMTkwb2UxcWhmZ24ifQ.3NZ3pBphaAX1sEQMePV5g44FCKlkCLdoo2H5uQFuMw0',
  MAIL_HOST: 'http://localhost:60003',
  // sec
  RESET_PASS_TOKEN_EXPIRES: 3600,
  URL: 'http://localhost:3000',
  SECRET: '83c7ed9f6234b144483258ea2c58942ebe418f57b027a3355cdae5267c7553fa',
};
