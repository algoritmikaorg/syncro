const cors = require('cors');

module.exports = (app) => {
  if (process.env.LOCATION_ENV === 'local') {
    app.use(cors());
  }
};
