const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/order');
const controllerAsyncPayment = require('./../controllers/payment');

/* GET home page. */
router.post('/', (req, res, next) => {
  const { id } = req.body;
  if (!id) return next();

  return controllerAsync(req)
    .then((result) => {
      if (!result || !result.id || !result.payment || !result.payment.id) return next(new Error('Order'));
      return res.redirect('/account');
    })
    .catch(next);
});

router.post('/payment', (req, res, next) => {
  const { id } = req.body;
  if (!id) return next();

  return controllerAsync(req)
    .then((result) => {
      if (!result || !result.id || !result.payment || !result.payment.id) return next(new Error('Order Payment'));
      return controllerAsyncPayment.orderPayment(result.data);
    })
    .then((url) => {
      if (!url) return next(new Error('Acquiring not url'));
      res.redirect(url);
    })
    .catch(next);
});

router.post('/draft', (req, res, next) => {
  return controllerAsync.setDraft(req)
    .then((result) => {
      return res.json({ data: result, error: false });
    })
    .catch((err) => {
      console.error(err);
      res.json({ data: null, error: true });
    });
});

router.get('/draft/:id', (req, res, next) => {
  return controllerAsync.draft(req)
    .then((result) => {
      if (!result) return next();
      const currentPageData = Object.assign({}, result, { user: req.user });
      return res.render('pages/order', { currentPageData });
    })
    .catch(next);
});

router.get('/payment/:id', (req, res, next) => {
  return controllerAsync.payment(req)
    .then((result) => {
      if (!result) return next();
      const currentPageData = Object.assign({}, result, { user: req.user });
      return res.render('pages/order', { currentPageData });
    })
    .catch(next);
});

router.get('/cancel/:id', (req, res, next) => {
  const { id } = req.params;
  if (!req.isAuthenticated()) return next(new Error());
  if (!id || id.length < 25) return next();

  return controllerAsync.cancel(req)
    .then((result) => {
      if (result.deleteOrderAndPayment && result.deleteOrderAndPayment.id) return res.redirect('/account');
      return next();
    })
    .catch(next);
});

module.exports = router;
