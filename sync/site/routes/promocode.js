const express = require('express');
const router = express.Router();

const responseErrorObject = require('./../lib/responseErrorObject');
const controllerAsync = require('./../controllers/promocode');

router.post('/', (req, res, next) => {

  controllerAsync(req)
    .then((data) => {
      if (!data || !data.allDiscounts) return res.json(responseErrorObject(null));
      const [discount] = data.allDiscounts;
      const [certificate] = data.allCertificates;
      const [subscription] = data.allSubscriptions;

      if (!(discount || certificate || subscription)) return res.json({ data: null, error: true });
      if (discount) {
        console.log(discount);
        const { entityIds, tagsIds } = JSON.parse(req.body.data);
        discount.courses = discount.courses.filter(({ id }) => entityIds.includes(id));
        discount.cycles = discount.cycles.filter(({ id }) => entityIds.includes(id));
        discount.lectures = discount.lectures.filter(({ id }) => entityIds.includes(id));
        discount.tags = discount.tags.filter(({ id }) => tagsIds.includes(id));
      }
      return res.json({ data: discount || certificate || subscription, error: null });
    })
    .catch((err) => {
      console.error(err);
      return res.json({ data: null, error: true })
    });
});

module.exports = router;
