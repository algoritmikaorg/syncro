const express = require('express');
const petrovich = require('petrovich');

const router = express.Router();
const graphQlFetch = require('./../graphql').graphQlFetch;
const templateQuery = require('./../graphql/templateQuery');

const lecturerByIdQuery = (req, id) => {
  return graphQlFetch({ req, query: 'lecturerByIdQuery', variables: { id } })
    .then(({ data, errors, extensions }) => {
      return {
        data: Object.assign(
          data,
          { Lecturer: data.Lecturer ? Object.assign(data.Lecturer, {
              genitive: data.Lecturer ? petrovich.male.first.genitive(data.Lecturer.firstName) : ''
            }) : null
          }),
        errors,
        extensions,
      };
    });
};

router.get('/:id', (req, res, next) => {
  const { id } = req.params;
  Promise.all([
    templateQuery(req),
    lecturerByIdQuery(req, id),
  ])
    .then((values) => {
      let currentPageData = { user: req.user };
      let errors = null;
      const error = null;
      values.forEach(({ data, errors: err, extensions }) => {
        if (err) errors = err;
        currentPageData = Object.assign(currentPageData, data);
      });

      if (errors) {
        next(errors);
        return null;
      }
      if (!currentPageData.Lecturer) {
        next();
        return null;
      }

      res.render('pages/lecturer', { currentPageData });
    })
    .catch((error) => {
      next(error);
    });
});

module.exports = router;
