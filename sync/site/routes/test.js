const express = require('express');
const router = express.Router();
const cuid = require('cuid');

const { graphQlFetch } = require('./../graphql');

/* GET home page. */
router.get('/', (req, res, next) => {
  // const fn = graphqlExpress({ schema: req.app.locals.schema });
  // const fn = req.app.locals.fetchGraphQlEndPointData;
  graphQlFetch({ req, query: 'mainPageQuery' })
    .then((...args) => {
      console.log('>>>', args);
      console.log(args[0].errors);
    })
    .catch((err) => {
      console.log('err', err);
    });
  const query = {
    query: `
      {
        mainPage {
          lectures {
            id,
            title,
          }
        }
      }
    `,
    variables: null,
    operationName: null,
  };

  // fn(req, res, query)
  //   .then(data => res.json(data))
  //   .catch((err) => { console.log(err); });
});

router.get('/order', (req, res, next) => {
  res.render('pages/test-order', { id: cuid() });
});

module.exports = router;
