const url = require('url');
const runHttpQuery = require('apollo-server-core').runHttpQuery;
const GraphiQL = require('apollo-server-module-graphiql');

const graphqlEndPointRequest = (options) => {
  if (!options) {
    throw new Error('GraphQL requires options.');
  }

  return (req, res, next) => {
    runHttpQuery([req, res], {
      method: req.method,
      options,
      query: req.method === 'POST' ? req.body : req.query,
    })
      .then(gqlResponse => next(null, gqlResponse, req, res))
      .catch(error => next(error, null, req, res));
  };
};

module.exports = graphqlEndPointRequest;
