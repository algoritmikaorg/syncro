const { graphQlFetch } = require('./graphql');

const signupUserMutation = (req) => {
  return graphQlFetch({ req, query: 'signupUserMutation', variables: req.body })
    .then(({ data, errors, extensions }) => {
      return {
        data,
        errors,
        extensions,
      };
    });
};

module.exports = signupUserMutation;
