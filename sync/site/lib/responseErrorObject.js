const defaultMessage = 'Сервис не доступен. Попробуйте позже.';

const errorObj = message => ({ data: null, error: { message: message || defaultMessage } });

module.exports = (graphqlResult, property, message) => {
  if (!graphqlResult) return errorObj(message);
  const { data, errors, error } = graphqlResult;
  if (!data[property]) return errorObj(message);
  if (errors || error) return errorObj(message);
};
