const fetch = require('node-fetch');

const { MAIL_HOST } = require('./../config');

const send = (path, data) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(Object.assign({}, { data })),
  };
  return fetch(
    `${MAIL_HOST}${path}`,
    options,
  );
};

module.exports = {};
module.exports.send = send;
