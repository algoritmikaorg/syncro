const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const sassMiddleware = require('node-sass-middleware');
const helmet = require('helmet');

const config = require('./config');

const app = express();
app.set('trust proxy', true);

require('./app/cors')(app);
require('./app/session')(app);
require('./app/passport')(app);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.disable('x-powered-by');
app.use(helmet());

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public/assets/img', 'favicon.ico')));

require('./app/morgan')(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true,
}));
app.use(express.static(path.join(__dirname, 'public')));

// graphql;
require('./app/graphql')(app);
// locals
require('./app/locals')(app);
// mocks
require('./app/mocks')(app);
// routes
require('./app/routes')(app);

app.locals.IMAGE_HOST = config.IMAGE_HOST;
app.locals.FILE_HOST = config.FILE_HOST;

module.exports = app;
