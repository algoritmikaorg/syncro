const generateCode = require('./../lib/order/generateCode');

module.exports = (event) => {
  event.data.code = generateCode(event);
  return event;
};
