module.exports = (event) => {
  const { data } = event;
  const { quantityOfTickets } = data;

  data.quantityOfTicketsAvailable = quantityOfTickets;
  return event;
};
