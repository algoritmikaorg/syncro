module.exports = (event) => {
  if (event.data.totalPrice === 0) {
    event.data.status = 'PAID';
  }
  return event;
};
