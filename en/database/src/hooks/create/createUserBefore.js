const clearPhone = require('./../../lib/clearPhone');
const upperCaseFirstSymbol = require('./../../lib/upperCaseFirstSymbol');

module.exports = (event) => {
  const { data: { phone, firstName, secondName, lastName } } = event;
  if (phone) event.data.phone = clearPhone(phone);
  if (firstName) event.data.firstName = upperCaseFirstSymbol(firstName);
  if (secondName) event.data.secondName = upperCaseFirstSymbol(secondName);
  if (lastName) event.data.lastName = upperCaseFirstSymbol(lastName);
  return event;
};
