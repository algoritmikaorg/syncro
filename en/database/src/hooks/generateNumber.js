const generateCode = require('./../lib/order/generateCode');

module.exports = (event) => {
  event.data.number = generateCode(event);
  return event;
};
