const { fromEvent } = require('graphcool-lib');

module.exports = (event) => {
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  const { data } = event;
  const { useCount, id } = data;

  if (useCount === -1) {
    event.data.unused = 1;
    return event;
  }

  return api.request(`
    query {
      Discount(id: "${id}"){
        useCount
        _ordersMeta(filter: {
          status: PAID
        }) {
          count
        }
        _additionalForOrdersMeta(filter: {
          status: PAID
        }) {
          count
        }
      }
    }
  `)
    .then(({ Discount }) => {
      const { useCount: useCountL, _ordersMeta, _additionalForOrdersMeta } = Discount;
      const used = _ordersMeta.count + _additionalForOrdersMeta.count;
      event.data.unused = 1;
      if (useCount && used >= useCount) event.data.unused = 0;
      if (useCount) {
        if (useCount === -1) {
          event.data.unused = 1;
        } else if (used >= useCount) {
          event.data.unused = 0;
        }
      }
      if (useCountL) {
        if (useCountL === -1) {
          event.data.unused = 1;
        } else if (useCountL && used >= useCountL) {
          event.data.unused = 0;
        }
      }
      return event;
    })
    .catch((error) => {
      console.log(error);
      return { error: 'An unexpected error occured' };
    });
};
