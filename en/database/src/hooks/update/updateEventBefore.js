const { fromEvent } = require('graphcool-lib');

module.exports = (event) => {
  const { data } = event;
  const { quantityOfTickets, id } = data;
  data.quantityOfTicketsAvailable = quantityOfTickets;

  if (quantityOfTickets === null || quantityOfTickets === undefined) return event;

  if (quantityOfTickets === 0) {
    data.quantityOfTicketsAvailable = quantityOfTickets;
    return event;
  }

  if (quantityOfTickets < 0) {
    data.quantityOfTicketsAvailable = 0;
    return event;
  }

  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  return api.request(`
    query {
      Event(id: "${id}"){
        quantityOfTickets
        quantityOfTicketsAvailable
      }
    }
  `)
    .then((eventQueryResult) => {
      const { quantityOfTicketsAvailable } = eventQueryResult;
      if (quantityOfTicketsAvailable === null || quantityOfTicketsAvailable === undefined) {
        data.quantityOfTicketsAvailable = quantityOfTickets;
        return event;
      }
      data.quantityOfTicketsAvailable = quantityOfTicketsAvailable;

      if (quantityOfTicketsAvailable > quantityOfTickets) {
        data.quantityOfTicketsAvailable = quantityOfTickets;
        return event;
      }

      if (
        eventQueryResult.quantityOfTickets > quantityOfTickets
        || eventQueryResult.quantityOfTickets === quantityOfTickets
      ) return event;

      const d = quantityOfTickets - discountQueryResult.quantityOfTickets;
      data.quantityOfTicketsAvailable += data.quantityOfTicketsAvailable + d;
      data.quantityOfTicketsAvailable = data.quantityOfTicketsAvailable > quantityOfTickets ? quantityOfTickets : data.quantityOfTicketsAvailable;
      return event;
    })
    .catch((error) => {
      console.log(error);
      return { error: 'An unexpected error occured' };
    });

  return event
}
