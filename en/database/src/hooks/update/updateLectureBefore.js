module.exports = (event) => {
  const { data } = event;
  const { course, cycle } = data;
  if (course && course.length > 0) {
    event.data.lectureType = 'LECTURE_FOR_COURSE';
  }
  if (cycle && cycle.length > 0) {
    event.data.lectureType = 'LECTURE_FOR_CYCLE';
  }

  return event;
};
