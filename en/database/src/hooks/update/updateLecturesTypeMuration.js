const updateLecturesTypeMuration = (api, ids, type) => {
  const mutations = ids.map(id => `
    mutation {
      updateLecture (id: "${id}", lectureType: ${type}) {
        id
      }
    }
  `);

  return Promise.all(mutations.map(mutation => api.request(mutation)));
};

module.exports = updateLecturesTypeMuration;
