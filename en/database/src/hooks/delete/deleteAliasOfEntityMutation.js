module.exports = (filter, api) => {
  return api.request(`
    query {
      allAliases (filter: { ${filter} }) {
        id
      }
    }
  `)
    .then(({ allAliases }) => {
      if (allAliases.length === 0) return {};
      return api.request(`
        mutation {
          deleteAlias(id: "${result.allAliases[0].id}") {
            id
          }
        }
      `);
    });
};
