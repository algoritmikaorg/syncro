const fetcPostJson = require('./../../lib/fetcPostJson');

module.exports = (event) => {
  const { data } = event;
  return fetcPostJson({ data }, `${process.env.MAIL_HOST}/certificate`)
    .then(result => ({}));
};
