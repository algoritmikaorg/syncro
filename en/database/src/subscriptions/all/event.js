const fetcPostJson = require('./../../lib/fetcPostJson');

module.exports = (event) => {
  const { data } = event;
  return fetcPostJson({ data }, `${process.env.CRON_HOST}/event`);
};
