const fetcPostJson = require('./../../lib/fetcPostJson');

const { SECRET } = require('./../../config');

module.exports = (event) => {
  const { data } = event;
  const { User: { node, mutation } } = data;

  if (mutation === 'CREATED') {
    return Promise.all([
      fetcPostJson({ email: node.email }, `${process.env.SITE_HOST}/users/confirm-email`),
      fetcPostJson({ data }, `${process.env.MAIL_HOST}/user`),
    ])
      .then(results => ({ results }));
  }

  if (mutation === 'UPDATED') {
    return fetcPostJson({ data }, `${process.env.MAIL_HOST}/user`);
  }
};
