require('isomorphic-fetch');

module.exports = (event, data, path) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  return fetch(
    `${process.env.SITE_WEBHOOK_HOST}${path}`,
    options,
  )
    .then(res => event);
};
