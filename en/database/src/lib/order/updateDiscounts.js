module.exports = ({ discounts, additionalDiscounts }, api) => {
  const allDiscounts = discounts.concat(additionalDiscounts);
  const mutations = allDiscounts.map(({ id, useCount, unused }) => `
    mutation {
      updateDiscount(id: "${id}", unused: ${unused - 1}) {
        id
      }
    }
  `);

  return Promise.all(mutations.map(mutation => api.request(mutation)));
};
