const usedLectures = (id, lecturesOfPromocode) => {
  if (Array.isArray(lecturesOfPromocode)) {
    return lecturesOfPromocode.find(({ id: prId }) => prId === id) || 0;
  }
  return lecturesOfPromocode;
};

module.exports = ({ subscriptionsInPayments, data }, api) => {
  const { order: lecturesOfPromocode } = data;

  const mutations = subscriptionsInPayments.map(({ id, unusedLectures }) => `
    mutation {
      updateSubscription(id: "${id}", unusedLectures: ${unusedLectures - usedLectures(id, lecturesOfPromocode)}) {
        id
      }
    }
  `);

  return Promise.all(mutations.map(mutation => api.request(mutation)));
};
