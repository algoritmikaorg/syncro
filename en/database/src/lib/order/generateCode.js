const random = require('./../random');
const { CODE_LENGTH, RANDOM_PULL } = require('./../../config');

module.exports = (event) => {
  if (event && event.data && event.data.prefix) {
    code = `${event.data.prefix}-${random.string(CODE_LENGTH, RANDOM_PULL).toUpperCase()}`;
  } else {
    code = random.string(CODE_LENGTH).toUpperCase();
  }
  return code;
};
