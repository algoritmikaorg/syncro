const { fromEvent } = require('graphcool-lib');

module.exports = ({ order: { participants, events } }, api) => {
  const mutations = events.map(({ id, quantityOfTicketsAvailable }) => `
    mutation {
      updateEvent(id: "${id}", quantityOfTicketsAvailable: ${quantityOfTicketsAvailable - participants.length} ) {
        id
      }
    }
  `);

  return Promise.all(mutations.map(mutation => api.request(mutation)));
};
