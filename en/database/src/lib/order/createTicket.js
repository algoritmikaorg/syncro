const generateCode = require('./generateCode');

module.exports = ({ order: { events, participants } }, api) => {
  const mutations = participants
    .map(({ id: participantId }) => events
      .map(({ id: eventId }) => `
        mutation {
          createTicket(eventId: "${eventId}", userId: "${participantId}", number: "${generateCode()}") {
            id
          }
        }
      `))
    .reduce((prev, curr) => prev.concat(curr));

  return Promise.all(mutations.map(mutation => api.request(mutation)));
};
