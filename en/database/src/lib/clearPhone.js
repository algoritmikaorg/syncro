module.exports = (phone) => {
  if (!phone) return null;
  if (phone.length < 11) return null;
  const t = phone.replace(/\+|\(|\)|-|_| |/g, '');

  return t.replace(/^(7|8)/, '+7');
};
