const { fromEvent } = require('graphcool-lib');

const config = require('./../../config');

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!')
    return { error: 'Email Signup not configured correctly.' };
  }
  const { data } = event;
  const {
    id,
    userId,
  } = data;

  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  return api.request(`
    query {
      Order (id: "${id}") {
        id
        payment {
          id
        }
        user {
          id
        }
      }
    }`)
    .then((result) => {
      if (!result || !result.Order) return null;
      const { Order } = result;
      if (Order.user.id !== userId) return null;

      return api.request(`
        mutation {
          deleteOrder(id: "${Order.id}") {
            id
          }
          deletePayment(id: "${Order.payment.id}") {
            id
          }
        }
      `);
    })
    .then((result) => {
      if (!result) return event;
      const { deleteOrder, deletePayment } = result;
      event.data.id = deleteOrder.id;
      return event;
    })
    .catch((error) => {
      console.error(error);
      return { error: 'An unexpected error occured.' };
    });
};
