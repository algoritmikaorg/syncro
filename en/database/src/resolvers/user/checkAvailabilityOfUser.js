// проверка на существование пользователя по емаил, если не существует создаем
const { fromEvent } = require('graphcool-lib');
const bcrypt = require('bcryptjs');

const config = require('./../../config');
const random = require('./../../lib/random');
const getGraphcoolUser = require('./../email-password/lib/getGraphcoolUser');
const validate = require('./../email-password/lib/validate');
const createGraphcoolUser = require('./../email-password/lib/createGraphcoolUserWithRoleAndGenPass');

const checkUser = (api, users) => Promise.all(users.map(user => getGraphcoolUser(api, user.email)))
  .then((result) => {
    return result
      .map((item, index) => ({ input: users[index], user: item }));
  });

const createUsers = (api, users) => Promise.all(users.map(({ input, user }) => {
  if (user) return user;
  const salt = bcrypt.genSaltSync(config.SALT_ROUNDS);
  const errorsValidate = validate(input);
  if (errorsValidate.length !== 0) return Promise.reject(new Error(`Not a valid input data ${errorsValidate.join(', ')}`));
  generatedPassword = random.string(8);

  return bcrypt.hash(generatedPassword, salt)
    .then(hash => createGraphcoolUser(
      api,
      Object.assign(
        {},
        input,
        {
          hash,
          role: 'CLIENT',
        },
      ),
    ));
}));

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!');
    return { error: 'Email Authentication not configured correctly.' };
  }

  const users = event.data.users.map(user => JSON.parse(user));
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  return checkUser(api, users)
    .then((result) => {
      return createUsers(api, result);
    })
    .then((result) => {
      return { data: { ids: result.map(({ id }) => id) } };
    })
    .catch((err) => {
      console.error(err);
      return { error: 'An unexpected error occured' };
    });
};
