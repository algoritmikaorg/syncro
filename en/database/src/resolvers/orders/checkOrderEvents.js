const { fromEvent } = require('graphcool-lib');

const checkOrderEventsOfSite = require('./checkOrderEventsOfSite');
const checkOrderEventsOfAdmin = require('./checkOrderEventsOfAdmin');
//
const checkSource = (order, api) => {
  const { source } = order;
  if (source && source === 'ADMIN') return checkOrderEventsOfAdmin(order, api);
  return checkOrderEventsOfSite(order, api);
};

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!');
    return { error: 'Email Authentication not configured correctly.' };
  }
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  const { data: { order: orderJson } } = event;
  const order = typeof orderJson === 'string' ? JSON.parse(orderJson) : orderJson;

  const {
    participants,
    lectures,
  } = order;

  if (!participants || participants.length === 0) return { error: 'Wrong order params' };
  if (
    !lectures
    || Object.keys(lectures).filter(id => lectures[id].eventId).length === 0
  ) return { error: 'Wrong order params' };

  // let resultOfChecking = null;

  //  проверка продукта
  return checkSource(order, api)
    .then(({ resultOfChecking, resultOfCalcOrder }) => {
      return {
        data: {
          resultOfChecking: JSON.stringify(resultOfChecking),
          resultOfCalcOrder: JSON.stringify(resultOfCalcOrder),
        },
      };
    })
    .catch((err) => {
      console.error(err);
      return { error: 'An unexpected error occured' };
    });
};
