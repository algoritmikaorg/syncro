const getCertificateById = (id, api) => {
  return api.request(`
    query {
      CertificateProduct (id: "${id}") {
        id
        public
        title
        price
        lecturesCount
        months
        personsCount
        __typename
      }
    }
  `);
};

const getSubscriptionById = (id, api) => {
  return api.request(`
    query {
      SubscriptionProduct (id: "${id}") {
        id
        public
        title
        price
        lecturesCount
        months
        personsCount
        __typename
      }
    }
  `);
};

const getProductById = (order, api) => {
  const { certificate, subscription } = order;
  if (certificate && certificate.id) return getCertificateById(certificate.id, api);
  if (subscription && subscription.id) return getSubscriptionById(subscription.id, api);
  return Promise.reject(new Error('Wrong id'));
};

const checkEntity = (order, api) => {
  return getProductById(order, api)
    .then((data) => {
      const { CertificateProduct, SubscriptionProduct } = data;
      return Promise.resolve([CertificateProduct || SubscriptionProduct, null]);
    });
};

const checkDiscount = (Discount, order, Entity, __typename, isCheckPublic) => {
  const {
    id: productId, __typename: productTypename,
  } = Entity;

  if (
    Discount.__typename !== __typename
    || Discount.unused === 0
    || !Discount.public
    || new Date(Discount.validityPeriodFrom) > new Date()
    || new Date(Discount.validityPeriodTo) < new Date()
  ) return [Discount, Discount];

  if (Discount.allProducts) return [Discount, null];

  if (
    productTypename === 'CertificateProduct'
  ) {
    const certificateProductsIds = Discount.certificateProducts.map(({ id }) => id)
      .filter(id => id === productId);

    if (certificateProductsIds.length > 0) return [Discount, null];
  }

  if (
    productTypename === 'SubscriptionProduct'
  ) {
    const subscriptionProductsIds = Discount.subscriptionProducts.map(({ id }) => id)
      .filter(id => id === productId);

    if (subscriptionProductsIds.length > 0) return [Discount, null];
  }

  return [Discount, Discount];
};

const checkPromocode = (order, Entity, api) => {
  const {
    promocode,
  } = order;

  if (!promocode) return Promise.resolve([null, null]);

  const {
    id,
    __typename,
  } = promocode;

  if (!id || !__typename) return Promise.resolve([null, null]);

  return api.request(`
    query {
      Discount(
        id: "${id}"
      ) {
        id
        public
        title
        rate
        validityPeriodFrom
        validityPeriodTo
        unused
        allProducts
        certificateProducts {
          id
        }
        subscriptionProducts {
          id
        }
        __typename
      }
    }
  `)
    .then((result) => {
      const {
        Discount,
      } = result;

      if (Discount) return checkDiscount(Discount, order, Entity, __typename);
      return Promise.reject();
    });
};


const checkDiscounts = (order, Entity, api) => {
  const { discounts } = order;

  if (!discounts) return Promise.resolve([null, null]);

  const discountsIds = discounts
    .map(({ id }) => id)
    .filter(id => id);

  if (discountsIds.length === 0) return Promise.resolve([null, null]);

  return api.request(`
    query {
      allDiscounts (
        filter: {
          id_in: ["${discountsIds.join('","')}"]
        }
      ) {
        id
        public
        title
        rate
        validityPeriodFrom
        validityPeriodTo
        unused
        allProducts
        certificateProducts {
          id
        }
        subscriptionProducts {
          id
        }
        __typename
      }
    }
  `)
    .then((result) => {
      const { allDiscounts } = result;

      const checkResults = allDiscounts
        .map(Discount => checkDiscount(Discount, order, Entity, 'Discount'));

      const Discounts = checkResults
        .map(checkResult => checkResult[0])
        .filter(item => item);

      const DiscountsWithErrors = checkResults
        .map(checkResult => checkResult[1])
        .filter(item => item);

      if (DiscountsWithErrors && DiscountsWithErrors.length === 0) return [Discounts, null];

      return [Discounts, DiscountsWithErrors];
    });
};

// заказ без сертификатов и скидок
const calcSimple = (order, Entity) => {
  return Entity.price;
};

// скидка

const calcDiscount = (Entity, Discount) => {
  const {
    id: productId,
    __typename: productTypename,
  } = Entity;

  const {
    rate,
    allProducts,
  } = Discount;


  if (allProducts) return rate;
  if (
    productTypename === 'CertificateProduct'
  ) {
    certificateProductsIds = Discount.certificateProducts.map(({ id }) => id)
      .filter(id => id === productId);

    if (certificateProductsIds.length > 0) return rate;
  }

  if (
    productTypename === 'SubscriptionProduct'
  ) {
    subscriptionProductsIds = Discount.subscriptionProducts.map(({ id }) => id)
      .filter(id => id === productId);

    if (subscriptionProductsIds.length > 0) return rate;
  }
  return 0;
};


const calcPromocode = (Entity, Promocode) => {
  return calcDiscount(Entity, Promocode);
};

const calcDiscounts = (Entity, Discounts) => {
  return Discounts
    .map(Discount => calcDiscount(Entity, Discount))
    .reduce((prev, curr) => prev + curr);
};

const calcOrder = (order, { Entity, Promocode, Discounts }) => {
  let orderPrice = 0;
  let discountsPrice = 0;
  let discountsRate = 0;
  let totalPrice = 0;

  orderPrice += calcSimple(order, Entity);

  // // скидки
  if (Discounts && Discounts.length > 0) {
    discountsRate += calcDiscounts(Entity, Discounts);
  }

  //
  // // промокод
  if (discountsRate < 100 && Promocode) {
    discountsRate += calcPromocode(Entity, Promocode);
  }
  //
  if (discountsRate > 100) discountsRate = 100;
  discountsPrice = (orderPrice * discountsRate) / 100;
  totalPrice = orderPrice - discountsPrice;

  return {
    orderPrice,
    discountsPrice,
    discountsRate,
    totalPrice,
  };
};

module.exports = (order, api) => {
  return checkEntity(order, api)
    .then((result) => {
      const [
        Entity,
        EntityNoPublic,
      ] = result;

      if (!Entity || EntityNoPublic) return Promise.reject(new Error('Not found or unpublished'));


      return Promise.all([
        checkPromocode(order, Entity, api),
        checkDiscounts(order, Entity, api),
      ])
        .then((checkResult) => {
          // return Promise.resolve({ resultOfChecking: checkResult, resultOfCalcOrder: checkResult });
          const [
            [
              Promocode,
              PromocodeWithErrors,
            ],
            [
              Discounts,
              DiscountsWithErrors,
            ],
          ] = checkResult;
          const {
            promocode,
            discounts,
            additionalDiscounts,
          } = order;

          if (
            (promocode && !Promocode)
            || (discounts && discounts.length > 0
              && (!Discounts || Discounts.length === 0))
            || (additionalDiscounts && additionalDiscounts.length > 0
              && (!Discounts || Discounts.length === 0))
          ) return Promise.reject(new Error('Not found'));

          //
          const resultOfChecking = {
            EntityNoPublic,
            PromocodeWithErrors,
            DiscountsWithErrors,
          };

          //
          if (
            EntityNoPublic
            || PromocodeWithErrors
            || DiscountsWithErrors
          ) return Promise.resolve({ resultOfChecking, resultOfCalcOrder: null });
          //
          // return Promise.resolve({ resultOfChecking: checkResult, resultOfCalcOrder: checkResult });
          const resultOfCalcOrder = calcOrder(order, { Entity, Promocode, Discounts });

          return Promise.resolve({ resultOfChecking, resultOfCalcOrder });
        });
    });
};
