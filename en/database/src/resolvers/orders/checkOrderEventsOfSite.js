const { fromEvent } = require('graphcool-lib');

const getProductById = (order, api) => {
  const { product: { id } } = order;
  return api.request(`
    query {
      Course (id: "${id}") {
        id
        title
        public
        price
        tags {
          id
        }
        lectures (filter: { public: true }) {
          id
        }
        __typename
      }
      Cycle (id: "${id}") {
        id
        title
        public
        price
        tags {
          id
        }
        lectures (filter: { public: true }) {
          id
        }
        __typename
      }
      Lecture (id: "${id}") {
        id
        title
        public
        tags {
          id
        }
        __typename
      }
    }
  `);
};

const checkEntity = (order, api) => {
  return getProductById(order, api)
    .then((data) => {
      const { Course, Cycle, Lecture } = data;
      if (Course) return [Course, !Course.public ? Course : null];
      if (Cycle) return [Cycle, !Cycle.public ? Cycle : null];
      if (Lecture) return [Lecture, !Lecture.public ? Lecture : null];
      return [null, null];
    });
};

const allEventsQuery = (order, api) => {
  const { lectures } = order;
  const eventsIds = Object.keys(lectures).map(id => lectures[id].eventId);

  if (eventsIds.length === 0) {
    return null;
  }
  return api.request(`
    query {
      allEvents(
        filter: {
          id_in: ["${eventsIds.join('","')}"],
        }
      ) {
        id
        date
        price
        quantityOfTicketsAvailable
        lecture {
          id
          public
          title
        }
      }
    }
  `)
    .then(data => data.allEvents);
};

const checkTickets = (order, Entity, api) => {
  const { lectures, participants } = order;
  const { id: productId } = Entity;
  const lecturesIds = Object.keys(lectures);

  if (
    Entity.__typename === 'Lecture'
    && (
      !Entity.public
      || lecturesIds.length > 1
      || lecturesIds[0] !== productId
    )
  ) return [null, null];

  if (Entity.__typename === 'Course' || Entity.__typename === 'Cycle') {
    const { lectures: lecturesOfEntity } = Entity;
    const lecturesOfEntityIds = lecturesOfEntity.map(({ id }) => id);
    const lecturesIdsFiltered = lecturesIds.filter(id => lecturesOfEntityIds.includes(id));
    if (lecturesIdsFiltered.length !== lecturesIds.length) return [null, null];
  }

  const eventsIds = lecturesIds.map(id => lectures[id].eventId);
  if (eventsIds.length === 0) {
    return [null, null];
  }
  return allEventsQuery(order, api)
    .then((allEvents) => {
      if (!allEvents) return [null, null];

      const allEventsNoTicketsAvailable = allEvents
        .filter(({ quantityOfTicketsAvailable, lecture }) => quantityOfTicketsAvailable < participants.length || !lecture.public);

      return [allEvents, allEventsNoTicketsAvailable.length > 0 ? allEventsNoTicketsAvailable : null];
    });
};

const checkCertificate = (Certificate, order, __typename) => {
  if (
    Certificate.__typename !== __typename
    || Certificate.unusedLectures === 0
    || new Date(Certificate.start) > new Date()
    || new Date(Certificate.end) < new Date()
  ) return [Certificate, Certificate];

  return [Certificate, null];
};

const checkSubscription = (Subscription, order, __typename) => {
  if (
    Subscription.__typename !== __typename
    || Subscription.unusedLectures === 0
    || new Date(Subscription.start) > new Date()
    || new Date(Subscription.end) < new Date()
  ) return [Subscription, Subscription];

  return [Subscription, null];
};

const checkDiscount = (Discount, order, Entity, __typename) => {
  const {
    product,
    lectures,
    participants,
  } = order;

  const {
    id: productId,
    tags: productTags,
    lectures: productLectures,
    __typename: productTypename,
  } = Entity;

  const lecturesIdsOrder = Object.keys(lectures);

  if (
    Discount.__typename !== __typename
    || !Discount.public
    || Discount.unused === 0
    || new Date(Discount.validityPeriodFrom) > new Date()
    || new Date(Discount.validityPeriodTo) < new Date()
    || Discount.buyingLecturesCount > lecturesIdsOrder.length
    || Discount.participantsCount > participants.length
  ) return [Discount, Discount];

  if (Discount.allProducts) return [Discount, null];

  const productTagsIds = productTags.map(({ id }) => id);

  const tagsIds = Discount.tags
    .map(({ id }) => id)
    .filter(id => productTagsIds.includes(id));
  if (tagsIds.length > 0) return [Discount, null];

  let lecturesIds = Discount.lectures.map(({ id }) => id);
  let coursesIds = Discount.courses.map(({ id }) => id);
  let cyclesIds = Discount.cycles.map(({ id }) => id);

  if (
    productTypename === 'Course'
    || productTypename === 'Cycle'
    // || Object.keys(lectures).length === productLectures.length
  ) {
    coursesIds = coursesIds
      .filter(id => id === productId);
    cyclesIds = cyclesIds
      .filter(id => id === productId);
    lecturesIds = lecturesIds
      .filter(id => lecturesIdsOrder.includes(id));

    if (coursesIds.length > 0 || cyclesIds.length > 0 || lecturesIds.length > 0) return [Discount, null];
  } else {
    lecturesIds = lecturesIds
      .filter(id => id === productId);
    if (lecturesIds.length > 0) return [Discount, null];
  }

  return [Discount, Discount];
};

const checkPromocode = (order, Entity, api) => {
  const {
    promocode,
  } = order;

  if (!promocode) return [null, null];

  const {
    id,
    __typename,
  } = promocode;

  if (!id || !__typename) return [null, null];

  return api.request(`
    query {
      Certificate (
        id: "${id}"
      ) {
        id
        title
        unusedLectures
        personsCount
        start
        end
        __typename
      }
      Subscription (
        id: "${id}"
      ) {
        id
        title
        unusedLectures
        personsCount
        start
        end
        __typename
      }
      Discount(
        id: "${id}"
      ) {
        id
        public
        title
        rate
        validityPeriodFrom
        validityPeriodTo
        unused
        buyingLecturesCount
        participantsCount
        allProducts
        tags {
          id
        }
        lectures {
          id
        }
        courses {
          id
        }
        cycles {
          id
        }
        __typename
      }
    }
  `)
    .then((result) => {
      const {
        Certificate,
        Subscription,
        Discount,
      } = result;

      if (Certificate) return checkCertificate(Certificate, order, __typename);

      if (Subscription) return checkSubscription(Subscription, order, __typename);

      if (Discount) return checkDiscount(Discount, order, Entity, __typename);
    });
};


const checkDiscounts = (order, Entity, api) => {
  const { discounts } = order;
  const discountsIds = discounts
    .map(({ id }) => id)
    .filter(id => id);

  if (discounts.length === 0 && discountsIds.length === 0) return [null, null];

  return api.request(`
    query {
      allDiscounts (
        filter: {
          id_in: ["${discountsIds.join('","')}"]
        }
      ) {
        id
        public
        title
        rate
        validityPeriodFrom
        validityPeriodTo
        unused
        buyingLecturesCount
        participantsCount
        allProducts
        tags {
          id
        }
        lectures {
          id
        }
        courses {
          id
        }
        cycles {
          id
        }
        __typename
      }
    }

  `)
    .then((result) => {
      const { allDiscounts } = result;

      const checkResults = allDiscounts
        .map(Discount => checkDiscount(Discount, order, Entity, 'Discount'));

      const Discounts = checkResults
        .map(checkResult => checkResult[0])
        .filter(item => item);

      const DiscountsWithErrors = checkResults
        .map(checkResult => checkResult[1])
        .filter(item => item);

      return [
        Discounts.length > 0 ? Discounts : null,
        DiscountsWithErrors.length > 0 ? DiscountsWithErrors : null,
      ];
    });
};

const buyFull = (order, Entity) => {
  const { lectures } = order;
  const lecturesIds = Object.keys(lectures);
  if (
    Entity.__typename === 'Course'
    || Entity.__typename === 'Cycle'
    && (
      Entity.lectures.length === lecturesIds.length
    )
  ) {
    return true;
  }
  return false;
};

const priceOfEvent = (lectureId, lectures, allEvents) => {
  const { eventId } = lectures[lectureId];
  const [{ price }] = allEvents.filter(({ id }) => id === eventId);
  return price;
};

// заказ без сертификатов и скидок
const calcSimple = (order, Entity, allEvents) => {
  const { lectures } = order;
  const lecturesIds = Object.keys(lectures);
  const isBuyFull = buyFull(order, Entity);
  let orderPrice = 0;

  if (isBuyFull) {
    // покупка полного курса, лекции
    orderPrice = Entity.price;
  } else {
    orderPrice = lecturesIds
      .map(id => priceOfEvent(id, lectures, allEvents))
      .reduce((prev, curr) => prev + curr);
  }

  return orderPrice;
};

// сертификат
const calcCertAndSubBuyProduct = (order, orderPrice, Entity, allEvents, Promocode) => {
  const {
    lectures,
    participants,
  } = order;
  const {
    rate,
    __typename,
    unusedLectures,
    personsCount,
  } = Promocode;

  let cerDiscountPrice = 0;
  let lecturesOfPromocode = 0;

  const lecturesIds = Object.keys(lectures);
  const lecturesLength = lecturesIds.length;
  const participantsLength = participants.lenght;
  const lecturesOfParticipiants = lecturesLength * participantsLength;
  const pricePartials = Entity.price / lecturesIds.length;

  if (unusedLectures === -1) {
    // безлимит
    cerDiscountPrice = Entity.price;
    cerDiscountPrice *= participantsLength > personsCount ? personsCount : participantsLength;
  } else if (lecturesLength < unusedLectures) {
    // остаток лекций больше чем лекций в заказе
    // остаток больше чем лекций * количество участников
    if (unusedLectures > lecturesOfParticipiants) {
      cerDiscountPrice = Entity.price * participantsLength;
      lecturesOfPromocode = lecturesOfParticipiants;
    } else {
      // остаток меньше чем лекций * количество участников
      cerDiscountPrice = pricePartials * unusedLectures;
      lecturesOfPromocode = unusedLectures;
    }
  } else {
    // лекций в заказе больше чем остаток
    cerDiscountPrice = pricePartials * unusedLectures;
    lecturesOfPromocode = unusedLectures;
  }
  return { cerDiscountPrice, lecturesOfPromocode };
};

const calcInWhile = (lecturesIds, personsCount, unusedLectures, order, allEvents) => {
  const { lectures } = order;
  let cerDiscountPrice = 0;
  let personsCountTmp = personsCount;
  let unusedLecturesCountTmp = unusedLectures;
  while (personsCountTmp > 0) {
    personsCountTmp -= 1;
    cerDiscountPrice += lecturesIds
      .map((id) => {
        if (unusedLecturesCountTmp === 0) return 0;
        unusedLecturesCountTmp -= 1;
        return priceOfEvent(id, lectures, allEvents);
      })
      .reduce((prev, curr) => prev + curr);
  }
  return cerDiscountPrice;
};

const calcCertAndSubBuyLectures = (order, orderPrice, Entity, allEvents, Promocode) => {
  const { lectures, participants } = order;
  const {
    rate,
    __typename,
    unusedLectures,
    personsCount,
  } = Promocode;

  let cerDiscountPrice = 0;
  let lecturesOfPromocode = 0;

  const lecturesIds = Object.keys(lectures);
  const lecturesLength = lecturesIds.length;
  const participantsLength = participants.lenght;
  const lecturesOfParticipiants = lecturesLength * participantsLength;

  if (unusedLectures === -1) {
    // безлимит
    cerDiscountPrice = orderPrice;
    cerDiscountPrice *= participantsLength > personsCount ? personsCount : participantsLength;
  } else if (lecturesLength < unusedLectures) {
    // остаток лекций больше чем лекций в заказе
    // остаток больше чем лекций * количество участников
    if (unusedLectures > lecturesOfParticipiants) {
      cerDiscountPrice = orderPrice * participantsLength;
      lecturesOfPromocode = lecturesOfParticipiants;
    } else {
      // остаток меньше чем лекций * количество участников
      cerDiscountPrice = calcInWhile(lecturesIds, personsCount, unusedLectures, order, allEvents);
      lecturesOfPromocode = unusedLectures;
    }
  } else {
    // лекций в заказе больше чем остаток
    cerDiscountPrice = calcInWhile(lecturesIds, personsCount, unusedLectures, order, allEvents);
    lecturesOfPromocode = unusedLectures;
  }
  return { cerDiscountPrice, lecturesOfPromocode };
};

const calcCertificateAndSubscription = (order, orderPrice, Entity, allEvents, Promocode) => {
  const isBuyFull = buyFull(order, Entity);
  if (isBuyFull) return calcCertAndSubBuyProduct(order, orderPrice, Entity, allEvents, Promocode);
  return calcCertAndSubBuyLectures(order, orderPrice, Entity, allEvents, Promocode);
};

// скидка

const findRate = (Discount, Entity) => {
  const {
    rate,
    allProducts,
  } = Discount;
  if (allProducts) return rate;
  const tagsIdsDiscount = Discount.tags.map(({ id }) => id);
  const tagsIdsEntity = Entity.tags.map(({ id }) => id);
  const tagsIdsDiscountFiltered = tagsIdsDiscount.filter((id) => tagsIdsEntity.includes(id));

  if (tagsIdsDiscountFiltered.length > 0) return rate;
  return 0;
};

const calcDiscountByProduct = (order, orderPrice, Entity, allEvents, Discount) => {
  const {
    rate,
  } = Discount;
  const resRate = findRate(Discount, Entity);
  if (resRate > 0) return resRate;
  // const cyclesIdsDiscount = Discount.cycles.map(({ id }) => id);
  // const coursesIdsDiscount = Discount.courses.map(({ id }) => id);
  // if (cyclesIdsDiscount.includes(Entity.id)) return rate;
  // if (coursesIdsDiscount.includes(Entity.id)) return rate;
  // return 0;
  return rate;
};

const calcDiscountByLectures = (order, orderPrice, Entity, allEvents, Discount) => {
  const { lectures, participants } = order;
  const {
    rate,
  } = Discount;

  const resRate = findRate(Discount, Entity);
  if (resRate > 0) return resRate;

  const lecturesIdsDiscount = Discount.lectures.map(({ id }) => id);
  const lecturesIds = Object.keys(lectures);
  const lectureId = lecturesIdsDiscount.find(id => lecturesIds.includes(id));
  if (!lectureId) return 0;
  const priceOfLecture = priceOfEvent(lectureId, lectures, allEvents);
  const priceRateOfLecture = (priceOfLecture * rate) / 100;
  const rateOfLectureToOrder = (priceRateOfLecture * 100) / orderPrice;
  return rateOfLectureToOrder * participants.length;
};

const calcDiscount = (order, orderPrice, Entity, allEvents, Discount) => {
  const isBuyFull = buyFull(order, Entity);
  if (isBuyFull) return calcDiscountByProduct(order, orderPrice, Entity, allEvents, Discount);
  return calcDiscountByLectures(order, orderPrice, Entity, allEvents, Discount);
};

const calcDiscounts = (order, orderPrice, Entity, allEvents, Discounts) => {
  const isBuyFull = buyFull(order, Entity);
  if (isBuyFull) return calcDiscountByProduct(order, orderPrice, Entity, allEvents, Discounts[0]);
  return Discounts
    .map(Discount => calcDiscountByLectures(order, orderPrice, Entity, allEvents, Discount))
    .reduce((prev, curr) => prev + curr);
};

// заказ с промокодом
const calcPromocode = (order, orderPrice, Entity, allEvents, Promocode) => {
  const { participants } = order;
  let discountsRate = 0;
  let lecturesOfPromocode = 0;
  if (!Promocode) return discountsRate;
  const {
    __typename,
  } = Promocode;

  if (__typename === 'Certificate' || __typename === 'Subscription') {
    const resultCert = calcCertificateAndSubscription(order, orderPrice, Entity, allEvents, Promocode);
    lecturesOfPromocode = resultCert.lecturesOfPromocode;
    discountsRate = (resultCert.cerDiscountPrice * 100) / (orderPrice * participants.length);
  }
  if (__typename === 'Discount') {
    discountsRate = calcDiscount(order, orderPrice, Entity, allEvents, Promocode);
  }

  return {
    discountsRate,
    lecturesOfPromocode,
  };
};


// заказ с скидкой

const calcOrder = (order, { Entity, allEvents, Promocode, Discounts }) => {
  const {
    participants,
    events,
    lectures,
    certificate,
    subscription,
    promocode,
    discounts,
    discountsOfLectures,
    tickets,
    product,
    // order: {
    //   orderPrice,
    //   discountsPrice,
    //   discountsRate,
    //   totalPrice,
    // },
  } = order;

  const participantsLength = participants.length;
  const lecturesIds = Object.keys(lectures);

  let orderPrice = 0;
  let discountsPrice = 0;
  let discountsRate = 0;
  let totalPrice = 0;
  let lecturesOfPromocode = 0;

  orderPrice += calcSimple(order, Entity, allEvents);

  // скидки
  if (Discounts) {
    discountsRate += calcDiscounts(order, orderPrice, Entity, allEvents, Discounts);
  }

  // промокод
  if (discountsRate < 100 && Promocode) {
    const resultPromocode = calcPromocode(order, orderPrice, Entity, allEvents, Promocode);
    discountsRate += resultPromocode.discountsRate;
    lecturesOfPromocode = resultPromocode.lecturesOfPromocode;
  }

  orderPrice *= participantsLength;
  if (discountsRate > 100) discountsRate = 100;
  discountsPrice = (orderPrice * discountsRate) / 100;
  totalPrice = orderPrice - discountsPrice;

  return {
    orderPrice,
    discountsPrice,
    discountsRate,
    totalPrice,
    lecturesOfPromocode,
  };
};

module.exports = (order, api) => {
  return checkEntity(order, api)
    .then((result) => {
      const [
        Entity,
        EntityNoPublic,
      ] = result;

      if (!Entity || EntityNoPublic) return Promise.reject(new Error('Not found or unpublished'));

      return Promise.all([
        // проверка наличия билетов
        checkTickets(order, Entity, api),
        // проверка промокода
        checkPromocode(order, Entity, api),
        // проверка скидок
        checkDiscounts(order, Entity, api),
      ])
        .then((checkResult) => {
          const [
            [
              allEvents,
              allEventsNoTicketsAvailable,
            ],
            [
              Promocode,
              PromocodeWithErrors,
            ],
            [
              Discounts,
              DiscountsWithErrors,
            ],
          ] = checkResult;

          const {
            discounts,
            promocode,
          } = order;

          if (
            !allEvents || allEvents.length === 0
            || (promocode && !Promocode)
            || (discounts && discounts.length > 0 && (!Discounts || Discounts.length === 0))
          ) {
            return Promise.reject(new Error('Not found'));
          }

          const resultOfChecking = {
            EntityNoPublic,
            allEventsNoTicketsAvailable,
            PromocodeWithErrors,
            DiscountsWithErrors,
          };

          if (
            EntityNoPublic
            || allEventsNoTicketsAvailable
            || PromocodeWithErrors
            || DiscountsWithErrors
          ) return { resultOfChecking, resultOfCalcOrder: null };

          const resultOfCalcOrder = calcOrder(order, { Entity, allEvents, Promocode, Discounts });
          return Promise.resolve({ resultOfChecking, resultOfCalcOrder });
        });
    });
};
