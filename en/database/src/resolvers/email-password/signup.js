const { fromEvent } = require('graphcool-lib');
const bcrypt = require('bcryptjs');

const config = require('./../../config');
const createGraphcoolUser = require('./lib/createGraphcoolUser');
const getGraphcoolUser = require('./lib/getGraphcoolUser');
const validate = require('./lib/validate');

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!')
    return { error: 'Email Signup not configured correctly.' };
  }
  const { data } = event;
  const {
    password,
    email,
    firstName,
    lastName,
    phone = null,
  } = data;

  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');
  const salt = bcrypt.genSaltSync(config.SALT_ROUNDS);
  const errorsValidate = validate(data);
  if (errorsValidate.length !== 0) return { error: `Not a valid input data ${errorsValidate.join(', ')}` };

  let user = null;

  return getGraphcoolUser(api, email)
    .then((graphcoolUser) => {
      if (graphcoolUser) {
        return Promise.reject(new Error('Email already in use'));
      }

      return bcrypt.hash(password, salt);
    })
    .then((hash) => {
      return createGraphcoolUser(api, {
        email,
        firstName,
        lastName,
        phone,
        hash,
      });
    })
    .then((graphcoolUser) => {
      user = graphcoolUser;
      return graphcool.generateAuthToken(graphcoolUser.id, 'User');
    })
    .then((token) => {
      return { data: Object.assign({}, user, { token }) };
    })
    .catch((error) => {
      console.error(error);
      if (error.message === 'Email already in use') return { error: error.message };
      // don't expose error message to client!
      return { error: 'An unexpected error occured.' };
    });
};
