module.exports = (api, {
  email,
  hash,
  firstName,
  lastName,
  phone,
}) => (
  api.request(`
    mutation {
      createUser(
        email: "${email}",
        password: "${hash}",
        firstName: "${firstName}",
        lastName: "${lastName}",
        phone: "${phone}",
      ) {
        id
        firstName
        lastName
        email
      }
    }`)
    .then((userMutationResult) => {
      return userMutationResult.createUser;
    })
);
