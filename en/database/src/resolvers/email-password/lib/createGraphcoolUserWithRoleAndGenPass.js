module.exports = (api, {
  email,
  hash,
  firstName,
  lastName,
  phone,
  role,
}) => (
  api.request(`
    mutation {
      createUser(
        email: "${email}",
        password: "${hash}",
        firstName: "${firstName}",
        lastName: "${lastName}",
        phone: "${phone}",
        role: ${role},
        generatedPassword: ${true},
      ) {
        id
        createdAt
        updatedAt

        email
        firstName
        lastName
        phone
        role
      }
    }`)
    .then((userMutationResult) => {
      return userMutationResult.createUser;
    })
);
