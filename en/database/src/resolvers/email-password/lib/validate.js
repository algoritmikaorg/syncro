const validator = require('validator');
const locale = 'ru-RU';
const userRoles = ['CLIENT', 'ADMIN', 'MODERATOR', 'CURATOR'];
const hasOwnProperty = (obj, property) => Object.prototype.hasOwnProperty.call(obj, property);

module.exports = (input) => {
  const {
    email,
    firstName,
    lastName,
    password,
    phone,
    role,
  } = input;
  const errors = [];

  if (hasOwnProperty(input, 'email') && (validator.isEmpty(email) || !validator.isEmail(email))) {
    errors.push('email');
  }

  if (hasOwnProperty(input, 'password') && (validator.isEmpty(password) || !validator.isLength(password, { min: 8 }))) {
    errors.push('password');
  }


  if (hasOwnProperty(input, 'phone') && (validator.isEmpty(phone) || !validator.isMobilePhone(phone, locale))) {
    errors.push('phone');
  }

  if (hasOwnProperty(input, 'firstName') && (validator.isEmpty(firstName) || !validator.isAlpha(firstName, locale))) {
    errors.push('firstName');
  }

  if (hasOwnProperty(input, 'lastName') && (validator.isEmpty(lastName) || !validator.isAlpha(lastName, locale))) {
    errors.push('lastName');
  }

  if (hasOwnProperty(input, 'role') && (validator.isEmpty(role) || userRoles.indexOf(role) === -1)) {
    errors.push('role');
  }

  return errors;
};
