module.exports = (api, email) => (
  api.request(`
    query {
      User(email: "${email}"){
        id
      }
    }`)
    .then((userQueryResult) => {
      return userQueryResult.User;
    })
    .catch((error) => {
      // Log error but don't expose to caller
      console.log(error);
      return { error: 'An unexpected error occured' };
    })
);
