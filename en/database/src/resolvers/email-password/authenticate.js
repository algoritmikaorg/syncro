const { fromEvent } = require('graphcool-lib');
const bcrypt = require('bcryptjs');

const getGraphcoolUser = require('./lib/getGraphcoolUser');

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!');
    return { error: 'Email Authentication not configured correctly.' };
  }

  const { email, password } = event.data;
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');
  let user = null;

  return getGraphcoolUser(api, email)
    .then((graphcoolUser) => {
      if (graphcoolUser === null) {
        // returning same generic error so user can't find out what emails are registered.
        return Promise.reject(new Error('Invalid Credentials(email)'));
      }
      user = graphcoolUser;

      return bcrypt.compare(password, graphcoolUser.password)
        .then((passwordCorrect) => {
          if (passwordCorrect) {
            return graphcoolUser.id;
          }
          return Promise.reject(new Error('Invalid Credentials(password)'));
        });
    })
    .then((graphcoolUserId) => {
      return graphcool.generateAuthToken(graphcoolUserId, 'User');
    })
    .then((token) => {
      const { id, blocked, role } = user;
      return { data: { id, token, blocked, role } };
    })
    .catch((error) => {
      console.log(`Error: ${JSON.stringify(error)}`);

      // don't expose error message to client!
      return { error: 'An unexpected error occured' }
    });
};
