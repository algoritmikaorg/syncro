const { fromEvent } = require('graphcool-lib');

const validate = require('./lib/validate');
const getGraphcoolUser = require('./lib/getGraphcoolUser');
const createToken = require('./lib/createToken');

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!');
    return { error: 'Email Authentication not configured correctly.' };
  }
  const { data } = event;
  const { email, host } = data;
  const errorsValidate = validate(data);
  if (errorsValidate.length !== 0) return { error: `Not a valid input data ${errorsValidate.join(', ')}` };

  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');

  return getGraphcoolUser(api, email)
    .then((graphcoolUser) => {
      if (graphcoolUser === null) {
        //returning same generic error so user can't find out what emails are registered.
        return Promise.reject(new Error('Invalid Credentials(email)'));
      }

      // return { data: graphcoolUser};
      return createToken(api, graphcoolUser, host)
    })
    .then((token) => {
      return { data: token };
    })
    .catch((error) => {
      console.log(error);
      // don't expose error message to client!
      return { error: 'An unexpected error occured' }
    });
};
