const { fromEvent } = require('graphcool-lib');
const bcrypt = require('bcryptjs');
// const util = require('util');

const getGraphcoolUser = require('./lib/getGraphcoolUser');
const getUser = require('./lib/getUser');
const createGraphcoolUserWithRole = require('./lib/createGraphcoolUserWithRole');
const validate = require('./lib/validate');
const config = require('./../../config');

module.exports = (event) => {
  if (!event.context.graphcool.pat) {
    console.log('Please provide a valid root token!')
    return { error: 'Email Signup not configured correctly.' };
  }
  const { data } = event;
  const {
    password,
    email,
    role,
    firstName,
    lastName,
    phone,
  } = data;

  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');
  const salt = bcrypt.genSaltSync(config.SALT_ROUNDS);
  const errorsValidate = validate(data);
  if (errorsValidate.length !== 0) return { error: `Not a valid input data ${errorsValidate.join(', ')}` };

  if (!event.context.auth) return { error: 'No user auth' };
  const userId = event.context.auth.nodeId;

  return getUser(api, userId)
    .then((user) => {
      if (!user) return Promise.reject(new Error(`No user with id: ${userId}`));
      if (user && user.role !== 'ADMIN') return Promise.reject(new Error('No user role permission'));
      return getGraphcoolUser(api, email);
    })
    .then((graphcoolUser) => {
      if (graphcoolUser) {
        return Promise.reject(new Error('Email already in use'));
      }
      return bcrypt.hash(password, salt);
    })
    .then(hash => createGraphcoolUserWithRole(api, {
      email,
      role,
      firstName,
      lastName,
      phone,
      hash,
    }))
    .then((graphcoolUserId) => {
      return { data: { id: graphcoolUserId } };
    })
    .catch((error) => {
      console.log(error);
      // don't expose error message to client!
      return { error: 'An unexpected error occured.' };
    });
};
