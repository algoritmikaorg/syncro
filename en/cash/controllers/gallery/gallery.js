const redis = require('./../../db/redis');

const redisBatchUpdate = (data) => {
  const { type } = data;

  const commands = [];
  commands.push(
    ['set', `gallery:${type}`, JSON.stringify(data)],
  );

  return redis.batch(commands).execAsync();
};

const redisBatchSet = (data) => {
  const { type } = data;
  return redis.batch([
    ['set', `gallery:${type}`, JSON.stringify(data)],
  ]).execAsync();
};

const redisBatchDelete = (previousValues) => {
  const { type } = previousValues;
  return redis.batch([
    ['del', `gallery:${type}`],
  ]).execAsync();
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchSet(node);
    }
    case 'UPDATED': {
      return redisBatchUpdate(node);
    }
    case 'DELETED': {
      return redisBatchDelete(previousValues);
    }
    default: {
      return redisBatchSet(node);
    }
  }
};
