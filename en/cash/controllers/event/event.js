const redis = require('./../../db/redis');

const redisBatchUpdate = (data) => {
  return Promise.resolve('OK');
};

const redisBatchSet = (data) => {
  return Promise.resolve('OK');
};

const redisBatchDelete = (previousValues) => {
  return Promise.resolve('OK');
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchSet(node);
    }
    case 'UPDATED': {
      return redisBatchUpdate(node);
    }
    case 'DELETED': {
      return redisBatchDelete(previousValues);
    }
    default: {
      return redisBatchSet(node);
    }
  }
};
