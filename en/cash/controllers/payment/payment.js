const redis = require('./../../db/redis');

const redisBatchPaymentUpdate = (prev, next) => {
  const { id, data, status } = next;
  if (status === 'PAID') return Promise.resolve();
  if (data) data.paymentId = id;
  return redis.setAsync(`payment-data:${id}`, JSON.stringify(data));
};

const redisBatchPaymentSet = ({ id, data, status }) => {
  if (status === 'PAID') return Promise.resolve();
  if (data) data.paymentId = id;
  return redis.setAsync(`payment-data:${id}`, JSON.stringify(data));
};

const redisBatchPaymentDelete = (previousValues) => {
  const { id } = previousValues;
  return redis.delAsync(`payment-data:${id}`);
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchPaymentSet(node);
    }
    case 'UPDATED': {
      return redisBatchPaymentUpdate(previousValues, node);
    }
    case 'DELETED': {
      return redisBatchPaymentDelete(previousValues);
    }
    default: {
      if (node) return redisBatchPaymentSet(node);
      return Promise.reject();
    }
  }
};
