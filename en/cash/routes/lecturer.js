const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/lecturer');

/* GET home page. */
router.post('/', (req, res, next) => {
  if (!req.body.data || !req.body.data.Lecturer) return next(new Error('Wrond Lecturer data'));
  const { data: { Lecturer: { node, mutation, previousValues } } } = req.body;

  controllerAsync(mutation, previousValues, node)
    .then((result) => {
      return res.json({ data: { status: 'OK' }, error: null });
    })
    .catch(next);
});

module.exports = router;
