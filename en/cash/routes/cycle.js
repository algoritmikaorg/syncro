const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/cycle');

/* GET home page. */
router.post('/', (req, res, next) => {
  if (!req.body.data || !req.body.data.Cycle) return next(new Error('Wrond Cycle data'));
  const { data: { Cycle: { node, mutation, previousValues } } } = req.body;

  controllerAsync(mutation, previousValues, node)
    .then((result) => {
      return res.json({ data: { status: 'OK' }, error: null });
    })
    .catch(next);
});

module.exports = router;
