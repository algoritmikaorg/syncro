const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/tag');

/* GET home page. */
router.post('/', (req, res, next) => {
  if (!req.body.data || !req.body.data.Tag) return next(new Error('Wrond Tag data'));
  const { data: { Tag: { node, mutation, previousValues } } } = req.body;

  controllerAsync(mutation, previousValues, node)
    .then((result) => {
      return res.json({ data: { status: 'OK' }, error: null });
    })
    .catch(next);
});

module.exports = router;
