const sharp = require('sharp');
const request = require('request');
const async = require('async');
const fs = require('fs-extra');

const { getConfig } = require('./../utils');

const {
  PROJECT_ID,
  FILE_API_ENDPOINT,
  CONTENT_LENGTH,
  DIR,
} = require('./../config');

module.exports = (req, res, next) => {
  const { params: { secret } } = req;

  const {
    crop,
    resize,
    filename,
    filenameForSave,
    filesLimitReached,
    fileAlreadyCreated,
  } = res.locals;

  // async.waterfall([
  //
  // ], (err, result) => {
  //
  // });

  if (!fileAlreadyCreated && filesLimitReached) {
    const error = new Error('Limit of the number of files reached');
    error.status = 400;
    next(error);
  } else if (!fileAlreadyCreated && !filesLimitReached) {

    request.get({ url: `${FILE_API_ENDPOINT}/${secret}`, encoding: null }, (err, response, body) => {
      if (err) {
        const error = new Error(err.toString());
        error.status = 400;
        next(error);
        return null;
      }

      if (response.statusCode !== 200) {
        res.status(response.statusCode).send(body);
        return null;
      }

      const stream = sharp(body);

      try {
        const imageOutputConfig = getConfig({ resize, crop: crop && resize ? `${crop}:${resize}` : undefined });
        stream.limitInputPixels(false);

        if (imageOutputConfig.crop) {
          stream
            .resize(imageOutputConfig.resize.width, imageOutputConfig.resize.height)
            .crop(sharp.strategy.entropy);
        }

        if (imageOutputConfig.resize) {
          stream.resize(imageOutputConfig.resize.width, imageOutputConfig.resize.height);
          if (imageOutputConfig.resize.force) {
            stream.ignoreAspectRatio();
          }
        }

        async.series([
          (callback) => {
            fs.ensureDir(`${DIR}/${PROJECT_ID}/${secret}`, callback);
          },
          (callback) => {
            stream
              .toFile(`${DIR}/${PROJECT_ID}/${secret}/${filenameForSave}`, callback);
          },
        ], (err, result) => {
          if (err) {
            console.error(err);
            next(err);
            return null;
          }
          const { files } = req.app.locals;
          if (!files[PROJECT_ID]) files[PROJECT_ID] = {};
          if (!files[PROJECT_ID][secret]) files[PROJECT_ID][secret] = {};
          files[PROJECT_ID][secret][filenameForSave] = true;
          next();
        });

      } catch (err1) {
        console.error(err1);
        const error = new Error(err1.toString());
        error.status = 400;
        next(error);
      }
    });
  } else {
    next();
  }
};
