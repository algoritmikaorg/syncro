const checkParams = require('./checkParams');
const reqHead = require('./reqHead');
const transformImage = require('./transformImage');
const replaceReqUrl = require('./replaceReqUrl');
const checkFileAndFileLimit = require('./checkFileAndFileLimit');

module.exports = [checkParams, reqHead, checkFileAndFileLimit, transformImage, replaceReqUrl];
