const { checkParamForTransform, getConfig, parseParams } = require('./../utils');

module.exports = (req, res, next) => {
  const { params } = req;
  const {
    param1,
    param2,
    param3,
  } = params;
  let crop;
  let resize;
  let filename;

  if (param1 && param2 && param3) {
    if (checkParamForTransform(param1)) crop = param1;
    if (checkParamForTransform(param2)) resize = param2;
    filename = param3;
  } else if (param1 && param2) {
    if (checkParamForTransform(param1)) resize = param1;
    if (checkParamForTransform(param2)) {
      crop = param1;
      resize = param2;
    } else {
      filename = param2;
    }
  } else if (param1) {
    if (checkParamForTransform(param1)) resize = param1;
  }

  const [parsingParamsError] = parseParams(crop, resize, filename);
  if (parsingParamsError) {
    parsingParamsError.status = 400;
    next(parsingParamsError);
    return null;
  }

  res.locals.crop = crop;
  res.locals.resize = resize;
  res.locals.outputFilename = filename;
  next();
};
