const checkParamForTransform = require('./checkParamForTransform');
const parserParams = require('./parserParams');
module.exports = {
  checkParamForTransform,
  ...parserParams,
};
