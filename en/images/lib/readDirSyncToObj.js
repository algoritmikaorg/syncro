const fs = require('fs');

const r = (dir, obj) => {
  const items = fs.readdirSync(dir);
  items.forEach((item) => {
    const stat = fs.statSync(`${dir}/${item}`);
    if (stat.isDirectory()) {
      obj[item] = {};
      return r(`${dir}/${item}`, obj[item]);
    }
    obj[item] = true;
  });
  return obj;
};

module.exports = r;
