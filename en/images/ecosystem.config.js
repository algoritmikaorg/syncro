module.exports = {
  apps: [
    {
      name: 'image',
      script: './bin/www',
      watch: false,
      env: {
        PORT: 60002,
        NODE_ENV: 'development',
        LOCATION_ENV: 'dev',
      },
      env_production: {
        PORT: 60002,
        NODE_ENV: 'production',
        LOCATION_ENV: 'prod',
      },
    },
  ],
};
