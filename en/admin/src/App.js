import React, { Component } from 'react';
// import 'typeface-roboto/index.css';
import Snackbar from './components/Snackbar';

import Routes from './routes';

class App extends Component {
  render() {
    return (
      <div>
        <Routes />
        <Snackbar
          setHandlerSnackbar={this.props.brokerSnackbar.setHandlerSnackbar}
        />
      </div>
    );
  }
}

export default App;
