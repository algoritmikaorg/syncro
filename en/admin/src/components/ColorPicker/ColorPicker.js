import React, { Component } from 'react'
import compose from 'recompose/compose'

import TextField from 'material-ui/TextField'

import { DEFAULT_CONVERTER, converters } from './transformers'
import PickerDialog from './PickerDialog'

class ColorPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
      showPicker: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleShowPicker = this.handleShowPicker.bind(this);
    this.handleChangePicker = this.handleChangePicker.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    this.setState({
      value,
    });
  }
  handleChange(event) {
    const value = event.target.value;
    this.setState({
      value,
    });
    this.props.onChange(event);
  }
  handleShowPicker() {
    this.setState({
      showPicker: !this.state.showPicker,
    });
  }
  handleChangePicker(c) {
    const value = converters[this.props.convert](c)
    this.setState({
      value,
    });
    this.props.onChangeNoEvents(value);
  }
  render() {
    const {
      name,
      id,
      helperText,
      label,
      className,
      onChange,
      disabled,
      required,
      error,
    } = this.props;
    const { value, showPicker } = this.state;
    return (
      <div>
        <TextField
          className={className}
          name={name}
          id={id}
          helperText={helperText}
          value={value}
          label={label}
          onClick={this.handleShowPicker}
          onChange={this.handleChange}
          type={'text'}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
        />
        {showPicker && (
          <PickerDialog
            value={value}
            onClick={this.handleShowPicker}
            onChange={this.handleChangePicker}
          />
        )}
      </div>
    );
  }
}

// ColorPicker.propTypes = {
//   value: PropTypes.string,
//   onChange: PropTypes.func,
//   convert: PropTypes.oneOf(Object.keys(converters))
// }

ColorPicker.defaultProps = {
  convert: DEFAULT_CONVERTER
}

export default ColorPicker;
