import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import EditIcon from 'material-ui-icons/Edit';
import DeleteIcon from 'material-ui-icons/Delete';
import IconButton from 'material-ui/IconButton';
import { ulid } from './../../../node_modules/ulid/dist/index.umd.js';

import columnData from './columnData';

const ulid1 = ulid();
const ulid2 = ulid();

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

const EventsList = (props) => {
  const {
    data,
    classes,
    onEdit,
    onDelete,
    showActions,
  } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            {
              showActions &&
              <TableCell padding="none" />
            }
            {
              columnData.map(({ id, label }, index) => (
                <TableCell
                  key={`${ulid1}-${id}-${index}`}
                >
                  {label}
                </TableCell>
              ))
            }
          </TableRow>
        </TableHead>
        <TableBody>
          {
            data.map((event, index) => (
              <TableRow key={index}>
                {
                  showActions &&
                    (
                      <TableCell padding="none">
                        <IconButton onClick={onEdit(index)} title="Редактировать">
                          <EditIcon />
                        </IconButton>
                        <IconButton onClick={onDelete(index)} title="Удалить">
                          <DeleteIcon />
                        </IconButton>
                      </TableCell>
                    )
                }
                {
                  columnData.map(({ id, numeric, disablePadding, format }, index) => {
                    let value = '';
                    if (format instanceof Function) {

                      if (event[id]) {
                        value = format(event[id], event);
                      } else {
                        value = event[id] || '';
                      }
                    } else if (Array.isArray(event[id])) {
                      value = event[id].join('\n');
                    } else {
                      value = event[id] || '';
                    }
                    return (
                      <TableCell
                        key={`${ulid2}-${id}-${index}`}
                        numeric={numeric}
                        padding={disablePadding ? 'none' : 'default'}
                      >
                        {value}
                      </TableCell>
                    );
                  })
                }
              </TableRow>
            ))
          }
        </TableBody>
      </Table>
    </Paper>
  );
};

EventsList.defaultProps = {
  data: [],
  showActions: true,
};

export default withStyles(styles)(EventsList);
