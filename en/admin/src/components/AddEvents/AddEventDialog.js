import React, { Component } from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

import {
  CREATE_EVENT_MUTATION,
  EDIT_EVENT_MUTATION,
  EVENT_QUERY,
  DELETE_EVENT_MUTATION,
} from './../../constants';

import gql from './gql';
import MutationMaterial from './../MutationMaterial';

import fields from './fields';

// const addValueToFields = (fields, values) => {
//   return fields.map((field) => {
//     const { id } = field;
//     if (values[id]) {
//       field.value = values[id];
//     }
//     return field;
//   });
// };

const AddEventDialog = (props) => {
  const {
    open,
    onClose,
    onSubmit,
    onSubmitMutation,
    lecturersIds,
    locationId,
    price,
    quantityOfTickets,
    lectureId,
    curatorId,
    data,
    isMutation,
  } = props;

  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let title = '';
  let currentFileds = null;

  const gqlQueryName = EVENT_QUERY;
  const gqlQueryItemName = 'Event'
  const resulMutatiomtMessageSuccses = 'Новое мероприятие создано';

  if (data && data.id) {
    gqlMutationName = EDIT_EVENT_MUTATION;
    mutationResultObjectName = 'updateEvent';
    title = 'Редактирование мероприятия';
    currentFileds = fields.update;
  } else {
    gqlMutationName = CREATE_EVENT_MUTATION;
    mutationResultObjectName = 'createEvent';
    title = 'Создание нового мероприятия';
    currentFileds = fields.create;
  }

  return (
    <div>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Можно задать лектора, локацию, цену, количество билетов для конкретной даты.
          </DialogContentText>
          <MutationMaterial
            {...props}
            fields={currentFileds}
            gql={gql}
            gqlMutationName={gqlMutationName}
            mutationResultObjectName={mutationResultObjectName}
            // gqlQueryName={gqlQueryName}
            // gqlQueryItemName={gqlQueryItemName}
            resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
            showActions={false}
            // showFormActions={false}
            dontSavePassResultTo={!isMutation ? onSubmit : null}
            onSuccessMutation={isMutation ? onSubmitMutation : null}
            mutationType="create"
            data={
              Object.assign(
                {},
                {
                  lecturersIds,
                  locationId,
                  curatorId,
                  price,
                  quantityOfTickets,
                  lectureId,
                  data: '',
                  time: '',
                },
                data)
            }
            // data={data || {
            //   lecturersIds,
            //   locationId,
            //   price,
            //   tickets,
            //   lectureId,
            // }}
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Отменить
          </Button>
          {/* <Button onClick={this.handleClickSubmit} color="primary">
            Сохранить
          </Button> */}
        </DialogActions>
      </Dialog>
    </div>
  );
}

AddEventDialog.defaultProps = {
  onClose() {

  },
  onSubmit() {

  },
  open: false,
};

export default AddEventDialog;
