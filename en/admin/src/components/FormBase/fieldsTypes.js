import * as ENUM from './../../enum';

const fieldsTypes = {
  password: {
    type: 'password',
  },
  repassword: {
    type: 'password',
  },
  role: {
    type: 'select',
    enum: ENUM.USER_ROLES,
  },
  createdAt: {
    type: 'date-from-to',
  },
  orders: {
    type: 'number-min-max',
  },
  tags: {
    type: 'tags',
  },
  anons: {
    type: 'multiline',
  },
  anonsForLecture: {
    type: 'multiline',
  },
  description: {
    type: 'multiline',
  },
  skills: {
    type: 'multiline',
  },
  specialization: {
    type: 'multiline',
  },
};

export default fieldsTypes;
