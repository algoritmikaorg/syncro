import React, { Component } from 'react';
import * as R from 'ramda';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';

import withProgress from './../../hoc/withProgress';
import Actions from './Actions';
import TextFieldList from './TextFieldList';
import AddingRelationList from './AddingRelationList';

import { validateFields } from './../../utils';
import * as ENUM from './../../enum';
import fieldsTypes from './fieldsTypes';


const styles = theme => ({
  container: theme.mixins.gutters({
    paddingBottom: 20,
    backgroundColor: 'white',
    paddingTop: 20,
  }),
  form: {
    display: 'flex',
    justifyContent: 'center',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
});

const outputFields = (state) => {
  const output = {};
  const { fields } = state;
  Object.keys(fields).forEach((key) => {
    switch (true) {
      case (key === 'tags'): {
        output[key] = state[key];
        break;
      }
      case (key === 'tags_some'): {
        if (fields[key].value) output[key] = { title: fields[key].value };
        break;
      }
      case (key === 'skills'): {
        const value = fields[key].value.replace(/,\s{1,}/g, ',');
        output[key] = value.split(',');
        break;
      }
      case Object.prototype.hasOwnProperty.call(fields[key], 'value'): {
        output[key] = fields[key].value;
        break;
      }
      case Object.prototype.hasOwnProperty.call(fields[key], 'to'): {
        output[`${key}_lte`] = fields[key].to;
        output[`${key}_gte`] = fields[key].from;
        break;
      }
      default: {
        output[key] = null;
      }
    }
  });
  return output;
};

class FormBase extends Component {
  constructor(props) {
    super(props);
    const { fields, disabledAll, disabledActions } = props;
    this.state = {
      fields: R.clone(fields),
      errors: [],
      disabledAll,
      disabledActions,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
      clearFields,
      fields: fieldsProps,
      type,
      req,
    } = nextProps;

    const fields = this.state.fields;

    if (type === 'edit' && !req) {
      Object.keys(fields).forEach((key) => {
        if (fieldsProps[key].value !== null) {
          fields[key].value = fieldsProps[key].value || '';
        }
      });
    }

    if (clearFields && clearFields.length > 0) {
      Object.keys(fields).forEach((key) => {
        if (clearFields.indexOf(key) !== -1) {
          fields[key].value = '';
        }
      });
    }
    this.setState({
      fields,
    });
  }

  handleChange = name => (event) => {
    const state = R.clone(this.state);
    if (name.indexOf('.') !== -1) {
      const fName = name.split('.');
      const { max, min, instance } = state.fields[fName[0]];
      let fValue = event.target.value;
      if (instance === Number) fValue = parseFloat(fValue);
      if (max !== undefined) {
        fValue = fValue <= max ? fValue : max;
      }
      if (min !== undefined) {
        fValue = fValue >= min ? fValue : min;
      }
      state.fields[fName[0]][fName[1]] = fValue;
    } else {
      state.fields[name].value = event.target.value;
    }

    if (state.errors.length > 0) {
      state.errors = validateFields(state);
    }

    this.setState(state, () => {
      this.props.onChange(outputFields(state));
    });
  };

  handleSubmit(event) {
    event.preventDefault();
    const state = R.clone(this.state);
    state.errors = validateFields(state.fields);
    if (state.errors.length > 0) {
      this.setState(state);
      return;
    }

    this.props.onSubmit(outputFields(state));
  }

  handleEdit(event) {
    const state = R.clone(this.state);
    state.disabledActions = {
      edit: true,
      save: false,
      cancel: false,
    };
    state.disabledAll = false;
    this.setState(state);
  }

  handleCancel() {
    const {
      fields,
    } = this.props;
    const state = R.clone(this.state);
    state.fields = R.clone(fields);
    state.errors = [];
    state.disabledAll = true;
    state.disabledActions = {
      edit: false,
      save: true,
      cancel: true,
    };
    this.setState(state);
  }

  handleReset() {
    const {
      fields,
    } = this.props;
    const state = R.clone(this.state);
    state.fields = R.clone(fields);
    state.errors = [];
    this.setState(state, this.props.onReset);
  }

  handleSelect(name, selected, titles) {
    const { fields } = this.state;
    fields[name].value = titles.join(', ');
    this.setState({
      [name]: selected,
      fields,
    });
  }

  render() {
    const {
      classes,
      formTitle,
      action,
      showActions,
      req,
      type,
      addingRelatedList,
    } = this.props;

    const {
      fields,
      disabledAll,
      disabledActions,
      errors,
    } = this.state;

    let disabledAllCur = disabledAll;
    let disabledActionsCur = disabledActions;

    if (req) {
      disabledAllCur = req;
      disabledActionsCur = R.clone(disabledActions);
      Object.keys(disabledActionsCur).forEach((key) => {
        disabledActionsCur[key] = true;
      })
    }

    return (
      <div
        className={classes.container}
      >
        {formTitle !== null && (
          <Typography className={classes.title} type="subheading" color="inherit" noWrap>
            {formTitle}
          </Typography>
        )}
        <div className={classes.form}>
          <form
            noValidate
            autoComplete="off"
            onSubmit={this.handleSubmit}
          >
            <TextFieldList
              fields={fields}
              disabledAll={disabledAllCur}
              onChange={this.handleChange}
              fieldsTypes={fieldsTypes}
              errors={errors}
            />
          </form>
        </div>
        {

        }
        {
          addingRelatedList &&
          (
            <AddingRelationList
              fields={fields}
              disabledAll={disabledAllCur}
              onSelect={this.handleSelect}
              fieldsTypes={fieldsTypes}
              errors={errors}
            />
          )
        }

        <Actions
          action={action}
          disabled={disabledActionsCur}
          show={showActions}
          onEdit={this.handleEdit}
          onCancel={this.handleCancel}
          onReset={this.handleReset}
          onSubmit={this.handleSubmit}
          type={type}
        />
      </div>
    );
  }
}

FormBase.defaultProps = {
  fields: {},
  disabledActions: {
    edit: false,
    cancel: true,
    save: true,
  },
  disabledAll: true,
  showActions: {
    edit: true,
    cancel: true,
    save: true,
  },
  // confirm: () => {},
  onChange: () => {},
  onSubmit: () => {},
  addingRelatedList: false,
};

export default withStyles(styles)(FormBase);
