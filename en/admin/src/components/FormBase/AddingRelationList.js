import React from 'react';
import { withStyles } from 'material-ui/styles';
import * as R from 'ramda';

import TextFieldItem from './TextFieldItem';
// import TagsFastSearch from './../../pages/TagsOld/TagsFastSearch';


const styles = theme => ({

});

const AddingRelationList = (props) => {
  const {
    fields,
    fieldsTypes,
    errors,
    disabledAll,
    onSelect,
  } = props;

  const fieldsKeys = Object.keys(fields);
  return (
    <div>
      {
        Object.keys(fields).map((name, index) => {
          const field = fields[name];
          const fieldType = fieldsTypes[name] ? fieldsTypes[name].type : null;
          switch (fieldType) {
            case 'tags': {
              return (
                // <TagsFastSearch
                //   key={index}
                //   onSelect={onSelect}
                //   field={R.clone(field)}
                // />
                <div></div>
              )
            }
            default: {
              return null;
            }
          }
        })
      }
    </div>
  );
};

export default withStyles(styles)(AddingRelationList);
