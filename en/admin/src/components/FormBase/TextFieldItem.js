import React from 'react';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';
import {
  FormLabel,
  FormControl,
  FormGroup,
  FormControlLabel,
  FormHelperText,
} from 'material-ui/Form';
import { withStyles } from 'material-ui/styles';
import moment from 'moment';

import CheckboxField from './fields/CheckboxField';


const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
  menu: {
    width: 200,
  },
  dateContainer: {
    display: 'inline-flex',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

const TextFieldItem = (props) => {
  const {
    classes,
    value,
    from,
    to,
    min,
    max,
    name,
    label,
    error,
    fieldType,
    required,
    disabled,
    helperText,
    onChange,
    data,
    format,
  } = props;

  let fValue = value;
  if (format instanceof Function) {
    fValue = format(value);
  }

  switch (fieldType.type) {
    case 'select': {
      return (
        <TextField
          id={`select-${name}`}
          select
          label={label}
          className={classes.textField}
          value={fValue}
          onChange={onChange(name)}
          SelectProps={{
            MenuProps: {
              className: classes.menu,
            },
          }}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
        >
          {fieldType.enum.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      );
    }
    case 'number-min-max': {
      return (
        <FormControl>
          <FormLabel component="legend">{label}</FormLabel>
          <FormGroup>
            <TextField
              id={`number-${name}-from`}
              type={'number'}
              className={classes.textField}
              value={from || ''}
              onChange={onChange(`${name}.from`)}
              margin="normal"
              disabled={disabled}
              required={required}
              error={error}
              label={'мин'}
              InputLabelProps={{
                shrink: true,
              }}
              min={0}
              max={from}
            />
            <TextField
              id={`number-${name}-to`}
              type={'number'}
              className={classes.textField}
              value={to || ''}
              onChange={onChange(`${name}.to`)}
              margin="normal"
              disabled={disabled}
              required={required}
              error={error}
              label={'макс'}
              min={to}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </FormGroup>
        </FormControl>
      );
    }
    case 'date-from-to': {
      return (
        <FormControl>
          <FormLabel component="legend">{label}</FormLabel>
          <FormGroup>
            <TextField
              id={`date-${name}-from`}
              type={'date'}
              className={classes.textField}
              value={moment(from).format('YYYY-MM-DD')}
              onChange={onChange(`${name}.from`)}
              margin="normal"
              disabled={disabled}
              required={required}
              error={error}
              label={'от'}
              InputLabelProps={{
                shrink: true,
              }}
              min={moment(min).format('YYYY-MM-DD')}
              max={moment(from).format('YYYY-MM-DD')}
            />
            <TextField
              id={`date-${name}-to`}
              type={'date'}
              className={classes.textField}
              value={moment(to).format('YYYY-MM-DD')}
              onChange={onChange(`${name}.to`)}
              margin="normal"
              disabled={disabled}
              required={required}
              error={error}
              label={'до'}
              max={moment(max).format('YYYY-MM-DD')}
              min={moment(from).format('YYYY-MM-DD')}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </FormGroup>
        </FormControl>
      );
    }
    case 'multiline': {
      return (
        <TextField
          id={name}
          label={label}
          type={fieldType.type}
          className={classes.fullWidth}
          value={fValue}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
          fullWidth
          multiline
        />
      );
    }
    case 'tags': {
      return (
        <TextField
          id={name}
          label={label}
          type={fieldType.type}
          className={classes.fullWidth}
          value={fValue}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
          fullWidth
          multiline
        />
      );
    }
    case 'number': {
      return (
        <TextField
          id={`number-${name}`}
          type={'number'}
          className={classes.textField}
          value={from || ''}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          label={label}
          InputLabelProps={{
            shrink: true,
          }}
          min={0}
        />
      );
    }
    default: {
      return (
        <TextField
          id={name}
          label={label}
          type={fieldType.type}
          className={classes.textField}
          value={fValue}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
        />
      );
    }
  }
};

TextFieldItem.defaultProps = {
  fieldType: {
    type: 'text',
  },
}

export default withStyles(styles)(TextFieldItem);
