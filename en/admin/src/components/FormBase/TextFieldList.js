import React from 'react';
import { withStyles } from 'material-ui/styles';

import TextFieldItem from './TextFieldItem';

const styles = theme => ({

});

const TextFieldList = (props) => {
  const {
    fields,
    fieldsTypes,
    errors,
    disabledAll,
    onChange,
  } = props;

  const fieldsKeys = Object.keys(fields);

  return (
    <div>
      {
        Object.keys(fields).map((name, index) => {
          const field = fields[name];
          return (
            <TextFieldItem
              key={index}
              value={field.value}
              name={name}
              error={errors.indexOf(name) !== -1}
              fieldType={fieldsTypes[name]}
              label={field.label}
              from={field.from}
              to={field.to}
              min={field.min}
              max={field.max}
              required={field.required}
              disabled={disabledAll || field.disabled}
              helperText={field.helperText}
              format={field.format}
              onChange={onChange}
            />
          );
        })
      }
    </div>
  );
};

export default withStyles(styles)(TextFieldList);
