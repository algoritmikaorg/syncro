import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import keycode from 'keycode';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import FilterListIcon from 'material-ui-icons/FilterList';
import Button from 'material-ui/Button';

import generateOutputFile from './../../libs/generateOutputFile';

import ExtTableBaseHead from './ExtTableBaseHead';
import ExtTableBaseBody from './ExtTableBaseBody';
import ExtTableBaseFooter from './ExtTableBaseFooter';
import ExtTableBaseToolbar from './ExtTableBaseToolbar';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 800,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  button: {},
  containerInner: {},
  container: {},
  form: {},
  menu: {},
  textField: {},
  fullWidth: {},
  dateContainer: {},
  tagsDemo: {},
  input: {},
});

class ExtTableBase extends React.Component {
  constructor(props, context) {
    super(props, context);
    const {
      data,
      selected,
      selectedTitle,
      rowsPerPage,
      page,
      order,
      orderBy,
    } = props;

    this.state = {
      selected,
      selectedTitle,
      data,
      page,
      rowsPerPage,
      order,
      orderBy,
    };
    this.handleGenerateOutputFile = this.handleGenerateOutputFile.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleView = this.handleView.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    // const { selected: initSelected } = nextProps;
    // let { selected } = this.state;
    // if (selected.length === 0) {
    //   selected = initSelected.slice();
    // }
    this.setState({
      data: nextProps.data,
      selected: nextProps.selected.slice(),
      selectedTitle: nextProps.selectedTitle.slice(),
    });
  }
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    let data =
      order === 'desc'
        ? this.state.data.slice().sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.data.slice().sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ data, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.data.map(n => n.id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleKeyDown = (event, n) => {
    if (keycode(event) === 'space') {
      this.handleClick(event, n);
    }
  };

  handleClick = (event, n) => {
    const { id } = n;
    let title = null;
    if (n.title) {
      title = n.title;
    } else {
      title = `${n.firstName} ${n.lastName}`;
    }
    const { selected, selectedTitle } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];
    let newSelectedTitle = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
      newSelectedTitle = newSelectedTitle.concat(selectedTitle, title);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
      newSelectedTitle = newSelectedTitle.concat(selectedTitle.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
      newSelectedTitle = newSelectedTitle.concat(selectedTitle.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
      newSelectedTitle = newSelectedTitle.concat(
        selectedTitle.slice(0, selectedIndex),
        selectedTitle.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected, selectedTitle: newSelectedTitle }, () => {
      // this.props.onSelect({ selected: newSelected, data: this.props.data });
      this.props.onSelect(newSelected, newSelectedTitle);
    });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  }
  handleEdit = n => (event) => {
    const {
      linkForUpdateMaterial,
      history,
      replaceMutationAndViewMaterialLink
    } = this.props;

    let link = linkForUpdateMaterial;
    if (replaceMutationAndViewMaterialLink) {
      link = replaceMutationAndViewMaterialLink(n, link);
    }
    history.push(`${link}/${n.id}`);
  }
  handleView = n => (event) => {
    const {
      linkForViewMaterial,
      history,
      replaceMutationAndViewMaterialLink,
    } = this.props;
    let link = linkForViewMaterial;
    if (replaceMutationAndViewMaterialLink) {
      link = replaceMutationAndViewMaterialLink(n, link);
    }

    history.push(`${link}/${n.id}`);
  }
  handleDelete = n => (event) => {
    this.props.onDelete(n);
  }
  handleGenerateOutputFile() {
    generateOutputFile(this.state.data, this.props.columnData);
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const {
      classes,
      columnData,
      toolbar,
      showActions,
      isRequested,
      hideDeleteAction,
      formatInputData,
    } = this.props;

    const { data, order, orderBy, selected, rowsPerPage, page } = this.state;

    return (
      <Paper className={classes.root}>
        {
          toolbar &&
          <ExtTableBaseToolbar
            numSelected={selected.length}
            title={'Title'}
          />
        }
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <ExtTableBaseHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              columnData={columnData}
              showActions={showActions}
            />
            <ExtTableBaseBody
              data={data}
              onClick={this.handleClick}
              onKeyDown={this.handleKeyDown}
              page={page}
              rowsPerPage={rowsPerPage}
              columnData={columnData}
              isSelected={this.isSelected}
              onEdit={this.handleEdit}
              onDelete={this.handleDelete}
              onView={this.handleView}
              showActions={showActions}
              hideDeleteAction={hideDeleteAction}
              formatInputData={formatInputData}
            />
            <ExtTableBaseFooter
              data={data}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
          </Table>
          {
            showActions &&
            (
              <Button
                raised
                color="primary"
                onClick={this.handleGenerateOutputFile}
                disabled={isRequested}
              >
                Сохранить в файл
              </Button>
            )
          }

        </div>
      </Paper>
    );
  }
}

ExtTableBase.propTypes = {
  classes: PropTypes.object.isRequired,
};

ExtTableBase.defaultProps = {
  columnData: [],
  data: [],
  page: 0,
  rowsPerPage: 5,
  order: 'desc',
  orderBy: 'createdAt',
  selectOnlyOne: true,
  showActions: true,
  onSelect: () => {},
  onEdit: () => {},
  onDelete: () => {},
  onView: () => {},
  selected: [],
  selectedTitle: [],
};

export default withStyles(styles)(ExtTableBase);
