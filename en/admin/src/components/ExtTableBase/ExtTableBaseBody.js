import React from 'react';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Checkbox from 'material-ui/Checkbox';
import EditIcon from 'material-ui-icons/Edit';
import DeleteIcon from 'material-ui-icons/Delete';
import IconButton from 'material-ui/IconButton';
import PageviewIcon from 'material-ui-icons/Pageview';

import ExtTableBaseCellMenu from './ExtTableBaseCellMenu';

const ExtTableBody = (props) => {
  const {
    data,
    onClick,
    onKeyDown,
    page,
    rowsPerPage,
    columnData,
    isSelected,
    onEdit,
    onDelete,
    onView,
    showActions,
    hideDeleteAction,
    formatInputData,
  } = props;

  return (
    <TableBody>
      {data.slice(page * rowsPerPage, (page * rowsPerPage) + rowsPerPage).map((n) => {
        const {
          id,
          lectureType,
        } = n;
        const isSelected = props.isSelected(n.id);
        let formatedData = n;
        if (formatInputData instanceof Function) {
          formatedData = formatInputData(n);
        }

        return (
          <TableRow
            key={id}
            hover
            onClick={event => onClick(event, n)}
            onKeyDown={event => onKeyDown(event, n)}
            role="checkbox"
            aria-checked={isSelected}
            tabIndex={-1}
            selected={isSelected}
          >
            {
              showActions &&
              (
                <TableCell padding="none">
                  <IconButton onClick={onView(n)} title="Подробности">
                    <PageviewIcon />
                  </IconButton>
                  <IconButton onClick={onEdit(n)} title="Редактировать">
                    <EditIcon />
                  </IconButton>
                  { !hideDeleteAction &&
                    (
                      <IconButton onClick={onDelete(n)} title="Удалить">
                        <DeleteIcon />
                      </IconButton>
                    )
                  }

                </TableCell>
              )
            }

            {
              columnData.map((column, index) => {
                const {
                  numeric,
                  id: name,
                  disablePadding,
                  format,
                } = column;
                let value = formatedData[name];

                if (name.indexOf('_') !== -1) {
                  const keys = name.split('_');
                  value = formatedData[keys[0]][keys[1]];
                }

                if (format instanceof Function) {
                  value = format(value, n);
                }

                return (
                  <TableCell
                    key={`${id}-${name}-${index}`}
                    numeric={numeric}
                    padding={disablePadding ? 'none' : 'default'}
                  >
                    {value}
                  </TableCell>
                );
              })
            }
          </TableRow>
        );
      })}
    </TableBody>
  )
}

export default ExtTableBody;
