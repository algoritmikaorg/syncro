import React, { Component } from 'react';

import ExtForm from './../ExtForm';

const Filter = (props) => {
  const {
    fields,
    onSubmit,
    onChange,
    isRequested,
  } = props;

  return (
    <div>
      <ExtForm
        {...props}
        fields={fields}
        type="filter"
        addRelationList={false}
        disabledAllInputs={isRequested}
        disabledAllActions={isRequested}
        actions={['reset', 'search']}
      />
    </div>
  );
};

export default Filter;
