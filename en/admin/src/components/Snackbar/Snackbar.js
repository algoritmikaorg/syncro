import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Snackbar from 'material-ui/Snackbar';
import { ulid } from './../../../node_modules/ulid/dist/index.umd.js';

import SnackbarItem from './SnackbarItem';

class SnackbarNotify extends Component {
  constructor(props, context) {
    super(props);
    this.state = {
      message: null,
      open: false,
    };
    this.handleSnackbar = this.handleSnackbar.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    props.setHandlerSnackbar(this.handleSnackbar);
  }

  handleSnackbar(message) {
    const id = ulid();
    this.setState({ message: Object.assign(message, { id }), open: true });
  }

  handleRequestClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    // return React.Children.only(this.props.children)
    const { message, open } = this.state;
    let id = 'message-id';
    let text = '';
    let type = '';
    if (message) {
      id = message.id;
      text = message.message;
      type = message.name;
    }

    return (
      <SnackbarItem
        id={id}
        type={type}
        autoHideDuration={6000}
        onClose={this.handleRequestClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        open={open}
        message={<span id={id}>{text}</span>}
      />
    );
  }
}

export default SnackbarNotify;
