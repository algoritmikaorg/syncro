class BrokerSnackbar {
  constructor() {
    this.showSnackbar = this.showSnackbar.bind(this);
    this.setHandlerSnackbar = this.setHandlerSnackbar.bind(this);
  }
  showSnackbar(message) {
    this.handlerSnackbar(message);
  }
  setHandlerSnackbar(handlerSnackbar) {
    this.handlerSnackbar = handlerSnackbar;
  }
}


export default BrokerSnackbar;
