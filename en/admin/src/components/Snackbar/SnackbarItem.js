import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import { withStyles } from 'material-ui/styles';
import { red, grey, lightGreen } from 'material-ui/colors';

const styles = {
  error: {
    backgroundColor: red[500],
    color: grey[50],
  },
  success: {
    backgroundColor: lightGreen[500],
    color: grey[50],
  }
};

const SnackbarStyled = (props) => {
  const {
    classes,
    id,
    open,
    autoHideDuration,
    onClose,
    anchorOrigin,
    message,
    type,
  } = props;

  let contentClass = type === 'Error' ? classes.error : (type === 'Success' ? classes.success : '');

  return(
    <Snackbar
      SnackbarContentProps={{
        'aria-describedby': props.id,
        className: contentClass,
      }}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      anchorOrigin={anchorOrigin}
      open={open}
      message={message}
    />
  );
};

export default withStyles(styles)(SnackbarStyled);
