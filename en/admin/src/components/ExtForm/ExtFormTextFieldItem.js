import React from 'react';
import moment from 'moment';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';
import {
  FormLabel,
  FormControl,
  FormGroup,
  FormControlLabel,
  FormHelperText,
} from 'material-ui/Form';
import { withStyles } from 'material-ui/styles';
import Checkbox from 'material-ui/Checkbox';

import ImageUpload from './../ImageUpload';
import ColorPicker from './../ColorPicker';
import ExtFormRelation from './ExtFormRelation';
import { format as formatUtils } from './../../utils';

const styles = theme => ({
  container: theme.mixins.gutters({
    paddingBottom: 20,
    backgroundColor: 'white',
    paddingTop: 20,
  }),
  form: {
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
  dateContainer: {
    display: 'inline-flex',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  tagsDemo: {
    display: 'inline-flex',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
  },
  input: {},
});

const ExtFormTextFieldItem = (props) => {
  const {
    classes,
    field,
    values,
    onChange,
    disabledAllInputs,
    errors,
    onChangeNoEvents,
    mutationType,
  } = props;

  const {
    type,
    id,
    to,
    from,
    format,
    min,
    max,
    required,
    disabled: disabledField,
    label,
    helperText,
    enumValues,
    width,
    height,
  } = field;

  const name = id;
  let value = values[name];
  // if (name.indexOf('_') !== -1) {
  //   const keys = name.split('_');
  //   value = values[keys[0]][keys[1]];
  // }

  const formatedValue = format ? format(value) : (value || '');
  const error = errors.indexOf(id) !== -1;
  const disabled = disabledField || disabledAllInputs;

  switch (type) {
    case 'time': {
      return (
        <div>
          <TextField
            id={name}
            label={label}
            type="time"
            // defaultValue="12:00"
            value={value}
            onChange={onChange(name)}
            error={error}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              step: 300, // 5 min
            }}
          />
        </div>
      );
    }
    case 'date-time': {
      return (
        <div>
          <TextField
            id={id}
            label={label}
            type="datetime-local"
            // defaultValue="2017-05-24T10:30"
            value={value}
            onChange={onChange(name)}
            error={error}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </div>
      );
    }
    case 'image': {
      return (
        <div>
          <TextField
            id={name}
            label={label}
            type={type}
            className={classes.fullWidth}
            value={value || ''}
            onChange={onChange(name)}
            margin="normal"
            disabled={disabled}
            required={required}
            error={error}
            helperText={helperText}
            fullWidth
            multiline
          />
          <ImageUpload
            width={width}
            height={height}
            value={value}
            onLoaded={onChangeNoEvents(name)}
          />
        </div>
      );
    }
    case 'relation': {
      if (!mutationType) return (
        <div>
          {
            props.type !== 'filter' &&
            (
              <TextField
                id={name}
                label={label}
                type={type}
                className={classes.fullWidth}
                value={values[`${id}_title`] ? values[`${id}_title`].join(', ') : ''}
                onChange={onChange(name)}
                margin="normal"
                disabled={disabled}
                required={required}
                error={error}
                helperText={helperText}
                fullWidth
                multiline
              />
            )
          }

          <ExtFormRelation
            {...props}
            onChange={onChange(name)}
            onChangeNoEvents={onChangeNoEvents(name)}
            disabled={disabled}
          />
        </div>
      );

      return null;
    }
    case 'demo-tags': {
      return (
        <div
          id={name}
          className={classes.tagsDemo}
          style={{
            backgroundColor: values.color,
            color: values.textColor,
          }}
          margin="normal"
        >
          {values.title}
        </div>
      );
    }
    case 'colorPicker': {
      return (
        <ColorPicker
          name={name}
          value={value}
          className={classes.textField}
          onChange={onChange(name)}
          onChangeNoEvents={onChangeNoEvents(name)}
          label={label}
          helperText={helperText}
          disabled={disabled}
          required={required}
          error={error}
        />
      );
    }
    case 'checkbox': {
      return (
        <FormControlLabel
          control={
            <Checkbox
              checked={formatedValue}
              onChange={onChange(name)}
              value={String(formatedValue)}
            />
          }
          label={label}
        />
      );
    }
    case 'tags': {
      return (
        <TextField
          id={name}
          label={label}
          type={type}
          className={classes.fullWidth}
          value={formatedValue}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
          fullWidth
          multiline
        />
      );
    }
    case 'select': {
      return (
        <TextField
          id={`select-${name}`}
          select
          label={label}
          className={classes.textField}
          value={formatedValue}
          onChange={onChange(name)}
          SelectProps={{
            MenuProps: {
              className: classes.menu,
            },
          }}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
        >
          {enumValues.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      );
    }
    case 'multiline': {
      return (
        <TextField
          id={name}
          label={label}
          type={type}
          className={classes.fullWidth}
          value={formatedValue}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
          fullWidth
          multiline
        />
      );
    }
    case 'array': {
      return (
        <TextField
          id={name}
          label={label}
          type={type}
          className={classes.fullWidth}
          value={formatedValue}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
          fullWidth
          multiline
        />
      );
    }
    case 'alias': {
      return (
        <TextField
          id={name}
          label={label}
          type={type}
          className={classes.fullWidth}
          value={formatedValue}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
          fullWidth
          multiline
        />
      );
    }
    case 'number': {
      if (from && to) {
        const nameFrom = `${id}${from}`;
        const nameTo = `${id}${to}`;
        const valueFrom = values[nameFrom];
        const valueTo = values[nameTo];
        return (
          <FormControl>
            <FormLabel component="legend">{label}</FormLabel>
            <FormGroup>
              <TextField
                id={nameFrom}
                type={'number'}
                valueFrom={classes.textField}
                value={from || ''}
                // onChange={onChange(nameFrom)}
                onChange={onChange(nameFrom, formatUtils.parseIntNumberField)}
                margin="normal"
                disabled={disabled}
                required={required}
                error={errors.indexOf(nameFrom) !== -1}
                label={'мин'}
                InputLabelProps={{
                  shrink: true,
                }}
                min={0}
                max={valueFrom}
              />
              <TextField
                id={`number-${name}-to`}
                type={'number'}
                className={classes.textField}
                value={valueTo || ''}
                onChange={onChange(nameTo, formatUtils.parseIntNumberField)}
                margin="normal"
                disabled={disabled}
                required={required}
                error={errors.indexOf(nameTo) !== -1}
                label={'макс'}
                min={valueFrom}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </FormGroup>
          </FormControl>
        );
      }
      return (
        <TextField
          id={name}
          label={label}
          value={value}
          onChange={onChange(name, formatUtils.parseIntNumberField)}
          error={error}
          className={classes.fullWidth}
          type="number"
          required={required}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
          fullWidth
        />
      );
    }
    case 'number-float': {
      return (
        <TextField
          id={name}
          label={label}
          value={value}
          onChange={onChange(name, formatUtils.parseFloatNumberField)}
          error={error}
          className={classes.fullWidth}
          type="number"
          step="any"
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
          fullWidth
        />
      );
    }
    case 'number-short': {
      return (
        <TextField
          id={name}
          label={label}
          value={value}
          onChange={onChange(name, formatUtils.parseIntNumberField)}
          error={error}
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
        />
      );
    }
    case 'date': {
      if (to && from) {
        const nameFrom = `${id}${from}`;
        const nameTo = `${id}${to}`;
        const valueFrom = values[nameFrom];
        const valueTo = values[nameTo];

        return (
          <FormControl>
            <FormLabel component="legend">{label}</FormLabel>
            <FormGroup>
              <TextField
                id={nameFrom}
                type={'date'}
                className={classes.textField}
                value={format ? format(valueFrom) : valueFrom}
                onChange={onChange(nameFrom)}
                margin="normal"
                disabled={disabled}
                required={required}
                error={errors.indexOf(nameFrom) !== -1}
                label={'от'}
                InputLabelProps={{
                  shrink: true,
                }}
                min={format ? format(min) : min || ''}
                max={format ? format(valueFrom) : valueFrom}
              />
              <TextField
                id={nameTo}
                type={'date'}
                className={classes.textField}
                value={format ? format(valueTo) : valueTo}
                onChange={onChange(nameTo)}
                margin="normal"
                disabled={disabled}
                required={required}
                error={errors.indexOf(nameTo) !== -1}
                label={'до'}
                min={format ? format(valueFrom) : valueFrom}
                max={format ? format(max) : max || ''}
                InputLabelProps={{
                  shrink: true,
                }}
                multiple
              />
            </FormGroup>
          </FormControl>
        );
      }
      return (
        <div>
          <TextField
            id={id}
            label={label}
            type="date"
            onChange={onChange(name)}
            error={error}
            // defaultValue="2017-05-24"
            value={moment(value).format('YYYY-MM-DD')}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </div>
      );
    }
    default: {
      return (
        <TextField
          id={name}
          label={label}
          type={type}
          className={classes.textField}
          value={formatedValue}
          onChange={onChange(name)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          helperText={helperText}
        />
      );
    }
  }
};

ExtFormTextFieldItem.defaultProps = {
  onChange() {},
  onChangeNoEvents() {},
  onSubmit() {},
  onSelect() {},
};

export default withStyles(styles)(ExtFormTextFieldItem);
