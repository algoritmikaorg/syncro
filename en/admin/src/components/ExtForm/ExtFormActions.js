import React from 'react';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';

const isShow = actions => action => actions.indexOf(action) !== -1;

const styles = theme => ({
  container: {},
  actions: {
    marginTop: 20,
  },
  button: {
    margin: theme.spacing.unit,
  },
  form: {},
  textField: {},
  menu: {},
  input: {},
});

const ExtFormActions = (props) => {
  const {
    classes,
    actions,
    disabledAllActions,
    onReset,
    onSubmit,
  } = props;
  const isShowCurrent = isShow(actions);

  return (
    <div className={classes.actions}>
      {
        isShowCurrent('reset') &&
        (
          <Button
            raised
            className={classes.button}
            onClick={onReset}
            disabled={disabledAllActions}
          >
            Сбросить
          </Button>
        )
      }
      {
        isShowCurrent('search') &&
        (
          <Button
            raised
            className={classes.button}
            onClick={onSubmit}
            disabled={disabledAllActions}
            color="primary"
          >
            Найти
          </Button>
        )
      }
      {
        isShowCurrent('save') &&
        (
          <Button
            raised
            className={classes.button}
            onClick={onSubmit}
            disabled={disabledAllActions}
            color="primary"
          >
            Сохранить
          </Button>
        )
      }
    </div>
  );
};

export default withStyles(styles)(ExtFormActions);
