import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';
// import { slugify } from 'transliteration';
import slugify from './../../../node_modules/transliteration/lib/browser/transliteration.min.js';

import withProgress from './../../hoc/withProgress';

import { fields as fieldsUtils } from './../../utils';

import ExtFormActions from './ExtFormActions';
import ExtFormTextFieldList from './ExtFormTextFieldList';
import ExtFormRelationAddingList from './ExtFormRelationAddingList';

const styles = theme => ({
  container: theme.mixins.gutters({
    paddingBottom: 20,
    backgroundColor: 'white',
    paddingTop: 20,
  }),
  form: {
    display: 'flex',
    justifyContent: 'center',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {},
});

class ExtFrom extends Component {
  constructor(props) {
    super(props);
    const {
      fields,
      data,
      outterFormSubmitFunction,
    } = props;

    const currentState = fieldsUtils.generateStateFieldsObject(fields, data);

    this.state = currentState;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleChangeNoEvent = this.handleChangeNoEvent.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const {
      mutationType,
      data,
      fields,
      forceSaveFormData,
    } = nextProps;

    if (mutationType === 'create' || mutationType === 'update') {
      const currentState = fieldsUtils.generateStateFieldsObject(fields, data);
      this.setState(currentState);
    }
  }
  handleSubmit(event) {
    event.preventDefault();

    const errors = fieldsUtils.validateFields(this.props.fields, this.state);
    if (errors.length > 0) {
      this.setState({
        errors,
      });
    } else {
      const { onSubmit, dontSavePassResultTo } = this.props;
      const outputFields = fieldsUtils.outputFields(this.state);
      if (dontSavePassResultTo instanceof Function) {
        dontSavePassResultTo(outputFields);
      } else {
        onSubmit(outputFields);
      }
    }
  }
  handleReset() {
    const currentState = fieldsUtils.generateStateFieldsObject(this.props.fields);
    this.setState(currentState);
  }
  handleChangeNoEvent = name => (value) => {
    const state = Object.assign(this.state);
    state[name] = value;
    if (state.errors.length > 0) {
      state.errors = fieldsUtils.validateFields(this.props.fields, state);
    }
    this.setState(state);
    this.props.onChange(fieldsUtils.outputFields(state));
  }
  handleChange = (name, format) => (event) => {
    // event.preventDefault();
    event.stopPropagation();
    let value = event.target.value;
    const state = Object.assign(this.state);
    const stValue = state[name];

    if (event.target.type === 'checkbox') {
      value = !stValue;
    }
    if (format) value = format(value);
    if (name === 'title' && Object.prototype.hasOwnProperty.call(state, 'alias')) {
      state.alias = slugify(value);
    }

    if ((name === 'firstName' || name === 'lastName')
      && !Object.prototype.hasOwnProperty.call(state, 'title')
      && Object.prototype.hasOwnProperty.call(state, 'alias')
    ) {
      const str = (name === 'firstName') ? `${value} ${state.lastName}` : `${state.firstName} ${value}`;
      state.alias = slugify(str);
    }

    if (state.errors.length > 0) {
      state.errors = fieldsUtils.validateFields(this.props.fields, state);
    }

    state[name] = value;

    this.setState(state, () => {
      this.props.onChange(fieldsUtils.outputFields(state));
    });
  }
  handleSelect = name => (selected, selectedTitle) => {
    const newState = Object.assign(this.state);
    newState[name] = selected;
    newState[`${name}_title`] = selectedTitle;
    if (newState.errors.length > 0) {
      newState.errors = fieldsUtils.validateFields(this.props.fields, newState);
    }
    this.setState(newState);
    this.props.onChange(fieldsUtils.outputFields(newState));
  }
  render() {
    const {
      classes,
      title,
      disabledAllInputs,
      fields,
      mutationType,
      showFormActions,
    } = this.props;

    const {
      errors,
    } = this.state;

    return (
      <div
        className={classes.container}
      >
        <Typography
          className={classes.title}
          type="subheading"
          color="inherit"
          noWrap
        >
          {title}
        </Typography>

        <div className={classes.form}>
          <form
            noValidate
            autoComplete="off"
            onSubmit={this.handleSubmit}
            style={{ width: '100%' }}
          >
            <ExtFormTextFieldList
              {...this.props}
              fields={fields}
              disabledAllInputs={disabledAllInputs}
              onChange={this.handleChange}
              onChangeNoEvents={this.handleChangeNoEvent}
              onSubmit={this.handleSubmit}
              errors={errors}
              values={this.state}
              mutationType={mutationType}
            />
          </form>
        </div>
        <div>

          {
            mutationType &&
            <ExtFormRelationAddingList
              {...this.props}
              disabledAllInputs={disabledAllInputs}
              onChange={this.handleChange}
              onChangeNoEvents={this.handleChangeNoEvent}
              onSubmit={this.handleSubmit}
              errors={errors}
              fields={fields}
              values={this.state}
              onSelect={this.handleSelect}
            />
          }
        </div>
        {
          showFormActions &&
          <ExtFormActions
            {...this.props}
            onSubmit={this.handleSubmit}
            onReset={this.handleReset}
          />
        }

      </div>

    );
  }
}

ExtFrom.defaultProps = {
  onChange: () => {},
  onSubmit: () => {},
  dontSavePassResultTo: null,
  showFormActions: true,
  forceSaveFormData: false,
};

export default withStyles(styles)(ExtFrom);
