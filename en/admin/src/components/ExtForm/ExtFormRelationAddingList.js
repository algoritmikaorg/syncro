import React from 'react';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import { withStyles } from 'material-ui/styles';
import { ulid } from './../../../node_modules/ulid/dist/index.umd.js';

import ExtFormRelationAddingItem from './ExtFormRelationAddingItem';

const preFaceKey = ulid();

const styles = theme => ({
  container: {},
  form: {},
  button: {},
  menu: {},
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
  input: {},
});

const ExtFormRelationAddingList = (props) => {
  const {
    fields,
    disabledAllInputs,
    classes,
    errors,
    values,
  } = props;

  return (
    <div>
      {
        fields.map((field, index) => {
          if (field.type === 'relation' && props.mutationType) {
            const {
              id,
              label,
              format,
              required,
              helperText,
              disabled: disabledField,
            } = field;
            const disabled = disabledField || disabledAllInputs;
            const valuesTitle = values[`${id}_title`];
            const valuesIds = values[`${id}`];
            let valuesTitleFormated = '';
            let valuesIdsFormated = '';
            if (Array.isArray(valuesTitle)) {
              valuesTitleFormated = valuesTitle.join(', ');
            } else if (valuesTitle instanceof Object) {
              valuesTitleFormated = valuesTitle.title || '';
            } else {
              valuesTitleFormated = valuesTitle || '';
            }
            if (Array.isArray(valuesIds)) {
              valuesIdsFormated = valuesIds.join(', ');
            } else if (valuesIds instanceof Object) {
              valuesIdsFormated = valuesIds.id || '';
            } else {
              valuesIdsFormated = valuesIds || '';
            }

            const error = errors.indexOf(id) !== -1;

            return (
              <div
                key={`${preFaceKey}-${index}`}
              >
                <TextField
                  id={id}
                  label={label}
                  type={'text'}
                  className={classes.fullWidth}
                  value={valuesTitleFormated || valuesIdsFormated}
                  margin="normal"
                  disabled={disabled}
                  required={required}
                  error={error}
                  helperText={helperText}
                  fullWidth
                  multiline
                />
                <ExtFormRelationAddingItem
                  key={`ext-${preFaceKey}-${field.id}-${index}`}
                  {...props}
                  field={field}
                />
              </div>
            );
          }
        })
      }
    </div>
  );
};

// class ExtFormRelationAddingList extends PureComponent {
//   // shouldComponentUpdate(nextProps, nextState) {
//   //   return false;
//   // }
//   render() {
//     const {
//       fields,
//       disabledAllInputs,
//       classes,
//       errors,
//       values,
//     } = this.props;
//
//     return (
//       <div>
//         {
//           fields.map((field, index) => {
//             if (field.type === 'relation' && this.props.mutationType) {
//               const {
//                 id,
//                 label,
//                 format,
//                 required,
//                 helperText,
//                 disabled: disabledField,
//               } = field;
//               const disabled = disabledField || disabledAllInputs;
//               const valuesTitle = values[`${id}_title`];
//               const valuesIds = values[`${id}`];
//               let valuesTitleFormated = '';
//               let valuesIdsFormated = '';
//               if (Array.isArray(valuesTitle)) {
//                 valuesTitleFormated = valuesTitle.join(', ');
//               } else {
//                 valuesTitleFormated = valuesTitle || '';
//               }
//               if (Array.isArray(valuesIds)) {
//                 valuesIdsFormated = valuesIds.join(', ');
//               } else {
//                 valuesIdsFormated = valuesIds || '';
//               }
//
//               const error = errors.indexOf(id) !== -1;
//
//               return (
//                 <div
//                   key={`${preFaceKey}-${index}`}
//                 >
//                   <TextField
//                     id={id}
//                     label={label}
//                     type={'text'}
//                     className={classes.fullWidth}
//                     value={valuesTitleFormated || valuesIdsFormated }
//                     margin="normal"
//                     disabled={disabled}
//                     required={required}
//                     error={error}
//                     helperText={helperText}
//                     fullWidth
//                     multiline
//                   />
//                   <ExtFormRelationAddingItem
//                     {...this.props}
//                     field={field}
//                   />
//                 </div>
//               );
//             }
//           })
//         }
//       </div>
//     );
//   }
// }

ExtFormRelationAddingList.defaultProps = {
  onChange() {},
  onChangeNoEvents() {},
  onSubmit() {},
  onSelect() {},
};


export default withStyles(styles)(ExtFormRelationAddingList);
