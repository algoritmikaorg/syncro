import React from 'react';
// import Typography from 'material-ui/Typography';

import { format as formatUtil } from './../../utils';

import ExtTableBase from './../ExtTableBase';
import {
  lecturesAll as columnDataLectures,
  courses as columnDataCourses,
  cycles as columnDataCycles,
} from './../../pages/Catalog/columnData';

import SelectAllQueryShort from './../SelectAllQueryShort';

import columnDataTags from './../../pages/Tags/columnData';
import columnDataLecturers from './../../pages/Lecturers/columnData';
import columnDataEvents from './../AddEvents/columnData';

// import {
//   courses as columnDataCourses,
//   cycyles as columnDataCycles,
//   lectures as columnDataLectures,
// } from './../../pages/Catalog/columnData';

const ExtFormRelation = (props) => {
  const {
    field,
    values,
    data,
    mutationType,
    onChangeNoEvents,
  } = props;
  const {
    label,
    relation,
  } = field;

  switch (relation) {
    case 'curator-select': {
      return (
        <SelectAllQueryShort
          typeMaterial="curator"
          typeView="select"
          multiple
          ids={data ? data.curatorId : values.curatorId || []}
          onChange={onChangeNoEvents}
        />
      );
    }
    case 'lecturers-select': {
      return (
        <SelectAllQueryShort
          typeMaterial="lecturers"
          typeView="select"
          multiple
          ids={data ? data.lecturersIds : values.locationsIds || []}
          onChange={onChangeNoEvents}
        />
      );
    }
    case 'locations-select': {
      return (
        <SelectAllQueryShort
          typeMaterial="locations"
          typeView="select"
          multiple
          ids={data ? data.locationsIds : values.locationsIds || []}
          onChange={onChangeNoEvents}
        />
      );
    }
    case 'tags-select': {
      return (
        <SelectAllQueryShort
          typeMaterial="tags"
          typeView="select"
          multiple
          ids={data ? data.tagsIds : values.tagsIds || []}
          onChange={onChangeNoEvents}
        />
      );
    }
    case 'acquiring-select': {
      return (
        <SelectAllQueryShort
          typeMaterial="acquiring"
          typeView="select"
          ids={data ? data.acquiringId : values.acquiringId || []}
          onChange={onChangeNoEvents}
        />
      );
    }
    case 'lecturers': {
      // const currentData = formatUtil.tableDatalecturers()
      return (
        <ExtTableBase
          {...props}
          // data={data ? data.lecturers : values.lecturers || []}
          data={data ? data.lecturers : values.lecturers || []}
          showActions
          columnData={columnDataLecturers}
          linkForViewMaterial={'/lecturers/view'}
          linkForUpdateMaterial={'/lecturers/update'}
          hideDeleteAction
          // history={props.history}
        />
      );
    }
    case 'tags': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.tags : values.tags || []}
          showActions
          columnData={columnDataTags}
          linkForViewMaterial={'/tags/view'}
          linkForUpdateMaterial={'/tags/update'}
          hideDeleteAction
          // history={props.history}
        />
      );
    }
    case 'lectures': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.lectures : values.lectures || []}
          showActions
          columnData={columnDataLectures}
          linkForViewMaterial={'/catalog/lectures/view'}
          linkForUpdateMaterial={'/catalog/lectures/update'}
          hideDeleteAction
        />
      );
    }
    case 'lecturesForCycle': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.lectures : values.lectures || []}
          showActions
          columnData={columnDataLectures}
          linkForViewMaterial={'/catalog/lectures-for-cycle/view'}
          linkForUpdateMaterial={'/catalog/lectures-for-cycle/update'}
          hideDeleteAction
        />
      );
    }
    case 'lecturesForCourse': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.lectures : values.lectures || []}
          showActions
          columnData={columnDataLectures}
          linkForViewMaterial={'/catalog/lectures-for-course/view'}
          linkForUpdateMaterial={'/catalog/lectures-for-course/update'}
          hideDeleteAction
        />
      );
    }
    case 'courses': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.courses : values.courses || []}
          showActions
          columnData={columnDataCourses}
          linkForViewMaterial={'/catalog/courses/view'}
          linkForUpdateMaterial={'/catalog/courses/update'}
          hideDeleteAction
        />
      );
    }
    case 'cycles': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.cycles : values.cycles || []}
          showActions
          columnData={columnDataCycles}
          linkForViewMaterial={'/catalog/cycles/view'}
          linkForUpdateMaterial={'/catalog/cycles/update'}
          hideDeleteAction
        />
      );
    }
    case 'events': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.events : values.events || []}
          showActions={false}
          columnData={columnDataEvents}
          hideDeleteAction
        />
      );
    }
    case 'recommendedCourses': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.recommendedCourses : values.recommendedCourses || []}
          showActions
          columnData={columnDataCourses}
          linkForViewMaterial={'/catalog/courses/view'}
          linkForUpdateMaterial={'/catalog/courses/update'}
          hideDeleteAction
        />
      );
    }
    case 'recommendedCycles': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.recommendedCycles : values.recommendedCycles || []}
          showActions
          columnData={columnDataCycles}
          linkForViewMaterial={'/catalog/cycles/view'}
          linkForUpdateMaterial={'/catalog/cycles/update'}
          hideDeleteAction
        />
      );
    }
    case 'recommendedLectures': {
      return (
        <ExtTableBase
          {...props}
          data={data ? data.recommendedLectures : values.recommendedLectures || []}
          showActions
          columnData={columnDataLectures}
          linkForViewMaterial={'/catalog/lectures/view'}
          linkForUpdateMaterial={'/catalog/lectures/update'}
          hideDeleteAction
        />
      );
    }
    default: {
      return (
        <div>Нет связанных компонентов</div>
      );
    }
  }
};

export default ExtFormRelation;
