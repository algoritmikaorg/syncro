import React from 'react';
import { ulid } from './../../../node_modules/ulid/dist/index.umd.js';

import ExtFormTextFieldItem from './ExtFormTextFieldItem';

const prefaceKey = ulid();

const ExtFormTextFieldList = (props) => {
  const {
    fields,
    // onChange,
    // values,
    // errors,
    // disabledAllInputs,
    // onChangeNoEvents,
    // mutationType,
  } = props;

  return (
    <div>
      {
        fields.map((field, index) => (
          <ExtFormTextFieldItem
            {...props}
            key={`${prefaceKey}-${field.id}-${index}`}
            field={field}
          />
        ))
      }
    </div>
  );
};

export default ExtFormTextFieldList;
