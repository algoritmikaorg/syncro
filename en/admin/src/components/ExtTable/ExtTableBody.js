import React from 'react';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Checkbox from 'material-ui/Checkbox';
import EditIcon from 'material-ui-icons/Edit';
import DeleteIcon from 'material-ui-icons/Delete';
import IconButton from 'material-ui/IconButton';
import PageviewIcon from 'material-ui-icons/Pageview';

import ExtTableCellMenu from './ExtTableCellMenu';

const ExtTableBody = (props) => {
  const {
    data,
    onClick,
    onKeyDown,
    page,
    rowsPerPage,
    columnData,
    isSelected,
    onEdit,
    onDelete,
    onView,
    showActions,
  } = props;

  return (
    <TableBody>
      {data.slice(page * rowsPerPage, (page * rowsPerPage) + rowsPerPage).map((n) => {
        const {
          id,
        } = n;
        const isSelected = props.isSelected(n.id);
        return (
          <TableRow
            hover
            onClick={event => onClick(event, id)}
            onKeyDown={event => onKeyDown(event, id)}
            role="checkbox"
            aria-checked={isSelected}
            tabIndex={-1}
            key={id}
            selected={isSelected}
          >
            {
              showActions &&
              (
                <TableCell padding="none">
                  <IconButton onClick={onView(id)} title="Подробности">
                    <PageviewIcon />
                  </IconButton>
                  <IconButton onClick={onEdit(id)} title="Редактировать">
                    <EditIcon />
                  </IconButton>
                  <IconButton onClick={onDelete(n)} title="Удалить">
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              )
            }

            {
              columnData.map((column, index) => {
                const {
                  numeric,
                  id: name,
                  disablePadding,
                  format,
                } = column;
                let value = n[name];
                if (format instanceof Function) {
                  value = format(value);
                }

                return (
                  <TableCell
                    key={`${id}-${name}`}
                    numeric={numeric}
                    padding={disablePadding ? 'none' : 'default'}
                  >
                    {value}
                  </TableCell>
                );
              })
            }
          </TableRow>
        );
      })}
    </TableBody>
  )
}

export default ExtTableBody;
