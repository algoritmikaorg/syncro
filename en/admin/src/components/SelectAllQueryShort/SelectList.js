import React, { Component } from 'react';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  textField: {
    // marginLeft: theme.spacing.unit,
    // marginRight: theme.spacing.unit,
    width: 200,
  },
  formControl: {
    margin: theme.spacing.unit,
  },
});

const activeItemFontWeigth = (ids, id) => {
  if (Array.isArray(ids)) {
    return ids.indexOf(id) !== -1 ? '500' : '400';
  }
  return ids === id ? '500' : '400';
};

class SelectList extends Component {
  constructor(props) {
    super(props);
    const {
      ids,
      titles,
      multiple
    } = props;
    let stIds = null;
    let stTitle = titles || (multiple ? [] : '');
    if (Array.isArray(ids)) {
      stIds = ids;
    } else if (ids instanceof Object) {
      stIds = ids.id || '';
      stTitle = ids.title || '';
    } else {
      stIds = ids || (multiple ? [] : '');
    }
    this.state = {
      ids: stIds,
      titles: stTitle,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleMenuExited = this.handleMenuExited.bind(this);
  }
  handleChange(event) {
    const {
      gqlQueryName,
      gqlQueryItemsName,
    } = this.props;
    const data = this.props[gqlQueryName][gqlQueryItemsName] || [];
    const ids = event.target.value;

    const titles = data.filter(({ id }) => (
      ids.indexOf(id) !== -1
    )).map(({ title, firstName, lastName }) => {
      if (title) return title;
      if (firstName && lastName) return `${firstName} ${lastName}`;
    })
    this.setState(
      { ids, titles });
  };
  handleMenuExited() {
    this.props.onChange(this.state.ids, this.state.titles);
  }

  render() {
    const {
      gqlQueryName,
      gqlQueryItemsName,
      inputLabel,
      format,
      classes,
      multiple,
    } = this.props;
    const data = this.props[gqlQueryName][gqlQueryItemsName] || [];
    const inputId = `select-${gqlQueryItemsName}-multiple`;

    return (
      <FormControl className={classes.formControl}>
        <InputLabel
          htmlFor={inputId}
        >
          {inputLabel}
        </InputLabel>
        <Select
          multiple={multiple}
          value={this.state.ids}
          onChange={this.handleChange}
          input={<Input id={inputId} />}
          className={classes.textField}
          MenuProps={{
            onExited: this.handleMenuExited,
            // PaperProps: {
            //   style: {
            //     maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            //     width: 200,
            //   },
            // },
          }}
        >
          {data.map((item) => {
            const { id } = item;
            const { ids } = this.state;

            return (
              <MenuItem
                key={id}
                value={id}
                className={classes.textField}
                style={{
                  fontWeight: activeItemFontWeigth(ids, id),
                }}
              >
                {format.formatLabelItem(item)}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>

    );
  }
}

SelectList.defaultProps = {
  onChange: () => {},
};

export default withStyles(styles)(SelectList);
