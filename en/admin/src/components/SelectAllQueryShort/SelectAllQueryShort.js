import React, { Component } from 'react';

import ComponentWithGraphQLQuery from './../../containers/ComponentWithGraphQLQuery';

import {
  ALL_DISCOUNTS_QUERY_SHORT,
  ALL_LECTURERS_QUERY_SHORT,
  ALL_LOCATIONS_QUERY_SHORT,
  ALL_TAGS_QUERY_SHORT,
  ALL_ACQUIRINGS_QUERY_SHORT,
  ALL_CURATOR_QUERY_SHORT,
} from './../../constants';

import gql from './gql';
import SelectList from './SelectList';
import {
  discounts as formatDicounts,
  lecturers as formatLecturers,
  locations as formatLocations,
  tags as formatTags,
  acquiring as formatAcquiring,
  curator as formatCurator,
} from './format';

const SelectAllQueryShort = (props) => {
  const {
    typeMaterial,
    typeView,
  } = props;

  let component = null;
  let gqlQueryName = null;
  let gqlQueryItemsName = null;
  let inputLabel = null;
  let format = null;

  switch (typeMaterial) {
    case 'discounts': {
      gqlQueryName = ALL_DISCOUNTS_QUERY_SHORT;
      gqlQueryItemsName = 'allDiscounts';
      inputLabel = 'Тип скидки';
      format = formatDicounts;
      break;
    }
    case 'lecturers': {
      gqlQueryName = ALL_LECTURERS_QUERY_SHORT;
      gqlQueryItemsName = 'allLecturers';
      inputLabel = 'Лектор';
      format = formatLecturers;
      break;
    }
    case 'locations': {
      gqlQueryName = ALL_LOCATIONS_QUERY_SHORT;
      gqlQueryItemsName = 'allLocations';
      inputLabel = 'Локация';
      format = formatLocations;
      break;
    }
    case 'tags': {
      gqlQueryName = ALL_TAGS_QUERY_SHORT;
      gqlQueryItemsName = 'allTags';
      inputLabel = 'Направление';
      format = formatTags;
      break;
    }
    case 'acquiring': {
      gqlQueryName = ALL_ACQUIRINGS_QUERY_SHORT;
      gqlQueryItemsName = 'allAcquirings';
      inputLabel = 'Эквайринг';
      format = formatAcquiring;
      break;
    }
    case 'curator': {
      gqlQueryName = ALL_CURATOR_QUERY_SHORT;
      gqlQueryItemsName = 'allUsers';
      inputLabel = 'Координатор';
      format = formatCurator;
      break;
    }
    default: {
      gqlQueryName = ALL_TAGS_QUERY_SHORT;
      gqlQueryItemsName = 'allTags';
      inputLabel = 'Направление';
      format = formatTags;
    }
  }

  switch (typeView) {
    default: {
      component = SelectList;
    }
  }
  return (
    <ComponentWithGraphQLQuery
      inputLabel={inputLabel}
      {...props}
      gql={gql}
      gqlQueryName={gqlQueryName}
      gqlQueryItemsName={gqlQueryItemsName}
      component={component}
      format={format}
    />
  );
}

export default SelectAllQueryShort;
