const formatLabelItemSimple = (item) => {
  let output = '';
  if (Object.prototype.hasOwnProperty.call(item, 'title')) {
    output = item.title;
  } else if (Object.prototype.hasOwnProperty.call(item, 'firstName')) {
    output = `${item.firstName} ${item.lastName}`;
  }

  return output;
};

const discounts = {
  formatLabelItem(item) {
    return `${item.title} - ${item.rate}% - ${item.useCount}`;
  },
};
const lecturers = {
  formatLabelItem: formatLabelItemSimple,
};
const locations = {
  formatLabelItem: formatLabelItemSimple,
};
const tags = {
  formatLabelItem: formatLabelItemSimple,
};
const acquiring = {
  formatLabelItem: formatLabelItemSimple,
};
const curator = {
  formatLabelItem: formatLabelItemSimple,
};

export default {};
export { discounts };
export { lecturers };
export { locations };
export { tags };
export { acquiring };
export { curator };
