import gql from 'graphql-tag';

export default gql`
  query allTagsQueryShort($filter:TagFilter) {
    allTags(filter: $filter) {
      id,
      title,
    }
  }
`;
