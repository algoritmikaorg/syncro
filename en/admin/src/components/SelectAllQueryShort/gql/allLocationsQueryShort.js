import gql from 'graphql-tag';

export default gql`
  query allLocationsQueryShort($filter:LocationFilter) {
    allLocations(filter: $filter) {
      id,
      title,
      address,
      metro,
    }
  }
`;
