import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import FormBase from './../../components/FormBase';

const fields = {
  firstName: {
    value: '',
    label: 'Имя',
    required: true,
  },
  lastName: {
    value: '',
    label: 'Фамилия',
    required: true,
  },
  password: {
    value: '',
    label: 'Пароль',
    required: true,
    helperText: 'Минимальная длина 8 символов',
  },
  repassword: {
    value: '',
    label: 'Повторите пароль',
    required: true,
  },
  email: {
    value: '',
    label: 'E-mail',
    required: true,
  },
  phone: {
    value: '',
    label: 'Телефон',
    required: true,
  },
  role: {
    value: '',
    label: 'Роль',
    required: true,
  },
};

const User = (props) => {
  return (
    <FormBase
      fields={fields}
    />
  );
}

export default User;
