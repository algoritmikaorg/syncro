import React, { Component } from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

import {
  LECTURE_QUERY,
  EDIT_LECTURE_MUTATION,
} from './../../constants';

import gql from './gql';
import MutationMaterial from './../MutationMaterial';

import fields from './fields';

const LectureTypeChangeDialog = (props) => {
  const {
    open,
    onClose,
    onSubmit,
    onSubmitMutation,
    isMutation,
  } = props;

  const gqlMutationName = EDIT_LECTURE_MUTATION;
  const mutationResultObjectName = 'updateLecture';
  const title = 'Изменение типа лекции';

  const gqlQueryName = LECTURE_QUERY;
  const gqlQueryItemName = 'Lecture'
  const resulMutatiomtMessageSuccses = 'Обновлено';


  return (
    <div>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>

          </DialogContentText> */}
          <MutationMaterial
            {...props}
            fields={fields}
            gql={gql}
            gqlMutationName={gqlMutationName}
            mutationResultObjectName={mutationResultObjectName}
            gqlQueryName={gqlQueryName}
            gqlQueryItemName={gqlQueryItemName}
            resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
            showActions={false}
            onSuccessMutation={onSubmitMutation}
            mutationType="update"
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Отменить
          </Button>
          {/* <Button onClick={this.handleClickSubmit} color="primary">
            Сохранить
          </Button> */}
        </DialogActions>
      </Dialog>
    </div>
  );
}

LectureTypeChangeDialog.defaultProps = {
  onClose() {

  },
  onSubmit() {

  },
  open: false,
};

export default LectureTypeChangeDialog;
