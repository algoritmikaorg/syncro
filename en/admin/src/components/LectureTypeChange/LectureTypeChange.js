import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';

import LectureTypeChangeDialog from './LectureTypeChangeDialog';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

class LectureTypeChange extends PureComponent {
  state = {
    open: false,
  }
  handleClick = () => {
    this.setState({
      open: true,
    });
  }
  handleMutationSuccses = () => {
    this.setState({
      open: false,
    });
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Button
          className={classes.button}
          raised
          dense
          color={'primary'}
          onClick={this.handleClick}
        >
          Изменить тип лекции
        </Button>
        {
          this.state.open &&
          (
            <LectureTypeChangeDialog
              {...this.props}
              onSubmitMutation={this.handleMutationSuccses}
              open={this.state.open}
            />
          )
        }
      </div>
    );
  }
}

export default withStyles(styles)(LectureTypeChange);
