const create = [
  {
    id: 'date',
    label: 'Дата',
    required: true,
    type: 'date',
  },
  {
    id: 'time',
    label: 'Время',
    required: true,
    type: 'time',
    outputType: 'time',
  },
  {
    id: 'price',
    label: 'Цена',
    required: true,
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'quantityOfTickets',
    label: 'Количество билетов',
    required: true,
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'lectureId',
    label: 'Лекция',
    required: true,
    disabled: true,
    type: 'multiline',
  },
  {
    id: 'lecturersIds',
    label: 'Лекторы',
    required: true,
    disabled: true,
    multiple: true,
    relation: 'lecturers-select',
    type: 'relation',
  },
  {
    id: 'locationId',
    label: 'Локация',
    required: true,
    disabled: true,
    relation: 'locations-select',
    type: 'relation',
  },
  {
    id: 'curatorId',
    label: 'Координатор',
    required: true,
    disabled: true,
    relation: 'curator-select',
    type: 'relation',
  },
];


const update = create.slice();
update.unshift(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const view = update.slice();
view.push(
  {
    id: 'ordersIds',
    label: 'Заказы связанные с мероприятием',
    disabled: true,
    relation: 'orders',
    type: 'relation',
  },
);

const filter = [
  {
    id: 'lecturers',
    label: 'Лектор',
  },
  {
    id: 'location',
    label: 'Локация',
  },
  {
    id: 'curator',
    label: 'Координатор'
  },
  {
    id: 'price',
    label: 'Цена',
  },
  {
    id: 'rate',
    label: 'Размер скидки',
  },
  {
    id: 'date',
    label: 'Даты проведения',
    type: 'date',
    from: '_gte',
    to: '_lte',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
