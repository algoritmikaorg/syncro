import gql from 'graphql-tag';

export default gql`
  query EventQuery($id: ID) {
    Event(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      date,
      quantityOfTickets,
      price,
      # attendings {
      #   user {
      #     id,
      #
      #     firstName,
      #     lastName,
      #   },
      # },
      lecture {
        id,

        title,
      },
      lecturers {
        id,

        firstName,
        lastName,
      },
      location {
        id,

        title,
        address,
        metro,
      },
      curator {
        id

        firstName
        lastName
        email
      }
      orders {
        payment {
          id,

          createdAt,
          commission,
          certificate,
          subscription,
          type,

        },
        user {
          id,

          firstName,
          lastName,
        },
      },
      tickets {
        id
        number
        user {
          id,

          firstName,
          lastName,
        },
      },
      reviews {
        user {
          id,

          firstName,
          lastName,
        },
      },
      _attendingsMeta {
        count
      },
      _lecturersMeta {
        count
      },
      _ticketsMeta {
        count
      },
      _reviewsMeta {
        count
      }
    }
  }
`;
