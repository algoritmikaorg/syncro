import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import Message from './Message';

const Notification = (props) => {
  const {
    id,
    message
  }
  return (
    <Snackbar
      anchorOrigin={{ 'bottom', 'right' }}
      open={open}
      SnackbarContentProps={{
        'aria-describedby': id,
      }}
      message={
        <Message id={id} message={message} />
      }
    />
  );
}

export default Notification;
