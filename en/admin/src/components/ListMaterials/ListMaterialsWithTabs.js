import React from 'react';

import TabsContainer from './../../containers/TabsContainer';
import ListMaterialsContainer from './../../containers/ListMaterialsContainer';
import ListMaterialsContent from './ListMaterialsContent';

const ListMaterialsWithTabs = (props) => {
  const { tabs } = props;
  return (
    <div>
      {
        <TabsContainer
          tabs={tabs}
        >
          {
            tabs.map((tab, index) => (
              <ListMaterialsContainer
                {...props}
                key={index}
                component={ListMaterialsContent}
                filterType={tab.id}
              />
            ))
          }
        </TabsContainer>
      }
    </div>
  );
};

export default ListMaterialsWithTabs;
