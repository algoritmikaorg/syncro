import React from 'react';
import NoteAdd from 'material-ui-icons/NoteAdd';

const ActionIcon = (props) => {
  const { type, className } = props;
  switch (type) {
    case 'create': {
      return (
        <NoteAdd className={className} />
      );
    }
    default: {
      return (
        <NoteAdd className={className} />
      );
    }
  }
};

export default ActionIcon;
