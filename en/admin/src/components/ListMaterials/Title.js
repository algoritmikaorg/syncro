import React from 'react';
import Typography from 'material-ui/Typography';

const Title = (props) => {
  const { title, className } = props;
  return (
    <Typography
      type="title"
      gutterBottom
      className={className}
    >
      {title}
    </Typography>
  );
};

export default Title;
