import React from 'react';

import Actions from './Actions';
import Title from './Title';
import ListMaterialsWithTabs from './ListMaterialsWithTabs';
import ListMaterialsContent from './ListMaterialsContent';
import ListMaterialsContainer from './../../containers/ListMaterialsContainer';

const genActionsItems = (items, { url }) => {
  return items.map((item) => {
    item.to = `${url}/${item.to}`;
    return item;
  });
};

const ListMaterials = (props) => {
  const {
    actionsButtons,
    title,
    match,
    tabs,
    showActions,
  } = props;

  // const actionItems = genActionsItems(actionsButtons, match);
  const actionItems = actionsButtons;

  return (
    <div>

      <Title
        title={title}
      />
      {
        showActions &&
        (
          <Actions
            items={actionItems}
          />
        )
      }

      {
        tabs ?
          (
            <ListMaterialsWithTabs
              {...props}
            />
          ) :
          (
            <ListMaterialsContainer
              {...props}
              component={ListMaterialsContent}
              filterType={'none'}
            />
          )
      }
    </div>
  );
};

export default ListMaterials;
