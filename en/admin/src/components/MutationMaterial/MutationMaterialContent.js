import React, { Component } from 'react';

import ExtForm from './../ExtForm';
import withProgress from './../../hoc/withProgress';
import withSnackbar from './../../hoc/withSnackbar';

class MutationMaterialContent extends Component {
  constructor(props) {
    super(props);
    const {
      fields,
      gqlMutationName,
      mutationResultObjectName,
      gqlQueryName,
      mutationType,
      data,
    } = this.props;

    this.state = {
      gqlMutationNameResult: `${gqlMutationName}Result`,
      gqlMutationNameError: `${gqlMutationName}Error`,
      gqlMutationNameLoading: `${gqlMutationName}Loading`,
      gqlMutationNameReset: `${gqlMutationName}Reset`,
      data,
      isRequested: false,
      isSuccess: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
      gqlMutationNameResult,
      gqlMutationNameError,
      gqlMutationNameLoading,
      gqlMutationNameReset,
    } = this.state;

    const {
      resulMutatiomtMessageSuccses,
      mutationResultObjectName,
    } = this.props;

    if (nextProps[gqlMutationNameResult]) {
      const id = nextProps[gqlMutationNameResult].data[mutationResultObjectName].id
      this.props.snackbar({ message: `${resulMutatiomtMessageSuccses} - id: ${id}`, name: 'Success' });
      this.setState({
        isRequested: false,
        isSuccess: true,
      });
      return false;
    }
    if (nextProps[gqlMutationNameError]) {
      this.props.snackbar(nextProps[gqlMutationNameError]);
      this.props[gqlMutationNameReset]();
      this.setState({
        isRequested: false,
      });
      return false;
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const {
      gqlMutationNameResult,
    } = this.state;
    const {
      mutationResultObjectName,
    } = this.props;

    if (this.state.isSuccess) {
      if (this.props[gqlMutationNameResult] && this.props[gqlMutationNameResult].data) {
        this.props.onSuccessMutation(this.props[gqlMutationNameResult].data[mutationResultObjectName]);
      }
    }
  }
  confirm = async () => {
    const { data } = this.state;
    const { gqlMutationName } = this.props;
    this.props[gqlMutationName]({
      variables: data,
    });
  }

  handleSubmit(data) {
    const state = {
      isRequested: true,
      data,
    };
    this.setState(state, () => {
      this.confirm();
    });
  }

  render() {
    const {
      fields,
      gqlMutationName,
      mutationResultObjectName,
      gqlQueryName,
      gqlQueryItemName,
    } = this.props;

    const { data, isRequested } = this.state;
    let itemData = null;
    let isRequredQuery = null;
    if (gqlQueryName) {
      itemData = this.props[gqlQueryName][gqlQueryItemName];
      isRequredQuery = this.props[gqlQueryName].loading;
    }
    const Form = withProgress([gqlMutationName, gqlQueryName])(ExtForm);
    const isRequestedCurrent =  isRequredQuery || isRequested;

    return (
      <div>
        <Form
          {...this.props}
          fields={fields}
          data={itemData || data}
          addRelationList
          disabledAllInputs={isRequestedCurrent}
          disabledAllActions={isRequestedCurrent}
          actions={['undo', 'save']}
          onSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

MutationMaterialContent.defaultProps = {
  data: [],
  onSuccessMutation() {},
};

export default withSnackbar()(MutationMaterialContent);
