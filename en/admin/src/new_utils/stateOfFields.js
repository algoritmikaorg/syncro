const stateOfFields = (fields, data) => {
  const output = { errors: [] };

  fields.forEach(({ id, defaultValue }) => {
    let value = defaultValue || '';
    if (data && Object.prototype.hasOwnProperty.call(data, id)) value = data[id];
    output[id] = value;
    switch (id) {
      case 'alias': {
        if (data && data.alais) {
          output.alias = data.alias;
          output.aliasId = data.aliasId;
          output.aliasManuallyChanged = data.aliasManuallyChanged;
        } else {
          output.alias = '';
          output.aliasId = '';
          output.aliasManuallyChanged = '';
        }
        break;
      }
      default: {
        output[id] = value;
      }
    }
  });
  return output;
};

export default stateOfFields;
