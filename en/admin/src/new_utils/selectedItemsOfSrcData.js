const selectedItemsOfSrcData = (value, data) => {
  if (!value) return [];
  return data
    .filter(({ id }) => {
      if (Array.isArra(value)) {
        return value.indexOf(id) !== -1;
      }
      return id === value;
    });
};

export default selectedItemsOfSrcData;
