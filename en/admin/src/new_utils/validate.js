import validator from 'validator';
const locale = 'ru-RU';

const validate = (field, state) => {
  const {
    type,
    subType,
    id,
    required,
    enumValues,
  } = field;

  const value = state[id];
  if (required && !value) return false;

  switch (id) {
    case 'email': {
      if (!validator.isEmail(value)) return false;
      break;
    }
    case 'password': {
      if (!validator.isLength(value, { min: 8 })) return false;
      break;
    }
    case 'repassword': {
      if (state.password !== value) return false;
      break;
    }
    case 'phone': {
      if (!validator.isMobilePhone(value, locale)) return false;
      break;
    }
    case 'firstName': {
      // if (!validator.isAlpha(value, locale)) return false;
      if (!/^[А-ЯЁ-]+$/i.test(value)) return false;
      break;
    }
    case 'lastName': {
      // if (!validator.isAlpha(value, locale)) return false;
      if (!/^[А-ЯЁ-]+$/i.test(value)) return false;
      break;
    }
    case 'role': {
      const values = enumValues.map(({ value }) => value);
      if (values.indexOf(value) === -1) return false;
      break;
    }
    default: {
      return true;
    }
  }
  return true;
};

const validateFields = (fields, state) => {
  const output = [];
  fields.forEach((field) => {
    if (field.required || field.validate) {
      const isValid = validate(field, state);
      if (!isValid) output.push(field.id);
    }
  });
  return output;
};

export default validateFields;
