const selectedItems = (selectedIds = [], data = []) => {
  const output = [];
  const selectedIdsArray = Array.isArray(selectedIds) ? selectedIds : [selectedIds];
  return data.filter(n => selectedIds.indexOf(n.id) !== -1);
};

export default selectedItems;
