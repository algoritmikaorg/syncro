const tabsPayment = [
  {
    label: 'Оплаченные',
    id: 'paid',
  },
  {
    label: 'Не оплаченные',
    id: 'unpaid',
  },
];

export default tabsPayment;
