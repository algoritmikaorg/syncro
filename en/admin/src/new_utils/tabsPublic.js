const tabsPublic = [
  {
    label: 'Опубликованные',
    id: 'public',
  },
  {
    label: 'Не опубликованные',
    id: 'not_public',
  },
];

export default tabsPublic;
