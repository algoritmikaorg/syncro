const formatOutputFormData = (formData, fields) => {
  const output = {};
  if (formData.tab) {
    output.tab = formData.tab;
  }
  fields.forEach((field) => {
    const { id, outputType, outputSubType } = field;
    if (formData[id] !== null && formData !== undefined) {
      switch (outputType) {
        case 'lowerCase': {
          output[id] = formData[id] ? formData[id].toLowerCase() : '';
          break;
        }
        case 'id': {
          output[`${id}Id`] = formData[id];
          break;
        }
        case 'number': {
          let value = null;
          if (outputSubType === 'int') value = parseInt(formData[id], 10);
          if (outputSubType === 'float') value = parseFloat(formData[id]);
          if (!isNaN(value)) {
            output[id] = value;
          }
          break;
        }
        case 'array': {
          if (outputSubType === ',') {
            output[id] = formData[id].replace(/\s{0,},\s{0,}/g, ',').split(',');
          } else if (outputSubType === '\n') {
            output[id] = formData[id].replace(/\s{0,}\n\s{0,}/g, '\n').split('\n');
          }
          break;
        }
        case 'alias': {
          output.alias = {
            alias: formData.alias,
          };
          break;
        }
        case 'boolean': {
          output[id] = Boolean(formData[id]);
          break;
        }
        case 'relation': {
          if (outputSubType === 'ids') {
            output[`${id}Ids`] = formData[id].map(item => item.id);
          } else if (Array.isArray(formData[id])) {
            output[`${id}Id`] = formData[id][0].id;
          } else {
            output[`${id}Id`] = formData[id].id;
          }
          break;
        }
        case 'events': {
          output[id] = formData[id].map((event) => {
            const {
              location,
              lecturers,
              curator,
              date,
              time,
              price,
              quantityOfTickets,
            } = event;
            return {
              locationId: location.id,
              lecturersIds: lecturers.map(({ id: lecturerId }) => lecturerId),
              curatorId: curator.id,
              date: new Date(`${date} ${time}`),
              price: parseInt(price, 10),
              quantityOfTickets: parseInt(quantityOfTickets, 10),
            };
          });
          break;
        }
        case 'time': {
          output.date = new Date(`${formData.date} ${formData.time}`);
          break;
        }
        default: {
          output[id] = formData[id];
        }
      }
    }
  });

  return output;
};

export default formatOutputFormData;
