const tabsActive = [
  {
    label: 'Активные',
    id: 'public',
  },
  {
    label: 'Не опубликованные',
    id: 'not_public',
  },
];

export default tabsActive;
