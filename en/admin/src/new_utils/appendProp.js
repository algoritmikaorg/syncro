const appendProp = (obj, p1, p2) => {
  const output = Object.assign({}, obj);
  if (p1 && !output[p1]) {
    output[p1] = {};
  }
  if (p2 && !output[p1][p2]) {
    output[p1][p2] = {};
  }
  return output;
};

export default appendProp;
