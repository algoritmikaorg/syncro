const tabsDate = [
  {
    id: 'active',
    label: 'Активные',
  },
  {
    id: 'expired',
    label: 'Архив',
  },
  {
    label: 'Не опубликованные',
    id: 'not_public',
  },
];

export default tabsDate;
