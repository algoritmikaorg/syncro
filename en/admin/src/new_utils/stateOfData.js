const stateOfData = (fields, data) => {
  const output = { errors: [] };

  fields.forEach(({ id, defaultValue }) => {
    let value = defaultValue || '';
    if (data && Object.prototype.hasOwnProperty.call(data, id)) value = data[id];
    switch (id) {
      case 'alias': {
        if (data && data.aliasId) {
          output.alias = data.alias;
          output.aliasId = data.aliasId;
          output.aliasManuallyChanged = data.aliasManuallyChanged;
        } else {
          output.alias = '';
          output.aliasId = '';
          output.aliasManuallyChanged = '';
        }
        break;
      }
      default: {
        output[id] = value;
      }
    }
  });

  return output;
};

export default stateOfData;
