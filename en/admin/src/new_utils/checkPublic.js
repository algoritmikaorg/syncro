import { PUBLIC } from './../enum';

const checkPublic = (filter, input) => {
  const output = Object.assign({}, filter);

  if (Object.prototype.hasOwnProperty.call(input, 'public')) {
    output.public = input.public === 'true';
  } else if (!Object.prototype.hasOwnProperty.call(input, 'tab')) {
    output.public = PUBLIC[0].value === 'true';
  }
  return output;
};


export default checkPublic;
