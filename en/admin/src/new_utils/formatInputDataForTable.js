import moment from 'moment';
const formatDate = date => moment(date).format('DD.MM.YY HH:mm');
const formatDateShort = date => moment(date).format('DD.MM.YY');
const formatTime = date => moment(date).format('HH:mm');

const formatDataofTypeName = (typename, n) => {
  const output = {};
  switch (typename) {
    case 'Ticket': {
      const {
        user: {
          firstName,
          lastName,
          email,
          phone,
        },
      } = n;
      output.firstName = firstName;
      output.lastName = lastName;
      output.email = email;
      output.phone = phone;
      break;
    }
    case 'Payment': {
      output.type = n.type === 'CASH' ? 'наличные' : 'интернет-эквайринг';
      if (n.order) {
        output.productOfPayment = 'лекция';
      } else if (n.certificate) {
        output.productOfPayment = 'сертификат';
      } else if (n.subscription) {
        output.productOfPayment = 'абонмент';
      }
      break;
    }
    case 'Event': {
      const { curator, lecture, _ticketsMeta } = n;
      const { course, cycle } = lecture;
      if (curator) output.curator = `${curator.firstName} ${curator.lastName}\n${curator.email}`;
      output.lecture = lecture.title;

      if (course && course.length > 0) {
        output.tags = course[0].tags.map(({ title }) => title).join('\n');
        output.course = course[0].title;
      } else if (cycle && cycle.length > 0) {
        output.tags = cycle[0].tags.map(({ title }) => title).join('\n');
        output.cycle = cycle[0].title;
      } else {
        output.course = '';
        output.cycle = '';
        if (lecture.tags) output.tags = lecture.tags.map(({ title }) => title).join('\n');
      }
      if (_ticketsMeta) output._ticketsMeta = _ticketsMeta.count;
      break;
    }
    default: {

    }
  }
  return output;
};

const formatInputDataForTable = (n) => {
  let output = {};
  output.src = Object.assign({}, n);

  if (output.src.createdAt) {
    output.src.createdAt = new Date(output.src.createdAt).getTime();
  }
  if (output.src.updatedAt) {
    output.src.updatedAt = new Date(output.src.updatedAt).getTime();
  }
  if (output.src.date) {
    output.src.date = new Date(output.src.date).getTime();
  }

  const keys = Object.keys(n);
  keys.forEach((key) => {
    switch (key) {
      case '__typename': {
        output = Object.assign({}, output, formatDataofTypeName(n[key], n));
        break;
      }
      case 'order': {
        output.orderId = n[key] ? n[key].id : '';
        break;
      }
      case 'payment': {
        output[key] = n[key].status === 'PAID' ? 'да' : 'нет';
        output.paymentType = n[key].type === 'CASH' ? 'наличные' : 'интернет-эквайринг';
        break;
      }
      case 'firstName': {
        const { firstName } = n;
        output.title = firstName;
        output.firstName = firstName;
        break;
      }
      case 'lastName': {
        const { lastName } = n;
        if (output.title) {
          output.title = `${output.title} ${lastName}`;
        } else {
          output.title = lastName;
        }
        output.lastName = lastName;
        break;
      }
      case 'fullName': {
        const { firstName, lastName } = n;
        output.fullName = `${firstName} ${lastName}`;
        break;
      }
      case 'user': {
        const { firstName, lastName, email } = n.user;
        output.userFullName = `${firstName} ${lastName}`;
        output.userFullNameEmail = `${firstName} ${lastName}\n${email}`;
        break;
      }
      case 'tags': {
        const { tags } = n;
        if (tags.length === 0) {
          output.tags = '';
        } else {
          output.tags = tags.map(tag => tag.title).join(',\n');
        }
        break;
      }
      case 'alias': {
        output.alias = n.alias.alias;
        break;
      }
      case 'createdAt': {
        output[key] = formatDate(n[key]);
        break;
      }
      case 'updatedAt': {
        output[key] = formatDate(n[key]);
        break;
      }
      case 'author': {
        output.author = `${n.author.firstName} ${n.author.lastName}`;
        break;
      }
      case 'events': {
        const lecturers = [];
        const locations = [];
        const dates = [];
        const prices = [];
        const curators = [];
        const lectures = [];
        const courses = [];
        const cycles = [];
        if (n.events && n.events.length > 0) {
          n.events.forEach((event) => {
            if (event.date) {
              const date = formatDate(event.date);
              if (dates.indexOf(date) === -1) dates.push(date);
            }
            if (event.price) {
              if (prices.indexOf(event.price) === -1) prices.push(event.price);
            }
            if (event.lecturers) {
              event.lecturers.forEach(({ firstName, lastName }) => {
                const lecturerFullName = `${firstName} ${lastName}`;
                if (lecturers.indexOf(lecturerFullName) === -1) {
                  lecturers.push(lecturerFullName);
                }
              });
            }
            if (event.location) {
              const locationTitle = event.location.title;
              if (locations.indexOf(locationTitle) === -1) {
                locations.push(locationTitle);
              }
            }
            if (event.curator) {
              const curator = `${event.curator.firstName} ${event.curator.lastName}`;
              if (curators.indexOf(curator) === -1) curators.push(curator);
            }
            if (event.lecture) {
              // const lecture = event.lecture.title;
              lectures.push(event.lecture.title);
              if (event.lecture.course) {
                courses.push(event.lecture.course.map(({ title }) => title).join('\n'));
              }
              if (event.lecture.cycle) {
                cycles.push(event.lecture.cycle.map(({ title }) => title).join('\n'));
              }
            }
          });
        }
        output.lecturers = lecturers.join(',\n');
        output.locations = locations.join(',\n');
        output.dates = dates.join(',\n');
        output.prices = prices.join(',\n');
        output.curators = curators.join(',\n');
        output.lecturesTitle = lectures.join(',\n');
        output.cyclesTitles = cycles.join(',\n');
        output.courseTitles = courses.join(',\n');
        break;
      }
      case 'lecture': {
        output.lectureTitle = n.lecture.title;
        break;
      }
      case 'lectures': {
        const haveEvents = n.lectures.some(lecture => Object.prototype.hasOwnProperty.call(lecture, 'events'));
        if (haveEvents) {
          const lecturers = [];
          const locations = [];
          const events = [];
          if (n.lectures && n.lectures.length > 0) {
            n.lectures.forEach((lecture) => {
              lecture.events.forEach((event) => {
                events.push(event);
                event.lecturers.forEach(({ firstName, lastName }) => {
                  const lecturerFullName = `${firstName} ${lastName}`;
                  if (lecturers.indexOf(lecturerFullName) === -1) {
                    lecturers.push(lecturerFullName);
                  }
                });
                const locationTitle = event.location.title;
                if (locations.indexOf(locationTitle) === -1) {
                  locations.push(locationTitle);
                }
              });
            });
          }
          output.lecturers = lecturers.join(',\n');
          output.locations = locations.join(',\n');
          output.events = events.map(({ date }) => formatDate(date)).join(',\n');
          output.lectures = n.lectures.map(({ title }) => title).join(',\n');
        } else {
          output.lectures = n.lectures.map(({ title }) => title).join(',\n');
        }

        break;
      }
      case 'lecturesCount': {
        if (n.lecturesCount === -1) {
          output[key] = 'безлимит';
        } else {
          output[key] = n[key];
        }
        break;
      }
      case 'discounts': {
        output.discounts = n.discounts.map(({ title }) => title).join(',\n');
        break;
      }
      case 'lecturers': {
        output.lecturers = n.lecturers.map(({ firstName, lastName }) => `${firstName} ${lastName}`).join(',\n');
        break;
      }
      case 'location': {
        output.location = n.location.title;
        break;
      }
      case 'locations': {
        output.locations = n.locations.map(({ title }) => title).join(',\n');
        break;
      }
      case '_ticketsMeta': {
        output.rest = n.quantityOfTickets - n._ticketsMeta.count;
        break;
      }
      case 'buyFull': {
        output.buyFull = n.buyFull ? 'Да' : 'Нет';
        break;
      }
      case 'date': {
        output.date = formatDate(n.date);
        output.dateShort = formatDateShort(n.date);
        output.time = formatTime(n.date);
        if (!output.title) {
          output.title = output.date;
        }
        break;
      }
      case 'start': {
        output.start = formatDate(n.start);
        break;
      }
      case 'end': {
        output.end = formatDate(n.end);
        break;
      }
      case 'validityPeriodFrom': {
        output.validityPeriodFrom = formatDate(n.validityPeriodFrom);
        break;
      }
      case 'validityPeriodTo': {
        output.validityPeriodTo = formatDate(n.validityPeriodTo);
        break;
      }
      case 'public': {
        output[key] = n[key] ? 'Да' : 'Нет';
        break;
      }
      case 'allProducts': {
        output[key] = n[key] ? 'Да' : 'Нет';
        break;
      }
      case 'curator': {
        output[key] = `${n.curator.firstName} ${n.curator.lastName}`;
        break;
      }
      case 'participants': {
        output[key] = n.participants.map(({ firstName, lastName, email }) => `${firstName} ${lastName}\n${email}`).join('\n');
        break;
      }
      default: {
        output[key] = n[key];
      }
    }
  });

  return output;
};

export default formatInputDataForTable;
