const formatInputDataForForm = (data) => {
  const output = {};
  Object.keys(data).forEach((key) => {
    switch (key) {
      case 'author': {
        // output.author = `${data.author.firstName} ${data.author.lastName}`;
        output.author = data.author.id;
        break;
      }
      case 'alias': {
        output.alias = data.alias ? data.alias.alias : '';
        output.aliasId = data.alias ? data.alias.id : undefined;
        output.aliasManuallyChanged = data.alias ? data.alias.manuallyChanged : false;
        break;
      }
      case 'skills': {
        output.skills = data.skills.join(',');
        break;
      }
      case 'themes': {
        output.themes = data.themes.join('\n');
        break;
      }
      case 'lecture': {
        output.lectureId = data.lecture.id;
        output.lecture = data.lecture;
        break;
      }
      case 'user': {
        const { firstName, lastName } = data.user;
        output.user = Object.assign({}, data.user, {
          title: `${firstName} ${lastName}`,
        });
        break;
      }
      default: {
        output[key] = data[key];
      }
    }
  });

  return output;
};

export default formatInputDataForForm;
