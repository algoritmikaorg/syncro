import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';

import LecturersSelect from './../BasicForm/relations/LecturersSelect';
import LocationsSelect from './../BasicForm/relations/LocationsSelect';
import CuratorSelect from './../BasicForm/relations/CuratorSelect';
import { formatInputDataArrayForTable } from './../../new_utils';

import EventDialog from './EventDialog';
import EventsList from './EventsList';

import DialogDeleteItem from './../DialogDeleteItem';
import formatEventDataForForm from './formatEventDataForForm';

const styles = theme => ({
  root: {
    // marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 3,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
});

const dateTimeOfEvent = (event) => {
  const formatedData = formatEventDataForForm(event);
  return `${formatedData.date} ${formatedData.time}`;
};

class EventOfLectureNoMutations extends Component {
  constructor(props) {
    super(props);
    const {
      location,
      lecturers,
      curator,
      price,
      quantityOfTickets,
      lectureId,
      value,
    } = props;

    this.state = {
      open: false,
      events: value,
      eventsForTable: formatInputDataArrayForTable(value.slice()),
      itemToDelete: null,
      itemToEdit: null,
      location,
      lecturers,
      curator,
      lectureId: 'draft',
      price,
      quantityOfTickets,
    };
    this.handleShowDialog = this.handleShowDialog.bind(this);
    this.handleDialogSubmit = this.handleDialogSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeNoEvents = this.handleChangeNoEvents.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const {
      value,
    } = nextProps;

    this.setState({
      events: value,
      eventsForTable: formatInputDataArrayForTable(value.slice()),
    });
  }
  handleChange = id => (event) => {
    this.setState({
      [id]: event.target.value,
    });
  }
  handleChangeNoEvents = id => (selectedIds, selected) => {
    this.setState({
      [id]: selected || selectedIds,
    });
  }
  handleShowDialog() {
    this.setState({
      open: !this.state.open,
    });
  }

  handleDialogSubmit(newEvent) {
    const { itemToEdit } = this.state;
    let newEvents = [];
    if (itemToEdit !== null) {
      newEvents = this.state.events.map((event, index) => {
        if (index === itemToEdit) {
          return newEvent;
        }
        return event;
      });
    } else {
      newEvents = this.state.events.slice();
      newEvents.push(newEvent);
    }
    this.setState({
      open: !this.state.open,
      events: newEvents,
      eventsForTable: formatInputDataArrayForTable(newEvents.slice()),
    }, () => {
      // this.props.onChange(newEvents);
      this.props.onSelect(null, newEvents);
    });
  }
  handleEdit = index => () => {
    this.setState({
      open: true,
      itemToEdit: index,
    });
  }
  handleDelete = index => () => {
    this.setState({
      itemToDelete: index,
    });
  }
  handleCancelDelete() {
    this.setState({
      itemToDelete: null,
    });
  }
  handleDeleteConfirm() {
    const newEvents = this.state.events;
    newEvents.splice(this.state.itemToDelete, 1);

    this.setState({
      events: newEvents,
      eventsForTable: formatInputDataArrayForTable(newEvents.slice()),
      itemToDelete: null,
    },  () => {
      // this.props.onChange(newEvents);
      this.props.onSelect(null, newEvents);
    });
  }
  render() {
    const {
      classes,
    } = this.props;
    const {
      open,
      lecturers,
      location,
      curator,
      price,
      quantityOfTickets,
      lectureId,
      events,
      eventsForTable,
      itemToDelete,
      itemToEdit,
    } = this.state;

    return (
      <div className={classes.root}>
        <Typography type="title" gutterBottom>
          Даты проведения лекции
        </Typography>
        <div className={classes.container}>
          <LecturersSelect
            value={lecturers}
            id="lecturers"
            onChangeNoEvent={this.handleChangeNoEvents}
            multiple
          />
          <LocationsSelect
            value={location}
            id="location"
            onChangeNoEvent={this.handleChangeNoEvents}
          />
          <CuratorSelect
            value={curator}
            id="curator"
            onChangeNoEvent={this.handleChangeNoEvents}
          />
          <div>
            <TextField
              id={'price'}
              label={'Цена билета'}
              value={price}
              onChange={this.handleChange('price')}
              className={classes.textField}
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            />
            <TextField
              id={'quantityOfTickets'}
              label={'Количество билетов'}
              value={quantityOfTickets}
              onChange={this.handleChange('quantityOfTickets')}
              className={classes.textField}
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            />
          </div>
        </div>
        <div>
          <Button
            raised
            color="primary"
            className={classes.button}
            onClick={this.handleShowDialog}
          >
              Добавить дату
          </Button>
        </div>
        <EventsList
          data={eventsForTable}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
        />
        {
          <EventDialog
            open={open}
            data={events[itemToEdit]}
            onClose={this.handleShowDialog}
            onSubmit={this.handleDialogSubmit}
            lecturers={lecturers}
            location={location}
            curator={curator}
            price={price}
            quantityOfTickets={quantityOfTickets}
            lectureId={lectureId}
          />
        }
        <DialogDeleteItem
          open={itemToDelete !== null ? true : false}
          title={itemToDelete !== null ? `${dateTimeOfEvent(events[itemToDelete])}` : ''}
          onCancel={this.handleCancelDelete}
          onConfirm={this.handleDeleteConfirm}
        />
      </div>
    );
  }
}

EventOfLectureNoMutations.propTypes = {
  classes: PropTypes.object.isRequired,
};

EventOfLectureNoMutations.defaultProps = {
  lecturersIds: [],
  locationId: '',
  price: '',
  quantityOfTickets: '',
  lectureId: 'draft',
  value: [],
  onChange() {},
};

export default withStyles(styles)(EventOfLectureNoMutations);
