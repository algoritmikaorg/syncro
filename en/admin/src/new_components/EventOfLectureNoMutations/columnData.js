const columnData = [
  {
    id: 'dateShort',
    label: 'Дата',
  },
  {
    id: 'time',
    label: 'Время',
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
  },
  {
    id: 'location',
    label: 'Локация',
  },
  {
    id: 'curator',
    label: 'Координатор',
  },
  {
    id: 'price',
    label: 'Цена',
  },
  {
    id: 'quantityOfTickets',
    label: 'Количество билетов',
  },
];
export default columnData;
