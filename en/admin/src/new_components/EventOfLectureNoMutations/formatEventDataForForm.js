import moment from 'moment';

const formatEventDataForForm = (data) => {
  const output = {};
  if (!data) return output;
  Object.keys(data).forEach((key) => {
    switch (key) {
      // case 'lecturers': {
      //   output.lecturersIds = data.lecturers.map(({ id }) => id);
      //   break;
      // }
      // case 'location': {
      //   output.locationId = data.location.id;
      //   break;
      // }
      // case 'curator': {
      //   output.curatorId = data.curator.id;
      //   break;
      // }
      case 'date': {
        const eventDate = moment(data[key]);
        output[key] = eventDate.format('YYYY-MM-DD');
        output.time = eventDate.format('HH:mm');
        break;
      }
      default: {
        if (data[key]) output[key] = data[key];
      }
    }
  });

  return output;
};

export default formatEventDataForForm;
