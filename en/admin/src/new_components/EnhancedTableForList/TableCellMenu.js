import React, { Component } from 'react'
import { TableCell } from 'material-ui/Table';
import IconButton from 'material-ui/IconButton';
import Menu, { MenuItem } from 'material-ui/Menu';
import MoreVertIcon from 'material-ui-icons/MoreVert';

const options = [
  {
    id: 'edit',
    label: 'Редактировать',
  },
  {
    id: 'delete',
    label: 'Удалить',
  },
  {
    id: 'view',
    label: 'Просмотреть',
  },
];

const ITEM_HEIGHT = 48;

class TableCellMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClick(event) {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleClose(event, id) {
    this.setState({ anchorEl: null }, () => {
      switch (id) {
        case 'edit': {
          this.props.onEdit();
          break;
        }
        case 'delete': {
          this.props.onDelete();
          break;
        }
        case 'view': {
          this.props.onView();
        }
        default: {}
      }
    });
  }

  render() {
    const open = Boolean(this.state.anchorEl);
    const {
      onEdit,
      onView,
      onDelete,
      showDeleteAction,
    } = this.props;

    let currentOptions = options.slice();
    if (!showDeleteAction) {
      currentOptions = currentOptions.filter(({ id }) => id !== 'delete');
    }
    return (
      <TableCell padding="checkbox">
        <div>
          <IconButton
            aria-label="Меню"
            aria-owns={open ? 'long-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}
          >
            <MoreVertIcon />
          </IconButton>
          <Menu
            id="long-menu"
            anchorEl={this.state.anchorEl}
            open={open}
            onClose={this.handleClose}
            PaperProps={{
              style: {
                maxHeight: ITEM_HEIGHT * 4.5,
                width: 200,
              },
            }}
          >
            {currentOptions.map(({ id, label }) => (
              <MenuItem
                key={id}
                onClick={(event) => { this.handleClose(event, id) }}
              >
                {label}
              </MenuItem>
            ))}
          </Menu>
        </div>
      </TableCell>
    );
  }
}

export default TableCellMenu;
