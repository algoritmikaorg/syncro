import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';

import Button from 'material-ui/Button';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import green from 'material-ui/colors/green';
import red from 'material-ui/colors/red';
import blue from 'material-ui/colors/blue';
import lightBlue from 'material-ui/colors/lightBlue';
import yellow from 'material-ui/colors/yellow';

import { ulid } from './../../../node_modules/ulid/dist/index.umd.js';

import { sort } from './../../new_utils';

import generateOutputFile from './../../libs/generateOutputFile';
import EnhancedTableHead from './EnhancedTableHead';
import TableCellMenu from './TableCellMenu';

let counter = 0;
function createData(title, calories, fat, carbs, protein) {
  counter += 1;
  return { id: counter, title, calories, fat, carbs, protein };
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 800,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  rowDefault: {},
  rowRed: {
    backgroundColor: red[200],
    color: '#fff',
  },
  rowGreen: {
    backgroundColor: green[200],
    color: '#fff',
  },
  rowBlue: {
    backgroundColor: blue[200],
    color: '#fff',
  },
  rowLightBlue: {
    backgroundColor: lightBlue[200],
    color: '#fff',
  },
  rowYellow: {
    backgroundColor: yellow[200],
    color: '#fff',
  },
});

class EnhancedTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    const {
      page,
      rowsPerPage,
      order,
      orderBy,
      data,
    } = props;

    this.state = {
      order,
      orderBy,
      data: sort(data, orderBy, order),
      page,
      rowsPerPage,
    };
    this.handleGenerateOutputFile = this.handleGenerateOutputFile.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const { orderBy, order } = this.state;
    const { data } = nextProps;

    this.setState({
      data: sort(data, orderBy, order),
    });
  }
  handleRequestSort = (event, property) => {
    const orderBy = property;
    const {
      data: dataState,
      order: orderState,
      orderBy: orderByState,
    } = this.state;

    let order = 'asc';

    if (orderByState === property && orderState === 'asc') order = 'desc';

    const data = sort(dataState, orderBy, order);

    this.setState({ data, order, orderBy });
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  }

  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value });
  }

  handleEdit = n => (event) => {
    this.props.onEdit(n);
  }
  handleDelete = n => (event) => {
    this.props.onDelete(n);
  }
  handleView = n => (event) => {
    this.props.onView(n);
  }

  handleGenerateOutputFile() {
    generateOutputFile(this.state.data, this.props.columnData);
  }

  handleClick() {

  }

  render() {
    const {
      classes,
      title,
      columnData,
      tableTitle,
      toolbarActions,
      showDeleteAction,
      showMenu,
      coloredRow,
    } = this.props;

    const {
      data,
      order,
      orderBy,
      rowsPerPage,
      page,
    } = this.state;

    let emptyRows = rowsPerPage - Math.min(rowsPerPage, (data.length - page) * rowsPerPage);
    if (data.length === 0) emptyRows = 0;

    return (
      <div>
        <Paper className={classes.root}>
          <div className={classes.tableWrapper}>
            <Table className={classes.table}>
              <EnhancedTableHead
                order={order}
                orderBy={orderBy}
                onRequestSort={this.handleRequestSort}
                rowCount={data.length}
                columnData={columnData}
              />
              <TableBody>
                {data.slice(page * rowsPerPage, (page * rowsPerPage) + rowsPerPage).map((n) => {
                  return (
                    <TableRow
                      hover
                      tabIndex={-1}
                      key={ulid()}
                      className={coloredRow(classes, n)}
                    >
                      {
                        showMenu &&
                        <TableCellMenu
                          onEdit={this.handleEdit(n)}
                          onDelete={this.handleDelete(n)}
                          onView={this.handleView(n)}
                          showDeleteAction={showDeleteAction}
                        />
                      }

                      {
                        columnData.map((column) => {
                          const {
                            id,
                            label,
                            disablePadding,
                            numeric,
                            component: CellComponent,
                          } = column;
                          if (id === 'component') {
                            return (
                              <TableCell key={id} padding="checkbox">
                                <CellComponent
                                  {...n}
                                />
                              </TableCell>
                            );
                          }
                          return (
                            <TableCell
                              key={id}
                              padding={disablePadding ? 'none' : 'default'}
                              numeric={numeric || false}
                            >
                              {n[id] || ''}
                            </TableCell>
                          );
                        })
                      }

                    </TableRow>
                  );
                })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </div>
          <Button
            raised
            color="primary"
            onClick={this.handleGenerateOutputFile}
          >
            Сохранить в файл
          </Button>
        </Paper>
      </div>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

EnhancedTable.defaultProps = {
  showMenu: true,
  data: [],
  order: 'desc',
  orderBy: 'updatedAt',
  page: 0,
  rowsPerPage: 10,
  columnData: [],
  onSelect() {},
  onEdit() {},
  onDelete() {},
  onView() {},
  coloredRow(classes, item) { return classes.rowDefault },
};

export default withStyles(styles)(EnhancedTable);
