import React, { Component } from 'react';
// import { withStyles } from 'material-ui/styles';
import Avatar from 'material-ui/Avatar';

import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import AccountCircle from 'material-ui-icons/AccountCircle';
import Switch from 'material-ui/Switch';
import { FormControlLabel, FormGroup } from 'material-ui/Form';
import Menu, { MenuItem } from 'material-ui/Menu';
import Badge from 'material-ui/Badge';
import MailIcon from 'material-ui-icons/Mail';

class Account extends Component {
  state = {
    anchorEl: null,
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleLogout = () => {
    localStorage.clear();
    window.location.reload();
  }

  render() {
    const { anchorEl } = this.state;
    const { account, messages } = this.props;
    const open = Boolean(anchorEl);

    return (
      <div>
        <IconButton color="inherit">
          {
            messages.lemgth > 0 ?
            (
              <Badge badgeContent={messages.length} color="inherit">
                <MailIcon color="inherit" />
              </Badge>
            ) :
            (
              <MailIcon color="inherit" />
            )
          }

        </IconButton>

        <IconButton
          aria-owns={open ? 'menu-appbar' : null}
          aria-haspopup="true"
          onClick={this.handleMenu}
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={open}
          onClose={this.handleClose}
        >
          <MenuItem disabled>{account.firstName} {account.lastName}</MenuItem>
          <MenuItem disabled>{account.role}</MenuItem>
          <MenuItem onClick={this.handleLogout}>Выйти</MenuItem>
        </Menu>
      </div>

    );
  }
}

Account.defaultProps = {
  messages: [],
  account: {},
};

export default Account;
