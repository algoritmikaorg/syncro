import React from 'react';
import Dialog from 'material-ui/Dialog';

import DialogInner from './DialogInner';

const Login = props => (
  <Dialog open={props.open} aria-labelledby="auth-dialog-title">
    <DialogInner {...props} />
  </Dialog>
);

export default Login;
