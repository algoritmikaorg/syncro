import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';

import BasicFields from './../BasicForm/BasicFields';
import { stateOfFields, stateOfData, validate } from './../../new_utils';

import {
  LOGGED_IN_USER_WITH_ROLE,
  AUTHENTICATE_USER_MUTATION,
  USER_RESET_PASS_SEND_EMAIL_MUTATION,
} from './../../constants';

const userResetPassSendEmailResult = `${USER_RESET_PASS_SEND_EMAIL_MUTATION}Result`;
const userResetPassSendEmailReset = `${USER_RESET_PASS_SEND_EMAIL_MUTATION}Reset`;

class Form extends Component {
  constructor(props) {
    super(props);
    const { fields } = props;
    this.state = stateOfFields(fields);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[userResetPassSendEmailResult]
      && nextProps[userResetPassSendEmailResult].data
      && nextProps[userResetPassSendEmailResult].data.userResetPassToken
    ) {
      nextProps[userResetPassSendEmailReset]();
      nextProps.onChangeFormType('signIn');
    }
  }
  handleChange = name => (event) => {
    const { fields } = this.props;
    const newState = Object.assign({}, this.state);
    let { value } = event.target;

    if (newState.errors.length > 0) {
      newState.errors = validate(fields, newState);
    }

    newState[name] = value;
    // this.setState(newState, this.props.onChange(newState));
    this.setState(newState);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { onSubmit, fields } = this.props;
    const errors = validate(fields, this.state);
    if (errors.length > 0) {
      this.setState({
        errors,
      });
    } else {
      onSubmit(this.state);
    }
  }

  render() {
    const {
      email,
      password,
      errors,
    } = this.state;
    const {
      isRequested,
      onSubmit,
      onChangeFormType,
      type,
      fields,
    } = this.props;
    const disabled = isRequested;

    return (
      <form
        noValidate
        autoComplete="off"
        onSubmit={this.handleSubmit}
      >
        <DialogContent>
          <BasicFields
            fields={fields}
            errors={errors}
            values={this.state}
            onChange={this.handleChange}
            disabledAllInputs={disabled}
          />
          {/* <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email"
            type="email"
            fullWidth
            value={email}
            error={errors.indexOf('email') !== -1}
            disabled={disabled}
          />
          <TextField
            id="password"
            label="Пароль"
            type="password"
            margin="normal"
            fullWidth
            onChange={this.handleChange('password')}
            value={password}
            error={errors.indexOf('password') !== -1}
            disabled={disabled}
          /> */}
        </DialogContent>
          {
            type === 'signIn' &&
            <DialogActions>
              <Button
                color="primary"
                disabled={disabled}
                onClick={() => { onChangeFormType('resetPassEmail') }}
                >
                Забыли пароль?
              </Button>
              <Button
                type="submit"
                onClick={this.handleSubmit}
                color="primary"
                disabled={disabled}
                >
                  Войти
                </Button>
            </DialogActions>
          }
          {
            (type === 'resetPassEmail' || type === 'resetPass') &&
            <DialogActions>
              <Button
                color="primary"
                disabled={disabled}
                onClick={() => { onChangeFormType('signIn') }}
                >
                Отменить
              </Button>
              <Button
                type="submit"
                onClick={this.handleSubmit}
                color="primary"
                disabled={disabled}
                >
                  Отправить
                </Button>
            </DialogActions>
          }
      </form>
    );
  }
}

export default Form;
