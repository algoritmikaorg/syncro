import React from 'react';
import BasicForm from './../BasicForm';

const FilterOfQuery = (props) => {
  const { fields, formTitle, isRequested, onSubmit, data } = props;

  return (
    <div>
      <BasicForm
        formTitle={formTitle}
        fields={fields}
        data={data}
        disabledAllInputs={isRequested}
        disabledAllActions={isRequested}
        actions={['reset', 'search']}
        onSubmit={onSubmit}
      />
    </div>
  );
};

export default FilterOfQuery;
