import React from 'react';

import { formatInputDataArrayForTable } from './../../new_utils';
import EnhancedTableForList from './../../new_components/EnhancedTableForList';

import tagsColumnData from './../../new_pages/Tags/columnData';
import {
  lecturesShort as lecturesColumnData,
  coursesShort as coursesColumnData,
  cyclesShort as cyclesColumnData,
  subscriptionsShort as subscriptionsColumnData,
  certificatesShort as certificatesColumnData,
} from './../../new_pages/Catalog/columnData';

import ordersColumnDataUser from './../../new_pages/Orders/columnDataUser';
import paymentsColumnDataUser from './../../new_pages/Payments/columnDataUser';
import eventsColumnData from './../../new_pages/Events/columnData';
import eventsColumnDataShort from './../../new_pages/Events/columnDataShort';
import eventsColumnDataCurator from './../../new_pages/Events/columnDataShortCurator';
import lecturersColumnData from './../../new_pages/Lecturers/columnDataShort';
import discountsColumnData from './../../new_pages/Discounts/columnDataShort';
import reviewsColumnData from './../../new_pages/Reviews/columnDataShort';
import usersColumnData from './../../new_pages/Users/columnData';

const lecturesOfEvents = (events) => {
  const output = [];
  const ids = [];
  events.forEach(({ lecture }) => {
    const { id } = lecture;
    if (ids.indexOf(id) === -1) {
      output.push(lecture);
      ids.push(id);
    }
  });
  return output;
};

const ViewRelationField = (props) => {
  const {
    data,
    field,
    history,
  } = props;

  const {
    id,
    relation,
  } = field;

  if (!data) return (<div>Нет данных</div>);

  switch (relation) {
    case 'users': {
      return (
        <EnhancedTableForList
          columnData={usersColumnData}
          data={formatInputDataArrayForTable(data.users || [data.user] || [])}
          onView={(n) => {
            history.push(`/users/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/users/update/${n.id}`);
          }}
        />
      );
    }
    case 'participants': {
      return (
        <EnhancedTableForList
          columnData={usersColumnData}
          data={formatInputDataArrayForTable(data.participants || data.users || [])}
          onView={(n) => {
            history.push(`/users/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/users/update/${n.id}`);
          }}
        />
      );
    }
    case 'tags': {
      return (
        <EnhancedTableForList
          columnData={tagsColumnData}
          data={formatInputDataArrayForTable(data.tags)}
          onView={(n) => {
            history.push(`/tags/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/tags/update/${n.id}`);
          }}
        />
      );
    }
    case 'lecturesOfEvents': {
      const lectures = lecturesOfEvents(data.events);
      const tableData = formatInputDataArrayForTable(lectures);
      return (
        <EnhancedTableForList
          columnData={lecturesColumnData}
          data={tableData}
          onView={(n) => {
            history.push(`/catalog/lectures/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/lectures/update/${n.id}`);
          }}
        />
      );
    }
    case 'allLectures': {
      return (
        <EnhancedTableForList
          columnData={lecturesColumnData}
          data={formatInputDataArrayForTable(data.allLectures)}
          onView={(n) => {
            history.push(`/catalog/lectures/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/lectures/update/${n.id}`);
          }}
        />
      );
    }
    case 'lectures': {
      return (
        <EnhancedTableForList
          columnData={lecturesColumnData}
          data={formatInputDataArrayForTable(data.lectures)}
          onView={(n) => {
            history.push(`/catalog/lectures/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/lectures/update/${n.id}`);
          }}
        />
      );
    }
    case 'lecturesCreated': {
      return (
        <EnhancedTableForList
          columnData={lecturesColumnData}
          data={formatInputDataArrayForTable(data.lecturesCreated)}
          onView={(n) => {
            history.push(`/catalog/lectures/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/lectures/update/${n.id}`);
          }}
        />
      );
    }
    case 'recommendedLectures': {
      return (
        <EnhancedTableForList
          columnData={lecturesColumnData}
          data={formatInputDataArrayForTable(data.recommendedLectures)}
          onView={(n) => {
            history.push(`/catalog/lectures/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/lectures/update/${n.id}`);
          }}
        />
      );
    }
    case 'discounts': {
      let discounts = null;
      if (data.payment) {
        discounts = data.payment.discounts.concat(data.payment.additionalDiscounts || []);
      } else {
        discounts = data.discounts.concat(data.additionalDiscounts || []);
      }
      return (
        <EnhancedTableForList
          columnData={discountsColumnData}
          data={formatInputDataArrayForTable(discounts)}
          onView={(n) => {
            history.push(`/discounts/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/discounts/update/${n.id}`);
          }}
        />
      );
    }
    case 'lecturers': {
      return (
        <EnhancedTableForList
          columnData={lecturersColumnData}
          data={formatInputDataArrayForTable(data.lecturers)}
          onView={(n) => {
            history.push(`/lecturers/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/lecturers/update/${n.id}`);
          }}
        />
      );
    }
    case 'courses': {
      return (
        <EnhancedTableForList
          columnData={coursesColumnData}
          data={formatInputDataArrayForTable(data.courses)}
          onView={(n) => {
            history.push(`/catalog/courses/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/courses/update/${n.id}`);
          }}
        />
      );
    }
    case 'coursesCreated': {
      return (
        <EnhancedTableForList
          columnData={coursesColumnData}
          data={formatInputDataArrayForTable(data.coursesCreated)}
          onView={(n) => {
            history.push(`/catalog/courses/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/courses/update/${n.id}`);
          }}
        />
      );
    }
    case 'course': {
      return (
        <EnhancedTableForList
          columnData={coursesColumnData}
          data={formatInputDataArrayForTable(data.course)}
          onView={(n) => {
            history.push(`/catalog/courses/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/courses/update/${n.id}`);
          }}
        />
      );
    }
    case 'cycle': {
      return (
        <EnhancedTableForList
          columnData={cyclesColumnData}
          data={formatInputDataArrayForTable(data.cycle)}
          onView={(n) => {
            history.push(`/catalog/cycles/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/cycles/update/${n.id}`);
          }}
        />
      );
    }
    case 'cycles': {
      return (
        <EnhancedTableForList
          columnData={cyclesColumnData}
          data={formatInputDataArrayForTable(data.cycles)}
          onView={(n) => {
            history.push(`/catalog/cycles/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/cycles/update/${n.id}`);
          }}
        />
      );
    }
    case 'cyclesCreated': {
      return (
        <EnhancedTableForList
          columnData={cyclesColumnData}
          data={formatInputDataArrayForTable(data.cyclesCreated)}
          onView={(n) => {
            history.push(`/catalog/cycles/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/cycles/update/${n.id}`);
          }}
        />
      );
    }
    case 'subscriptions': {
      let subscriptions = null;
      if (data.payment) {
        subscriptions = data.payment.subscriptionsInPayments;
      } else {
        subscriptions = data.subscriptions;
      }

      return (
        <EnhancedTableForList
          columnData={subscriptionsColumnData}
          data={formatInputDataArrayForTable(subscriptions)}
          onView={(n) => {
            history.push(`/catalog/subscriptions/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/subscriptions/update/${n.id}`);
          }}
        />
      );
    }
    case 'certificates': {
      let certificates = null;
      if (data.payment) {
        certificates = data.payment.certificatesInPayments;
      } else {
        certificates = data.certificates;
      }

      return (
        <EnhancedTableForList
          columnData={certificatesColumnData}
          data={formatInputDataArrayForTable(certificates)}
          onView={(n) => {
            history.push(`/catalog/certificates/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/catalog/certificates/update/${n.id}`);
          }}
        />
      );
    }
    case 'payments': {
      return (
        <EnhancedTableForList
          columnData={paymentsColumnDataUser}
          data={formatInputDataArrayForTable(data.payments || [data.payment] || [])}
          onView={(n) => {
            history.push(`/payments/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/payments/update/${n.id}`);
          }}
        />
      );
    }
    case 'orders': {
      return (
        <EnhancedTableForList
          columnData={ordersColumnDataUser}
          data={formatInputDataArrayForTable(data.orders)}
          onView={(n) => {
            history.push(`/orders/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/orders/update/${n.id}`);
          }}
        />
      );
    }
    case 'eventsOfLectures': {
      return (
        <EnhancedTableForList
          columnData={eventsColumnData}
          data={formatInputDataArrayForTable(data.events)}
          onView={(n) => {
            history.push(`/lectures/view/${n.lecture.id}`);
          }}
          onEdit={(n) => {
            history.push(`/events/update/${n.lecture.id}`);
          }}
        />
      );
    }
    case 'events': {
      return (
        <EnhancedTableForList
          columnData={eventsColumnDataShort}
          data={formatInputDataArrayForTable(data.events)}
          // onView={(n) => {
          //   history.push(`/events/view/${n.id}`);
          // }}
          // onEdit={(n) => {
          //   history.push(`/events/update/${n.id}`);
          // }}
          // onView={(n) => {
          //   history.push(`/lectures/view/${n.id}`);
          // }}
          // onEdit={(n) => {
          //   history.push(`/events/update/${n.id}`);
          // }}
        />
      );
    }
    case 'curator': {
      return (
        <EnhancedTableForList
          columnData={eventsColumnDataCurator}
          data={formatInputDataArrayForTable(data.curator)}
          onView={(n) => {
            history.push(`/users/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/users/update/${n.id}`);
          }}
          // onView={(n) => {
          //   history.push(`/lectures/view/${n.id}`);
          // }}
          // onEdit={(n) => {
          //   history.push(`/events/update/${n.id}`);
          // }}
        />
      );
    }
    case 'reviews': {
      return (
        <EnhancedTableForList
          columnData={reviewsColumnData}
          data={formatInputDataArrayForTable(data.reviews)}
          onView={(n) => {
            history.push(`/reviews/view/${n.id}`);
          }}
          onEdit={(n) => {
            history.push(`/reviews/update/${n.id}`);
          }}
        />
      );
    }
    default: {
      return (
        <div>Нет связанных компонетов</div>
      );
    }
  }
};

export default ViewRelationField;
