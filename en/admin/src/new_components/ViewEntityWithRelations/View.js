import React from 'react';

import ViewBasicFields from './ViewBasicFields';
import ViewRelationFields from './ViewRelationFields';

const View = (props) => {
  const {
    gqlQueryName,
    queryResultObjectName,
    fields,
    history,
  } = props;
  let data = null;
  if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
    data = props[gqlQueryName][queryResultObjectName];
  }

  return (
    <div>
      <ViewBasicFields
        fields={fields}
        data={data}
      />
      <ViewRelationFields
        fields={fields}
        data={data}
        history={history}
      />
    </div>
  );
};

export default View;
