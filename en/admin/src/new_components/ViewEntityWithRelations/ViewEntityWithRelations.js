import React, { Component } from 'react';

import QueryWithQueryOptions from './../../new_containers/QueryWithQueryOptions';

import View from './View';

const modifyQueryOptions = (ownProps) => {
  const { id } = ownProps.match.params;
  return (
    {
      variables: {
        id,
      },
    }
  );
};

const ViewEntityWithRelations = (props) => {
  const {
    gql,
    gqlQueryName,
    queryResultObjectName,
    match,
  } = props;

  return (
    <QueryWithQueryOptions
      gql={gql}
      gqlQueryName={gqlQueryName}
      queryResultObjectName={queryResultObjectName}
      modifyQueryOptions={modifyQueryOptions}
      match={match}
    >
      <View
        {...props}
      />
    </QueryWithQueryOptions>
  );
};


export default ViewEntityWithRelations;
