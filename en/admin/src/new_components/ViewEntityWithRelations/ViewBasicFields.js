import React from 'react';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';

import { formatInputDataForTable } from './../../new_utils';

import ViewBasicField from './ViewBasicField';

const ViewBasicFields = (props) => {
  const {
    fields,
    data,
  } = props;

  let formatedData = {};
  if (data) {
    formatedData = formatInputDataForTable(data);
  }

  const currentFileds = fields
    .filter(({ type }) => type !== 'relation');

  return (
    <div>
      {
        currentFileds.map(field => (
          <div key={field.id} style={{ marginBottom: 20, marginTop: 20 }}>
            <Typography type="body2" gutterBottom>
              {field.label}:
            </Typography>

            <ViewBasicField
              formatedData={formatedData}
              field={field}
            />
            <Divider />
          </div>
        ))
      }
    </div>
  );
};

export default ViewBasicFields;
