const queryOptions = certificate => (ownProps) => {
  return { variables: { order: certificate.payment.data } };
};

export default queryOptions;
