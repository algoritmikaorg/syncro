import React, { Component } from 'react';
import moment from 'moment';

import checkResultOfCheck from './../../CertificateOrder/MutationOrder/checkResultOfCheck';

class MutationOrder extends Component {
  constructor(props) {
    super(props);
    const {
      gqlMutationName,
      subscription,
      id,
      paymentId,
    } = props;

    this.state = {
      subscription,
      id,
      paymentId,
      checkOrderCertAndSub: null,
      gqlMutationNameResult: `${gqlMutationName}Result`,
      gqlMutationNameError: `${gqlMutationName}Error`,
      gqlMutationNameLoading: `${gqlMutationName}Loading`,
      gqlMutationNameReset: `${gqlMutationName}Reset`,
    };
  }
  componentWillReceiveProps(nextProps) {
    const {
      gqlMutationNameResult,
      gqlMutationNameError,
      gqlMutationNameLoading,
      gqlMutationNameReset,
      checkOrderCertAndSub,
      subscription,
      id,
      paymentId,
    } = this.state;

    const {
      mutationResultObjectName,
      resultMutationMessageSuccses,
      gqlQueryName,
      queryResultObjectName,
      gqlMutationName,
    } = nextProps;

    if (!checkOrderCertAndSub && nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      const checkResults = checkResultOfCheck(subscription.payment.data, nextProps[gqlQueryName][queryResultObjectName]);
      const { acquiring } = nextProps[gqlQueryName].allSettings[0];

      if (!checkResults.resultOfChecking && !checkResults.resultOfCalcOrder) {
        subscription.payment.data.order = checkResults.order;
        let variables = null;

        if (id && paymentId) {
          delete subscription.payment.userId;
          variables = Object.assign(
            {},
            subscription,
            subscription.payment,
            {
              id,
              paymentId,
            },
          );
        } else {
          subscription.payment = Object.assign({}, subscription.payment, {
            acquiringId: acquiring.id,
            commission: acquiring.commission,
          });
          variables = subscription;
        }

        this.props[gqlMutationName]({
          variables,
        });
      }
      return this.setState({
        checkOrderCertAndSub: checkResults,
      });
    }

    if (nextProps[gqlMutationNameResult]) {
      const result = nextProps[gqlMutationNameResult].data[mutationResultObjectName];
      this.props.snackbar({ message: `${resultMutationMessageSuccses} - id: ${result.id}`, name: 'Success' });
    }
    if (nextProps[gqlMutationNameError]) {
      const result = nextProps[gqlMutationNameError].errors;
      this.props.snackbar(nextProps[gqlMutationNameError].errors || nextProps[gqlMutationNameError]);
    }
  }
  componentDidUpdate() {
    const {
      gqlMutationNameResult,
    } = this.state;

    const {
      mutationResultObjectName,
      onSuccessMutation,
    } = this.props;

    if (this.props[gqlMutationNameResult]
      && this.props[gqlMutationNameResult].data
      && onSuccessMutation instanceof Function
    ) {
      onSuccessMutation(this.props[gqlMutationNameResult].data[mutationResultObjectName], this.props);
    }
  }
  render() {
    const {
      checkOrderEvents,
    } = this.state;

    if (!checkOrderEvents) {
      return (
        <div>Проверка заказа</div>
      );
    }

    if (checkOrderEvents && (checkOrderEvents.resultOfChecking || checkOrderEvents.resultOfCalcOrder)) {
      const { resultOfChecking, resultOfCalcOrder } = checkOrderEvents;
      return (
        <div>
          <h3 className="error">
            Невозможно произвести данный заказ
          </h3>
          {
            resultOfChecking && resultOfChecking.EntityNoPublic &&
            (
              <p>
                Материал снят с публикации
              </p>
            )
          }
          {
            resultOfChecking && resultOfChecking.allEventsNoTicketsAvailable &&
            (
              <div>
                <p>
                  Нет достаточного количества билетов:
                </p>
                <ul>
                  {
                    resultOfChecking.allEventsNoTicketsAvailable.map(({ id, title, date }) => (
                      <li key={id}>{title} - {moment(date).format('LLLL')}</li>
                    ))
                  }
                </ul>
              </div>
            )
          }
          {
            resultOfChecking && resultOfChecking.DiscountsWithErrors &&
            (
              <div>
                <p>
                  Скидки не доступны.
                </p>
                <ul>
                  {
                    resultOfChecking.DiscountsWithErrors.map(({ id, title }) => (
                      <li key={id}>{title}</li>
                    ))
                  }
                </ul>
              </div>
            )
          }
          {
            resultOfChecking && resultOfChecking.PromocodesWithErrors &&
            (
              <div>
                <p>Сертификат или абонемент не доступны</p>
                <ul>
                  {
                    resultOfChecking.PromocodesWithErrors.map(({ id, title }) => (
                      <li key={id}>{title}</li>
                    ))
                  }
                </ul>

              </div>
            )
          }
          {
            resultOfCalcOrder &&
            (
              <div>
                <p>Сумма заказа расчитана не верно.</p>
                <p>Итоговая сумма {resultOfCalcOrder.totalPrice}</p>
              </div>
            )
          }
        </div>
      );
    }

    return (
      <div>Сохранение заказа</div>
    );
  }
}

export default MutationOrder;
