const checkOrder = (props) => {
  const errors = [];
  const {
    product,
    user,
    paymentType,
  } = props;


  if (!product || product.length === 0) {
    errors.push('Не выбран продукт');
  }

  if (!user || user.length === 0) errors.push('Не выбран пользователь!');
  if (!paymentType) errors.push('Выберите тип оплаты');
  return errors;
};

export default checkOrder;
