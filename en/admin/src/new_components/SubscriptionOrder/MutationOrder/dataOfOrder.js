import calcOrderPrice from './../../CertificateOrder/calcOrderPrice';

const dataOfOrder = (props) => {
  const {
    product,
    user,
    discounts,
    additionalDiscounts,
    paymentType,
  } = props;

  const {
    orderPrice,
    discountRate,
    additionalDiscountsRate,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
    totalDiscountPrice: discountPrice,
    totalPrice,
  } = calcOrderPrice({
    product,
    additionalDiscounts,
    discounts,
  });

  const lecturesCount = product[0].lecturesCount === 'безлимит' ? -1 : product[0].lecturesCount;

  const discountsIds = discountsWithMaxRate.map(({ id }) => id);
  const additionalDiscountsIds = additionalDiscountsWithMaxRate.map(({ id }) => id);

  let subscription = {
    userId: user[0].id,
    draft: true,
    code: 'draft',
    title: product[0].title,
    price: product[0].price,
    // lecturesCount: parseInt(lecturesCount, 10),
    // unusedLectures: parseInt(lecturesCount, 10),
    // start: new Date(),
    // end: moment().add(product[0].months, 'months'),
    productId: product[0].id,
    paymentId: null,
    // prefix: product[0].prefix,
    // personsCount: parseInt(product[0].personsCount, 10),
  };
  const dataOfPayment = {
    source: 'ADMIN',
    userAnOrder: user[0],
    discounts,
    additionalDiscounts,
    subscription: product[0],
    order: {
      orderPrice,
      discountRate,
      additionalDiscountsRate,
      totalPrice,
    },
  };

  if (paymentType === 'CASH') {
    delete subscription.paymentId;
    subscription.payment = {
      acquiringId: null,
      commission: 0,
      orderPrice,
      discountPrice,
      totalPrice,
      type: 'CASH',
      userId: user[0].id,
      status: 'PAID',
      discountsIds,
      additionalDiscountsIds,
      data: dataOfPayment,
    };
  }
  if (paymentType === 'INTERNET_ACQUIRING') {
    delete subscription.paymentId;
    subscription.payment = {
      acquiringId: null,
      commission: 0,
      orderPrice,
      discountPrice,
      totalPrice,
      type: 'INTERNET_ACQUIRING',
      userId: user[0].id,
      status: 'PENDING',
      discountsIds,
      additionalDiscountsIds,
      data: dataOfPayment,
    };
  }

  return subscription;
};

export default dataOfOrder;
