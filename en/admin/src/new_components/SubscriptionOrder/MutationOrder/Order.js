import React from 'react';
import Typography from 'material-ui/Typography';
import List, { ListItemText } from 'material-ui/List';

import gql from './../../../new_pages/Subscriptions/gql';
import {
  CREATE_SUBSCRIPTION_MUTATION,
  EDIT_SUBSCRIPTION_MUTATION,
  CHECK_ORDER_CERT_AND_SUB,
} from './../../../constants';
import modifyMutationOptions from './../../../new_pages/Subscriptions/modifyMutationOptions';
import Mutation from './../../../new_containers/Mutation';
import QueryWithOptionsAndMutation from './../../../new_containers/QueryWithOptionsAndMutation';

import OrderMutation from './MutationOrder';
import queryOptions from './queryOptions';

import checkOrder from './checkOrder';
import dataOfOrder from './dataOfOrder';

const Order = (props) => {
  const {
    onSuccessMutation,
    product,
    user,
    id,
    paymentId,
  } = props;

  const checkedOrderErrors = checkOrder(props);
  let subscription = null;
  if (checkedOrderErrors.length === 0) subscription = dataOfOrder(props);

  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let resultMutationMessageSuccses = '';

  if (id && paymentId) {
    gqlMutationName = EDIT_SUBSCRIPTION_MUTATION;
    mutationResultObjectName = 'updateSubscription';
    resultMutationMessageSuccses = 'Абонемент обновлен';
  } else {
    gqlMutationName = CREATE_SUBSCRIPTION_MUTATION;
    mutationResultObjectName = 'createSubscription';
    resultMutationMessageSuccses = 'Абонемент создан';
  }

  return (
    <div>
      <Typography type="title" gutterBottom>
        Сохранение заказа
      </Typography>
      {
        checkedOrderErrors.length === 0 &&
        <QueryWithOptionsAndMutation
          gql={gql}
          gqlQueryName={CHECK_ORDER_CERT_AND_SUB}
          queryResultObjectName="checkOrderCertAndSub"
          queryOptions={queryOptions(subscription)}
          gqlMutationName={gqlMutationName}
          mutationResultObjectName={mutationResultObjectName}
          resultMutationMessageSuccses={resultMutationMessageSuccses}
          modifyMutationOptions={modifyMutationOptions}
          onSuccessMutation={onSuccessMutation}
        >
          <OrderMutation {...props} subscription={subscription} id={id} paymentId={paymentId} />
        </QueryWithOptionsAndMutation>
      }
      {
        checkedOrderErrors.length > 0 &&
        <div style={{ marginTop: 20 }} >
          <Typography type="title" gutterBottom>
            Ошибки
          </Typography>
          <List>
            {
              checkedOrderErrors.map((error, index) => (
                <ListItemText
                  key={index}
                  primary={error}
                />
              ))
            }
          </List>
        </div>
      }
    </div>

    // <Mutation
    //   gql={gql}
    //   gqlMutationName={gqlMutationName}
    //   mutationResultObjectName='Subscriptions'
    //   onSuccessMutation={onSuccessMutation}
    // >
    //
    // </Mutation>
  );
};

export default Order;
