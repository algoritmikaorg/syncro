import React, { Component } from 'react';
// import Typography from 'material-ui/Typography';

import SelectRelatedItems from './../../SelectRelatedItems';
import gql from './../../../new_pages/Catalog/gql';
import {
  ALL_SUBSCRIPTION_PRODUCTS_QUERY_SHORT,
} from './../../../constants';
import {
  subscriptionsShort as columnDataSubscriptions,
} from './../../../new_pages/Catalog/columnData';
import filter from './../../../new_pages/Catalog/fields/filterSubscriptionsShort';
import modifyQueryFilter from './../../../new_pages/Catalog/modifyQueryFilter';

const SelectProduct = (props) => {
  const {
    product,
    onSelectProduct,
  } = props;

  return (
    <div style={{ marginTop: 20 }} >
      <SelectRelatedItems
        srcData={product}
        relationTitle="Выберите тип абонемента"
        tableTitle="Абонемент"
        gql={gql}
        gqlQueryName={ALL_SUBSCRIPTION_PRODUCTS_QUERY_SHORT}
        queryResultObjectName="allSubscriptionProducts"
        onChange={onSelectProduct}
        columnData={columnDataSubscriptions}
        filter={filter}
        modifyQueryFilter={modifyQueryFilter.subscriptions}
      />
    </div>
  );
};

export default SelectProduct;
