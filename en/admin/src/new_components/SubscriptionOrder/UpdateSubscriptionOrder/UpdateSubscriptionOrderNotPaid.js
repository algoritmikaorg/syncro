import React, { Component } from 'react';
import moment from 'moment';

import Order from './../SubscriptionOrder';
import { formatInputDataForTable } from './../../../new_utils';
import checkDiscountsCertSubsParam from './../../OrderDiscounts/checkDiscountsCertSubsParam';

class UpdateOrderNotPaid extends Component {
  constructor(props) {
    super(props);
    const { order } = props;
    this.state = {
      order,
    };
  }
  componentWillReceiveProps(nextProps) {
    const { order } = nextProps;
    this.setState({
      order,
    });
  }
  render() {
    const { order } = this.state;

    const {
      id,
      user,
      payment,
    } = order;
    const { id: paymentId, type: paymentType } = payment;

    let discounts = [];
    let additionalDiscounts = [];
    const product = [Object.assign(
      {},
      formatInputDataForTable(order.product),
      {
        src: order.product,
      },
    )];

    if (payment.discounts && payment.discounts.length > 0) {
      discounts = payment.discounts.map((discount) => {
        return Object.assign(
          {},
          formatInputDataForTable(discount),
          {
            src: discount,
          },
        );
      });
      discounts = checkDiscountsCertSubsParam({ product }, discounts);
    }
    if (payment.additionalDiscounts && payment.additionalDiscounts.length > 0) {
      additionalDiscounts = payment.additionalDiscounts.map((discount) => {
        return Object.assign(
          {},
          formatInputDataForTable(discount),
          {
            src: discount,
          },
        );
      });
      additionalDiscounts = checkDiscountsCertSubsParam({ product }, additionalDiscounts);
    }

    return (
      <Order
        history={this.props.history}
        id={id}
        paymentId={paymentId}
        user={[user]}
        product={product}
        discounts={discounts}
        additionalDiscounts={additionalDiscounts}
        paymentType={paymentType}
      />
    );
  }
}

export default UpdateOrderNotPaid;
