import React, { Component } from 'react';

import UpdateSubscriptionOrderPaid from './UpdateSubscriptionOrderPaid';
import UpdateSubscriptionOrderNotPaid from './UpdateSubscriptionOrderNotPaid';

class UpdateSubscriptionOrder extends Component {
  constructor(props) {
    super(props);
    const {
      gqlQueryName,
      queryResultObjectName,
    } = props;

    let order = null;
    if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
      order = props[gqlQueryName][queryResultObjectName];
    }

    this.state = {
      order,
    };
  }
  componentWillReceiveProps(nextProps) {
    const {
      gqlQueryName,
      queryResultObjectName,
    } = nextProps;
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      this.setState({
        order: nextProps[gqlQueryName][queryResultObjectName],
      });
    }
  }
  render() {
    const { props, state } = this;
    const { order } = state;

    if (order) {
      if (order.payment && order.payment.status === 'PAID') {
        return <UpdateSubscriptionOrderPaid {...props} order={order} />
      }
      return <UpdateSubscriptionOrderNotPaid {...props} order={order} />
    }
    return (
      <div>Загрузка...</div>
    );
  }
}

export default UpdateSubscriptionOrder;
