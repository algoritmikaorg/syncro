import gql from 'graphql-tag';

export default gql`
  mutation deleteEventMutation(
      $id: ID!
    ){
    deleteEvent(
      id: $id,
    ) {
      id
    }
  }
`;
