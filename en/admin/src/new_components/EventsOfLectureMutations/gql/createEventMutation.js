import gql from 'graphql-tag';

export default gql`
  mutation CreateEventMutation(
      $date: DateTime!,
      $price: Int!,
      $quantityOfTickets: Int!,
      $lectureId: ID!,
      $locationId: ID!,
      $curatorId: ID!,
      $lecturersIds: [ID!]!,
    ){
    createEvent(
      date: $date,
      price: $price,
      quantityOfTickets: $quantityOfTickets,
      lectureId: $lectureId,
      locationId: $locationId,
      curatorId: $curatorId,
      lecturersIds: $lecturersIds,
    ) {
      id,
      createdAt,
      updatedAt,

      date,
      quantityOfTickets,
      price,
      lecture {
        id,

        title,
      },
      lecturers {
        id,

        firstName,
        lastName,
      },
      location {
        id,

        title,
        address,
        metro,
      },
      curator {
        id

        firstName
        lastName
        email
      }
    }
  }
`;
