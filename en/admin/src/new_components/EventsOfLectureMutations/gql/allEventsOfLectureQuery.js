import gql from 'graphql-tag';

export default gql`
  query AllEventsOfLectureQuery($id: ID!) {
    allEvents(
      filter: {
        lecture: {
          id: $id
        }
      }
    ) {
      id
      createdAt
      updatedAt
      date
      quantityOfTickets
      price
      lecture {
        id
      }
      lecturers {
        id
        firstName
        lastName
      }
      location {
        id
        title
        address
        metro
      }
      curator {
        id
        firstName
        lastName
        email
      }
    }
  }
`;
