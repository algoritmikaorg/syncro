import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import moment from 'moment';

import {
  CREATE_EVENT_MUTATION,
  EDIT_EVENT_MUTATION,
  ALL_EVENTS_OF_LECTURE_QUERY,
  DELETE_EVENT_MUTATION,
} from './../../constants';

import gql from './gql';
import formatEventDataForForm from './../EventOfLectureNoMutations/formatEventDataForForm';

import Mutation from './../../new_containers/Mutation';
import QueryWithQueryOptions from './../../new_containers/QueryWithQueryOptions';
import EventsOfLectureList from './EventsOfLectureList';

import LecturersSelect from './../BasicForm/relations/LecturersSelect';
import LocationsSelect from './../BasicForm/relations/LocationsSelect';
import CuratorSelect from './../BasicForm/relations/CuratorSelect';

import EventMutationDialog from './EventMutationDialog';
import DeleteEventDialog from './DeleteEventDialog';

import fields from './../EventOfLectureNoMutations/fields';

const styles = theme => ({
  paper: {
    padding: 16,
    // textAlign: 'center',
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing.unit * 3,
    marginTop: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    // padding: theme.spacing.unit * 2,
  },
});

const modifyQueryOptions = (ownProps) => {
  const id = ownProps.match.params.id;
  return (
    {
      variables: {
        id,
      },
    }
  );
};

const modifyDeleteMutationOptions = (ownProps) => {
  const lectureId = ownProps.match.params.id;
  const {
    mutationResultObjectName,
    queryResultObjectName,
  } = ownProps;
  return (
    {
      update: (proxy, { data: resultData }) => {
        const eventId = resultData[mutationResultObjectName].id;
        const query = {
          query: gql[ALL_EVENTS_OF_LECTURE_QUERY],
          variables: {
            id: lectureId,
          },
        };
        const data = proxy.readQuery(query);
        const newData = data[queryResultObjectName].filter(({ id }) => id !== eventId);
        proxy.writeQuery(Object.assign({}, query, { data: { [queryResultObjectName]: newData } }));
      },
    }
  );
};

const modifyCreateMutationOptions = (ownProps) => {
  const lectureId = ownProps.match.params.id;
  const {
    mutationResultObjectName,
    queryResultObjectName,
  } = ownProps;
  return (
    {
      update: (proxy, { data: resultData }) => {
        const eventId = resultData[mutationResultObjectName].id;
        const query = {
          query: gql[ALL_EVENTS_OF_LECTURE_QUERY],
          variables: {
            id: lectureId,
          },
        };
        const data = proxy.readQuery(query);
        const newData = data[queryResultObjectName];
        newData.push(resultData[mutationResultObjectName]);

        proxy.writeQuery(Object.assign({}, query, { data: { [queryResultObjectName]: newData } }));
      },
    }
  );
};

const modifyUpdateMutationOptions = (ownProps) => {
  const lectureId = ownProps.match.params.id;
  const {
    mutationResultObjectName,
    queryResultObjectName,
  } = ownProps;
  return (
    {
      refetchQueries: [
        'AllEventsOfLectureQuery',
        'AllEventsQuery',
      ],
    }
  );
};

// const modifyUpdateMutationOptions = (ownProps) => {
//   const lectureId = ownProps.match.params.id;
//   const {
//     mutationResultObjectName,
//     queryResultObjectName,
//   } = ownProps;
//   return (
//     {
//       update: (proxy, { data: resultData }) => {
//         const eventId = resultData[mutationResultObjectName].id;
//         const query = {
//           query: gql[ALL_EVENTS_OF_LECTURE_QUERY],
//           variables: {
//             id: lectureId,
//           },
//         };
//         const data = proxy.readQuery(query);
//         const newData = data[queryResultObjectName]
//           .map((item) => {
//             if (item.id === eventId) {
//               return resultData[mutationResultObjectName]
//             }
//           });
//
//         proxy.writeQuery(Object.assign({}, query, { data: { [queryResultObjectName]: newData } }));
//       },
//     }
//   );
// };

class EventsOfLectureMutations extends Component {
  constructor(props) {
    super(props);

    const {
      lecturers,
      location,
      curator,
      price,
      quantityOfTickets,
      match,
    } = props;

    this.state = {
      lecturers,
      location,
      curator,
      price,
      quantityOfTickets,
      lectureId: match.params.id,
      itemToEdit: null,
      itemToDelete: null,
    };
    this.handleHideDialog = this.handleHideDialog.bind(this);
    this.handleShowDialog = this.handleShowDialog.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeNoEvents = this.handleChangeNoEvents.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.handleSuccsesMutation = this.handleSuccsesMutation.bind(this);
  }

  handleChangeNoEvents = id => (selectedIds, selected) => {
    this.setState({
      [id]: selected || selectedIds,
    });
  }
  handleChange = id => (event) => {
    this.setState({
      [id]: event.target.value,
    });
  }
  handleEdit = (index, itemToEdit, itemToEditSrc) => () => {
    this.setState({
      status: 'edit',
      itemToEdit: Object.assign({}, itemToEditSrc, { time: itemToEdit.time }),
      itemToDelete: null,
    });
  }
  handleDelete = (index, itemToDelete, itemToDeleteSrc) => () => {
    this.setState({
      status: 'delete',
      itemToDelete: Object.assign({}, itemToDeleteSrc, { time: itemToDelete.time }),
      itemToEdit: null,
    });
  }
  handleShowDialog() {
    this.setState({
      status: 'create',
      itemToEdit: null,
      itemToDelete: null,
    });
  }
  handleHideDialog() {
    this.setState({
      status: null,
      itemToEdit: null,
      itemToDelete: null,
    });
  }
  handleCancelDelete() {
    this.setState({
      itemToDelete: null,
    });
  }
  handleDeleteConfirm() {
    this.setState({
      itemToDelete: null,
    });
  }
  handleSuccsesMutation() {
    this.setState({
      status: null,
      itemToEdit: null,
      itemToDelete: null,
    });
  }
  render() {
    const {
      classes,
      match,
    } = this.props;

    const {
      lecturers,
      location,
      curator,
      price,
      quantityOfTickets,
      lectureId,
      status,
      itemToEdit,
      itemToDelete,
    } = this.state;

    const gqlQueryName = ALL_EVENTS_OF_LECTURE_QUERY;
    const queryResultObjectName = 'allEvents';
    let gqlMutationName = null;
    let mutationResultObjectName = null;
    let resultMutationMessageSuccses = '';
    let currentFileds = null;
    let title = null;
    let openEditEventDialog = false;
    let openDeleteEventDialog = false;
    let openCreateEventDialog = false;

    let modifyMutationOptions = null;

    if (itemToEdit) {
      openEditEventDialog = true;
    }

    if (itemToDelete) {
      openDeleteEventDialog = true;
    }

    if (status === 'create') {
      openCreateEventDialog = true;
      currentFileds = fields.create;
    }

    switch (status) {
      case 'edit': {
        gqlMutationName = EDIT_EVENT_MUTATION;
        mutationResultObjectName = 'updateEvent';
        title = 'Редактирование мероприятия';
        currentFileds = fields.update;
        resultMutationMessageSuccses = 'Мероприятие обновлено';
        modifyMutationOptions = modifyUpdateMutationOptions;
        break;
      }
      case 'create': {
        gqlMutationName = CREATE_EVENT_MUTATION;
        mutationResultObjectName = 'createEvent';
        title = 'Создание нового мероприятия';
        currentFileds = fields.create;
        resultMutationMessageSuccses = 'Мероприятие создано';
        modifyMutationOptions = modifyCreateMutationOptions;
        break;
      }
      default: {
        gqlMutationName = DELETE_EVENT_MUTATION;
        mutationResultObjectName = 'deleteEvent';
      }
    }

    return (
      <Paper className={classes.paper}>
        <Typography type="title" gutterBottom>
          Даты проведения лекции
        </Typography>
        <div className={classes.container}>
          <LecturersSelect
            value={lecturers}
            id="lecturers"
            onChangeNoEvent={this.handleChangeNoEvents}
            multiple
          />
          <LocationsSelect
            value={location}
            id="location"
            onChangeNoEvent={this.handleChangeNoEvents}
          />
          <CuratorSelect
            value={curator}
            id="curator"
            onChangeNoEvent={this.handleChangeNoEvents}
          />
          <div>
            <TextField
              id={'price'}
              label={'Цена билета'}
              value={price}
              onChange={this.handleChange('price')}
              className={classes.textField}
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            />
            <TextField
              id={'quantityOfTickets'}
              label={'Количество билетов'}
              value={quantityOfTickets}
              onChange={this.handleChange('quantityOfTickets')}
              className={classes.textField}
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            />
          </div>
          <div style={{ width: '100%' }}>
            <Button
              raised
              color="primary"
              className={classes.button}
              onClick={this.handleShowDialog}
            >
                Добавить дату
            </Button>
          </div>
          <div style={{ width: '100%' }}>
            <QueryWithQueryOptions
              gql={gql}
              gqlQueryName={gqlQueryName}
              queryResultObjectName={queryResultObjectName}
              match={match}
              modifyQueryOptions={modifyQueryOptions}
            >
              <EventsOfLectureList
                title={title}
                onEdit={this.handleEdit}
                onDelete={this.handleDelete}
              />
            </QueryWithQueryOptions>
          </div>
          <Mutation
            gql={gql}
            gqlMutationName={DELETE_EVENT_MUTATION}
            mutationResultObjectName="deleteEvent"
            modifyMutationOptions={modifyDeleteMutationOptions}
            queryResultObjectName={queryResultObjectName}
            data={itemToDelete}
            match={match}
          >
            <DeleteEventDialog
              open={openDeleteEventDialog}
              title={itemToDelete !== null ? moment(itemToDelete.date).format('DD.MM.YYYY HH:mm') : ''}
              data={itemToDelete}
              onCancel={this.handleCancelDelete}
              onConfirm={this.handleDeleteConfirm}
            />
          </Mutation>
          <Mutation
            gql={gql}
            gqlMutationName={gqlMutationName}
            mutationResultObjectName={mutationResultObjectName}
            modifyMutationOptions={modifyMutationOptions}
            queryResultObjectName={queryResultObjectName}
            data={itemToEdit || itemToDelete}
            fields={currentFileds}
            match={match}
            onSuccessMutation={this.handleSuccsesMutation}
            resultMutationMessageSuccses={resultMutationMessageSuccses}
          >
            <EventMutationDialog
              open={openEditEventDialog || openCreateEventDialog}
              onClose={this.handleHideDialog}
              onSubmitMutation={this.handleDialogSubmitMutation}
              lecturers={lecturers}
              location={location}
              curator={curator}
              price={price}
              quantityOfTickets={quantityOfTickets}
              lectureId={lectureId}
              title={openEditEventDialog ? 'Редактирование мероприятия' : 'Создание мероприятия'}
            />
          </Mutation>

        </div>
      </Paper>
    );
  }
}

EventsOfLectureMutations.defaultProps = {
  lectureid: 'draft',
  lecturers: [],
  price: '',
  quantityOfTickets: '',
};

export default withStyles(styles)(EventsOfLectureMutations);
