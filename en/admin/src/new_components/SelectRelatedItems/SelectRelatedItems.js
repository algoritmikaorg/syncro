import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

import QueryWithProgress from './../../new_containers/QueryWithProgress';
import RelationDialog from './RelationDialog';
import { selectedItemsOfSrcData } from './../../new_utils';
import ListChips from './ListChips';

class SelectRelatedItems extends Component {
  constructor(props) {
    super(props);
    const { srcData } = props;
    const selected = srcData;

    this.state = {
      openDialog: false,
      selected,
      use: false,
    };

    this.handleOpenDialog = this.handleOpenDialog.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleSubmitDialog = this.handleSubmitDialog.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { use } = this.state;
    const { srcData } = nextProps;
    if (!use) {
      const selected = srcData;
      if (selected) {
        this.setState({
          selected,
        });
      }
    }
  }

  handleOpenDialog() {
    this.setState({
      openDialog: true,
    });
  }

  handleCloseDialog() {
    this.setState({
      openDialog: false,
    });
  }

  handleSubmitDialog(selected) {
    this.setState({
      selected,
      openDialog: false,
      use: true,
    }, () => {
      this.props.onChange(selected.map(({ id }) => id), selected);
    });
  }
  render() {
    const {
      tableTitle,
      gql,
      gqlQueryName,
      queryResultObjectName,
      queryOptions,
      relationTitle,
      columnData,
      multiple,
      all,
      filter,
      modifyQueryFilter,
      modifyQueryOptions,
    } = this.props;

    const {
      selected,
      openDialog
    } = this.state;

    return (
      // <div style={{ width: '100%', marginBottom: 20, marginTop: 20, marginLeft: 8, marginRight: 8 }}>
      // <div style={{ marginBottom: 20, marginTop: 20, marginLeft: 8, marginRight: 8 }}>
      <div>
        <Typography type="title" gutterBottom>
          {relationTitle}
        </Typography>
        {
          selected.length === 0 ?
          (
            <div>
              <Typography type="body1" gutterBottom>
                {'Ничего не выбрано'}
              </Typography>
              <Button
                raised
                dense
                color="primary"
                onClick={this.handleOpenDialog}
              >
                Выбрать
              </Button>
            </div>
          ) :
          (
            <div>
              <ListChips
                selected={selected}
              />
              <Button
                raised
                dense
                color="primary"
                onClick={this.handleOpenDialog}
              >
                Изменить
              </Button>
            </div>

          )
        }
        <RelationDialog
          gql={gql}
          gqlQueryName={gqlQueryName}
          queryResultObjectName={queryResultObjectName}
          modifyQueryFilter={modifyQueryFilter}
          modifyQueryOptions={modifyQueryOptions}

          selected={selected}
          open={openDialog}
          onSubmit={this.handleSubmitDialog}
          onClose={this.handleCloseDialog}
          columnData={columnData}
          fullScreen
          tableTitle={relationTitle}
          multiple={multiple}
          all={all}
          filter={filter}
        />
      </div>
    );
  }
}

SelectRelatedItems.defaultProps = {
  value: [],
  srcData: [],
  onChange() {},
};

export default SelectRelatedItems;
