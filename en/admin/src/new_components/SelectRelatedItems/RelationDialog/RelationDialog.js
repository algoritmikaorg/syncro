import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';

import QueryWithProgress from './../../../new_containers/QueryWithProgress';

import Chips from './../../ChipsArray';
import FilterOfQuery from './../../FilterOfQuery';
import RelationDialogContent from './RelationDialogContent';
import { formatInputDataArrayForTable, formatOutputFormData } from './../../../new_utils';

const mtDate = new Date();

const fixReloadBagOfDateGtLt = (filter) => {
  const r3 = (item) => {
    if (item instanceof Object) {
      const keys = Object.keys(item);
      Object.keys(item).map((key) => {
        if (item[key] instanceof Object) {
          return r3(item[key]);
        } else if (item[key] === 'replaceByDate') {
          item[key] = mtDate;
        }
      });
    }
    return item;
  };
  r3(filter);
  return filter;
};

class RelationDialog extends Component {
  constructor(props) {
    super(props);
    const {
      selected,
      queryFilterData,
    } = this.props;

    this.state = {
      selected,
      queryFilterData,
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleFilterQuerySubmit = this.handleFilterQuerySubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
      selected,
      queryFilterData,
    } = nextProps;
    const newState = {};
    if (selected.length > 0) {
      newState.selected = selected;
    }
    if (queryFilterData) {
      newState.queryFilterData = queryFilterData;
    }
    if (Object.keys(newState).length > 0) {
      this.setState(newState);
    }
  }

  handleClose() {
    this.setState({
      selected: this.props.selected,
    }, this.props.onClose);
  }

  handleSubmit() {
    this.props.onSubmit(this.state.selected);
  }

  handleSelect(selected) {
    this.setState({
      selected,
    });
  }

  handleFilterQuerySubmit(queryFilterData) {
    this.setState({
      queryFilterData,
    });
  }

  render() {
    const {
      fullScreen,
      dialogTitle,
      open,
      gql,
      gqlQueryName,
      queryResultObjectName,
      columnData,
      multiple,
      all,
      filter,
      modifyQueryFilter,
      modifyQueryOptions
    } = this.props;

    const {
      selected,
      queryFilterData,
    } = this.state;

    const queryFilterDataFormated = filter && queryFilterData ? formatOutputFormData(queryFilterData, filter) : {};

    return (
      <div>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          {
            dialogTitle &&
            <DialogTitle id="responsive-dialog-title">
              {dialogTitle}
            </DialogTitle>
          }
          <DialogContent>
            <Chips
              selected={selected}
              onChange={this.handleSelect}
            />
            {
              filter &&
              <FilterOfQuery
                formTitle="Фильтр"
                data={queryFilterData}
                fields={filter}
                onSubmit={this.handleFilterQuerySubmit}
              />
            }
            <QueryWithProgress
              gql={gql}
              gqlQueryName={gqlQueryName}
              queryResultObjectName={queryResultObjectName}
              modifyQueryFilter={() => fixReloadBagOfDateGtLt(modifyQueryFilter(queryFilterDataFormated))}
              modifyQueryOptions={modifyQueryOptions}
            >
              <RelationDialogContent
                selected={selected}
                columnData={columnData}
                onSelect={this.handleSelect}
                multiple={multiple}
                all={all}
                filter={filter}
                onFilterQuerySubmit={this.handleFilterQuerySubmit}
                queryFilterData={queryFilterData}
              />
            </QueryWithProgress>

          </DialogContent>

          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Отменить
            </Button>
            <Button onClick={this.handleSubmit} color="primary" autoFocus>
              Сохранить
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

RelationDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

RelationDialog.defaultProps = {
  selected: [],
  // queryFilterData: {},
  formatInputData: data => data || [],
  modifyQueryFilter: () => {},
};

export default withMobileDialog()(RelationDialog);
