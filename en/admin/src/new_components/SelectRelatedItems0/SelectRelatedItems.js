import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

import QueryWithProgress from './../../new_containers/QueryWithProgress';
import RelationDialog from './RelationDialog';
import { selectedItemsOfSrcData } from './../../new_utils';
import ListChips from './ListChips';

class SelectRelatedItems extends Component {
  constructor(props) {
    super(props);
    const { srcData } = props;
    const selected = srcData;

    this.state = {
      openDialog: false,
      selected,
      use: false,
    };
    this.handleAdd = this.handleAdd.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleSubmitDialog = this.handleSubmitDialog.bind(this);
    this.handleCancelDialog = this.handleCancelDialog.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { use } = this.state;
    const { srcData } = nextProps;
    if (!use) {
      const selected = srcData;
      if (selected) {
        this.setState({
          selected,
        });
      }
    }
  }
  handleAdd() {
    this.setState({
      openDialog: true,
    });
  }
  handleEdit() {
    this.setState({
      openDialog: true,
    });
  }
  handleCancelDialog() {
    this.setState({
      openDialog: false,
    });
  }
  handleSubmitDialog(selected) {
    this.setState({
      selected,
      openDialog: false,
      use: true,
    }, () => {
      this.props.onChange(selected.map(({ id }) => id), selected);
    });
  }
  render() {
    const {
      tableTitle,
      gql,
      gqlQueryName,
      queryResultObjectName,
      queryOptions,
      relationTitle,
      columnData,
      multiple,
      all,
      filter,
      modifyQueryFilter,
      modifyQueryOptions,
    } = this.props;

    const {
      selected,
      openDialog
    } = this.state;

    return (
      // <div style={{ width: '100%', marginBottom: 20, marginTop: 20, marginLeft: 8, marginRight: 8 }}>
      // <div style={{ marginBottom: 20, marginTop: 20, marginLeft: 8, marginRight: 8 }}>
      <div>
        <Typography type="title" gutterBottom>
          {relationTitle}
        </Typography>
        {
          selected.length === 0 ?
          (
            <div>
              <Typography type="body1" gutterBottom>
                {'Ничего не выбрано'}
              </Typography>
              <Button
                raised
                dense
                color="primary"
                onClick={this.handleAdd}
              >
                Выбрать
              </Button>
            </div>
          ) :
          (
            <div>
              <ListChips
                selected={selected}
              />
              <Button
                raised
                dense
                color="primary"
                onClick={this.handleEdit}
              >
                Изменить
              </Button>
            </div>

          )
        }
        <QueryWithProgress
          gql={gql}
          gqlQueryName={gqlQueryName}
          queryResultObjectName={queryResultObjectName}
          modifyQueryFilter={modifyQueryFilter}
          modifyQueryOptions={modifyQueryOptions}
        >
          <RelationDialog
            gql={gql}
            gqlQueryName={gqlQueryName}
            queryResultObjectName={queryResultObjectName}
            modifyQueryFilter={modifyQueryFilter}
            modifyQueryOptions={modifyQueryOptions}

            selected={selected}
            open={openDialog}
            onSubmit={this.handleSubmitDialog}
            onCancel={this.handleCancelDialog}
            columnData={columnData}
            fullScreen
            tableTitle={relationTitle}
            multiple={multiple}
            all={all}
            filter={filter}
          />
        </QueryWithProgress>
      </div>
    );
  }
}

SelectRelatedItems.defaultProps = {
  value: [],
  srcData: [],
  onChange() {},
};

export default SelectRelatedItems;
