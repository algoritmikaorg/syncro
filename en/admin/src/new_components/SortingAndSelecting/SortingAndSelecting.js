import React, { Component } from 'react';

import EnhancedTable from './../EnhancedTable';
import Chips from './../ChipsArray';
import { selectedItems } from './../../new_utils';

class SortingAndSelecting extends Component {
  constructor(props) {
    super(props);
    const {
      selectedIds,
      gqlQueryName,
      queryResultObjectName,
      formatInputData,
    } = props;

    let data = [];
    let selected = [];
    if (props[gqlQueryName]) {
      data = formatInputData(props[gqlQueryName][queryResultObjectName]);
      selected = selectedItems(selectedIds, data);
    }

    this.state = {
      selected,
      data,
    };
    this.handleSelect = this.handleSelect.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const {
      selectedIds,
      gqlQueryName,
      queryResultObjectName,
      formatInputData,
    } = nextProps;

    let data = [];
    let selected = [];

    if (nextProps[gqlQueryName]) {
      data = formatInputData(nextProps[gqlQueryName][queryResultObjectName]);
      selected = selectedItems(selectedIds, data);
    }

    this.setState({
      selected,
      data,
    });
  }
  handleSelect(selected) {
    this.setState({
      selected,
    });
  }
  // handleSelect(selected) {
  //   const selected = [...this.state.selected];
  //   selected.push(selectedItem);
  //   this.setState({
  //     selected,
  //   });
  // }
  // handelUnSelect(unSelectedItem) {
  //   const selected = [...this.state.selected];
  //   const index = selected.indexOf(unSelectedItem);
  //   selected.splice(index, 1);
  //   this.setState({
  //     selected,
  //   });
  // }
  render() {
    const { tableTitle, columnData } = this.props;
    const { selected, data } = this.state;
    return (
      <div>
        <Chips
          selected={selected}
          onChange={this.handleSelect}
        />
        <EnhancedTable
          selected={selected}
          onSelect={this.handleSelect}
          tableTitle={tableTitle}
          columnData={columnData}
          data={data}
        />
      </div>
    );
  }
}

SortingAndSelecting.defaultProps = {
  selected: [],
  columnData: [],
  formatInputData: data => data || [],
};

export default SortingAndSelecting;
