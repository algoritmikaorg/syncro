import React from 'react';
import MutationEntities from './../MutationEntities';

const DialogContentInner = (props) => {
  return (
    <MutationEntities {...props} />
  );
};

export default DialogContentInner;
