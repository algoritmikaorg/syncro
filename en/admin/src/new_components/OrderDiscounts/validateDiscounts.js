import checkDiscountsParam from './checkDiscountsParam';
import checkDiscountsCertSubsParam from './checkDiscountsCertSubsParam';

const eventsAll = (events) => {
  if (!events) return [];
  const eventsKeys = Object.keys(events);
  if (eventsKeys.length === 0) return [];
  return eventsKeys
    .map(key => events[key])
    .reduce((prev, curr) => prev.concat(curr));
};

const validateCourseCyclesDiscounts = (props, data) => {
  const {
    events,
  } = props;

  return checkDiscountsParam(props, { events: eventsAll(events) }, data);
};

const validateSingleLecturesDiscounts = (props, data) => {
  const {
    events,
  } = props;

  return checkDiscountsParam(props, { events: eventsAll(events) }, data);
};

const validateCertSubsDiscounts = (props, data) => {
  return checkDiscountsCertSubsParam(props, data);
};

const validateDiscounts = (props, data) => {
  if (!data || data.length === 0) return [];
  const {
    courses,
    cycles,
    lectures,
    product,
  } = props;

  if ((courses && courses.length > 0) || (cycles && cycles.length > 0)) {
    return validateCourseCyclesDiscounts(props, data);
  } else if (lectures && lectures.length > 0) {
    return validateSingleLecturesDiscounts(props, data);
  } else if (product && product.length > 0) {
    return validateCertSubsDiscounts(props, data);
  }

  return [];
};

export default validateDiscounts;
