const modifyQueryOptionsCertificate = (ownProps) => {
  const date = new Date();
  const { product } = ownProps;
  const filter = {
    OR: [
      {
        allProducts: true,
        public: true,
        validityPeriodTo_gt: date,
        validityPeriodFrom_lt: date,
        promocode: '',
        unused_gt: 0,
      },
      {
        public: true,
        validityPeriodTo_gt: date,
        validityPeriodFrom_lt: date,
        unused_gt: 0,
        promocode: '',
        certificateProducts_some: {
          id: product[0].id,
        },
      },
    ],
  };
  const variables = { filter };
  return (
    {
      variables,
    }
  );
};

const modifyQueryOptionsSubscription = (ownProps) => {
  const date = new Date();
  const { product } = ownProps;
  const filter = {
    OR: [
      {
        allProducts: true,
        public: true,
        validityPeriodTo_gt: date,
        validityPeriodFrom_lt: date,
        promocode: '',
        unused_gt: 0,
      },
      {
        public: true,
        validityPeriodTo_gt: date,
        validityPeriodFrom_lt: date,
        unused_gt: 0,
        promocode: '',
        subscriptionProducts_some: {
          id: product[0].id,
        },
      },
    ],
  };
  const variables = { filter };
  return (
    {
      variables,
    }
  );
};

const modifyQueryOptionsLectures = (ownProps) => {
  const date = new Date();
  const {
    lectures,
    courses,
    cycles,
    participants,
  } = ownProps;

  const tags = [];
  let events = [];
  switch (true) {
    case courses && courses.length > 0: {
      tags.push(...courses[0].src.tags);
      // events = lectures.map(({ src }) => src.events || []).reduce((prev, curr) => prev.concat())
      break;
    }
    case cycles && cycles.length > 0: {
      tags.push(...cycles[0].src.tags);
      break;
    }
    case lectures && lectures.length > 0: {
      lectures.forEach((lecture) => {
        tags.push(...lecture.src.tags);
      });
      break;
    }
    default: {}
  }

  const filter = {
    AND: [
      {
        public: true,
        validityPeriodTo_gt: date,
        validityPeriodFrom_lt: date,
        unused_gt: 0,
        promocode: '',
      },
      {
        OR: [
          {
            allProducts: true,

          },
          {
            tags_some: {
              id_in: tags.map(({ id }) => id),
            },
          },
          {
            lectures_some: {
              id_in: lectures.map(({ id }) => id),
            },
          },
          {
            courses_some: {
              id_in: courses.map(({ id }) => id),
            },
          },
          {
            cycles_some: {
              id_in: cycles.map(({ id }) => id),
            },
          },
        ],
      },
    ],
  };
  const variables = { filter };
  return (
    {
      variables,
    }
  );
};

export default {
  certificate: modifyQueryOptionsCertificate,
  subscription: modifyQueryOptionsSubscription,
  lectures: modifyQueryOptionsLectures,
};
