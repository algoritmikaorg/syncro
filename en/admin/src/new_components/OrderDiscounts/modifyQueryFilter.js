import { checkPublic } from './../../new_utils';
// replaceByDate
const modifyQueryFilter = (input = {}, type) => {
  let filter = {};
  filter.public = true;

  if (input.tab) {
    switch (input.tab.id) {
      case 'public': {
        filter.public = true;
        break;
      }
      case 'not_public': {
        filter.public = false;
        break;
      }
      default: {}
    }
  }

  filter = checkPublic(filter, input);

  if (input.title) filter.title_contains = input.title;
  if (input.promocode) filter.promocode_contains = input.promocode;
  if (input.rate) filter.rate = input.rate;
  return filter;
};

const modifyQueryFilterCertificate = props => (input = {}, type) => {
  const { product, discounts } = props;
  const filter = modifyQueryFilter(input, type);
  return Object.assign({}, filter, {
    id_not_in: discounts.map(({ id }) => id),
    validityPeriodTo_gt: 'replaceByDate',
    validityPeriodFrom_lt: 'replaceByDate',
    OR: [
      {
        allProducts: true,
        unused_gt: 0,
      },
      {
        certificateProducts_some: {
          id: product[0].id,
        },
        unused_gt: 0,
      },
    ],
  });
};

const modifyQueryFilterSubscription = props => (input = {}, type) => {
  const { product, discounts } = props;
  const filter = modifyQueryFilter(input, type);
  return Object.assign({}, filter, {
    id_not_in: discounts.map(({ id }) => id),
    validityPeriodTo_gt: 'replaceByDate',
    validityPeriodFrom_lt: 'replaceByDate',
    OR: [
      {
        allProducts: true,
        unused_gt: 0,
      },
      {
        subscriptionProducts_some: {
          id: product[0].id,
        },
        unused_gt: 0,
      },
    ],
  });
};

const modifyQueryFilterLectures = props => (input = {}, type) => {
  const {
    lectures,
    courses,
    cycles,
    participants,
  } = props;

  const tags = [];

  switch (true) {
    case courses && courses.length > 0: {
      tags.push(...courses[0].src.tags);
      break;
    }
    case cycles && cycles.length > 0: {
      tags.push(...cycles[0].src.tags);
      break;
    }
    case lectures && lectures.length > 0: {
      lectures.forEach((lecture) => {
        tags.push(...lecture.src.tags);
      });
      break;
    }
    default: {}
  }

  const filter = modifyQueryFilter(input, type);
  return Object.assign({}, filter, {
    AND: [
      {
        validityPeriodTo_gt: 'replaceByDate',
        validityPeriodFrom_lt: 'replaceByDate',
        unused_gt: 0,
      },
      {
        OR: [
          {
            allProducts: true,

          },
          {
            tags_some: {
              id_in: tags.map(({ id }) => id),
            },
          },
          {
            lectures_some: {
              id_in: lectures.map(({ id }) => id),
            },
          },
          {
            courses_some: {
              id_in: courses.map(({ id }) => id),
            },
          },
          {
            cycles_some: {
              id_in: cycles.map(({ id }) => id),
            },
          },
        ],
      },
    ],
  });
};

export default {
  certificate: modifyQueryFilterCertificate,
  subscription: modifyQueryFilterSubscription,
  lectures: modifyQueryFilterLectures,
};
