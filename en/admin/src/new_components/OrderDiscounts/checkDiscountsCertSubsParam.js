const checkValid = (lectures, product, discount) => {
  const { tags } = product || lectures[0].src || [];
  const tagsIds = tags.map(({ id }) => id);
  let result = false;

  if (discount.tags && discount.tags.length > 0) {
    result = discount.tags.some(({ id }) => tagsIds.includes(id));
    if (result) return true;
  }

  if (product && product.lectures.length === lectures.length) {
    // if (product._)
    if (discount.courses && discount.courses.length > 0) {
      result = discount.courses.some(({ id }) => id === product.id);
      if (result) return true;
    }
    if (discount.cycles && discount.cycles.length > 0) {
      result = discount.cycles.some(({ id }) => id === product.id);
      if (result) return true;
    }
  } else {
    const lecturesIds = lectures.map(({ id }) => id);
    result = discount.lectures.some(({ id }) => lecturesIds.includes(id));
  }

  return result;
};

const checkDiscountsCertSubsParam = (props, discounts) => {
  const {
    product,
  } = props;

  const __typename = product[0].src.__typename;

  return discounts.map((item) => {
    const discount = item.src ? item.src : item;
    const {
      certificateProducts: certificatesDiscount,
      subscriptionProducts: subscriptionsDiscount,
    } = discount;

    let productCheckValid = false;

    switch (true) {
      case discount.allProducts: {
        productCheckValid = true;
        break;
      }
      case
        __typename === 'CertificateProduct'
        && certificatesDiscount
        && certificatesDiscount.length > 0:
      {
        productCheckValid = certificatesDiscount.some(({ id }) => id === product[0].id);
        break;
      }
      case
        __typename === 'SubscriptionProduct'
        && subscriptionsDiscount
        && subscriptionsDiscount.length > 0:
      {
        productCheckValid = subscriptionsDiscount.some(({ id }) => id === product[0].id);
        break;
      }
      default: {}
    }

    return Object.assign(
      {},
      discount,
      {
        valid: productCheckValid,
      },
    );
  });
};

export default checkDiscountsCertSubsParam;
