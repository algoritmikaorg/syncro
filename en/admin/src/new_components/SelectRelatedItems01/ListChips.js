import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import FaceIcon from 'material-ui-icons/Face';
import Done from 'material-ui-icons/Done';
import grey from 'material-ui/colors/grey';

const styles = theme => ({
  chip: {
    margin: theme.spacing.unit,
  },
  svgIcon: {
    color: grey[800],
  },
  row: {
    display: 'flex',
    justifyContent: 'left',
    flexWrap: 'wrap',
    marginBottom: 16,
  },
});

const ListChips = (props) => {
  const { classes, selected } = props;
  return (
    <div className={classes.row}>
      {
        selected.map(({ id, title }) => (
          <Chip key={id} label={title} className={classes.chip} />
        ))
      }
    </div>
  );
}

ListChips.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListChips);
