import React from 'react';
import {
  DialogContent,
} from 'material-ui/Dialog';

import EnhancedTable from './../../EnhancedTable';
import FilterOfQuery from './../../FilterOfQuery';
import Chips from './../../ChipsArray';
import { selectedItems } from './../../../new_utils';

const RelationDialogContent = (props) => {
  const {
    tableTitle,
    columnData,
    selected,
    data,
    onSelect,
    multiple,
    all,
    filter,
    onFilterQuerySubmit,
    isRequested,
  } = props;

  return (
    <div>
      <Chips
        selected={selected}
        onChange={onSelect}
      />
      {
        filter &&
        <FilterOfQuery
          formTitle="Фильтр"
          fields={filter}
          isRequested={isRequested}
          onSubmit={onFilterQuerySubmit}
        />
      }
      <EnhancedTable
        selected={selected}
        onSelect={onSelect}
        tableTitle={tableTitle}
        columnData={columnData}
        data={data}
        multiple={multiple}
        all={all}
      />
    </div>
  );
};

export default RelationDialogContent;
