import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';

import RelationDialogContent from './RelationDialogContent';
import QueryWithProgress from './../../../new_containers/QueryWithProgress';
import { formatInputDataArrayForTable } from './../../../new_utils';

class RelationDialog extends Component {
  constructor(props) {
    super(props);
    const {
      selected,
      gqlQueryName,
      queryResultObjectName,
      formatInputData,
    } = this.props;

    let data = [];
    if (this.props[gqlQueryName] && this.props[gqlQueryName][queryResultObjectName]) {
      data = formatInputData(this.props[gqlQueryName][queryResultObjectName]);
    }
    this.state = {
      selected,
      data,
      formFilterData: null,
      isRequested: data ? false : true,
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleFilterQuerySubmit = this.handleFilterQuerySubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const {
      gqlQueryName,
      queryResultObjectName,
      formatInputData,
      selected,
    } = nextProps;

    let data = [];
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      data = formatInputData(nextProps[gqlQueryName][queryResultObjectName]);
    }

    if (selected.length > 0) {
      this.setState({
        selected,
        data,
        isRequested: false,
      });
    } else {
      this.setState({
        data,
        isRequested: false,
      });
    }
  }

  handleClose() {
    this.setState({
      selected: this.props.selected,
    }, this.props.onCancel);
  }

  handleSubmit() {
    this.props.onSubmit(this.state.selected);
  }

  handleSelect(selected) {
    this.setState({
      selected,
    });
  }

  refetch = async () => {
    const { gqlQueryName, queryResultObjectName, modifyQueryFilter, refetchLoading, refetchDone, formatInputData } = this.props;
    const { formFilterData } = this.state;

    const result = await this.props[gqlQueryName].refetch({ filter: modifyQueryFilter(formFilterData) });
    if (result.data) {
      const data = formatInputData(result.data[queryResultObjectName]);
      this.setState({
        data,
        isRequested: false,
      });
    } else {
      this.setState({
        isRequested: false,
      });
    }
  }

  handleFilterQuerySubmit(formFilterData) {
    this.setState({
      isRequested: true,
      formFilterData,
    }, this.refetch);
  }

  render() {
    const {
      fullScreen,
      dialogTitle,
      open,
      gql,
      gqlQueryName,
      queryResultObjectName,
      columnData,
      multiple,
      all,
      filter,
    } = this.props;
    const { selected, data, isRequested } = this.state;
    return (
      <div>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          {
            dialogTitle &&
            <DialogTitle id="responsive-dialog-title">
              {dialogTitle}
            </DialogTitle>
          }
          <DialogContent>
            <RelationDialogContent
              selected={selected}
              data={formatInputDataArrayForTable(data)}
              columnData={columnData}
              onSelect={this.handleSelect}
              multiple={multiple}
              all={all}
              filter={filter}
              onFilterQuerySubmit={this.handleFilterQuerySubmit}
              isRequested={isRequested}
            />
          </DialogContent>

          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Отменить
            </Button>
            <Button onClick={this.handleSubmit} color="primary" autoFocus>
              Сохранить
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

RelationDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

RelationDialog.defaultProps = {
  selected: [],
  formatInputData: data => data || [],
};

export default withMobileDialog()(RelationDialog);
