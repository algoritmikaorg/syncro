import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { LinearProgress } from 'material-ui/Progress';

const styles = {
  root: {
    width: '100%',
  },
  button: {},
  containerInner: {},
  container: {},
  form: {},
  menu: {},
  textField: {},
  fullWidth: {},
  input: {},
};

const LinearIndeterminate = (props) => {
  const { classes, queries, isRefetchRequested } = props;
  let show = false;
  if ((props.data && props.data.loading) || isRefetchRequested) {
    show = true;
  } else {
    show = queries.some((query) => {
      if (
        (Object.prototype.hasOwnProperty.call(props, query)
        &&
        (
          props[query].loading
          ||
          props[`${query}Loading`]
        ))
      ) {
        return true;
      }
      return false;
    });
  }


  return (
    <div className={classes.root}>
      {
        show &&
        <LinearProgress color="secondary" />
      }
    </div>
  );
};

LinearIndeterminate.propTypes = {
  classes: PropTypes.object.isRequired,
  queries: PropTypes.arrayOf(PropTypes.string),
};

LinearIndeterminate.defaultProps = {
  queries: [],
};

export default withStyles(styles)(LinearIndeterminate);
