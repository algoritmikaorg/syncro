import React, { Component } from 'react';
import Button from 'material-ui/Button';
import FileUpload from 'material-ui-icons/FileUpload';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';
import config from './../../config';
import { GC_AUTH_TOKEN } from './../../constants';


const { FILE_API_ENDPOINT } = config;

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  container: {
    width: '100%',
    margin: theme.spacing.unit,
  },
});

class FileUploadInput extends Component {
  constructor(props) {
    super(props);
    const {
      value,
    } = props;
    this.state = {
      secret: value,
      res: null,
      isRequested: false,
      file: null,
    };
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
      value,
    } = nextProps;
    this.setState({
      secret: value,
    });
  }

  handleButtonClick() {
  }

  handleChange(event) {
    const { files } = event.target;

    const name = Object.keys(files)
      .map(key => files[key].name);
      // .map(({ name }));

    this.setState({
      name,
      files,
      isRequested: true,
    }, this.uploadFile);
  }

  uploadFile = async () => {
    const { files } = this.state;
    const token = localStorage.getItem(GC_AUTH_TOKEN);
    const data = new FormData()
    data.append('data', files[0])

    try {
      const response = await fetch(
        FILE_API_ENDPOINT,
        {
          method: 'POST',
          body: data,
          headers: {
            Authorization: `Bearer ${token.replace(/\n/g, '')}`,
          },
        },
      );

      const file = await response.json();
      const { secret } = file;
      this.setState({
        isRequested: false,
        success: true,
        secret: [secret],
      }, () => {
        this.props.onLoaded([secret]);
      });
    } catch (e) {
      this.setState({
        isRequested: false,
        success: false,
        secret: null,
      }, () => {
        this.props.snackbar(new Error('Ошибка. Файл не загружен'));
      });
    }
  }

  render() {
    const {
      classes,
      label,
      multiple,
    } = this.props;
    const {
      name,
      isRequested,
      secret,
    } = this.state;

    return (
      <div className={classes.container}>
        <Typography type="title" gutterBottom>
          {label}
        </Typography>
        {/* <Typography type="body2" gutterBottom>
          Файл: { name.join(', ') || 'не выбран' }
        </Typography> */}
        <Typography type="body2" gutterBottom>
          Файл: { isRequested && 'Загрузка...'}
          {
            !isRequested && secret && secret.length > 0 &&
            <a href={`${FILE_API_ENDPOINT}/${secret[0]}`} target="_blank">{secret[0]}</a>
          }
          {!isRequested && secret && secret.length === 0 && 'не выбран'}
        </Typography>
        <Button
          className={classes.button}
          raised
          component="label"
          color="default"
          disabled={isRequested}
          onClick={this.handleButtonClick}
        >
          {'Загрузить'}
          <FileUpload className={classes.rightIcon} />
          <input
            onChange={this.handleChange}
            style={{ display: 'none' }}
            type="file"
            // multiple={multiple}
          />
        </Button>
      </div>
    );
  }
}

FileUploadInput.defaultProps = {
  value: [],
  onLoaded() {},
};

export default withStyles(styles)(FileUploadInput);
