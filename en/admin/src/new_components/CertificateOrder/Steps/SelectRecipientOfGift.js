import React from 'react';
import Typography from 'material-ui/Typography';
import SelectUser from './../../Order/CreateOrder/Steps/SelectUser';

const SelectRecipientOfGift = (props) => {
  const {
    recipientOfGift,
    onSelectRecipientOfGift,
  } = props;

  return (
    <div>
      <Typography type="title" gutterBottom>
        Сертификат в подарок
      </Typography>
      <Typography type="body1" gutterBottom>
        Если сертификат не в подарок, пропустите этот шаг
      </Typography>
      <SelectUser {...props} user={recipientOfGift} onSelectUser={onSelectRecipientOfGift} />
    </div>
  );
};

export default SelectRecipientOfGift;
