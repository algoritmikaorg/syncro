import React from 'react';
import Typography from 'material-ui/Typography';
import List, { ListItemText } from 'material-ui/List';

import gql from './../../../new_pages/Certificates/gql';
import {
  CREATE_CERTIFICATE_MUTATION,
  EDIT_CERTIFICATE_MUTATION,
  CHECK_ORDER_CERT_AND_SUB,
} from './../../../constants';
import modifyMutationOptions from './../../../new_pages/Certificates/modifyMutationOptions';
import Mutation from './../../../new_containers/Mutation';
import QueryWithOptionsAndMutation from './../../../new_containers/QueryWithOptionsAndMutation';

import OrderMutation from './MutationOrder';
import queryOptions from './queryOptions';

import checkOrder from './checkOrder';
import dataOfOrder from './dataOfOrder';

const Order = (props) => {
  const {
    onSuccessMutation,
    product,
    user,
    id,
    paymentId,
  } = props;

  const checkedOrderErrors = checkOrder(props);
  let certificate = null;
  if (checkedOrderErrors.length === 0) certificate = dataOfOrder(props);

  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let resultMutationMessageSuccses = '';

  if (id && paymentId) {
    gqlMutationName = EDIT_CERTIFICATE_MUTATION;
    mutationResultObjectName = 'updateCertificate';
    resultMutationMessageSuccses = 'Сертификат обновлен';
  } else {
    gqlMutationName = CREATE_CERTIFICATE_MUTATION;
    mutationResultObjectName = 'createCertificate';
    resultMutationMessageSuccses = 'Сертификат создан';
  }

  return (
    <div>
      <Typography type="title" gutterBottom>
        Сохранение заказа
      </Typography>
      {
        checkedOrderErrors.length === 0 &&
        <QueryWithOptionsAndMutation
          gql={gql}
          gqlQueryName={CHECK_ORDER_CERT_AND_SUB}
          queryResultObjectName="checkOrderCertAndSub"
          queryOptions={queryOptions(certificate)}
          gqlMutationName={gqlMutationName}
          mutationResultObjectName={mutationResultObjectName}
          resultMutationMessageSuccses={resultMutationMessageSuccses}
          modifyMutationOptions={modifyMutationOptions}
          onSuccessMutation={onSuccessMutation}
        >
          <OrderMutation {...props} certificate={certificate} id={id} paymentId={paymentId} />
        </QueryWithOptionsAndMutation>
      }
      {
        checkedOrderErrors.length > 0 &&
        <div style={{ marginTop: 20 }} >
          <Typography type="title" gutterBottom>
            Ошибки
          </Typography>
          <List>
            {
              checkedOrderErrors.map((error, index) => (
                <ListItemText
                  key={index}
                  primary={error}
                />
              ))
            }
          </List>
        </div>
      }
    </div>

    // <Mutation
    //   gql={gql}
    //   gqlMutationName={gqlMutationName}
    //   mutationResultObjectName='Certificates'
    //   onSuccessMutation={onSuccessMutation}
    // >
    //
    // </Mutation>
  );
};

export default Order;
