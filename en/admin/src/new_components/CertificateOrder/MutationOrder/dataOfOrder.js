import calcOrderPrice from './../calcOrderPrice';

const dataOfOrder = (props) => {
  const {
    id,
    paymentId,
    recipientOfGift,
    product,
    user,
    discounts,
    additionalDiscounts,
    paymentType,
  } = props;

  const gift = recipientOfGift && recipientOfGift.length > 0 ? true : false;

  const {
    orderPrice,
    discountRate,
    additionalDiscountsRate,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
    totalDiscountPrice: discountPrice,
    totalPrice,
  } = calcOrderPrice({
    product,
    additionalDiscounts,
    discounts,
  });

  const lecturesCount = product[0].lecturesCount === 'безлимит' ? -1 : product[0].lecturesCount;

  const discountsIds = discountsWithMaxRate.map(({ id }) => id);
  const additionalDiscountsIds = additionalDiscountsWithMaxRate.map(({ id }) => id);

  const certificate = {
    userId: gift ? recipientOfGift[0].id : user[0].id,
    draft: true,
    gift,
    code: 'draft',
    title: product[0].title,
    price: product[0].price,
    // lecturesCount: parseInt(lecturesCount, 10),
    // unusedLectures: parseInt(lecturesCount, 10),
    // start: new Date(),
    // end: moment().add(product[0].months, 'months'),
    productId: product[0].id,
    paymentId: null,
    // prefix: product[0].prefix,
    // personsCount: parseInt(product[0].personsCount, 10),
  };

  const dataOfPayment = {
    source: 'ADMIN',
    userAnOrder: user[0].src,
    userAnGift: gift ? recipientOfGift[0] : null,
    discounts,
    additionalDiscounts,
    certificate: product[0].src,
    order: {
      orderPrice,
      discountRate,
      additionalDiscountsRate,
      totalPrice,
    },
  };

  if (paymentType === 'CASH') {
    certificate.payment = {
      orderPrice,
      discountPrice,
      totalPrice,
      type: 'CASH',
      userId: user[0].id,
      status: 'PAID',
      discountsIds,
      additionalDiscountsIds,
      data: dataOfPayment,
    };
  }
  if (paymentType === 'INTERNET_ACQUIRING') {
    certificate.payment = {
      orderPrice,
      discountPrice,
      totalPrice,
      type: 'INTERNET_ACQUIRING',
      userId: user[0].id,
      status: 'PENDING',
      discountsIds,
      additionalDiscountsIds,
      data: dataOfPayment,
    };
  }

  return certificate;
};

export default dataOfOrder;
