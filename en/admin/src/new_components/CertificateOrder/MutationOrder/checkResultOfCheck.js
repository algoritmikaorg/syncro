const checkResultOfCheck = (order, result) => {
  const resultOfChecking = JSON.parse(result.resultOfChecking);
  const resultOfCalcOrder = JSON.parse(result.resultOfCalcOrder);

  const {
    EntityNoPublic,
    PromocodeWithErrors,
    PromocodesWithErrors,
    DiscountsWithErrors,
  } = resultOfChecking;

  const output = {
    EntityNoPublic: EntityNoPublic ? {
      id: EntityNoPublic.id,
      title: EntityNoPublic.title,
    } : null,
    PromocodeWithErrors: PromocodeWithErrors ? {
      id: PromocodeWithErrors.id,
      title: PromocodeWithErrors.title,
    } : null,
    PromocodesWithErrors: PromocodesWithErrors ? PromocodesWithErrors
      .map(({ id, title }) => ({ id, title })) : null,
    DiscountsWithErrors: DiscountsWithErrors ? DiscountsWithErrors
      .map(({ id, title }) => ({ id, title })) : null,
  };

  if (
    EntityNoPublic
    || PromocodeWithErrors
    || PromocodesWithErrors
    || DiscountsWithErrors
  ) {
    return {
      resultOfChecking: output,
    };
  }

  const {
    order: {
      orderPrice,
      discountsPrice,
      discountsRate,
      totalPrice,
    },
  } = order;

  if (
    orderPrice !== resultOfCalcOrder.orderPrice
    || totalPrice !== resultOfCalcOrder.totalPrice
  ) {
    return {
      resultOfCalcOrder,
    };
  }

  return {
    resultOfChecking: null,
    resultOfCalcOrder: null,
    order: resultOfCalcOrder,
  };
};

export default checkResultOfCheck;
