import React, { Component } from 'react';

import Result from './Result';
import Steps from './Steps';
import MobileStepper from './../Stepper/MobileStepper';

import { precisionRound } from './../../new_utils';
import calcOrderPrice from './calcOrderPrice';

class CertificateOrder extends Component {
  constructor(props) {
    super(props);
    const {
      id,
      paymentId,
      user,
      product,
      discounts,
      additionalDiscounts,
      recipientOfGift,
      paymentType,
    } = props;

    this.state = {
      activeStep: 0,
      id,
      paymentId,
      user,
      product,
      discounts,
      additionalDiscounts,
      recipientOfGift,
      paymentType,
    };

    this.handleBack = this.handleBack.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleSelectUser = this.handleSelectUser.bind(this);
    this.handleSelectProduct = this.handleSelectProduct.bind(this);
    this.handleSelectDiscounts = this.handleSelectDiscounts.bind(this);
    this.handleSelectAdditionalDiscounts = this.handleSelectAdditionalDiscounts.bind(this);
    this.handleSuccsesMutation = this.handleSuccsesMutation.bind(this);
    this.handleSelectRecipientOfGift = this.handleSelectRecipientOfGift.bind(this);
    this.handleSelectPaymentType = this.handleSelectPaymentType.bind(this);
  }
  handleNext() {
    this.setState({
      activeStep: this.state.activeStep + 1,
    });
  }

  handleBack() {
    this.setState({
      activeStep: this.state.activeStep - 1,
    });
  }

  handleSelectUser(selectedIds, selected) {
    this.setState({
      user: selected || [],
    });
  }
  handleSelectRecipientOfGift(selectedIds, selected) {
    this.setState({
      recipientOfGift: selected || [],
    });
  }
  handleSelectProduct(selectedIds, selected) {
    this.setState({
      product: selected || [],
    });
  }
  handleSelectDiscounts(selected) {
    this.setState({
      discounts: selected,
    });
  }
  handleSelectAdditionalDiscounts(selectedIds, selected) {
    this.setState({
      additionalDiscounts: selected || [],
    });
  }
  handleSelectPaymentType(event) {
    const { value } = event.target;
    this.setState({
      paymentType: value,
    });
  }
  handleSuccsesMutation() {
    const { history } = this.props;
    history.push('/certificates');
  }
  render() {
    const {
      id,
      paymentId,
      user,
      product,
      discounts,
      additionalDiscounts,
      recipientOfGift,
      activeStep,
      paymentType,
    } = this.state;

    const {
      orderPrice,
      discountRate,
      additionalDiscountsRate,
      totalDiscountPrice,
      totalPrice,
    } = calcOrderPrice({
      product,
      additionalDiscounts,
      discounts,
    });

    return (
      <div>
        <Result
          user={user}
          product={product}
          discounts={discounts}
          recipientOfGift={recipientOfGift}
          additionalDiscounts={additionalDiscounts}
          paymentType={paymentType}
        />
        <div style={{ marginTop: 25, marginBottom: 25 }}>
          <Steps
            activeStep={activeStep}
            onSelectUser={this.handleSelectUser}
            onSelectRecipientOfGift={this.handleSelectRecipientOfGift}
            onSelectProduct={this.handleSelectProduct}
            onSelectDiscounts={this.handleSelectDiscounts}
            onSelectAdditionalDiscounts={this.handleSelectAdditionalDiscounts}
            onSelectPaymentType={this.handleSelectPaymentType}
            onSuccessMutation={this.handleSuccsesMutation}
            id={id}
            paymentId={paymentId}
            user={user}
            recipientOfGift={recipientOfGift}
            product={product}
            discounts={discounts}
            additionalDiscounts={additionalDiscounts}
            paymentType={paymentType}
            orderPrice={orderPrice}
            discountPrice={totalDiscountPrice}
            totalPrice={totalPrice}
          />
        </div>
        <MobileStepper
          steps={6}
          activeStep={activeStep}
          onBack={this.handleBack}
          onNext={this.handleNext}
        />
      </div>
    );
  }
}

CertificateOrder.defaultProps = {
  user: [],
  product: [],
  discounts: [],
  additionalDiscounts: [],
  recipientOfGift: [],
};

export default CertificateOrder;
