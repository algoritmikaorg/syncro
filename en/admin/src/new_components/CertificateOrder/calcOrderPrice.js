import calcSimpleDiscounts from './../Order/calcSimpleDiscounts';

const calcOrderPrice = ({ product, discounts, additionalDiscounts }) => {

  let orderPrice = 0;
  let totalPrice = 0;

  let totalDiscountPrice = 0;

  if (product && product.length > 0) {
    orderPrice = product.map(({ price }) => price).reduce((a, b) => a + b);
  }

  const {
    discountRate,
    additionalDiscountsRate,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
  } = calcSimpleDiscounts(discounts, additionalDiscounts);

  let totalDiscountRate = discountRate + additionalDiscountsRate;
  totalDiscountRate = totalDiscountRate > 100 ? 100 : totalDiscountRate;

  if (totalDiscountRate > 100) {
    totalPrice = 0;
  } else {
    totalDiscountPrice = (totalDiscountRate * orderPrice) / 100;
    totalPrice = orderPrice - totalDiscountPrice;
  }


  return {
    orderPrice,
    discountRate,
    additionalDiscountsRate,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
    totalDiscountPrice,
    totalPrice,
  };
};

export default calcOrderPrice;
