import React, { Component } from 'react';
import Order from './../CertificateOrder';

class UpdateCertificateOrderPaid extends Component {
  constructor(props) {
    super(props);
    const { order } = props;
    this.state = {
      order,
    };
  }
  componentWillReceiveProps(nextProps) {
    const { order } = nextProps;
    this.setState({
      order,
    });
  }
  render() {
    const { order } = this.state;
    return (
      <div>Paid</div>
    );
  }

}

export default UpdateCertificateOrderPaid;
