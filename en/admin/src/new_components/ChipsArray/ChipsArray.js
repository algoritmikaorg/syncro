import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Chip from 'material-ui/Chip';
import Typography from 'material-ui/Typography';

const styles = theme => ({
  chip: {
    margin: theme.spacing.unit / 2,
  },
  row: {
    display: 'flex',
    justifyContent: 'left',
    flexWrap: 'wrap',
    minHeight: 40,
    marginBottom: 16,
  },
});

class ChipsArray extends React.Component {
  constructor(props) {
    super(props);
    const { selected } = props;
    this.state = {
      selected,
    };
  }
  // state = {
  //   chipData: [
  //     { key: 0, label: 'Angular' },
  //     { key: 1, label: 'JQuery' },
  //     { key: 2, label: 'Polymer' },
  //     { key: 3, label: 'ReactJS' },
  //     { key: 4, label: 'Vue.js' },
  //   ],
  // };

  componentWillReceiveProps(nextProps) {
    const { selected } = nextProps;
    this.setState({
      selected,
    });
  }

  handleDelete = n => () => {
    const selected = [...this.state.selected];
    const index = selected.indexOf(n);
    selected.splice(index, 1);
    this.setState({ selected }, this.props.onChange(selected));
  }


  render() {
    const { classes } = this.props;
    const { selected } = this.state;
    return (
      <div className={classes.row}>
        {
          selected.length > 0 ?
          (
            selected.map(n => (
              <Chip
                label={n.title}
                key={n.id}
                onDelete={this.handleDelete(n)}
                className={classes.chip}
              />
            ))
          ) :
          (

            <Typography type="subheading">Ничего не выбрано</Typography>
          )
        }
      </div>
    );
  }
}

ChipsArray.propTypes = {
  classes: PropTypes.object.isRequired,
};

ChipsArray.defaultProps = {
  selected: [],
};

export default withStyles(styles)(ChipsArray);
