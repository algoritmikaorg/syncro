import React, { Component } from 'react';

import FilterOfQuery from './../FilterOfQuery';
import BasicTable from './../BasicTable';
import EnhancedTableForList from './../EnhancedTableForList';
import Actions from './../Actions';
import TabsContainer from './../../new_containers/TabsContainer';
import { formatInputDataArrayForTable, formatOutputFormData } from './../../new_utils';

class List extends Component {
  constructor(props) {
    super(props);

    const {
      gqlQueryName,
      gqlMutationName,
      queryResultObjectName,
      modifyQueryResponse,
    } = props;

    let data = [];

    if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
      if (modifyQueryResponse && modifyQueryResponse instanceof Function) {
        data = modifyQueryResponse(props[gqlQueryName][queryResultObjectName]);
        data = formatInputDataArrayForTable(data);
      } else {
        data = formatInputDataArrayForTable(props[gqlQueryName][queryResultObjectName]);
      }
    }

    this.state = {
      data,
      isRequested: true,
      formFilterData: null,
    };
    this.handleFilterQuerySubmit = this.handleFilterQuerySubmit.bind(this);
    this.handleView = this.handleView.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleChangeTab = this.handleChangeTab.bind(this);
    this.refetch = this.refetch.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
      gqlQueryName,
      queryResultObjectName,
      modifyQueryFilter,
      modifyQueryResponse,
    } = nextProps;

    let isRequested = false
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName].loading) {
      isRequested = true;
    }
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName].error) {
      isRequested = true;
      this.props.snackbar(nextProps[gqlQueryName].error);
      this.setState({
        isRequested: false,
      });
      return false;
    }

    let data = [];
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      if (modifyQueryResponse && modifyQueryResponse instanceof Function) {
        data = modifyQueryResponse(nextProps[gqlQueryName][queryResultObjectName]);
        data = formatInputDataArrayForTable(data);
      } else {
        data = formatInputDataArrayForTable(nextProps[gqlQueryName][queryResultObjectName]);
      }
    }

    this.setState({
      isRequested,
      data,
    });
  }

  refetch = async () => {
    const {
      gqlQueryName,
      queryResultObjectName,
      modifyQueryFilter,
      modifyQueryOptions,
      refetchLoading,
      refetchDone,
      modifyQueryResponse,
    } = this.props;

    const { formFilterData } = this.state;

    let options = {};
    if (modifyQueryOptions && modifyQueryOptions instanceof Function) {
      options = modifyQueryFilter(this.props, formFilterData || {});
    } else {
      options = { filter: modifyQueryFilter(this.props, formFilterData || {}) };
    }

    const result = await this.props[gqlQueryName].refetch(options);

    if (result.data) {
      let data = null;
      if (modifyQueryResponse && modifyQueryResponse instanceof Function) {
        data = modifyQueryResponse(result.data[queryResultObjectName]);
        data = formatInputDataArrayForTable(data);
      } else {
        data = formatInputDataArrayForTable(result.data[queryResultObjectName]);
      }

      this.setState({
        data,
        isRequested: false,
      });
    } else {
      this.setState({
        isRequested: false,
      });
    }
  }

  handleChangeTab(tab) {
    const { formFilterData } = this.state;
    const newFormFilterData = Object.assign({}, formFilterData, { tab });
    this.setState({
      isRequested: true,
      formFilterData: newFormFilterData,
    }, this.refetch);
  }

  handleFilterQuerySubmit(inputFormFilterData) {
    const { fields } = this.props;
    const formFilterData = Object.assign({}, this.state.formFilterData, inputFormFilterData);
    this.setState({
      isRequested: true,
      formFilterData: formatOutputFormData(formFilterData, fields.filter),
    }, this.refetch);
  }
  handleEdit(n) {
    const {
      linkForUpdateMaterial,
      history,
      replaceMutationAndViewMaterialLink,
    } = this.props;
    let link = linkForUpdateMaterial;
    if (replaceMutationAndViewMaterialLink) {
      link = replaceMutationAndViewMaterialLink(n, link);
    }
    history.push(`${link}/${n.id}`);
  }
  handleView(n) {
    const {
      linkForViewMaterial,
      history,
      replaceMutationAndViewMaterialLink,
    } = this.props;
    let link = linkForViewMaterial;
    if (replaceMutationAndViewMaterialLink) {
      link = replaceMutationAndViewMaterialLink(n, link);
    }

    history.push(`${link}/${n.id}`);
  }

  render() {
    const {
      columnData,
      gqlQueryName,
      queryResultObjectName,
      fields,
      actionsButtons,
      tabs,
      showMenu,
      coloredRow,
    } = this.props;
    const {
      isRequested,
      data,
    } = this.state;

    const Table = (props) => (
      <EnhancedTableForList
        {...props}
        data={data}
        columnData={columnData}
        coloredRow={coloredRow}
        isRequested={isRequested}
        onView={this.handleView}
        onEdit={this.handleEdit}
        onDelete={this.handleDelete}
        showDeleteAction
      />
    );

    return (
      <div style={{ marginBottom: 20 }}>
        <Actions
          items={actionsButtons}
        />
        <FilterOfQuery
          formTitle="Фильтр"
          fields={fields.filter}
          isRequested={isRequested}
          onSubmit={this.handleFilterQuerySubmit}
        />
        {
          tabs.length > 0 ?
          (
            <TabsContainer
              tabs={tabs}
              onChange={this.handleChangeTab}
            >
              {
                tabs.map((tab, index) => (
                  <Table
                    {...this.props}
                    key={`${tab.id}`}
                    filterType={tab.id}
                  />
                ))
              }
            </TabsContainer>
          ) :
          (
            <EnhancedTableForList
              {...this.props}
              data={data}
              columnData={columnData}
              isRequested={isRequested}
              onView={this.handleView}
              onEdit={this.handleEdit}
              onDelete={this.handleDelete}
              showMenu={showMenu}
              coloredRow={coloredRow}
              showDeleteAction
            />
          )
        }
      </div>
    );
  }
}

List.defaultProps = {
  tabs: [],
};

export default List;
