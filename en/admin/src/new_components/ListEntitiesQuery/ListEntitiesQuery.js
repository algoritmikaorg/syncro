import React, { Component } from 'react';
import { compose } from 'react-apollo';

import Query from './../../new_containers/Query';
import QueryWithQueryOptions from './../../new_containers/QueryWithQueryOptions';

import ListQuery from './ListQuery';

class ListEntitiesQuery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRefetchRequested: false,
    };
    this.refetchLoading = this.refetchLoading.bind(this);
    this.refetchDone = this.refetchDone.bind(this);
  }
  refetchLoading() {
    this.setState({
      isRefetchRequested: true,
    });
  }
  refetchDone() {
    this.setState({
      isRefetchRequested: false,
    });
  }
  render() {
    const {
      match,
      gql,
      gqlQueryName,
      queryResultObjectName,
      columnData,
      modifyQueryFilter,
      modifyQueryOptions,
      tabs,
    } = this.props;
    const {
      isRefetchRequested,
    } = this.state;

    if (modifyQueryOptions) {
      return (
        <QueryWithQueryOptions
          match={match}
          gql={gql}
          gqlQueryName={gqlQueryName}
          queryResultObjectName={queryResultObjectName}
          modifyQueryOptions={modifyQueryOptions}
          isRefetchRequested={isRefetchRequested}
        >
          <ListQuery
            {...this.props}
            refetchLoading={this.refetchLoading}
            refetchDone={this.refetchDone}
            isRefetchRequested={isRefetchRequested}
          />
        </QueryWithQueryOptions>
      );
    }

    return (
      <Query
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryFilter={modifyQueryFilter}
        isRefetchRequested={isRefetchRequested}
      >
        <ListQuery
          {...this.props}
          refetchLoading={this.refetchLoading}
          refetchDone={this.refetchDone}
          isRefetchRequested={isRefetchRequested}
        />
      </Query>
    );
  }
}

ListEntitiesQuery.defaultProps = {
  tabs: [],
};

export default ListEntitiesQuery;
