import React, { Component } from 'react';
import { compose } from 'react-apollo';

import QueryWithProgress from './../../new_containers/QueryWithProgress';

import RedirectToMutation from './RedirectToMutation';

const EntityQueryAndMutationRedirect = (props) => {
  const {
    gql,
    gqlQueryName,
    queryResultObjectName,
    columnData,
    modifyQueryFilter,
    gqlMutationName,
    tabs,
  } = props;

  return (
    <QueryWithProgress
      gql={gql}
      gqlQueryName={gqlQueryName}
      queryResultObjectName={queryResultObjectName}
      modifyQueryFilter={modifyQueryFilter}
    >
      <RedirectToMutation
        {...props}
      />
    </QueryWithProgress>
  );
};

EntityQueryAndMutationRedirect.defaultProps = {
  modifyQueryFilter: () => ({}),
};

export default EntityQueryAndMutationRedirect;
