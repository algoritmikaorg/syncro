import React from 'react';
import moment from 'moment';
import TextField from 'material-ui/TextField';
import {
  FormControlLabel,
} from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';

const Date = (props) => {
  const {
    label,
    type,
    className,
    value,
    id,
    disabled,
    required,
    error,
    helperText,
    onChange,
  } = props;

  return (
    <FormControlLabel
      control={
        <Checkbox
          checked={value}
          onChange={onChange(id)}
          value={value.toString()}
        />
      }
      label={label}
    />
  );
};

export default Date;
