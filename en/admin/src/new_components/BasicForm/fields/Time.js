import React from 'react';
import moment from 'moment';
import TextField from 'material-ui/TextField';

const Time = (props) => {
  const {
    label,
    type,
    className,
    value,
    id,
    disabled,
    required,
    error,
    helperText,
    onChange,
  } = props;

  return (
    <TextField
      label={label}
      type="time"
      value={value}
      onChange={onChange(id)}
      error={error}
      className={className}
      InputLabelProps={{
        shrink: true,
      }}
      InputProps={{
        step: 300, // 5 min
      }}
    />
  );
};

export default Time;
