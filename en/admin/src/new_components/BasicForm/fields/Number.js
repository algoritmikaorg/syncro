import React from 'react';

import TextField from 'material-ui/TextField';

const Number = (props) => {
  const {
    label,
    type,
    subType,
    className,
    classNameFullWidth,
    value,
    id,
    disabled,
    required,
    error,
    helperText,
    onChange,
  } = props;

  switch (subType) {
    case 'fullWidth': {
      return (
        <TextField
          type={'number'}
          className={classNameFullWidth}
          value={value}
          onChange={onChange(id)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          label={label}
          InputLabelProps={{
            shrink: true,
          }}
          min={0}
          helperText={helperText}
          fullWidth
        />
      );
    }
    default: {
      return (
        <TextField
          type={'number'}
          className={className}
          value={value}
          onChange={onChange(id)}
          margin="normal"
          disabled={disabled}
          required={required}
          error={error}
          label={label}
          InputLabelProps={{
            shrink: true,
          }}
          min={0}
          helperText={helperText}
        />
    );
    }
  }
};

export default Number;
