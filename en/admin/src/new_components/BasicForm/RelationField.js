import React from 'react';
import { withStyles } from 'material-ui/styles';

import EventOfLectureNoMutations from './../../new_components/EventOfLectureNoMutations';

import TagsSelect from './relations/TagsSelect';
import Tags from './relations/Tags';
import Users from './relations/Users';
import LecturersSelect from './relations/LecturersSelect';
import Lecturers from './relations/Lecturers';
import LocationsSelect from './relations/LocationsSelect';
import Locations from './relations/Locations';
import Lectures from './relations/Lectures';
import Recomendations from './relations/Recomendations';
import Courses from './relations/Courses';
import Cycles from './relations/Cycles';
import Subscriptions from './relations/Subscriptions';
import Certificates from './relations/Certificates';
import Discounts from './relations/Discounts';
import CuratorSelect from './relations/CuratorSelect';
import Acquirings from './relations/Acquirings';
import AcquiringSelect from './relations/AcquiringSelect';

import Text from './relations/Text';

const styles = theme => ({
  container: theme.mixins.gutters({
    paddingBottom: 20,
    backgroundColor: 'white',
    paddingTop: 20,
  }),
  form: {
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  fullWidth: {
    // marginTop: 20,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
    marginTop: 25,
    marginBottom: 25,
  },
  dateContainer: {
    display: 'inline-flex',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  tagsDemo: {
    display: 'inline-flex',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
  },
  input: {},
});

const RelationField = (props) => {
  const {
    field,
    onChangeNoEvent,
    onChange,
    disabledAllInputs,
    errors,
    values,
    classes,
    srcData,
  } = props;

  const {
    relation,
    disabled: disabledField,
    required,
    helperText,
    label,
    id,
    defaultValue,
    multiple,
  } = field;

  const value = values[id] || defaultValue || '';
  const error = errors.indexOf(id) !== -1;
  const disabled = disabledField || disabledAllInputs;

  switch (relation) {
    case 'user': {
      return (
        <div className={classes.fullWidth}>
          <Users
            {...field}
            value={value}
            srcData={srcData ? [srcData.user] : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'users': {
      return (
        <div className={classes.fullWidth}>
          <Users
            {...field}
            value={value}
            srcData={srcData ? srcData.users : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'tags': {
      return (
        <div className={classes.fullWidth}>
          <Tags
            {...field}
            value={value}
            srcData={srcData ? srcData.tags : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'acquirings-select': {
      return (
        <AcquiringSelect
          {...field}
          value={value}
          error={error}
          onChangeNoEvent={onChangeNoEvent}
          multiple={multiple}
        />
      );
    }
    case 'tags-select': {
      return (
        <TagsSelect
          {...field}
          value={value}
          error={error}
          onChangeNoEvent={onChangeNoEvent}
          multiple={multiple}
        />
      );
    }
    case 'lecturers': {
      return (
        <div className={classes.fullWidth}>
          <Lecturers
            {...field}
            value={value}
            srcData={srcData ? srcData.lecturers : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'lecturers-select': {
      return (
        <LecturersSelect
          {...field}
          value={value}
          error={error}
          onChangeNoEvent={onChangeNoEvent}
          multiple={multiple}
        />
      );
    }
    case 'locations': {
      return (
        <div className={classes.fullWidth}>
          <Locations
            {...field}
            value={value}
            srcData={srcData ? srcData.locations : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'locations-select': {
      return (
        <LocationsSelect
          {...field}
          value={value}
          error={error}
          onChangeNoEvent={onChangeNoEvent}
          multiple={multiple}
        />
      );
    }
    case 'acquirings': {
      return (
        <div className={classes.fullWidth}>
          <Acquirings
            {...field}
            value={value}
            srcData={srcData ? srcData.locations : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'lecture': {
      return (
        <div className={classes.fullWidth}>
          <Lectures
            {...field}
            value={value}
            srcData={srcData ? [srcData.lecture] : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'lectures': {
      return (
        <div className={classes.fullWidth}>
          <Lectures
            {...field}
            value={value}
            srcData={srcData ? srcData.lectures : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'recommendedLectures': {
      return (
        <div className={classes.fullWidth}>
          <Recomendations
            {...field}
            value={value}
            srcData={srcData}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'courses': {
      return (
        <div className={classes.fullWidth}>
          <Courses
            {...field}
            value={value}
            srcData={srcData ? srcData.courses : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'course': {
      return (
        <div className={classes.fullWidth}>
          <Courses
            {...field}
            value={value}
            srcData={srcData ? srcData.course : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'cycles': {
      return (
        <div className={classes.fullWidth}>
          <Cycles
            {...field}
            value={value}
            srcData={srcData ? srcData.cycles : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'cycle': {
      return (
        <div className={classes.fullWidth}>
          <Cycles
            {...field}
            value={value}
            srcData={srcData ? srcData.cycle : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'certificateProducts': {
      return (
        <div className={classes.fullWidth}>
          <Certificates
            {...field}
            value={value}
            srcData={srcData ? srcData.certificateProducts : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'subscriptionProducts': {
      return (
        <div className={classes.fullWidth}>
          <Subscriptions
            {...field}
            value={value}
            srcData={srcData ? srcData.subscriptionProducts : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'certificates': {
      return (
        <div className={classes.fullWidth}>
          <Certificates
            {...field}
            value={value}
            srcData={srcData ? srcData.certificates : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'subscriptions': {
      return (
        <div className={classes.fullWidth}>
          <Subscriptions
            {...field}
            value={value}
            srcData={srcData ? srcData.subscriptions : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'discounts': {
      return (
        <div className={classes.fullWidth}>
          <Discounts
            {...field}
            value={value}
            srcData={srcData ? srcData.discounts : []}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'curator-select': {
      return (
        <div className={classes.fullWidth}>
          <CuratorSelect
            {...field}
            value={value}
            error={error}
            onChangeNoEvent={onChangeNoEvent}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'events': {
      return (
        <div className={classes.fullWidth}>
          <EventOfLectureNoMutations
            value={value}
            srcData={srcData ? srcData.events : []}
            // onChange={onChangeNoEvent(id)}
            onSelect={onChangeNoEvent(id)}
            lectureId={srcData ? srcData.id : null}
          />
        </div>
      );
    }
    default: {
      return (
        <Text
          {...field}
          value={value}
          error={error}
          className={classes.fullWidth}
          onChange={onChange}
        />
      );
    }
  }
};

export default withStyles(styles)(RelationField);
