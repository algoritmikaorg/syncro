import React from 'react';

import BasicField from './BasicField';

const BasicFields = (props) => {
  const { fields } = props;
  return fields
    .filter(({ type }) => type !== 'relation')
    .map(field => (
      <BasicField key={field.id} {...props} field={field} />
    ));
};

export default BasicFields;
