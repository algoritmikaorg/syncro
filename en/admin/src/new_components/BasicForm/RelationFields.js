import React from 'react';

import RelationField from './RelationField';

const RelationFields = (props) => {
  const { fields } = props;
  const currentFields = fields.filter
  return fields
    .filter(({ type }) => type === 'relation')
    .map(field => (
      <RelationField key={field.id} {...props} field={field} />
    ));
};

export default RelationFields;
