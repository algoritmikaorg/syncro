import React from 'react';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';

import Multilain from './fields/Multilain';
import Text from './fields/Text';
import Number from './fields/Number';
import Checkbox from './fields/Checkbox';
import DateField from './fields/Date';
import Time from './fields/Time';
import Select from './fields/Select';
import Alias from './fields/Alias';

import ImageUpload from './../ImageUpload';
import FileUploadInput from './../FileUploadInput';
import ColorPicker from './../ColorPicker';

const styles = theme => ({
  container: theme.mixins.gutters({
    paddingBottom: 20,
    backgroundColor: 'white',
    paddingTop: 20,
  }),
  form: {
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
  dateContainer: {
    display: 'inline-flex',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  tagsDemo: {
    display: 'inline-flex',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
  },
  fullWidthNoMargin: {
    width: '100%',
  },
  input: {},
});

const BasicField = (props) => {
  const {
    field,
    onChange,
    onChangeNoEvent,
    disabledAllInputs,
    errors,
    values,
    classes,
    snackbar,
  } = props;

  const {
    type,
    autoFocus,
    disabled: disabledField,
    required,
    helperText,
    label,
    id,
    defaultValue,
    width,
    height,
    size,
    fullWidth,
  } = field;

  let value = defaultValue || '';
  if (values && Object.prototype.hasOwnProperty.call(values, id)) value = values[id] || '';
  const error = errors.indexOf(id) !== -1;
  const disabled = disabledField || disabledAllInputs;

  switch (type) {
    case 'fileUpload': {
      return (
        <FileUploadInput
          {...field}
          snackbar={snackbar}
          value={value || []}
          error={error}
          disabled={disabled}
          className={classes.fullWidth}
          onLoaded={onChangeNoEvent(id)}
        />
      );
    }
    case 'alias': {
      return (
        <Alias
          {...field}
          value={value}
          aliasId={values.aliasId}
          error={error}
          disabled={disabled}
          className={classes.fullWidth}
          onChange={onChangeNoEvent(id)}
          autoFocus={autoFocus}
        />
      );
    }
    case 'multiline': {
      return (
        <Multilain
          {...field}
          value={value}
          error={error}
          disabled={disabled}
          className={classes.fullWidth}
          onChange={onChange}
          autoFocus={autoFocus}
        />
      );
    }
    case 'date': {
      return (
        <DateField
          {...field}
          value={value}
          error={error}
          disabled={disabled}
          className={classes.textField}
          onChange={onChange}
        />
      );
    }
    case 'time': {
      return (
        <Time
          {...field}
          value={value}
          error={error}
          disabled={disabled}
          className={classes.textField}
          onChange={onChange}
        />
      );
    }
    case 'checkbox': {
      return (
        <Checkbox
          {...field}
          value={value}
          error={error}
          disabled={disabled}
          className={classes.textField}
          onChange={onChange}
        />
      );
    }
    case 'number': {
      return (
        <Number
          {...field}
          value={value}
          error={error}
          disabled={disabled}
          className={classes.textField}
          classNameFullWidth={classes.fullWidth}
          onChange={onChange}
        />
      );
    }
    case 'select': {
      return (
        <Select
          {...field}
          value={value}
          error={error}
          disabled={disabled}
          classes={classes}
          onChange={onChange}
        />
      );
    }
    case 'image': {
      return (
        <div className={classes.fullWidthNoMargin}>
          <ImageUpload
            {...field}
            snackbar={snackbar}
            value={value || ''}
            error={error}
            disabled={disabled}
            onLoaded={onChangeNoEvent(id)}
          />
        </div>
      );
    }
    case 'demo-tags': {
      return (
        <div
          className={classes.tagsDemo}
          style={{
            backgroundColor: values.color,
            color: values.textColor,
          }}
          margin="normal"
        >
          {values.title}
        </div>
      );
    }
    case 'colorPicker': {
      return (
        <ColorPicker
          name={id}
          value={value}
          className={classes.textField}
          onChange={onChange(id)}
          onChangeNoEvents={onChangeNoEvent(id)}
          label={label}
          helperText={helperText}
          disabled={disabled}
          required={required}
          error={error}
        />
      );
    }
    default: {
      const fieldClassNames = classNames(
        {
          [classes.fullWidthNoMargin]: fullWidth || false,
          [classes.textField]: !fullWidth,
        },
      );
      return (
        <Text
          {...field}
          value={value}
          error={error}
          disabled={disabled}
          className={fieldClassNames}
          onChange={onChange}
          autoFocus={autoFocus}
        />
      )
    }
  }
};

export default withStyles(styles)(BasicField);
