import React from 'react';

import Select from './Select';

import gql from './../../../new_pages/Lecturers/gql';
import { ALL_LECTURERS_QUERY_SHORT } from './../../../constants';

const modifyQueryOptions = (modifyQueryFilter) => {
  return (ownProps) => {
    return {
      variables: {
        orderBy: 'firstName_ASC',
        filter: modifyQueryFilter(ownProps.allQueryFilter, ownProps.filterType),
      },
    };
  };
};

const LecturersSelect = (props) => {
  const { onChangeNoEvent, id, value, multiple } = props;
  return (
    <Select
      label="Лекторы"
      gql={gql}
      gqlQueryName={ALL_LECTURERS_QUERY_SHORT}
      queryResultObjectName="allLecturers"
      onSelect={onChangeNoEvent(id)}
      selected={value}
      multiple={multiple}
      modifyQueryOptions={modifyQueryOptions}
    />
  );
};

export default LecturersSelect;
