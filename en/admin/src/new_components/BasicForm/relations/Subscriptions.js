import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Catalog/gql';
import { ALL_SUBSCRIPTION_PRODUCTS_QUERY_SHORT } from './../../../constants';
import {
  subscriptionsShort as columnData,
} from './../../../new_pages/Catalog/columnData';
import { subscription as fields } from './../../../new_pages/Catalog/fields';
import modifyQueryFilter from './../../../new_pages/Catalog/modifyQueryFilter';

const Subscriptions = (props) => {
  const { onChangeNoEvent, id, label, value, multiple, srcData } = props;
  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData}
      relationTitle={label || 'Абонементы'}
      tableTitle="Абонементы"
      gql={gql}
      gqlQueryName={ALL_SUBSCRIPTION_PRODUCTS_QUERY_SHORT}
      queryResultObjectName="allSubscriptionProducts"
      onChange={onChangeNoEvent(id)}
      columnData={columnData}
      filter={fields.filterShort}
      modifyQueryFilter={modifyQueryFilter.subscriptions}
    />
  );
};

export default Subscriptions;
