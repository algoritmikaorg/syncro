import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Catalog/gql';
import { ALL_CYCLES_QUERY_SHORT } from './../../../constants';
import {
  cyclesShort as columnData,
} from './../../../new_pages/Catalog/columnData';
import { cycle as fields } from './../../../new_pages/Catalog/fields';
import modifyQueryFilter from './../../../new_pages/Catalog/modifyQueryFilter';

const Courses = (props) => {
  const { onChangeNoEvent, id, label, value, multiple, srcData } = props;
  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData}
      relationTitle={label || 'Циклы'}
      tableTitle="Циклы"
      gql={gql}
      gqlQueryName={ALL_CYCLES_QUERY_SHORT}
      queryResultObjectName="allCycles"
      onChange={onChangeNoEvent(id)}
      columnData={columnData}
      filter={fields.filterShort}
      modifyQueryFilter={modifyQueryFilter.cycles}
    />
  );
};

export default Courses;
