import React from 'react';

import Select from './Select';

import gql from './../../../new_pages/Locations/gql';
import { ALL_LOCATIONS_QUERY_SHORT } from './../../../constants';

const LocationsSelect = (props) => {
  const { onChangeNoEvent, id, value, multiple } = props;
  return (
    <Select
      label="Локации"
      gql={gql}
      gqlQueryName={ALL_LOCATIONS_QUERY_SHORT}
      queryResultObjectName="allLocations"
      onSelect={onChangeNoEvent(id)}
      selected={value}
      multiple={multiple}
    />
  );
};

export default LocationsSelect;
