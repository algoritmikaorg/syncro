import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Lecturers/gql';
import { ALL_LECTURERS_QUERY_SHORT } from './../../../constants';
import columnDataShort from './../../../new_pages/Lecturers/columnDataShort';

const Lecturers = (props) => {
  const { onChangeNoEvent, id, value, multiple, srcData } = props;
  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData}
      relationTitle="Лекторы"
      tableTitle="Лекторы"
      gql={gql}
      gqlQueryName={ALL_LECTURERS_QUERY_SHORT}
      queryResultObjectName="allLecturers"
      onChange={onChangeNoEvent(id)}
      columnData={columnDataShort}
    />
  );
};

export default Lecturers;
