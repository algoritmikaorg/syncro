import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Catalog/gql';
import { ALL_CERTIFICATE_PRODUCTS_QUERY_SHORT } from './../../../constants';
import {
  certificatesShort as columnData,
} from './../../../new_pages/Catalog/columnData';
import { certificate as fields } from './../../../new_pages/Catalog/fields';
import modifyQueryFilter from './../../../new_pages/Catalog/modifyQueryFilter';

const Certificates = (props) => {
  const { onChangeNoEvent, id, label, value, multiple, srcData } = props;
  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData}
      relationTitle={label || 'Сертификаты'}
      tableTitle="Сертификаты"
      gql={gql}
      gqlQueryName={ALL_CERTIFICATE_PRODUCTS_QUERY_SHORT}
      queryResultObjectName="allCertificateProducts"
      onChange={onChangeNoEvent(id)}
      columnData={columnData}
      filter={fields.filterShort}
      modifyQueryFilter={modifyQueryFilter.certificates}
    />
  );
};

export default Certificates;
