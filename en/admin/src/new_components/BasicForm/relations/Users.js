import React, { Component } from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Users/gql';
import {
  ALL_USERS_QUERY,
} from './../../../constants';

import columnData from './../../../new_pages/Users/columnData';
import fields from './../../../new_pages/Users/fields';
import modifyQueryFilter from './../../../new_pages/Users/modifyQueryFilter';

// import modifyMutationOptions from './modifyMutationOptionsUser';

const Users = (props) => {
  const { onChangeNoEvent, id, value, multiple, label, srcData, filterType } = props;

  return (
    <div>
      <SelectRelatedItems
        multiple={multiple}
        value={value}
        srcData={srcData}
        relationTitle={label || 'Пользователи'}
        tableTitle="Пользователи"
        gql={gql}
        gqlQueryName={ALL_USERS_QUERY}
        queryResultObjectName="allUsers"
        onChange={onChangeNoEvent(id)}
        columnData={columnData}
        filter={fields.filter}
        modifyQueryFilter={modifyQueryFilter(filterType)}
      />
    </div>
  );
};

export default Users;
