import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Tags/gql';
import { ALL_LOCATIONS_QUERY_SHORT } from './../../../constants';
import columnDataShort from './../../../new_pages/Locations/columnDataShort';

const Locations = (props) => {
  const { onChangeNoEvent, id, value, multiple, srcData } = props;
  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData}
      relationTitle="Локации"
      tableTitle="Локации"
      gql={gql}
      gqlQueryName={ALL_LOCATIONS_QUERY_SHORT}
      queryResultObjectName="allLocations"
      onChange={onChangeNoEvent(id)}
      columnData={columnDataShort}
    />
  );
};

export default Locations;
