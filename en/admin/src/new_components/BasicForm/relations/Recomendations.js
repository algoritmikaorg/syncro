import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Catalog/gql';
import { ALL_LECTURES_FOR_RECOMENDATIONS_QUERY_SHORT } from './../../../constants';
import {
  lecturesShort as columnDataLectures,
} from './../../../new_pages/Catalog/columnData';
import { lecture as fields } from './../../../new_pages/Catalog/fields';
import modifyQueryFilter from './../../../new_pages/Catalog/modifyQueryFilter';

const Lectures = (props) => {

  const { onChangeNoEvent, id, label, value, multiple, srcData } = props;

  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData ? srcData.recommendedLectures : []}
      relationTitle={label || 'Лекции'}
      tableTitle="Лекции"
      gql={gql}
      gqlQueryName={ALL_LECTURES_FOR_RECOMENDATIONS_QUERY_SHORT}
      queryResultObjectName="allLectures"
      onChange={onChangeNoEvent(id)}
      columnData={columnDataLectures}
      filter={fields.filterShort}
      modifyQueryFilter={modifyQueryFilter.recomendations(srcData ? srcData.id : '')}
    />
  );
};

export default Lectures;
