import React, { Component } from 'react';

import QueryWithProgress from './../../../new_containers/QueryWithProgress';
import SelectMenu from './SelectMenu';

import { formatInputDataArrayForTable } from './../../../new_utils';

const Select = (props) => {
  const {
    gql,
    gqlQueryName,
    queryResultObjectName,
    queryOptions,
    modifyQueryFilter,
    modifyQueryOptions,
    onSelect,
  } = props;
  return (
    <QueryWithProgress
      gql={gql}
      gqlQueryName={gqlQueryName}
      queryResultObjectName={queryResultObjectName}
      modifyQueryFilter={modifyQueryFilter}
      modifyQueryOptions={modifyQueryOptions}
    >
      <SelectMenu {...props} formatInputData={formatInputDataArrayForTable} />
    </QueryWithProgress>
  );
};

Select.defaultProps = {
  modifyQueryOptions(modifyQueryFilter) {
    return (ownProps) => {
      return {
        variables: {
          orderBy: 'title_ASC',
          filter: modifyQueryFilter(ownProps.allQueryFilter, ownProps.filterType),
        },
      };
    };
  },
};

export default Select;
