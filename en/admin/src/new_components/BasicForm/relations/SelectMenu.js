import React, { Component } from 'react';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { withStyles } from 'material-ui/styles';

import { selectedItems } from './../../../new_utils';

const styles = theme => ({
  textField: {
    // marginLeft: theme.spacing.unit,
    // marginRight: theme.spacing.unit,
    width: 200,
  },
  formControl: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
  },
});

const activeItemFontWeigth = (ids, id) => {
  if (Array.isArray(ids)) {
    return ids.indexOf(id) !== -1 ? '500' : '400';
  }
  return ids === id ? '500' : '400';
};

const findSelected = (data, selectedIds) => {
  let selected = null;
  if (Array.isArray(selectedIds)) {
    if (data) {
      selected = data.filter(({ id }) => selectedIds.indexOf(id) !== -1);
    } else {
      selected = [];
    }
  } else {
    if (data) {
      selected = data.filter(({ id }) => id === selectedIds)[0];
    } else {
      selected = '';
    }
  }
  return selected;
};

class SelectMenu extends Component {
  constructor(props) {
    super(props);
    const {
      selected: inputSelected,
      multiple,
      gqlQueryName,
      queryResultObjectName,
      formatInputData,
    } = props;

    let data = [];
    let selected = null;
    let srcData = null;
    let selectedIds = null;

    if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
      srcData = props[gqlQueryName][queryResultObjectName];
      data = formatInputData(props[gqlQueryName][queryResultObjectName]);
    }
    if (multiple) {
      selected = inputSelected || [];
      selectedIds = selected.map(({ id }) => id);
    } else {
      selected = inputSelected || {};
      selectedIds = selected.id || '';
    }

    this.state = {
      selectedIds,
      selected,
      data,
      srcData,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleMenuExited = this.handleMenuExited.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const {
      selected: inputSelected,
      multiple,
      gqlQueryName,
      queryResultObjectName,
      formatInputData,
    } = nextProps;

    let data = [];
    let selected = null;
    let srcData = null;
    let selectedIds = null;

    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      srcData = nextProps[gqlQueryName][queryResultObjectName];
      data = formatInputData(nextProps[gqlQueryName][queryResultObjectName]);
    }

    if (multiple) {
      selected = inputSelected || [];
      selectedIds = selected.map(({ id }) => id);
    } else {
      selected = inputSelected || {};
      selectedIds = selected.id || '';
    }


    this.setState({
      selectedIds,
      selected,
      data,
      srcData,
    });
  }
  handleChange(event) {
    const { srcData } = this.state;
    const selectedIds = event.target.value;
    const selected = findSelected(srcData, selectedIds);

    this.setState({
      selected,
      selectedIds,
    });
  }
  handleMenuExited() {
    const { selectedIds, selected } = this.state;
    this.props.onSelect(selectedIds, selected);
  }

  render() {
    const {
      queryResultObjectName,
      label,
      classes,
      multiple,
    } = this.props;

    const {
      data,
      selectedIds,
    } = this.state;

    const inputId = `select-${queryResultObjectName}-multiple`;

    return (
      <FormControl className={classes.formControl}>
        <InputLabel
          htmlFor={inputId}
        >
          {label}
        </InputLabel>
        <Select
          multiple={multiple}
          value={selectedIds}
          onChange={this.handleChange}
          input={<Input id={inputId} />}
          className={classes.textField}
          MenuProps={{
            onExited: this.handleMenuExited,
            // PaperProps: {
            //   style: {
            //     maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            //     width: 200,
            //   },
            // },
          }}
        >
          {data.map((item) => {
            const { id, title } = item;
            return (
              <MenuItem
                key={id}
                value={id}
                className={classes.textField}
                style={{
                  fontWeight: activeItemFontWeigth(selectedIds, id),
                }}
              >
                {title}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>

    );
  }
}

SelectMenu.defaultProps = {
  onSelect: () => {},
  formatInputData: data => data || [],
};

export default withStyles(styles)(SelectMenu);
