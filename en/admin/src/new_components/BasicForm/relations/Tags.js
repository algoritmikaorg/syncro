import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Tags/gql';
import { ALL_TAGS_QUERY_SHORT } from './../../../constants';
import columnDataShort from './../../../new_pages/Tags/columnDataShort';

const Tags = (props) => {
  const { onChangeNoEvent, id, value, multiple, label, srcData } = props;
  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData}
      relationTitle={label || 'Направления'}
      tableTitle="Направления"
      gql={gql}
      gqlQueryName={ALL_TAGS_QUERY_SHORT}
      queryResultObjectName="allTags"
      onChange={onChangeNoEvent(id)}
      columnData={columnDataShort}
    />
  );
};

export default Tags;
