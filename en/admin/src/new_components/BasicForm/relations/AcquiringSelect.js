import React from 'react';

import Select from './Select';

import gql from './../../../new_pages/Acquirings/gql';
import { ALL_ACQUIRINGS_QUERY_SHORT } from './../../../constants';

const AcquiringsSelect = (props) => {
  const { onChangeNoEvent, id, value, multiple } = props;
  return (
    <Select
      label="Интернет-эквайринг"
      gql={gql}
      gqlQueryName={ALL_ACQUIRINGS_QUERY_SHORT}
      queryResultObjectName="allAcquirings"
      onSelect={onChangeNoEvent(id)}
      selected={value}
      multiple={multiple}
    />
  );
};

export default AcquiringsSelect;
