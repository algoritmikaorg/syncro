import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Discounts/gql';
import { ALL_DISCOUNTS_QUERY_SHORT } from './../../../constants';
import columnDataShort from './../../../new_pages/Discounts/columnDataShort';
import fields from './../../../new_pages/Discounts/fields';
import modifyQueryFilter from './../../../new_pages/Discounts/modifyQueryFilter';

const Discounts = (props) => {
  const { onChangeNoEvent, id, value, multiple, srcData } = props;

  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData}
      relationTitle="Скидки"
      tableTitle="Скидки"
      gql={gql}
      gqlQueryName={ALL_DISCOUNTS_QUERY_SHORT}
      queryResultObjectName="allDiscounts"
      onChange={onChangeNoEvent(id)}
      columnData={columnDataShort}
      filter={fields.filter}
      modifyQueryFilter={modifyQueryFilter}
    />
  );
};

export default Discounts;
