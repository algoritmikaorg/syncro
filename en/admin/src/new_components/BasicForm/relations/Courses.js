import React from 'react';

import SelectRelatedItems from './../../SelectRelatedItems';

import gql from './../../../new_pages/Catalog/gql';
import { ALL_COURSES_QUERY_SHORT } from './../../../constants';
import {
  coursesShort as columnData,
} from './../../../new_pages/Catalog/columnData';
import { course as fields } from './../../../new_pages/Catalog/fields';
import modifyQueryFilter from './../../../new_pages/Catalog/modifyQueryFilter';

const Courses = (props) => {
  const { onChangeNoEvent, id, label, value, multiple, srcData } = props;
  return (
    <SelectRelatedItems
      multiple={multiple}
      value={value}
      srcData={srcData}
      relationTitle={label || 'Лекции'}
      tableTitle="Лекции"
      gql={gql}
      gqlQueryName={ALL_COURSES_QUERY_SHORT}
      queryResultObjectName="allCourses"
      onChange={onChangeNoEvent(id)}
      columnData={columnData}
      filter={fields.filterShort}
      modifyQueryFilter={modifyQueryFilter.courses}
    />
  );
};

export default Courses;
