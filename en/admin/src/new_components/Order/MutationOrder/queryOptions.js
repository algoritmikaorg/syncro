const queryOptions = orderItem => (ownProps) => {
  return { variables: { order: orderItem.payment.data } };
};

export default queryOptions;
