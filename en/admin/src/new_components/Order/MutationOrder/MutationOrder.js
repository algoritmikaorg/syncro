import React, { Component } from 'react';
import moment from 'moment';

const checkResultOfCheckEvents = (order, result) => {
  const resultOfChecking = JSON.parse(result.resultOfChecking);
  const resultOfCalcOrder = JSON.parse(result.resultOfCalcOrder);

  const {
    EntityNoPublic,
    allEventsNoTicketsAvailable,
    PromocodeWithErrors,
    PromocodesWithErrors,
    DiscountsWithErrors,
  } = resultOfChecking;

  const output = {
    EntityNoPublic: EntityNoPublic ? {
      id: EntityNoPublic.id,
      title: EntityNoPublic.title,
    } : null,
    allEventsNoTicketsAvailable: allEventsNoTicketsAvailable ? allEventsNoTicketsAvailable
      .map(({ id, date, lecture: { title } }) => ({ id, date, title })) : null,
    PromocodeWithErrors: PromocodeWithErrors ? {
      id: PromocodeWithErrors.id,
      title: PromocodeWithErrors.title,
    } : null,
    PromocodesWithErrors: PromocodesWithErrors ? PromocodesWithErrors
      .map(({ id, title }) => ({ id, title })) : null,
    DiscountsWithErrors: DiscountsWithErrors ? DiscountsWithErrors
      .map(({ id, title }) => ({ id, title })) : null,
  };

  if (
    EntityNoPublic
    || allEventsNoTicketsAvailable
    || PromocodeWithErrors
    || PromocodesWithErrors
    || DiscountsWithErrors
  ) {
    return {
      resultOfChecking: output,
    };
  }

  const {
    order: {
      orderPrice,
      discountsPrice,
      discountsRate,
      totalPrice,
    },
  } = order;

  if (
    orderPrice !== resultOfCalcOrder.orderPrice
    || totalPrice !== resultOfCalcOrder.totalPrice
  ) {
    return {
      resultOfCalcOrder,
    };
  }

  return {
    resultOfChecking: null,
    resultOfCalcOrder: null,
    order: resultOfCalcOrder,
  };
};

class MutationOrder extends Component {
  constructor(props) {
    super(props);
    const {
      gqlMutationName,
      orderItem,
      id,
      paymentId,
    } = props;

    this.state = {
      id,
      paymentId,
      orderItem,
      checkOrderEvents: null,
      gqlMutationNameResult: `${gqlMutationName}Result`,
      gqlMutationNameError: `${gqlMutationName}Error`,
      gqlMutationNameLoading: `${gqlMutationName}Loading`,
      gqlMutationNameReset: `${gqlMutationName}Reset`,
    };
  }
  // componentWillMount() {
  //   const { gqlMutationName } = this.props;
  //   const { orderItem } = this.state;
  //
  //   // this.props[gqlMutationName]({
  //   //   variables: orderItem,
  //   // });
  // }
  componentWillReceiveProps(nextProps) {
    const {
      gqlMutationNameResult,
      gqlMutationNameError,
      gqlMutationNameLoading,
      gqlMutationNameReset,
      checkOrderEvents,
      orderItem,
      id,
      paymentId,
    } = this.state;

    const {
      mutationResultObjectName,
      resultMutationMessageSuccses,
      gqlQueryName,
      queryResultObjectName,
      gqlMutationName,
    } = nextProps;

    if (!checkOrderEvents && nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      const checkResults = checkResultOfCheckEvents(orderItem.payment.data, nextProps[gqlQueryName][queryResultObjectName]);
      const { acquiring } = nextProps[gqlQueryName].allSettings[0];

      if (!checkResults.resultOfChecking && !checkResults.resultOfCalcOrder) {
        orderItem.payment.data.order = checkResults.order;
        let variables = null;
        if (id && paymentId) {
          delete orderItem.payment.userId;
          variables = Object.assign(
            {},
            orderItem,
            orderItem.payment,
            {
              id,
              paymentId,
            },
          );
        } else {
          orderItem.payment = Object.assign({}, orderItem.payment, {
            acquiringId: acquiring.id,
            commission: acquiring.commission,
          });
          variables = orderItem;
        }

        this.props[gqlMutationName]({
          variables,
        });
      }
      return this.setState({
        checkOrderEvents: checkResults,
      });
    }

    if (nextProps[gqlMutationNameResult]) {
      const result = nextProps[gqlMutationNameResult].data[mutationResultObjectName];
      this.props.snackbar({ message: `${resultMutationMessageSuccses} - id: ${result.id}`, name: 'Success' });
    }
    if (nextProps[gqlMutationNameError]) {
      const result = nextProps[gqlMutationNameError].errors;
      this.props.snackbar(nextProps[gqlMutationNameError].errors || nextProps[gqlMutationNameError]);
    }
  }
  componentDidUpdate() {
    const {
      gqlMutationNameResult,
    } = this.state;

    const {
      mutationResultObjectName,
      onSuccessMutation,
    } = this.props;

    if (this.props[gqlMutationNameResult]
      && this.props[gqlMutationNameResult].data
      && onSuccessMutation instanceof Function
    ) {
      onSuccessMutation(this.props[gqlMutationNameResult].data[mutationResultObjectName], this.props);
    }
  }
  render() {
    const {
      checkOrderEvents,
    } = this.state;

    if (!checkOrderEvents) {
      return (
        <div>Проверка заказа</div>
      );
    }

    if (checkOrderEvents && (checkOrderEvents.resultOfChecking || checkOrderEvents.resultOfCalcOrder)) {
      const { resultOfChecking, resultOfCalcOrder } = checkOrderEvents;
      return (
        <div>
          <h3 className="error">
            Невозможно произвести данный заказ
          </h3>
          {
            resultOfChecking && resultOfChecking.EntityNoPublic &&
            (
              <p>
                Материал снят с публикации
              </p>
            )
          }
          {
            resultOfChecking && resultOfChecking.allEventsNoTicketsAvailable &&
            (
              <div>
                <p>
                  Нет достаточного количества билетов:
                </p>
                <ul>
                  {
                    resultOfChecking.allEventsNoTicketsAvailable.map(({ id, title, date }) => (
                      <li key={id}>{title} - {moment(date).format('LLLL')}</li>
                    ))
                  }
                </ul>
              </div>
            )
          }
          {
            resultOfChecking && resultOfChecking.DiscountsWithErrors &&
            (
              <div>
                <p>
                  Скидки не доступны.
                </p>
                <ul>
                  {
                    resultOfChecking.DiscountsWithErrors.map(({ id, title }) => (
                      <li key={id}>{title}</li>
                    ))
                  }
                </ul>
              </div>
            )
          }
          {
            resultOfChecking && resultOfChecking.PromocodesWithErrors &&
            (
              <div>
                <p>Сертификат или абонемент не доступны</p>
                <ul>
                  {
                    resultOfChecking.PromocodesWithErrors.map(({ id, title }) => (
                      <li key={id}>{title}</li>
                    ))
                  }
                </ul>

              </div>
            )
          }
          {
            resultOfCalcOrder &&
            (
              <div>
                <p>Сумма заказа расчитана не верно.</p>
                <p>Итоговая сумма {resultOfCalcOrder.totalPrice}</p>
              </div>
            )
          }
        </div>
      );
    }

    return (
      <div>Сохранение заказа</div>
    );
  }
}

export default MutationOrder;
