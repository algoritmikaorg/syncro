import calcOrderPrice from './../calcOrderPrice';

// const maxValueArrayIndex = array => (
//   array
//     .map(({ rate }) => rate)
//     .reduce((bestIndexSoFar, currentlyTestedValue, currentlyTestedIndex, array) => currentlyTestedValue > array[bestIndexSoFar] ? currentlyTestedIndex : bestIndexSoFar, 0)
// );

const lecturesWithEvent = (events) => {
  const output = {};
  Object.keys(events)
    .forEach((key) => {
      const event = events[key][0].src;
      output[key] = {
        eventId: event.id,
        data: event.date,
        price: event.price,
        quantityOfTickets: event.quantityOfTickets,
      };
    });

  return output;
};

const dataOfOrder = (props) => {
  const {
    id,
    paymentId,
    lectures,
    events,
    courses,
    cycles,
    user,
    discounts = [],
    participants,
    additionalDiscounts = [],
    paymentType,
    subscriptions = [],
    certificates = [],
  } = props;

  const {
    orderPrice,
    discountPrice,
    discountRate,
    additionalDiscountsRate,
    totalDiscountPrice,
    totalPrice,
    certSubsPrice,
    certStatistic,
    subsStatistic,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
  } = calcOrderPrice(props);

  const discountsIds = discountsWithMaxRate.map(({ id }) => id);
  const additionalDiscountsIds = additionalDiscountsWithMaxRate.map(({ id }) => id);

  const certStatisticIds = Object.keys(certStatistic).map(key => certStatistic[key].id);
  const subsStatisticIds = Object.keys(subsStatistic).map(key => subsStatistic[key].id);

  const orderItem = {
    userId: user[0].id,
    eventsIds: Object.keys(events)
      .map(key => events[key])
      .reduce((prev, curr) => prev.concat(curr))
      .map(({ id }) => id),
    participantsIds: participants.map(({ id }) => id),
    totalPrice,
    paymentId: null,
  };

  let product = null;
  let promocode = null;

  if (courses && courses.length > 0) {
    product = courses[0].src;
  } else if (cycles && cycles.length > 0) {
    product = cycles[0].src;
  } else {
    product = lectures[0].src;
  }

  if (certStatisticIds && certStatisticIds.length > 0) {
    promocode = {};
    Object.keys(certStatistic).forEach((key) => {
      const cert = certStatistic[key].src;
      promocode[key] = {
        id: cert.id,
        start: cert.start,
        end: cert.end,
        unusedLectures: cert.unusedLectures,
        personsCount: cert.personsCount,
        title: cert.title,
        used: cert.used,
        __typename: cert.__typename,
      };
    });
  }
  if (subsStatisticIds && subsStatisticIds.length > 0) {
    promocode = {};
    Object.keys(subsStatistic).forEach((key) => {
      const sub = subsStatistic[key].src;
      promocode[key] = {
        id: sub.id,
        start: sub.start,
        end: sub.end,
        unusedLectures: sub.unusedLectures,
        personsCount: sub.personsCount,
        title: sub.title,
        used: sub.used,
        __typename: sub.__typename,
      };
    });
  }

  const paymentData = {
    source: 'ADMIN',
    tickets: participants.length,
    participants: participants.map(({ src }) => src),
    discounts: discountsWithMaxRate,
    additionalDiscounts: additionalDiscountsWithMaxRate,
    promocode,
    userAnOrder: user[0].src,
    lectures: lecturesWithEvent(events),
    product,
    order: {
      orderPrice,
      discountPrice: totalDiscountPrice,
      discountRate,
      additionalDiscountsRate,
      totalPrice,
    },
  };

  delete orderItem.paymentId;

  if (paymentType === 'CASH') {
    orderItem.payment = {
      commission: 0,
      orderPrice,
      discountPrice: totalDiscountPrice,
      totalPrice,
      type: 'CASH',
      userId: user[0].id,
      status: 'PAID',
      discountsIds,
      additionalDiscountsIds,
      certificatesInPaymentsIds: certStatisticIds,
      subscriptionsInPaymentsIds: subsStatisticIds,
      data: paymentData,
    };
  } else if (paymentType === 'INTERNET_ACQUIRING') {
    orderItem.payment = {
      commission: 0,
      orderPrice,
      discountPrice: totalDiscountPrice,
      totalPrice,
      type: 'INTERNET_ACQUIRING',
      userId: user[0].id,
      status: 'PENDING',
      discountsIds,
      additionalDiscountsIds,
      certificatesInPaymentsIds: certStatisticIds,
      subscriptionsInPaymentsIds: subsStatisticIds,
      data: paymentData,
    };
  }

  return orderItem;
};

export default dataOfOrder;
