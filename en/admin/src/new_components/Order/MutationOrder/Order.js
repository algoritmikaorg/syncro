import React from 'react';
import List, { ListItemText } from 'material-ui/List';
import Typography from 'material-ui/Typography';

import gql from './../../../new_pages/Orders/gql';
import {
  CREATE_ORDER_MUTATION,
  EDIT_ORDER_MUTATION,
  CHECK_ORDER_EVENTS,
} from './../../../constants';
import modifyMutationOptions from './../../../new_pages/Orders/modifyMutationOptions';
import Mutation from './../../../new_containers/Mutation';
import QueryWithOptionsAndMutation from './../../../new_containers/QueryWithOptionsAndMutation';

import OrderMutation from './MutationOrder';
import queryOptions from './queryOptions';

import checkOrder from './../checkOrder';
import dataOfOrder from './dataOfOrder';

const Order = (props) => {
  const {
    onSuccessMutation,
    product,
    user,
    id,
    paymentId,
  } = props;

  const checkedOrderErrors = checkOrder(props);
  let orderItem = null;
  if (checkedOrderErrors.length === 0) orderItem = dataOfOrder(props);

  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let resultMutationMessageSuccses = '';

  if (id && paymentId) {
    gqlMutationName = EDIT_ORDER_MUTATION;
    mutationResultObjectName = 'updateOrder';
    resultMutationMessageSuccses = 'Заказ обновлен';
  } else {
    gqlMutationName = CREATE_ORDER_MUTATION;
    mutationResultObjectName = 'createOrder';
    resultMutationMessageSuccses = 'Заказ создан';
  }

  return (
    <div>
      <Typography type="title" gutterBottom>
        Сохранение заказа
      </Typography>
      {
        checkedOrderErrors.length === 0 &&
        <QueryWithOptionsAndMutation
          gql={gql}
          gqlQueryName={CHECK_ORDER_EVENTS}
          queryResultObjectName="checkOrderEvents"
          queryOptions={queryOptions(orderItem)}
          gqlMutationName={gqlMutationName}
          mutationResultObjectName={mutationResultObjectName}
          resultMutationMessageSuccses={resultMutationMessageSuccses}
          modifyMutationOptions={modifyMutationOptions}
          onSuccessMutation={onSuccessMutation}
        >
          <OrderMutation {...props} orderItem={orderItem} id={id} paymentId={paymentId} />
        </QueryWithOptionsAndMutation>
      }
      {
        checkedOrderErrors.length > 0 &&
        <div style={{ marginTop: 20 }} >
          <Typography type="title" gutterBottom>
            Ошибки
          </Typography>
          <List>
            {
              checkedOrderErrors.map((error, index) => (
                <ListItemText
                  key={index}
                  primary={error}
                />
              ))
            }
          </List>
        </div>
      }
    </div>
  );
};

export default Order;
