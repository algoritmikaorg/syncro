// скидки
// 1.Одиночная лекция
// Скидки: лекция, направление, все продукты
// 2.Курс/Цикл
// Скидки: лекция, курс/цикл, направление, все продукты
// Если выбраны все лекции курса, то скидки курса. Иначе скидки каждой лекции.
import checkDiscountsParam from './../OrderDiscounts/checkDiscountsParam';
import calcSimpleDiscounts from './calcSimpleDiscounts';
import calcComplexDiscounts from './calcComplexDiscounts';

const calcDiscounts = (orderPrice, props, eventsAll, discounts, additionalDiscounts) => {
  let calcResult = null;

  let validDiscounts = null;
  let validAdditionalDiscounts = null;

  if (discounts && discounts.length > 0) {
    const checkedDiscounts = checkDiscountsParam(props, { events: eventsAll }, discounts);
    validDiscounts = checkedDiscounts.filter(({ valid }) => valid);
  }

  if (additionalDiscounts && additionalDiscounts.length > 0) {
    const checkedAdditionalDiscounts = checkDiscountsParam(
      props,
      {
        events: eventsAll,
      },
      additionalDiscounts,
    );
    validAdditionalDiscounts = checkedAdditionalDiscounts.filter(({ valid }) => valid);
  }

  if (
    (validDiscounts && validDiscounts.length > 0)
    || (validAdditionalDiscounts && validAdditionalDiscounts.length > 0)
  ) {
    const {
      lectures,
      courses,
      cycles,
    } = props;
    switch (true) {
      // одиночная лекция
      case (
        lectures && lectures.length > 0
        && (!courses || courses.length === 0)
        && (!cycles || cycles.length === 0)
      ): {
        calcResult = calcSimpleDiscounts(validDiscounts, validAdditionalDiscounts || []);
        break;
      }
      default: {
        // циклы, курсы
        calcResult = calcComplexDiscounts(orderPrice, props, validDiscounts || [], validAdditionalDiscounts || []);
      }
    }
  }

  if (calcResult) {
    return calcResult;
  }

  return {
    discountRate: 0,
    additionalDiscountsRate: 0,
    discountsWithMaxRate: [],
    additionalDiscountsWithMaxRate: [],
  };
};

export default calcDiscounts;
