const checkOrder = (props) => {
  const errors = [];
  const {
    lectures,
    events,
    courses,
    cycles,
    user,
    participants,
    paymentType,
  } = props;

  const eventsKeys = Object.keys(events);

  if ((!lectures || lectures.length === 0) && (!courses || courses.length === 0) && (!cycles || cycles.length === 0)) {
    errors.push('Не выбран продукт');
  }

  if (!events || events.length === 0) errors.push('Не выбраны даты!');

  if (!lectures || lectures.length === 0) errors.push('Не выбраны лекции!');

  if (eventsKeys.length < lectures.length) errors.push('Не на все лекции выбраны даты!');

  if (!user || user.length === 0) errors.push('Не выбран пользователь!');
  if (!participants || participants.length === 0) errors.push('Не выбраны участники');
  if (!paymentType) errors.push('Выберите тип оплаты');
  return errors;
};

export default checkOrder;
