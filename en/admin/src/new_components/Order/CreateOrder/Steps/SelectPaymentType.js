import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Radio, { RadioGroup } from 'material-ui/Radio';
import { FormLabel, FormControl, FormControlLabel, FormHelperText } from 'material-ui/Form';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing.unit * 3,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
});

const SelectPaymentType = (props) => {
  const {
    classes,
    paymentType,
    onSelectPaymentType,
  } = props;

  return (
    <div className={classes.root}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Тип оплаты</FormLabel>
        <RadioGroup
          aria-label="productType"
          name="productType"
          className={classes.group}
          value={paymentType}
          onChange={onSelectPaymentType}
        >
          <FormControlLabel value="INTERNET_ACQUIRING" control={<Radio />} label="Интернет-эквайринг(оплата через сайт)" />
          {/* <FormControlLabel value="acquiring" control={<Radio />} label="Банковская карта" /> */}
          <FormControlLabel value="CASH" control={<Radio />} label="Наличные(оплачен)" />
        </RadioGroup>
      </FormControl>
    </div>
  );
};

export default withStyles(styles)(SelectPaymentType);
