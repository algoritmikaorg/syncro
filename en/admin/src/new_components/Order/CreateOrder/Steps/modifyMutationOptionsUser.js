export default () => {
  return (
    {
      refetchQueries: [
        'AllUsersQuery',
        'AllUsersShortQuery',
      ],
    }
  );
};
