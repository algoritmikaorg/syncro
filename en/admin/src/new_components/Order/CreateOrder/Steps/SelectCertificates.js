import React, { Component } from 'react';
import Typography from 'material-ui/Typography';

import SelectRelatedItems from './../../../SelectRelatedItems';

import gql from './../../../../new_pages/Certificates/gql';
import { ALL_CERTIFICATES_OF_USER_QUERY } from './../../../../constants';
import columnDataShort from './../../../../new_pages/Certificates/columnDataShort';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';

const SelectCertificateOrSubscription = (props) => {
  const {
    participants,
    certificates,
    onSelectCertificates
  } = props;
  if (participants.length > 0) {
    return (
      <div style={{ marginTop: 20 }} >
        <Typography type="title" gutterBottom>
          Сертификаты
        </Typography>
        {
          participants.map(({ id, firstName, lastName }) => (
            <SelectRelatedItems
              key={id}
              srcData={certificates[id]}
              relationTitle={`${firstName} ${lastName}`}
              tableTitle="Сертификаты"
              gql={gql}
              gqlQueryName={ALL_CERTIFICATES_OF_USER_QUERY}
              queryResultObjectName="allCertificates"
              onChange={onSelectCertificates(id)}
              columnData={columnDataShort}
              modifyQueryFilter={modifyQueryFilter.certificates({ id })}
            />
          ))
        }
      </div>
    );
  }

  return (
    <div style={{ marginTop: 20, marginBottom: 20 }}>
      <Typography type="title" gutterBottom>
        Сертификаты
      </Typography>
      <Typography type="body1" gutterBottom>
        Нет выбранных участников
      </Typography>
    </div>
  );
};

export default SelectCertificateOrSubscription;
