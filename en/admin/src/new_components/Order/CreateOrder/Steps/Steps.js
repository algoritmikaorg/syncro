import React from 'react';

import Payment from './Payment';
import Save from './Save';
import SelectEvents from './SelectEvents';
import SelectLectures from './SelectLectures';
import SelectUser from './SelectUser';
import SelectProductType from './SelectProductType';
import SelectCoursesOrCycles from './SelectCoursesOrCycles';
import SelectParticipants from './SelectParticipants';
import SelectPaymentType from './SelectPaymentType';
import SelectCertificates from './SelectCertificates';
import SelectSubscriptions from './SelectSubscriptions';
import OrderDiscounts from './../../../OrderDiscounts';
import MutationOrder from './../../MutationOrder/';

const Steps = (props) => {
  const {
    activeStep,
    fileds,
  } = props;

  switch (activeStep) {
    case 0: {
      return <SelectUser {...props} />;
    }
    case 1: {
      return <SelectProductType {...props} />;
    }
    case 2: {
      return <SelectCoursesOrCycles {...props} />;
    }
    case 3: {
      return <SelectLectures {...props} />;
    }
    case 4: {
      return <SelectEvents {...props} />;
    }
    case 5: {
      return <SelectParticipants {...props} />;
    }
    case 6: {
      return <SelectCertificates {...props} />;
    }
    case 7: {
      return <SelectSubscriptions {...props} />;
    }
    case 8: {
      return <OrderDiscounts {...props} />;
    }
    case 9: {
      return <SelectPaymentType {...props} />;
    }
    case 10: {
      return <MutationOrder {...props} />;
    }
    default: {
      return <SelectUser {...props} />;
    }
  }
};

export default Steps;
