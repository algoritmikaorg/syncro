import React, { Component } from 'react';
import Typography from 'material-ui/Typography';

import SelectRelatedItems from './../../../SelectRelatedItems';
import gql from './../../../../new_pages/Catalog/gql';
import {
  ALL_COURSES_QUERY_SHORT,
  ALL_CYCLES_QUERY_SHORT,
} from './../../../../constants';
import {
  coursesShort as columnDataCourses,
  cyclesShort as columnDataCycles,
} from './../../../../new_pages/Catalog/columnData';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';

const SelectCoursesOrCycles = (props) => {
  const {
    productType,
    courses,
    cycles,
    onSelectCourses,
    onSelectCycles
  } = props;

  switch (productType) {
    case 'course': {
      return (
        <SelectRelatedItems
          srcData={courses}
          relationTitle="Выберите курс"
          tableTitle="Курсы"
          gql={gql}
          gqlQueryName={ALL_COURSES_QUERY_SHORT}
          queryResultObjectName="allCourses"
          onChange={onSelectCourses}
          columnData={columnDataCourses}
          filter={fields.filter.courses}
          modifyQueryFilter={modifyQueryFilter.courses}
        />
      );
    }
    case 'cycle': {
      return (
        <SelectRelatedItems
          srcData={cycles}
          relationTitle="Выберите цикл"
          tableTitle="Цикл"
          gql={gql}
          gqlQueryName={ALL_CYCLES_QUERY_SHORT}
          queryResultObjectName="allCycles"
          onChange={onSelectCycles}
          columnData={columnDataCycles}
          filter={fields.filter.cycles}
          modifyQueryFilter={modifyQueryFilter.cycles}
        />
      );
    }
    default: {
      return (
        <div style={{ marginTop: 20, marginBottom: 20 }}>
          <Typography type="body1" gutterBottom>
            Нажмите &quot;Дальше&quot;
          </Typography>
        </div>
      );
    }
  }
};

export default SelectCoursesOrCycles;
