import { appendProp } from './../../../../new_utils';

const modifyQueryFilterUsers = (input = {}, type) => {
  const filter = { role: 'CLIENT', blocked: false };
  if (input.firstName) {
    filter.firstName_contains = input.firstName;
  }
  if (input.lastName) {
    filter.lastName_contains = input.lastName;
  }
  if (input.email) {
    filter.email_contains = input.email;
  }
  if (input.phone) {
    filter.phone_contains = input.phone;
  }

  return filter;
};

const modifyQueryFilterLectures = (input = {}) => {
  let filter = {
    public: true,
    lectureType: 'SINGLE_LECTURE',
    events_some: { date_gte: 'replaceByDate' },
  };

  const {
    title,
    lecturersIds,
    locationsIds,
    tagsIds,
  } = input;

  if (title) {
    filter.title_contains = title;
  }
  if (lecturersIds && lecturersIds.length > 0) {
    filter = appendProp(filter, 'events_some', 'lecturers_some');
    filter.events_some.lecturers_some.id_in = lecturersIds;
  }
  if (locationsIds && locationsIds.length > 0) {
    filter = appendProp(filter, 'events_some', 'location');
    filter.events_some.location.id_in = locationsIds;
  }

  if (tagsIds && tagsIds.length > 0) {
    filter.tags_some = {
      id_in: tagsIds,
    };
  }
  return filter;
};

const modifyQueryFilterCourses = (input = {}) => {
  const filter = {
    public: true,
    lectures_some: {
      // lectureType: 'LECTURE_FOR_COURSE',
      events_some: { date_gte: 'replaceByDate' },
    },
  };
  const {
    title,
    lecturersIds,
    locationsIds,
    tagsIds,
  } = input;

  if (title) {
    filter.title_contains = title;
  }
  if (lecturersIds) {
    // filter = appendProp(filter, 'lectures_some', 'events_some');
    filter.lectures_some.events_some.lecturers_some = {
      id_in: lecturersIds,
    };
  }
  if (locationsIds) {
    // filter = appendProp(filter, 'lectures_some', 'events_some');
    filter.lectures_some.events_some.location = {
      id_in: locationsIds,
    };
  }

  if (tagsIds) {
    filter.tags_some = {
      id_in: tagsIds,
    };
  }

  return filter;
};

const modifyQueryFilterCycles = modifyQueryFilterCourses;

const modifyQueryFilterLecturesOfCourses = courses => (input = {}) => {
  const ids = courses.map(({ id }) => id);
  const filter = { public: true };
  filter.course_some = {
    id_in: ids,
  };
  return filter;
};

const modifyQueryFilterLecturesofCycles = cycles => (input = {}) => {
  const ids = cycles.map(({ id }) => id);
  const filter = { public: true };
  filter.cycles_some = {
    id_in: ids,
  };
  return filter;
};

const modifyQueryFilterLecturesOfCourse = ({ id }) => (input = {}) => {
  const filter = { public: true };
  filter.course_some = {
    id,
  };
  return filter;
};

const modifyQueryFilterLecturesofCycle = ({ id }) => (input = {}) => {
  const filter = { public: true };
  filter.cycle_some = {
    id,
  };
  return filter;
};

const modifyQueryFilterEvents = lectures => (input = {}) => {
  const ids = lectures.map(({ id }) => id);
  const filter = {
    lecture: {
      id_in: ids,
    },
  };
  return filter;
};

const modifyQueryFilterLectureEvent = lecture => (input = {}) => {
  const filter = {
    date_gte: 'replaceByDate',
    quantityOfTicketsAvailable_gt: 0,
  };
  filter.lecture = {
    id: lecture.id || 'none',
  };

  return filter;
};

const modifyQueryCertificates = ({ id }) => (input = {}) => {
  const filter = {};
  filter.user = {
    id,
  };
  filter.unusedLectures_gt = 0;
  filter.end_gt = 'replaceByDate';
  filter.payment = {
    status: 'PAID',
  };

  return filter;
};

const modifyQuerySubscriptions = ({ id }) => (input = {}) => {
  const filter = {};
  filter.user = {
    id,
  };
  filter.unusedLectures_gt = 0;
  filter.end_gt = 'replaceByDate';
  filter.payment = {
    status: 'PAID',
  };
  return filter;
};

export default {
  users: modifyQueryFilterUsers,
  lectures: modifyQueryFilterLectures,
  courses: modifyQueryFilterCourses,
  cycles: modifyQueryFilterCycles,
  lecturesOfCourses: modifyQueryFilterLecturesOfCourses,
  lecturesOfCycles: modifyQueryFilterLecturesofCycles,
  lecturesOfCourse: modifyQueryFilterLecturesOfCourse,
  lecturesOfCycle: modifyQueryFilterLecturesofCycle,
  events: modifyQueryFilterEvents,
  lectureEvent: modifyQueryFilterLectureEvent,
  certificates: modifyQueryCertificates,
  subscriptions: modifyQuerySubscriptions,
};
