import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Radio, { RadioGroup } from 'material-ui/Radio';
import { FormLabel, FormControl, FormControlLabel, FormHelperText } from 'material-ui/Form';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing.unit * 3,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
});

const SelectProductType = (props) => {
  const {
    classes,
    productType,
    onSelectProductType,
  } = props;
  return (
    <div className={classes.root}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Продукт</FormLabel>
        <RadioGroup
          aria-label="productType"
          name="productType"
          className={classes.group}
          value={productType}
          onChange={onSelectProductType}
        >
          <FormControlLabel value="lecture" control={<Radio />} label="Одиночная лекция" />
          <FormControlLabel value="course" control={<Radio />} label="Курс" />
          <FormControlLabel value="cycle" control={<Radio />} label="Цикл" />
        </RadioGroup>
      </FormControl>
    </div>
  );
};

export default withStyles(styles)(SelectProductType);
