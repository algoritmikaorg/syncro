import React, { Component } from 'react';

import SelectRelatedItems from './../../../SelectRelatedItems';
import MutationEntityInDialog from './../../../MutationEntityInDialog';

import gql from './../../../../new_pages/Users/gql';
import {
  ALL_USERS_QUERY,
  CREATE_USER_WITHOUT_PASS_MUTATION,
} from './../../../../constants';
import columnData from './../../../../new_pages/Users/columnData';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';
import filedsUser from './../../../../new_pages/Users/fields';

import modifyMutationOptions from './modifyMutationOptionsUser';

const SelectUser = (props) => {
  const {
    user,
    onSelectUser
  } = props;

  return (
    <div>
      <SelectRelatedItems
        srcData={user}
        relationTitle="Выберите пользователя"
        tableTitle="Пользователи"
        gql={gql}
        gqlQueryName={ALL_USERS_QUERY}
        queryResultObjectName="allUsers"
        onChange={onSelectUser}
        columnData={columnData}
        filter={fields.filter.users}
        modifyQueryFilter={modifyQueryFilter.users}
      />
      <MutationEntityInDialog
        title="Создать нового пользователя"
        subTitle="Пользователь будет создан с автоматическим паролем"
        gql={gql}
        gqlMutationName={CREATE_USER_WITHOUT_PASS_MUTATION}
        mutationResultObjectName="signupUserWithRoleAndGenPass"
        resultMutationMessageSuccses="Пользователь создан"
        fields={filedsUser.client.createWithoutPassword}
        modifyMutationOptions={modifyMutationOptions}
      />
    </div>
  );
};

export default SelectUser;
