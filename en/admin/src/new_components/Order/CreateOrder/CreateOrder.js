import React, { Component } from 'react';

import Steps from './Steps';
import MobileStepper from './../../Stepper/MobileStepper';
import Result from './Result';

const uniqList = list => list
  .filter((s1, pos, array) => array.findIndex(s2 => s2.id === s1.id) === pos);

class CreateOrder extends Component {
  constructor(props) {
    super(props);
    const {
      id,
      paymentId,
      user,
      lectures,
      courses,
      cycles,
      events,
      participants,
      certificates,
      subscriptions,
      discounts,
      productType,
      additionalDiscounts,
      paymentType,
    } = props;

    this.state = {
      id,
      paymentId,
      activeStep: 0,
      user,
      productType: productType || 'lecture',
      lectures,
      courses,
      cycles,
      events,
      participants,
      certificates,
      subscriptions,
      discounts,
      additionalDiscounts,
      paymentType,
    };
    this.handleBack = this.handleBack.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleSelectUser = this.handleSelectUser.bind(this);
    this.handleSelectProductType = this.handleSelectProductType.bind(this);
    this.handleSelectLectures = this.handleSelectLectures.bind(this);
    this.handleSelectCourses = this.handleSelectCourses.bind(this);
    this.handleSelectCycles = this.handleSelectCycles.bind(this);
    this.handleSelectLectureEvent = this.handleSelectLectureEvent.bind(this);
    this.handleSelectParticipants = this.handleSelectParticipants.bind(this);
    this.handleSelectCertificates = this.handleSelectCertificates.bind(this);
    this.handleSelectSubscriptions = this.handleSelectSubscriptions.bind(this);
    this.handleSelectPaymentType = this.handleSelectPaymentType.bind(this);
    this.handleSelectDiscounts = this.handleSelectDiscounts.bind(this);
    this.handleSelectAdditionalDiscounts = this.handleSelectAdditionalDiscounts.bind(this);
    this.handleSuccsesMutation = this.handleSuccsesMutation.bind(this);
  }
  handleNext() {
    this.setState({
      activeStep: this.state.activeStep + 1,
    });
  }

  handleBack() {
    this.setState({
      activeStep: this.state.activeStep - 1,
    });
  }

  handleSelectUser(selectedIds, selected) {
    this.setState({
      user: selected || [],
      participants: this.state.participants.concat(selected),
    });
  }
  handleSelectProductType(event) {
    this.setState({
      productType: event.target.value,
      lectures: [],
      courses: [],
      cycles: [],
      events: [],
      discounts: [],
      additionalDiscounts: [],
    });
  }
  handleSelectLectures(selectedIds, selected) {
    const { events } = this.state;
    const eventsKeys = Object.keys(events).filter(({ id }) => selectedIds.includes(id));
    const newEvents = {};
    eventsKeys.forEach((key) => {
      newEvents[key] = events[key];
    });

    this.setState({
      lectures: selected || [],
      events: newEvents,
      discounts: [],
    });
  }
  handleSelectCourses(selectedIds, selected) {
    const { courses } = this.state;
    if (courses.length === 0 || selected.length === 0 || selected[0].id !== courses[0].id) {
      this.setState({
        courses: selected || [],
        discounts: [],
        lectures: [],
        events: {},
      });
    }
  }
  handleSelectCycles(selectedIds, selected) {
    const { cycles } = this.state;
    if (cycles.length === 0 || selected.length === 0 || selected[0].id !== cycles[0].id) {
      this.setState({
        cycles: selected || [],
        discounts: [],
        lectures: [],
        events: {},
      });
    }
  }
  handleSelectLectureEvent = lectureId => (selectedIds, selected) => {
    const { events } = this.state;
    const newEvents = Object.assign({}, events);
    newEvents[lectureId] = selected.slice();
    if (newEvents[lectureId].length === 0) delete newEvents[lectureId];
    this.setState({
      events: newEvents,
    });
  }
  handleSelectParticipants(selectedIds, selected) {
    // const participants = uniqList(this.state.participants.concat(selected));
    this.setState({
      participants: selected,
    });
  }
  handleSelectCertificates = userId => (selectedIds, selected) => {
    const { certificates } = this.state;
    certificates[userId] = selected;
    this.setState({
      certificates,
    });
  }
  handleSelectSubscriptions = userId => (selectedIds, selected) => {
    const { subscriptions } = this.state;
    subscriptions[userId] = selected;
    this.setState({
      subscriptions,
    });
  }
  handleSelectDiscounts(selected) {
    this.setState({
      discounts: selected,
    });
  }
  handleSelectAdditionalDiscounts(selectedIds, selected) {
    this.setState({
      additionalDiscounts: selected || [],
    });
  }
  handleSelectPaymentType(event) {
    const { value } = event.target;
    this.setState({
      paymentType: value,
    });
  }
  handleSuccsesMutation() {
    const { history } = this.props;
    history.push('/orders');
  }
  render() {
    const {
      activeStep,
      user,
      productType,
      courses,
      cycles,
      lectures,
      events,
      participants,
      certificates,
      subscriptions,
      discounts,
      additionalDiscounts,
      paymentType,
      id,
      paymentId,
    } = this.state;
    const {
      steps,
    } = this.props;

    return (
      <div>
        <Result
          {...this.state}
        />
        <div style={{ marginTop: 25, marginBottom: 25 }}>
          <Steps
            activeStep={activeStep}
            onSelectUser={this.handleSelectUser}
            onSelectProductType={this.handleSelectProductType}
            onSelectLectures={this.handleSelectLectures}
            onSelectCourses={this.handleSelectCourses}
            onSelectCycles={this.handleSelectCycles}
            onSelectLectureEvent={this.handleSelectLectureEvent}
            onSelectParticipants={this.handleSelectParticipants}
            onSelectCertificates={this.handleSelectCertificates}
            onSelectSubscriptions={this.handleSelectSubscriptions}
            onSelectDiscounts={this.handleSelectDiscounts}
            onSelectAdditionalDiscounts={this.handleSelectAdditionalDiscounts}
            onSelectPaymentType={this.handleSelectPaymentType}
            onSuccessMutation={this.handleSuccsesMutation}
            user={user}
            productType={productType}
            courses={courses}
            cycles={cycles}
            lectures={lectures}
            events={events}
            participants={participants}
            certificates={certificates}
            subscriptions={subscriptions}
            discounts={discounts}
            additionalDiscounts={additionalDiscounts}
            paymentType={paymentType}
            id={id}
            paymentId={paymentId}
          />
        </div>
        <MobileStepper
          steps={11}
          activeStep={activeStep}
          onBack={this.handleBack}
          onNext={this.handleNext}
        />
      </div>
    );
  }
}

CreateOrder.defaultProps = {
  user: [],
  lectures: [],
  courses: [],
  cycles: [],
  events: {},
  participants: [],
  certificates: {},
  subscriptions: {},
  discounts: [],
  additionalDiscounts: [],
};

export default CreateOrder;
