import React from 'react';
import Typography from 'material-ui/Typography';
// import ListSubheader from 'material-ui/List/ListSubheader';
// import List, { ListItem, ListItemText } from 'material-ui/List';
import Grid from 'material-ui/Grid';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';

import { precisionRound } from './../../../new_utils';
import calcOrderPrice from './../calcOrderPrice';
// const styles = theme => ({
//   root: {
//     width: '100%',
//     // maxWidth: 360,
//     background: theme.palette.background.paper,
//     position: 'relative',
//     overflow: 'auto',
//     maxHeight: 300,
//   },
//   listSection: {
//     background: 'inherit',
//   },
// });

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: 30,
  },
  paper: {
    padding: 16,
    // textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});


const noDta = 'Нет данных';
const noSelect = 'Не выбрано';

const paymentTypeTitle = (paymentType) => {
  switch (paymentType) {
    case 'CASH': {
      return 'Наличные';
    }
    case 'INTERNET_ACQUIRING': {
      return 'Интернет-эквайринг';
    }
    case 'ACQUIRING': {
      return 'Эквайринг';
    }
    default: {
      return '';
    }
  }
};

const Result = (props) => {
  const {
    classes,
    user,
    lectures,
    courses,
    cycles,
    events,
    participants,
    paymentType,
    certificates,
    subscriptions,
  } = props;


  const certKeys = Object.keys(certificates);
  const subsKeys = Object.keys(subscriptions);
  const {
    orderPrice,
    discountRate,
    additionalDiscountsRate,
    totalDiscountPrice,
    totalPrice,
    certSubsPrice,
    certStatistic,
    subsStatistic,
    discounts,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
  } = calcOrderPrice(props);

  const subsStatisticKeys = Object.keys(subsStatistic).filter(key => subsStatistic[key].used !== 0);
  const certStatisticKeys = Object.keys(certStatistic).filter(key => certStatistic[key].used !== 0);

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <Typography type="subheading" gutterBottom>
              Данные заказа:
            </Typography>
          </Grid>
        </Grid>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Пользователь:
            </Typography>
            <Typography type="body1" gutterBottom>
              {
                user.length > 0 ?
                (
                  `${user[0].firstName} ${user[0].lastName}, ${user[0].email}`
                ) :
                (
                  noSelect
                )
              }
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Лекции:
            </Typography>
            <div>
              {
                lectures && lectures.length > 0 ?
                (
                  <List>
                    {
                      lectures.map(({ id, title }, index) => (
                        <li key={id}>
                          <Typography type="body1" gutterBottom>
                            {index + 1}. {title}
                          </Typography>
                          {
                            events && events[id] && events[id].map(({ id, date, price }) => (
                              <Typography key={id} type="body1" gutterBottom>
                                { date }: {price}₽
                              </Typography>
                            ))
                          }
                        </li>
                      ))
                    }
                  </List>
                ) :
                (
                  <Typography type="body1" gutterBottom>
                    {noSelect}
                  </Typography>
                )
              }
            </div>
          </Grid>
          <Grid item xs={12} sm={4} md={4} lg={4}>
            <Typography type="body2" gutterBottom>
              Участники:
            </Typography>
            <div>
              {
                participants && participants.length > 0 ?
                (
                  <List>
                    {
                      participants.map(({
                        id, firstName, lastName, email,
                      }) => (
                        <li key={id}>
                          <Typography type="body1" gutterBottom>
                            {`${firstName} ${lastName}, ${email}`}
                          </Typography>
                        </li>
                      ))
                    }
                  </List>
                ) :
                (
                  <Typography type="body1" gutterBottom>
                    {noSelect}
                  </Typography>
                )
              }
            </div>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: 25, marginBottom: 25 }} />
        <Grid container spacing={24}>
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <Typography type="body2" gutterBottom>
              Курс:
            </Typography>
            <Typography type="body1" gutterBottom>
              {
                courses.length > 0 ?
                (
                  `${courses[0].title}: ${courses[0].price}₽`
                ) :
                (
                  `${noSelect}`
                )
              }
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <Typography type="body2" gutterBottom>
              Цикл:
            </Typography>
            <Typography type="body1" gutterBottom>
              {
                cycles.length > 0 ?
                (
                  `${cycles[0].title}: ${cycles[0].price}₽`
                ) :
                (
                  `${noSelect}`
                )
              }
            </Typography>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: 25, marginBottom: 25 }} />
        <Grid container spacing={24}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Typography type="body2" gutterBottom>
              Сертификаты:
            </Typography>
            {/* <div>
              {
                certKeys && certKeys.length > 0 ?
                (
                  <List>
                    {
                      Object.keys(certificates)
                        .map(key => certificates[key])
                        .reduce((prev, curr) => prev.concat(curr))
                        .map(({
                        id, title, userFullName, lecturesCount, personsCount, unusedLectures,
                      }) => (
                        <li key={id}>
                          <Typography type="body1" gutterBottom>
                            {`${title}: ${userFullName}`}
                            <br />
                            {`остаток: ${unusedLectures}, персон: ${personsCount}`}
                          </Typography>
                        </li>
                      ))
                    }
                  </List>
                ) :
                (
                  <Typography type="body1" gutterBottom>
                    {noSelect}
                  </Typography>
                )
              }
            </div>
            <Divider /> */}
            <div>
              <List>
                {certStatisticKeys && certStatisticKeys.length > 0 &&
                  certStatisticKeys.map((key) => {
                    const {
                      id,
                      title,
                      userFullName,
                      used,
                      unusedLectures,
                      personsCount,
                    } = certStatistic[key];
                    return (
                      <li key={id}>
                        <Typography type="body1" gutterBottom>
                          {`${title}: ${userFullName}`}
                          <br />
                          {`остаток: ${unusedLectures}, персон: ${personsCount}`}
                          <br />
                          {`будет списано: ${used}`}
                        </Typography>
                      </li>
                    );
                  })
                }
              </List>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Typography type="body2" gutterBottom>
              Абонементы:
            </Typography>
            {/* <div>
              {
                subsKeys && subsKeys.length > 0 ?
                (
                  <List>
                    {
                      Object.keys(subscriptions)
                        .map(key => subscriptions[key])
                        .reduce((prev, curr) => prev.concat(curr))
                        .map(({
                        id, title, userFullName, lecturesCount, personsCount, unusedLectures,
                      }) => (
                        <li key={id}>
                          <Typography type="body1" gutterBottom>
                            {`${title}: ${userFullName}`}
                            <br />
                            {`остаток: ${unusedLectures}, персон: ${personsCount}`}
                          </Typography>
                        </li>
                      ))
                    }
                  </List>
                ) :
                (
                  <Typography type="body1" gutterBottom>
                    {noSelect}
                  </Typography>
                )
              }
            </div>
            <Divider /> */}
            <div>
              <List>
                {subsStatisticKeys && subsStatisticKeys.length > 0 &&
                  Object.keys(subsStatistic)
                    .map(key => subsStatistic[key])
                    .map(({
                      id,
                      title,
                      userFullName,
                      used,
                      unusedLectures,
                      personsCount,
                    }) => (
                      <li key={id}>
                        <Typography type="body1" gutterBottom>
                          {`${title}: ${userFullName}`}
                          <br />
                          {`остаток: ${unusedLectures}, персон: ${personsCount}`}
                          <br />
                          {`будет списано: ${used}`}
                        </Typography>
                      </li>
                    ))
                }
              </List>
            </div>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: 25, marginBottom: 25 }} />
        <Grid container spacing={24}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Typography type="body2" gutterBottom>
              Скидки:
            </Typography>
            <Typography type="body1" gutterBottom>
              {`Основная скидка: ${discountRate}%`}
            </Typography>
            <Typography type="body1" gutterBottom>
              {`Дополнительная скидка: ${additionalDiscountsRate}%`}
            </Typography>
            <div>
              <List>
                {discountsWithMaxRate && discountsWithMaxRate.length > 0 &&
                  discountsWithMaxRate.map(({
                      id,
                      title,
                      rate,
                    }) => (
                      <li key={id}>
                        <Typography type="body1" gutterBottom>
                          {`Название: ${title}`}
                          <br />
                          {`Cкидка: ${rate}%`}
                        </Typography>
                      </li>
                    ))
                }
              </List>
            </div>
            <div>
              <List>
                {additionalDiscountsWithMaxRate && additionalDiscountsWithMaxRate.length > 0 &&
                  additionalDiscountsWithMaxRate.map(({
                      id,
                      title,
                      rate,
                    }) => (
                      <li key={id}>
                        <Typography type="body1" gutterBottom>
                          {`Название: ${title}`}
                          <br />
                        {`Cкидка: ${rate}%`}
                        </Typography>
                      </li>
                    ))
                }
              </List>
            </div>

            <Typography type="body1" gutterBottom>
              {`Сумма всех скидок: ${discountRate + additionalDiscountsRate}%(${totalDiscountPrice}₽)`}
            </Typography>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: 25, marginBottom: 25 }} />
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Typography type="body2" gutterBottom>
            Тип оплаты:
          </Typography>
          <Typography type="body1" gutterBottom>
            {
              paymentType ? paymentTypeTitle(paymentType) : noSelect
            }
          </Typography>
        </Grid>
        <Divider style={{ marginTop: 25, marginBottom: 25 }} />
        <Grid container spacing={24}>
          <Grid item xs={12} sm={3} md={3} lg={3}>
            <Typography type="body2" gutterBottom>
              Сумма заказа:
            </Typography>
            <Typography type="body1" gutterBottom>
              {orderPrice}₽
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3} lg={3}>
            <Typography type="body2" gutterBottom>
              Сумма скидки:
            </Typography>
            <Typography type="body1" gutterBottom>
              {precisionRound(totalDiscountPrice, 2)}₽({discountRate}% + {additionalDiscountsRate}%)
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3} lg={3}>
            <Typography type="body2" gutterBottom>
              Серт./Абон.:
            </Typography>
            <Typography type="body1" gutterBottom>
              {precisionRound(certSubsPrice, 2)}₽
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3} lg={3}>
            <Typography type="body2" gutterBottom>
              Итого:
            </Typography>
            <Typography type="body1" gutterBottom>
              {precisionRound(totalPrice, 2)}₽
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    </div>


    // <div className={classes.root}>
    //   <Grid container spacing={24}>
    //     <Grid item xs={12} sm={4} md={4} lg={4}>
    //       <Paper className={classes.paper}>xs</Paper>
    //     </Grid>
    //     <Grid item xs={12} sm={4} md={4} lg={4}>
    //       <Paper className={classes.paper}>xs</Paper>
    //     </Grid>
    //     <Grid item xs={12} sm={4} md={4} lg={4}>
    //       <Paper className={classes.paper}>xs</Paper>
    //     </Grid>
    //   </Grid>
    //   <Grid container spacing={24}>
    //     <Grid item xs>
    //       <Paper className={classes.paper}>xs</Paper>
    //     </Grid>
    //     <Grid item xs={6}>
    //       <Paper className={classes.paper}>xs=6</Paper>
    //     </Grid>
    //     <Grid item xs>
    //       <Paper className={classes.paper}>xs</Paper>
    //     </Grid>
    //   </Grid>
    // </div>

    // <List className={classes.root} subheader={<div />}>
    //   {[0, 1, 2, 3, 4].map(sectionId => (
    //     <div key={`section-${sectionId}`} className={classes.listSection}>
    //       <ListSubheader>{`I'm sticky ${sectionId}`}</ListSubheader>
    //       {[0, 1, 2].map(item => (
    //         <ListItem button key={`item-${sectionId}-${item}`}>
    //           <ListItemText primary={`Item ${item}`} />
    //         </ListItem>
    //       ))}
    //     </div>
    //   ))}
    // </List>
    // <div>
    //   <Typography type="title" gutterBottom>
    //     Заказ
    //   </Typography>
    //
    //   <Typography type="body2" gutterBottom>
    //     Пользователь:
    //   </Typography>
    //   <Typography type="body1" gutterBottom>
    //     {
    //       user ?
    //       (
    //         `${user.firstName} ${user.lastName}, ${user.email}`
    //       ) :
    //       (
    //         noSelect
    //       )
    //     }
    //   </Typography>
    //

    // </div>
  );
};

export default withStyles(styles)(Result);
