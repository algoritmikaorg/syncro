import React, { Component } from 'react';
import moment from 'moment';

import Order from './../CreateOrder';
import { formatInputDataForTable } from './../../../new_utils';

class UpdateOrderNotPaid extends Component {
  constructor(props) {
    super(props);
    const { order } = props;
    this.state = {
      order,
    };
  }
  componentWillReceiveProps(nextProps) {
    const { order } = nextProps;
    this.setState({
      order,
    });
  }
  render() {
    const { order } = this.state;
    const {
      id,
      user,
      payment,
      participants,
      events,
    } = order;
    const { id: paymentId, type: paymentType } = payment;
    let productType = 'lecture';
    let courses = [];
    let cycles = [];
    const eventsOflectures = {};
    events.forEach((event) => {
      const { lecture } = event;
      const { id: lectureId } = lecture;
      if (!eventsOflectures[lectureId]) eventsOflectures[lectureId] = [];
      eventsOflectures[lectureId].push(Object.assign(
        {},
        formatInputDataForTable(event),
        {
          src: event,
        },
      ));
    });
    let lectures = [];
    events.forEach(({ lecture }) => {
      if (!lectures.some(({ id }) => lecture.id)) lectures.push(Object.assign({}, lecture, { src: lecture }))
    });
    const { course, cycle } = events[0].lecture;
    if (course && course.length > 0) {
      productType = 'course';
      courses = [Object.assign({}, course[0], { src: course[0] })];
    }
    if (cycle && cycle.length > 0) {
      productType = 'cycle';
      cycles = [Object.assign({}, cycle[0], { src: cycle[0] })];
    }
    let discounts = [];
    let additionalDiscounts = [];
    let certificates = [];
    let subscriptions = [];
    if (payment.discounts && payment.discounts.length > 0) {
      discounts = payment.discounts.map((discount) => {
        return Object.assign(
          {},
          formatInputDataForTable(discount),
          {
            src: discount,
          },
        );
      });
    }

    if (payment.additionalDiscounts && payment.additionalDiscounts.length > 0) {
      additionalDiscounts = payment.additionalDiscounts.map((discount) => {
        return Object.assign(
          {},
          formatInputDataForTable(discount),
          {
            src: discount,
          },
        );
      });
    }
    if (payment.certificatesInPayments && payment.certificatesInPayments.length > 0) {
      certificates = {};
      payment.certificatesInPayments.forEach((certificate) => {
        const { user: { id: userId } } = certificate;
        if (!certificates[userId]) certificates[userId] = [];
        certificates[userId].push(
          Object.assign(
            {},
            formatInputDataForTable(certificate),
            {
              src: certificate,
            },
          )
        )
      });
    }
    if (payment.subscriptionsInPayments && payment.subscriptionsInPayments.length > 0) {
      subscriptions = {};
      payment.subscriptionsInPayments.forEach((subscription) => {
        const { user: { id: userId } } = subscription;
        if (!subscriptions[userId]) subscriptions[userId] = [];
        subscriptions[userId].push(
          Object.assign(
            {},
            formatInputDataForTable(subscription),
            {
              src: subscription,
            },
          )
        );
      });
    }

    return (
      <Order
        history={this.props.history}
        id={id}
        paymentId={paymentId}
        user={[user]}
        productType={productType}
        events={eventsOflectures}
        lectures={lectures}
        courses={courses}
        cycles={cycles}
        participants={participants}
        certificates={certificates}
        subscriptions={subscriptions}
        discounts={discounts}
        additionalDiscounts={additionalDiscounts}
        paymentType={paymentType}
      />
    );
  }
}

export default UpdateOrderNotPaid;
