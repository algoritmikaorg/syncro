import calcSimpleDiscounts from './calcSimpleDiscounts';

// const filterRootDiscounts = (keys, discount) => {
//   const {
//     tagsKeys,
//     coursesKeys,
//     cyclesKeys,
//   } = keys;
//   const {
//     tags: tagsDiscount,
//     courses: coursesDiscount,
//     cycles: cyclesDiscount,
//     allProducts,
//   } = discount;
//
//   if (allProducts) return true;
//   if (tagsDiscount && tagsDiscount.length > 0) {
//     const f = tagsDiscount.filter(({ id }) => tagsKeys.includes(id));
//     if (f.length > 0) return true;
//   }
//   if (coursesDiscount && coursesDiscount.length > 0 && coursesKeys.length > 0) {
//     const f = coursesDiscount.filter(({ id }) => coursesKeys.includes(id));
//     if (f.length > 0) return true;
//   }
//   if (cyclesDiscount && cyclesDiscount.length > 0 && cyclesKeys.length > 0) {
//     const f = cyclesDiscount.filter(({ id }) => cyclesKeys.includes(id));
//     if (f.length > 0) return true;
//   }
//   return false;
// };
//
// const calcLecturesDiscounts = (lectures, events, discounts) => {
//   const eventsKeys = Object.keys(events);
//
//   const lecturesDiscountsResult = {};
//   eventsKeys.forEach((key) => {
//     const lecture = lectures.find(({ id }) => {
//       if (id === key) {
//         return true;
//       }
//       return undefined;
//     });
//     if (lecture) {
//       lecturesDiscountsResult[key] = {
//         title: lecture.title,
//         price: events[key].map(({ price }) => price).reduce((prev, curr) => prev.concat(curr)),
//         discountPrice: 0,
//         totalPrice: 0,
//         rate: 0,
//       };
//     }
//   });
//
//   discounts.forEach((discount) => {
//     const {
//       lectures: lecturesDiscount,
//       rate,
//       valid,
//     } = discount;
//
//     if (valid && lecturesDiscount && lecturesDiscount.length > 0) {
//       const f = lecturesDiscount.filter(({ id }) => eventsKeys.includes(id));
//       if (f.length > 0) {
//         const key = f[0].id;
//         lecturesDiscountsResult[key].rate = rate > lecturesDiscountsResult[key].rate ?
//           rate : lecturesDiscountsResult[key].rate;
//       }
//     }
//   });
//
//   const output = {};
//
//   Object.keys(lecturesDiscountsResult)
//     .filter((key) => {
//       if (lecturesDiscountsResult[key].rate === 0) return false;
//       return true;
//     })
//     .forEach((key) => {
//       const discount = lecturesDiscountsResult[key];
//       const { rate, price } = discount;
//       const discountPrice = rate >= 100 ? price : (price * (100 - rate)) / 100;
//
//       output[key] = Object.assign({}, discount, {
//         discountPrice,
//         totalPrice: price - discountPrice,
//       });
//     });
//
//   return output;
// };
//
// const calcComplexDiscounts = (data, discounts = [], additionalDiscounts = []) => {
//   const {
//     courses = [],
//     cycles = [],
//     lectures = [],
//     events = [],
//   } = data;
//
//   const tags = [];
//   if (courses.length > 0) {
//     tags.push(...courses[0].src.tags);
//   }
//   if (cycles.length > 0) {
//     tags.push(...cycles[0].src.tags);
//   }
//
//   const tagsKeys = tags.map(({ id }) => id);
//   const coursesKeys = courses.map(({ id }) => id);
//   const cyclesKeys = cycles.map(({ id }) => id);
//   const lecturesKeys = lectures.map(({ id }) => id);
//
//   const rootDiscounts = discounts
//     .filter(discount => filterRootDiscounts({
//       tagsKeys,
//       coursesKeys,
//       cyclesKeys,
//     }, discount));
//
//   const rootDiscountsKeys = rootDiscounts.map(({ id }) => id);
//   const lecturesDiscounts = discounts
//     .filter(({ id }) => rootDiscountsKeys.includes(id));
//   const lecturesDiscountsPrice = calcLecturesDiscounts(lectures, events, lecturesDiscounts);
//
//   const rootAdditionalDiscounts = additionalDiscounts
//     .filter(discount => filterRootDiscounts({
//       tagsKeys,
//       coursesKeys,
//       cyclesKeys,
//     }, discount));
//
//   const rootAdditionalDiscountsKeys = rootAdditionalDiscounts.map(({ id }) => id);
//   const lecturesAdditionalDiscounts = additionalDiscounts
//     .filter(({ id }) => rootAdditionalDiscountsKeys.includes(id));
//   const lecturesAdditionalDiscountsPrice = calcLecturesDiscounts(lectures, events, lecturesAdditionalDiscounts);
//
//   const {
//     discountRate,
//     additionalDiscountsRate,
//     discountsWithMaxRate,
//     additionalDiscountsWithMaxRate,
//   } = calcSimpleDiscounts(rootDiscounts, rootAdditionalDiscounts);
//
//   return {
//     discountRate,
//     additionalDiscountsRate,
//     discountsWithMaxRate,
//     additionalDiscountsWithMaxRate,
//     lecturesDiscountsPrice,
//     lecturesAdditionalDiscountsPrice,
//   };
// };

const calcComplexDiscounts = (orderPrice, props, discounts, additionalDiscounts) => {
  const {
    courses,
    cycles,
    lectures,
  } = props;
  let product = null;
  const validDiscounts = discounts.filter(({ valid }) => valid);
  const validAdditionalDiscounts = additionalDiscounts.filter(({ valid }) => valid);

  if (courses && courses.length > 0) product = courses[0].src;
  if (cycles && cycles.length > 0) product = cycles[0].src;

  if (lectures.length === product.lectures.length) {
    return calcSimpleDiscounts(discounts, additionalDiscounts);
  }


  let discountRate = 0;
  let additionalDiscountsRate = 0;

  let discountRateLectures = {};
  let additionalDiscountsRateLectures = {};

  const discountsWithMaxRate = [];
  const additionalDiscountsWithMaxRate = [];

  validDiscounts.forEach((discount) => {
    const { rate, lectures: lecturesDiscount } = discount;
    if (lecturesDiscount && lecturesDiscount.length > 0) {
      const indx = lectures
        .find(element => lecturesDiscount
          .some(({ id }) => id === element.id));
      if (indx) {
        const lecture = lectures[indx];
        if (!discountRateLectures[lecture.id]) {
          discountRateLectures[lecture.id] = {
            discountRate: 0,
            discountWithMaxRate: null,
            lecture: null,
          };
        }
        if (rate > discountRateLectures[lecture.id].discountRate) {
          discountRateLectures[lecture.id].discountRate = rate;
          discountRateLectures[lecture.id].discountsWithMaxRate = discount;
          discountRateLectures[lecture.id].lecture = lecture;
        }
      }
      return true;
    }
    if (rate > discountRate) {
      discountRate = rate;
      discountsWithMaxRate.push(discount);
    }
  });

  validAdditionalDiscounts.forEach((discount) => {
    const { rate, lectures: lecturesDiscount } = discount;
    if (lecturesDiscount && lecturesDiscount.length > 0) {
      const indx = lectures
        .find(element => lecturesDiscount
          .some(({ id }) => id === element.id));
      if (indx) {
        const lecture = lectures[indx];
        if (!additionalDiscountsRateLectures[lecture.id]) {
          additionalDiscountsRateLectures[lecture.id] = {
            discountRate: 0,
            discountWithMaxRate: null,
            lecture: null,
          };
        }
        if (rate > additionalDiscountsRateLectures[lecture.id].discountRate) {
          additionalDiscountsRateLectures[lecture.id].discountRate = rate;
          additionalDiscountsRateLectures[lecture.id].discountsWithMaxRate = discount;
          additionalDiscountsRateLectures[lecture.id].lecture = lecture;
        }
      }
      return true;
    } else if (rate > additionalDiscountsRate) {
      additionalDiscountsRate = rate;
      additionalDiscountsWithMaxRate.push(discount);
    }
  });

  Object.keys(discountRateLectures).forEach((key) => {
    const {
      discountRate: rate,
      discountWithMaxRate: discount,
      lecture,
    } = discountRateLectures[key];
    const { price } = lecture;
    const discountPrice = (price * rate) / 100;
    const rateOfOrderPrice = (discountPrice * 100) / orderPrice;
    discountRate += rateOfOrderPrice;
    discountsWithMaxRate.push(discount);
  });

  Object.keys(additionalDiscountsRateLectures).forEach((key) => {
    const {
      discountRate: rate,
      discountWithMaxRate: discount,
      lecture,
    } = discountRateLectures[key];
    const { price } = lecture;
    const discountPrice = (price * rate) / 100;
    const rateOfOrderPrice = (discountPrice * 100) / orderPrice;
    additionalDiscountsRate += rateOfOrderPrice;
    additionalDiscountsWithMaxRate.push(discount);
  });

  return {
    discountRate,
    additionalDiscountsRate,
    discountsWithMaxRate,
    additionalDiscountsWithMaxRate,
  };
};

export default calcComplexDiscounts;
