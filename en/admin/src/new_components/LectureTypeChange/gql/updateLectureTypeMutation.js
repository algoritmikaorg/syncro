import gql from 'graphql-tag';

export default gql `
  mutation UpdateLectureMutation(
      $id: ID!,
      $lectureType: LectureType!,
    ) {
    updateLecture(
      id: $id,
      lectureType: $lectureType,
    ) {
      id
      lectureType
    }
  }
`;
