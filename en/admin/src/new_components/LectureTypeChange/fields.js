import { LECTYRE_TYPES } from './../../enum';

const fields = [
  {
    id: 'id',
    label: 'id',
    type: 'multiline',
    required: true,
    disabled: true,
  },
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'select',
    required: true,
    enum: LECTYRE_TYPES,
  },
];

export default fields;
