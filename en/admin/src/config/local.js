const PROJECT_ID = 'cjd5n24x100080161u9vrln2c';
// const PROJECT_ID = 'cjdagzahz001w0190w199ns7j';
const DOMAIN = 'localhost:60000';

export default {
  SIMPLE_API_ENDPOINT: `http://${DOMAIN}/simple/v1/${PROJECT_ID}`,
  FILE_API_ENDPOINT: 'http://localhost:60001/file',
  IMAGE_HOST: 'http://localhost:60002/images',
  // FILE_API_ENDPOINT: 'http://localhost:60001/file',
  SUBSCRIPTIONS__API_ENDPOINT: `ws://${DOMAIN}/subscriptions/v1/${PROJECT_ID}`,
  // IMAGE_HOST: `http://${DOMAIN}/images/v1/${PROJECT_ID}`,
  PATH: '/synhro-admin',
  SITE: 'http://localhost:3000',
};
