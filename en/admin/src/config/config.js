import dev from './dev';
import prod from './prod';
import local from './local';

const { hostname } = window.location;

let config = null;

switch (hostname) {
  case 'localhost': {
    config = local;
    break;
  }
  case 'dev.synchronize.ru': {
    config = dev;
    break;
  }
  default: {
    config = prod;
  }
}
export default config;
