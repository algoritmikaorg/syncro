const PROJECT_ID = 'cjd67we9401sw0155segppqze';
const DOMAIN = 'synchronize.ru';

export default {
  SIMPLE_API_ENDPOINT: `http://api-dev.${DOMAIN}/simple/v1/${PROJECT_ID}`,
  FILE_API_ENDPOINT: `http://file-dev.${DOMAIN}/file`,
  SUBSCRIPTIONS__API_ENDPOINT: `ws://api-dev.${DOMAIN}/subscriptions/v1/${PROJECT_ID}`,
  IMAGE_HOST: `http://images-dev.${DOMAIN}/images`,
  PATH: '/synhro-admin',
  SITE: `http://${DOMAIN}`,
};
