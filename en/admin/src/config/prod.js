const PROJECT_ID = 'cjd67we9401sw0155segppqze';
const DOMAIN = 'synchronize.ru';

export default {
  SIMPLE_API_ENDPOINT: `http://api.${DOMAIN}/simple/v1/${PROJECT_ID}`,
  FILE_API_ENDPOINT: `http://file.${DOMAIN}/file`,
  IMAGE_HOST: `http://images.${DOMAIN}/images`,
  SUBSCRIPTIONS__API_ENDPOINT: `ws://api.${DOMAIN}/subscriptions/v1/${PROJECT_ID}`,
  PATH: '/synhro-admin',
  SITE: `http://${DOMAIN}`,
};
