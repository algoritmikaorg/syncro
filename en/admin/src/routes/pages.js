import {
  Home,
  Login,
  NotFound,
  Materials,
  Others,
  Settings,
  Template,
  Imports,
  Test,
} from './../pages';

import {
  Catalog,
  Content,
  Lecturers,
  Discounts,
  Certificates,
  Subscriptions,
  Locations,
  Tags,
  Events,
  Orders,
  Users,
  Reviews,
  Partners,
  Galleries,
  Acquirings,
  Messages,
  // Settings,
} from './../new_pages';


const pages = [
  {
    path: '/',
    component: Home,
    title: '',
    exact: true,
  },
  {
    path: '/login',
    component: Login,
    title: '',
  },
  {
    path: '/messages',
    component: Messages,
    title: 'Уведомления',
  },
  {
    path: '/catalog',
    component: Catalog,
    title: 'Каталог',
  },
  {
    path: '/content',
    component: Content,
    title: 'Содержимое сайта',
  },
  {
    path: '/reviews',
    component: Reviews,
    title: 'Отзывы',
  },
  {
    path: '/locations',
    component: Locations,
    title: 'Локации',
  },
  {
    path: '/certificates',
    component: Certificates,
    title: 'Сертификаты',
  },
  {
    path: '/subscriptions',
    component: Subscriptions,
    title: 'Абонементы',
  },
  {
    path: '/partners',
    component: Partners,
    title: 'Партнеры',
  },
  {
    path: '/users',
    component: Users,
    title: 'Пользователи',
  },
  {
    path: '/orders',
    component: Orders,
    title: 'Заказы',
  },
  {
    path: '/settings',
    component: Settings,
    title: 'Настройки',
  },
  {
    path: '/template',
    component: Template,
    title: 'Шаблон',
  },
  {
    path: '/tags',
    component: Tags,
    title: 'Направления',
  },
  {
    path: '/lecturers',
    component: Lecturers,
    title: 'Лекторы',
  },
  {
    path: '/discounts',
    component: Discounts,
    title: 'Скидки',
  },
  {
    path: '/events',
    component: Events,
    title: 'Мероприятия',
  },
  {
    path: '/acquirings',
    component: Acquirings,
    title: 'Эквайринг',
  },
  {
    path: '/galleries',
    component: Galleries,
    title: 'Галереи',
  },
];

export default pages;
