import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  ALL_SETTINGS_QUERY,
  SETTING_QUERY,
  CREATE_SETTINGS_MUTATION,
  EDIT_SETTINGS_MUTATION,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import settingsFields from './fields';

const SettingsMutation = (props) => {
  const {
    mutationType,
    typeMaterial,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;
  gqlQueryName = SETTING_QUERY;
  gqlQueryItemName = 'Setting';

  if (mutationType === 'create') {
    gqlMutationName = CREATE_SETTINGS_MUTATION;
    fields = settingsFields.createMailchimp;
    resulMutatiomtMessageSuccses = 'Создано';
    mutationResultObjectName = 'createSetting';
  } else {
    gqlMutationName = EDIT_SETTINGS_MUTATION;
    mutationResultObjectName = 'updateSetting';
    resulMutatiomtMessageSuccses = 'Обновлено';
    mutationResultObjectName = 'updateSetting';
  }

  const outputModifier = outputUtils.outputMutationModifier;
  switch (typeMaterial) {
    case 'mailchimp': {
      if (mutationType === 'create') {
        fields = settingsFields.createMailchimp;
      } else {
        fields = settingsFields.updateMailchimp;
      }
      break;
    }
    case 'mandrill': {
      if (mutationType === 'create') {
        fields = settingsFields.createMaindrill;
      } else {
        fields = settingsFields.updateMandrill;
      }
      break;
    }
    case 'smtp': {
      if (mutationType === 'create') {
        fields = settingsFields.createSmtp;
      } else {
        fields = settingsFields.updateSmtp;
      }
      break;
    }
    case 'contacts': {
      if (mutationType === 'create') {
        fields = settingsFields.createContacts;
      } else {
        fields = settingsFields.updateContacts;
      }
      break;
    }
    case 'acquiring': {
      if (mutationType === 'create') {
        fields = settingsFields.createAcquiring;
      } else {
        fields = settingsFields.updateAcquiring;
      }
      break;
    }
    default: {
      if (mutationType === 'create') {
        fields = settingsFields.createMailchimp;
      } else {
        fields = settingsFields.updateMailchimp;
      }
    }
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        showActions
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default SettingsMutation;
