import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_DISCOUNT_MUTATION,
  EDIT_DISCOUNT_MUTATION,
  DISCOUNT_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import tagsFields from './fields';
import tagsActions from './actions';


const DiscountsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_DISCOUNT_MUTATION;
    fields = tagsFields.create;
    mutationResultObjectName = 'createDiscount';
    resulMutatiomtMessageSuccses = 'Скидка создана';
  } else {
    gqlMutationName = EDIT_DISCOUNT_MUTATION;
    fields = tagsFields.update;
    mutationResultObjectName = 'updateDiscount';
    resulMutatiomtMessageSuccses = 'Скидка обновлена';
    gqlQueryName = DISCOUNT_QUERY;
    gqlQueryItemName = 'Discount';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
        // showActions={false}
      />
    </div>
  );
};

export default DiscountsMutation;
