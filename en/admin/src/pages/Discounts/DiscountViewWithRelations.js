import React from 'react';

import gql from './gql';

import {
  DISCOUNT_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import tagsFields from './fields';

const DiscountViewWithRelations = (props) => {

  const gqlQueryWithRelationsName = DISCOUNT_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Discount';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={tagsFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
        // showActions={false}
      />
    </div>
  );
};

export default DiscountViewWithRelations;
