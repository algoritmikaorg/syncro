import gql from 'graphql-tag';

export default gql`
  query AllDiscountsQuery($filter: DiscountFilter, $first: Int, $skip: Int, $orderBy: DiscountOrderBy) {
    allDiscounts(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      promocode,
      rate,
      allProducts,
      buyingLecturesCount,
      participantsCount,
      useCount,
      validityPeriodFrom,
      validityPeriodTo,
      courses {
        id,
        title,
      }
      cycles {
        id,
        title,
      }
      lectures {
        id,
        title,
      }
      orders {
        id
      }
      tags {
        id,
        title,
      }
      _coursesMeta {
        count
      }
      _cyclesMeta {
        count
      },
      _lecturesMeta {
        count
      },
      _ordersMeta {
        count
      },
      _tagsMeta {
        count
      },
    }
    _allDiscountsMeta {
      count
    }
  }
`;
