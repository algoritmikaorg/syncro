import * as R from 'ramda';
import { format } from './../../utils';

const create = [
  {
    id: 'title',
    label: 'Название',
    type: 'multiline',
  },
  {
    id: 'public',
    label: 'Опубликовано',
    type: 'checkbox',
  },
  {
    id: 'promocode',
    label: 'Промокод',
    type: 'text',
    type: 'multiline',
  },
  {
    id: 'rate',
    label: 'Величина скидки',
    required: true,
    type: 'number',
  },
  {
    id: 'validityPeriodFrom',
    label: 'Начало действия',
    required: true,
    type: 'date',
  },
  {
    id: 'validityPeriodTo',
    label: 'Окончания действия',
    required: true,
    type: 'date',
  },
  {
    id: 'useCount',
    label: 'Сколько раз можно использовать',
    helperText: '-1 - безлимитно',
    type: 'number',
  },
  {
    id: 'buyingLecturesCount',
    label: 'Количество лекций при которых начинает действовать скидка',
    helperText: '-1 - безлимитно',
    type: 'number',
  },
  {
    id: 'participantsCount',
    label: 'Количество участников при которых начинает действовать скидка',
    type: 'number',
  },
  {
    id: 'allProducts',
    label: 'Действут на все материалы',
    type: 'checkbox',
  },
  {
    id: 'lecturesIds',
    label: 'Лекции связанные со скидкой',
    disabled: true,
    relation: 'lectures',
    type: 'relation',
  },
  {
    id: 'coursesIds',
    label: 'Курсы связанные со скидкой',
    disabled: true,
    relation: 'courses',
    type: 'relation',
  },
  {
    id: 'cyclesIds',
    label: 'Циклы связанные со скидкой',
    disabled: true,
    relation: 'cycles',
    type: 'relation',
  },
  {
    id: 'subscriptionsIds',
    label: 'Типы Абонементов связанные со скидкой',
    disabled: true,
    relation: 'subscriptions',
    type: 'relation',
  },
  {
    id: 'certificatesIds',
    label: 'Типы Сертификатов связанные со скидкой',
    disabled: true,
    relation: 'certificates',
    type: 'relation',
  },
];


const update = create.slice();
update.splice(-1, 1);
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const view = update.slice();
view.push(
  {
    id: 'ordersIds',
    label: 'Заказы связанные со скидкой',
    disabled: true,
    relation: 'orders',
    type: 'relation',
  },
);

const filter = [
  {
    id: 'title_contains',
    label: 'Название',
  },
  {
    id: 'promocode_contains',
    label: 'Промокод',
  },
  {
    id: 'rate',
    label: 'Размер скидки',
    type: 'number-short',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
