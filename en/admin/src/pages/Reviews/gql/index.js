import {
  CREATE_REVIEW_MUTATION,
  ALL_REVIEWS_QUERY,
  EDIT_REVIEW_MUTATION,
  REVIEW_QUERY,
  DELETE_REVIEW_MUTATION,
  REVIEW_QUERY_WITH_RELATIONS,
} from './../../../constants';
import createReviewMutation from './createReviewMutation';
import allReviewsQuery from './allReviewsQuery';
import editReviewMutation from './editReviewMutation';
import reviewQuery from './reviewQuery';
import deleteReviewMutation from './deleteReviewMutation';
import reviewQueryWithRelations from './reviewQueryWithRelations';

const gql = {
  [CREATE_REVIEW_MUTATION]: createReviewMutation,
  [ALL_REVIEWS_QUERY]: allReviewsQuery,
  [EDIT_REVIEW_MUTATION]: editReviewMutation,
  [REVIEW_QUERY]: reviewQuery,
  [DELETE_REVIEW_MUTATION]: deleteReviewMutation,
  [REVIEW_QUERY_WITH_RELATIONS]: reviewQueryWithRelations,
};

export default gql;
