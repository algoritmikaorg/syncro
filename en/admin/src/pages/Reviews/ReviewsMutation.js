import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_REVIEW_MUTATION,
  EDIT_REVIEW_MUTATION,
  REVIEW_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import reviewsFields from './fields';
import reviewsActions from './actions';


const ReviewsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_REVIEW_MUTATION;
    fields = reviewsFields.create;
    mutationResultObjectName = 'createReview';
    resulMutatiomtMessageSuccses = 'Отзыв создан';
  } else {
    gqlMutationName = EDIT_REVIEW_MUTATION;
    fields = reviewsFields.update;
    mutationResultObjectName = 'updateReview';
    resulMutatiomtMessageSuccses = 'Отзыв обновлен';
    gqlQueryName = REVIEW_QUERY;
    gqlQueryItemName = 'Review';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default ReviewsMutation;
