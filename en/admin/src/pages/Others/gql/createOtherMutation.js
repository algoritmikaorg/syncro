import gql from 'graphql-tag';

export default gql`
  mutation CreateOtherMutation(
      $title: String!
      $description: String!
    ){
    createOther(
      title: $title,
      description: $description,
    ) {
      id
    }
  }
`;
