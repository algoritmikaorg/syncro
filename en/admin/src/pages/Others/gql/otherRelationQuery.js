import gql from 'graphql-tag';

export default gql`
  query otherQuery($id: ID) {
    Other(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
      title,
      description,
    }
  }
`;
