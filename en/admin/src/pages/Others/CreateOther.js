import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import * as R from 'ramda';

import gql from './gql';
import { create } from './fields';
import FormBase from './../../components/FormBase';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';

import { CREATE_OTHER_MUTATION } from './../../constants';

const Form = withProgress([CREATE_OTHER_MUTATION])(FormBase);

const createOtherMutationResult = `${CREATE_OTHER_MUTATION}Result`;
const createOtherMutationError = `${CREATE_OTHER_MUTATION}Error`;
const createOtherMutationLoading = `${CREATE_OTHER_MUTATION}Loading`;
const createOtherMutationReset = `${CREATE_OTHER_MUTATION}Reset`;

class CreateOther extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: null,
      clearFields: [],
      req: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[createOtherMutationResult]) {
      const id = nextProps[createOtherMutationResult].data.createOther.id
      this.props.snackbar({ message: `Материал создан - id: ${id}`, name: 'Success' });
      this.props.reqSuccess(id);
    }
    if (nextProps[createOtherMutationError]) {
      this.props.snackbar(nextProps[createOtherMutationError]);
      this.props[createOtherMutationReset]();
      this.setState({
        // clearFields: ['password', 'repassword'],
        req: false,
      }, () => {
        this.props.reqError(nextProps[createOtherMutationError]);
      });
    }
  }
  confirm = async () => {
    const { fields } = this.state;
    this.props[CREATE_OTHER_MUTATION]({
      variables: fields,
    });
  }
  handleSubmit(fields) {
    const state = {
      fields: R.clone(fields),
      req: true,
    };
    this.setState(state, () => {
      this.confirm();
    });
  }
  render() {
    const { clearFields, req } = this.state;

    return (
      <Form
        formTitle="Создать Материал"
        fields={create}
        disabledAll={false}
        disabledActions={
          {
            save: false,
          }
        }
        showActions={
          {
            save: true,
          }
        }
        onSubmit={this.handleSubmit}
        clearFields={clearFields}
        req={req}
        {...this.props}
      />
    );
  }
}

CreateOther.defaultProps = {
  reqSuccess: () => {},
  reqError: () => {},
};

export default compose(
  graphql(gql[CREATE_OTHER_MUTATION], { name: CREATE_OTHER_MUTATION }),
  withStateMutation({ name: CREATE_OTHER_MUTATION }),
  withSnackbar(),
)(CreateOther);
