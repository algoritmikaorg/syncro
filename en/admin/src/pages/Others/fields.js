import * as R from 'ramda';

export const create = {
  title: {
    value: '',
    label: 'Заголовок',
    required: true,
  },
  description: {
    value: '',
    label: 'Описание',
    required: true,
  },
};

export const view = {
  title: {
    value: '',
    label: 'Заголовок',
    disabled: true,
  },
  description: {
    value: '',
    label: 'Описание',
    disabled: true,
  },
};

export const edit = (() => {
  const output = R.clone(create);
  output.id = {
    value: '',
    label: 'ID',
    required: true,
    disabled: true,
  };
  return output;
})();

export const filter = {
  title: {
    value: '',
    label: 'Заголовок',
  },
};
