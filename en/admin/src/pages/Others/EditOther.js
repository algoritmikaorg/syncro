import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import * as R from 'ramda';

import gql from './gql';
import { edit } from './fields';
import FormBase from './../../components/FormBase';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';
import { EDIT_OTHER_MUTATION, OTHER_QUERY } from './../../constants';

const Form = withProgress([EDIT_OTHER_MUTATION, OTHER_QUERY])(FormBase);

const editOtherMutationResult = `${EDIT_OTHER_MUTATION}Result`;
const editOtherMutationError = `${EDIT_OTHER_MUTATION}Error`;
const editOtherMutationLoading = `${EDIT_OTHER_MUTATION}Loading`;
const editOtherMutationReset = `${EDIT_OTHER_MUTATION}Reset`;

class EditOther extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: null,
      req: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[editOtherMutationResult]) {
      const id = nextProps[editOtherMutationResult].data.updateOther.id
      this.props.snackbar({ message: `Материал обновлен - id: ${id}`, name: 'Success' });
      this.setState({
        req: false,
      });
      this.props.reqSuccess(id);
    }
    if (nextProps[editOtherMutationError]) {
      this.props.snackbar(nextProps[editOtherMutationError]);
      this.props[editOtherMutationReset]();
      this.setState({
        req: false,
      }, () => {
        this.props.reqError(nextProps[editOtherMutationError]);
      });
    }
  }
  confirm = async () => {
    const { fields } = this.state;
    this.props[EDIT_OTHER_MUTATION]({
      variables: fields,
    });
  }
  handleSubmit(fields) {
    const state = {
      fields: R.clone(fields),
      req: true,
    };
    this.setState(state, () => {
      this.confirm();
    });
  }
  render() {
    const { req } = this.state;
    let fields = null;
    if (this.props[OTHER_QUERY].Other) {
      fields = R.clone(edit);
      const other = this.props[OTHER_QUERY].Other;

      Object.keys(fields).forEach((key) => {
        fields[key].value = other[key];
      });
    }

    return (
      <Form
        formTitle="Редактировать Материала"
        fields={fields || edit}
        type={'edit'}
        disabledAll={false}
        disabledActions={
          {
            save: false,
          }
        }
        showActions={
          {
            save: true,
          }
        }
        onSubmit={this.handleSubmit}
        req={req || this.props[OTHER_QUERY].loading}
        {...this.props}
      />
    );
  }
}

EditOther.defaultProps = {
  reqSuccess: () => {},
  reqError: () => {},
};

export default compose(
  graphql(gql[OTHER_QUERY], {
    name: OTHER_QUERY,
    options(ownProps) {
      const id = ownProps.match.params.id;
      return (
        {
          variables: {
            id,
          },
        }
      );
    },
  }),
  graphql(gql[EDIT_OTHER_MUTATION], { name: EDIT_OTHER_MUTATION }),
  withStateMutation({ name: EDIT_OTHER_MUTATION }),
  withSnackbar(),
)(EditOther);
