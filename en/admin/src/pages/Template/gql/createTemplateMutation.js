import gql from 'graphql-tag';

export default gql `
  mutation CreateTemplateMutation(
    $scriptBody: String,
    $scriptHead: String,
    $styleHead: String,
    $contactTitle: String,
    $contactOfficeTitle: String,
    $contactOfficeAddress: String,
    $contactOfficePhone: String,
    $metaKeywords: String,
    $metaDescription: String,
    $titleHead: String,
  ){
    createTemplate(
      scriptBody: $scriptBody,
      scriptHead: $scriptHead,
      styleHead: $styleHead,
      contactTitle: $contactTitle,
      contactOfficeTitle: $contactOfficeTitle,
      contactOfficeAddress: $contactOfficeAddress,
      contactOfficePhone: $contactOfficePhone,
      metaKeywords: $metaKeywords,
      metaDescription: $metaDescription,
      titleHead: $titleHead,
    ) {
      id
      scriptBody
      scriptHead
      styleHead
      contactTitle
      contactOfficeTitle
      contactOfficeAddress
      contactOfficePhone

      metaKeywords
      metaDescription
      titleHead
    }
  }
`;
