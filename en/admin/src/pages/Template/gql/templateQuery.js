import gql from 'graphql-tag';

export default gql`
  query TemplateQuery($id: ID!) {
    Template(
      id: $id
    ) {
      id
      createdAt
      updatedAt

      scriptBody
      scriptHead
      styleHead
      contactTitle
      contactOfficeTitle
      contactOfficeAddress
      contactOfficePhone

      metaKeywords
      metaDescription
      titleHead
    }
  }
`;
