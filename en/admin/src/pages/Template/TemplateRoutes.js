import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Template from './Template';
import TemplateScripts from './TemplateScripts';
import TemplateStyles from './TemplateStyles';
import TemplateBlocks from './TemplateBlocks';
import TemplateScriptsMutation from './TemplateScriptsMutation';
import TemplateStylesMutation from './TemplateStylesMutation';
import TemplateBlocksMutation from './TemplateBlocksMutation';


import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const TemplateRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        {/* <Route
          exact
          path={`${url}`}
          component={rest => (
            <Template
              {...props}
              {...rest}
            />
          )}
        /> */}
        <Route
          exact
          path={`${url}/scripts`}
          component={rest => (
            <TemplateScripts {...props} {...rest} title={'Скрипты'} />
          )}
        />
        <Route
          exact
          path={`${url}/scripts/create`}
          component={rest => (
            <TemplateScriptsMutation
              {...props}
              {...rest}
              title={'Скрипты'}
              mutationType="create"
            />
          )}
        />
        <Route
          exact
          path={`${url}/scripts/update/:id`}
          component={rest => (
            <TemplateScriptsMutation
              {...props}
              {...rest}
              title={'Скрипты'}
              mutationType="update"
            />
          )}
        />
        <Route
          exact
          path={`${url}/styles`}
          component={rest => (
            <TemplateStyles {...props} {...rest} title={'Стили'} />
          )}
        />
        <Route
          exact
          path={`${url}/styles/create`}
          component={rest => (
            <TemplateStylesMutation
              {...props}
              {...rest}
              title={'Стили'}
              mutationType="create"
            />
          )}
        />
        <Route
          exact
          path={`${url}/styles/update/:id`}
          component={rest => (
            <TemplateStylesMutation
              {...props}
              {...rest}
              title={'Стили'}
              mutationType="update"
            />
          )}
        />
        <Route
          exact
          path={`${url}/blocks`}
          component={rest => (
            <TemplateBlocks {...props} {...rest} title={'Блоки'} />
          )}
        />
        <Route
          exact
          path={`${url}/blocks/create`}
          component={rest => (
            <TemplateBlocksMutation
              {...props}
              {...rest}
              title={'Блоки'}
              mutationType="create"
            />
          )}
        />
        <Route
          exact
          path={`${url}/blocks/update/:id`}
          component={rest => (
            <TemplateBlocksMutation
              {...props}
              {...rest}
              title={'Блоки'}
              mutationType="update"
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(TemplateRoutes, { title: 'Шаблон' });
export default TemplateRoutes;
