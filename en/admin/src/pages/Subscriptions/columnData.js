import { format } from './../../utils';

const columnData = [
  {
    id: 'userId',
    numeric: false,
    disablePadding: false,
    label: 'Покупатель',
  },
  {
    id: 'discountsIds',
    label: 'Скидка',
  },
  {
    id: 'eventsIds',
    label: 'Количество лекций',
  },
  {
    id: 'paymentId',
    label: 'Срок действия',
  },
  {
    id: 'participantsIds',
    label: 'Окончание срока действия',
  },
  {
    id: 'tickets',
    label: 'Остаток лекций',
  },
];
export default columnData;
