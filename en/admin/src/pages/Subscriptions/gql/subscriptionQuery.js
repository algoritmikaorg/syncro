import gql from 'graphql-tag';

export default gql`
  query subscriptionQuery($id: ID!) {
    Subscription(
      id: $id
    ) {
      id,
      createdAt,
      updatedAt,
    }
  }
`;
