import gql from 'graphql-tag';

export default gql`
  mutation UpdateSubscriptionMutation(
      $id: ID!,
    ){
    updateSubscription(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
    }
  }
`;
