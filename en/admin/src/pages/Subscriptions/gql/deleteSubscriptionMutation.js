import gql from 'graphql-tag';

export default gql`
  mutation deleteSubscriptionMutation(
      $id: ID!
    ){
    deleteSubscription(
      id: $id,
    ) {
      id
    }
  }
`;
