import * as R from 'ramda';

import { format } from './../../utils';

const create = [
  {
    id: 'userId',
    label: 'Покупатель',
    // disabled: true,
    required: true,
    type: 'text',
  },
];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
  // {
  //   id: 'alias_id',
  //   label: 'alias id',
  //   required: true,
  //   disabled: true,
  //   type: 'alias',
  // },
);

const view = update.slice();
// view.push(
//   {
//     id: 'lecturers',
//     label: 'Лекторы связанные с направлением',
//     disabled: true,
//     relation: 'lecturers',
//     type: 'relation',
//   },
// );

const filter = [
  {
    id: 'title_contains',
    value: '',
    label: 'Покупатель',
    type: 'text',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
