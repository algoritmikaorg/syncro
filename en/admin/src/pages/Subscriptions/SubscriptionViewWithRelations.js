import React from 'react';

import gql from './gql';

import {
  SUBSCRIPTION_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import subscriptionsFields from './fields';

const SubscriptionViewWithRelations = (props) => {
  const gqlQueryWithRelationsName = SUBSCRIPTION_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Subscription';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={subscriptionsFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
      />
    </div>
  );
};

export default SubscriptionViewWithRelations;
