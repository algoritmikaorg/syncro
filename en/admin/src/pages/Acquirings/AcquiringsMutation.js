import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_ACQUIRING_MUTATION,
  EDIT_ACQUIRING_MUTATION,
  ACQUIRING_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import acquiringsFields from './fields';
import acquiringsActions from './actions';


const AcquiringsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_ACQUIRING_MUTATION;
    fields = acquiringsFields.create;
    mutationResultObjectName = 'createAcquiring';
    resulMutatiomtMessageSuccses = 'Эквайринг создан';
  } else {
    gqlMutationName = EDIT_ACQUIRING_MUTATION;
    fields = acquiringsFields.update;
    mutationResultObjectName = 'updateAcquiring';
    resulMutatiomtMessageSuccses = 'Экваринг обновлен';
    gqlQueryName = ACQUIRING_QUERY;
    gqlQueryItemName = 'Acquiring';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default AcquiringsMutation;
