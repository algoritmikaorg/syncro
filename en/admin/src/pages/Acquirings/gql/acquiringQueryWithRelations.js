import gql from 'graphql-tag';

export default gql`
  query acquiringQueryWithRelations($id: ID!) {
    Acquiring(
      id: $id,
    ) {
      id
      createdAt
      updatedAt

      commission
      password
      payments
      shopId
      title
      token
      payments {
        id
        createdAt
        updatedAt

        order {
          id
          user {
            id
            firstName
            lastName
          }
        }
      }
      currentAcquiring {
        id
      }
      _paymentsMeta {
        count
      }
    }
  }
`;
