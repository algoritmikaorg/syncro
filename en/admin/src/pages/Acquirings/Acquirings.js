import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_ACQUIRINGS_QUERY,
  DELETE_ACQUIRING_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import acquiringsFields from './fields';
import acquiringsActions from './actions';
import acquiringsColumnData from './columnData';

const Acquirings = (props) => {

  const gqlAllQueryName = ALL_ACQUIRINGS_QUERY;
  const gqlAllQueryItemsName = 'allAcquirings';
  const gqlMutationDelete = DELETE_ACQUIRING_MUTATION;
  const gqlMutationDeleteItemName = 'deleteAcquiring';
  const resulMutatiomtMessageSuccses = 'Эквайринг удален';

  return (
    <div>
      <ListMaterials
        {...props}
        actionsButtons={acquiringsActions}
        columnData={acquiringsColumnData}
        fields={acquiringsFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showActions={props.showActions === undefined ? true : props.showActions}
        // filterModifier={filterModifier}
      />
    </div>
  );
};

export default Acquirings;
