import React from 'react';

import { format as formatUtil } from './../../utils';

import gql from './gql';

import {
  LOCATION_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import locationsFields from './fields';

const LocationViewWithRelations = (props) => {

  const gqlQueryWithRelationsName = LOCATION_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Location';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        // formatInputData={formatUtil.locationListInputData}
        fields={locationsFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
      />
    </div>
  );
};

export default LocationViewWithRelations;
