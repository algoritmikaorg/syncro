import {format} from './../../utils';

const columnData = [
  {
    id: 'title',
    numeric: false,
    disablePadding: false,
    label: 'Название',
  }, {
    id: 'metro',
    numeric: false,
    disablePadding: false,
    label: 'Метро',
  }, {
    id: 'address',
    numeric: false,
    disablePadding: false,
    label: 'Адресс',
  }, {
    id: 'image',
    numeric: false,
    disablePadding: false,
    label: 'Фото',
  },
];
export default columnData;
