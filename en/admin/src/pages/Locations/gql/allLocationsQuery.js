import gql from 'graphql-tag';

export default gql`
  query AllLocationsQuery($filter: LocationFilter, $first: Int, $skip: Int, $orderBy: LocationOrderBy) {
    allLocations(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
      image,
      metro,
      title,
      address,
    }
    _allLocationsMeta {
      count
    }
  }
`;
