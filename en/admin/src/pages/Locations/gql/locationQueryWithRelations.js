import gql from 'graphql-tag';

export default gql`
  query LocationQueryWithRelations($id: ID!) {
    Location(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      address,
      image
      events {
        id,
        date,
        lecture {
          id,
          title,
        }
        lecturers {
          id,

          firstName,
          lastName,
        }
        location {
          id,
          title,
        }
        price,
        quantityOfTickets,
        tickets {
          id
        },
      }
      metro,
      _eventsMeta {
        count
      }
      _eventsMeta {
        count
      }
    }
  }
`;
