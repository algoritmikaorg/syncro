import gql from 'graphql-tag';

export default gql`
  mutation CreateLocationMutation(
      $image: String,
      $metro: String!,
      $title: String!,
      $address: String!,
    ){
    createLocation(
      image: $image,
      metro: $metro,
      title: $title,
      address: $address,
    ) {
      id
    }
  }
`;
