import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_LOCATION_MUTATION,
  EDIT_LOCATION_MUTATION,
  LOCATION_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import locationsFields from './fields';
import locationsActions from './actions';


const LocationMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_LOCATION_MUTATION;
    fields = locationsFields.create;
    mutationResultObjectName = 'createLocation';
    resulMutatiomtMessageSuccses = 'Направление создано';
  } else {
    gqlMutationName = EDIT_LOCATION_MUTATION;
    fields = locationsFields.update;
    mutationResultObjectName = 'updateLocation';
    resulMutatiomtMessageSuccses = 'Направление обновлено';
    gqlQueryName = LOCATION_QUERY;
    gqlQueryItemName = 'Location';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default LocationMutation;
