import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Locations from './Locations';
import LocationMutation from './LocationMutation';
import LocationViewWithRelations from './LocationViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const LocationsRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Locations
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <LocationMutation {...props} {...rest} mutationType="create" title={'Новая локация'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <LocationMutation {...props} {...rest} mutationType="update" title={'Редактирование локации'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <LocationViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(LocationsRoutes, { title: 'Локации' });
export default LocationsRoutes;
