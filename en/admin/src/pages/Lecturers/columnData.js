import { format } from './../../utils';

const columnData = [
  { id: 'firstName', numeric: false, disablePadding: false, label: 'Имя' },
  { id: 'lastName', numeric: false, disablePadding: false, label: 'Фамилия' },
  { id: 'tags', numeric: false, disablePadding: false, label: 'Направления', format: format.tagsToTitleString },
];
export default columnData;
