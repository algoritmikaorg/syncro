import React from 'react';

import gql from './gql';

import {
  LECTURER_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import fields from './fields';

const LecturerViewWithRelations = (props) => {

  const gqlQueryWithRelationsName = LECTURER_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Lecturer';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={fields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
        // showActions={false}
      />
    </div>
  );
};

export default LecturerViewWithRelations;
