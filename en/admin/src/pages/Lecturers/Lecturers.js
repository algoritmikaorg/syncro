import React from 'react';

import gql from './gql';

import {
  ALL_LECTURERS_QUERY,
  DELETE_LECTURER_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import lecturersFields from './fields';
import lecturersActions from './actions';
import lecturersColumnData from './columnData';
import lecturersFilterModifier from './filter';

const lecturers = (props) => {

  const gqlAllQueryName = ALL_LECTURERS_QUERY;
  const gqlAllQueryItemsName = 'allLecturers';
  const gqlMutationDelete = DELETE_LECTURER_MUTATION;
  const gqlMutationDeleteItemName = 'deleteLecturer';
  const resulMutatiomtMessageSuccses = 'Направление удалено';

  return (
    <div>
      <ListMaterials
        {...props}
        actionsButtons={lecturersActions}
        columnData={lecturersColumnData}
        fields={lecturersFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showTitle={props.showTitle}
        showActions={props.showActions === undefined ? true : props.showActions}
        filterModifier={lecturersFilterModifier}
      />
    </div>
  );
};

export default lecturers;
