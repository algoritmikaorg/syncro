import {
  CREATE_LECTURER_MUTATION,
  ALL_LECTURERS_QUERY,
  EDIT_LECTURER_MUTATION,
  LECTURER_QUERY,
  DELETE_LECTURER_MUTATION,
  LECTURER_QUERY_WITH_RELATIONS,
  ALL_LECTURERS_QUERY_SHORT,
} from './../../../constants';

import createLecturerMutation from './createLecturerMutation';
import allLecturersQuery from './allLecturersQuery';
import editLecturerMutation from './editLecturerMutation';
import lecturerQuery from './lecturerQuery';
import deleteLectureMutation from './deleteLectureMutation';
import lecturerQueryWithRelations from './lecturerQueryWithRelations';
import allLecturersQueryShort from './allLecturersQueryShort';

const gql = {
  [CREATE_LECTURER_MUTATION]: createLecturerMutation,
  [ALL_LECTURERS_QUERY]: allLecturersQuery,
  [EDIT_LECTURER_MUTATION]: editLecturerMutation,
  [LECTURER_QUERY]: lecturerQuery,

  [DELETE_LECTURER_MUTATION]: deleteLectureMutation,
  [LECTURER_QUERY_WITH_RELATIONS]: lecturerQueryWithRelations,
  [ALL_LECTURERS_QUERY_SHORT]: allLecturersQueryShort,
};

export default gql;
