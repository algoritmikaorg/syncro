import React from 'react';

import gql from './gql';

import {
  LECTURE_QUERY_WITH_RELATIONS,
  LECTURE_FOR_COURSE_QUERY_WITH_RELATIONS,
  LECTURE_FOR_CYCLE_QUERY_WITH_RELATIONS,
  COURSE_QUERY_WITH_RELATIONS,
  CYCLE_QUERY_WITH_RELATIONS,
  CERTIFICATE_PRODUCT_QUERY_WITH_RELATIONS,
  SUBSCRIPTION_PRODUCT_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import {
  lecture as lectureFields,
  lectureForCourse as lectureForCourseFields,
  lectureForCycle as lectureForCycleFields,
  course as courseFields,
  cycle as cycleFields,
  subscription as subscriptionFields,
  certificate as certificateFields,
} from './fields';

const LecturerViewWithRelations = (props) => {
  const {
    typeMaterial,
    lectureType,
  } = props;

  let gqlQueryWithRelationsName = LECTURE_QUERY_WITH_RELATIONS;
  let gqlQueryWithRelationsNameItemName = 'Lecture';
  let currentFileds = lectureFields;

  switch (typeMaterial) {
    case 'lecture': {
      gqlQueryWithRelationsName = LECTURE_FOR_COURSE_QUERY_WITH_RELATIONS;
      gqlQueryWithRelationsNameItemName = 'Lecture';
      currentFileds = lectureFields;
      switch (lectureType) {
        case 'lectureForCourse': {
          gqlQueryWithRelationsName = LECTURE_FOR_CYCLE_QUERY_WITH_RELATIONS;
          currentFileds = lectureForCourseFields;
          break;
        }
        case 'lectureForCycle': {
          currentFileds = lectureForCycleFields;
          break;
        }
        default: {
          currentFileds = lectureFields;
        }
      }
      break;
    }
    case 'course': {
      gqlQueryWithRelationsName = COURSE_QUERY_WITH_RELATIONS;
      gqlQueryWithRelationsNameItemName = 'Course';
      currentFileds = courseFields;
      break;
    }
    case 'cycle': {
      gqlQueryWithRelationsName = CYCLE_QUERY_WITH_RELATIONS;
      gqlQueryWithRelationsNameItemName = 'Cycle';
      currentFileds = cycleFields;
      break;
    }
    case 'certificate': {
      gqlQueryWithRelationsName = CERTIFICATE_PRODUCT_QUERY_WITH_RELATIONS;
      gqlQueryWithRelationsNameItemName = 'Certificate';
      currentFileds = certificateFields;
      break;
    }
    case 'subscription': {
      gqlQueryWithRelationsName = SUBSCRIPTION_PRODUCT_QUERY_WITH_RELATIONS;
      gqlQueryWithRelationsNameItemName = 'Subscription';
      currentFileds = subscriptionFields;
      break;
    }
    default: {
    }
  }

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={currentFileds}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
        // showActions={false}
      />
    </div>
  );
};

export default LecturerViewWithRelations;
