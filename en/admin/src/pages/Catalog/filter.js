import * as R from 'ramda';

// const appendProperty = (input, property) => {
//   const filter = R.clone(input);
//   if (!Object.prototype.hasOwnProperty.call(filter, property)) {
//     filter[property] = {};
//   }
//   return filter;
// };

const appendProperty = (input, property) => {
  const output = Object.assign({}, input);
  if (!Object.prototype.hasOwnProperty.call(output, property)) {
    output[property] = {};
  }
  return output;
};

const changeQueryFilterLecture = (input = {}, type) => {
  const filter = { events_every: {} };
  if (input) {
    const {
      date_gte,
      date_lte,
      lecturersIds,
      locationIds,
      tagsIds,
      title,
    } = input;
    if (date_gte) filter.events_every.date_gte = date_gte || new Date();
    if (date_lte) filter.events_every.date_lte = date_lte || new Date();
    if (title) filter.title_contains = title;
    if (lecturersIds && lecturersIds.length > 0) filter.events_every.lecturers_every = { id_in: lecturersIds };
    if (locationIds && locationIds.length > 0) filter.events_every.location = { id_in: locationIds };
    if (tagsIds && tagsIds.length > 0) filter.tags_every = { id_in: tagsIds };
  }
  switch (type) {
    case 'active': {
      if (!input.date_gte) {
        filter.events_some = {
          date_gte: new Date(),
        }
      }
      filter.public = true;
      break;
    }
    case 'not_public': {
      filter.public = false;
      break;
    }
    case 'archive': {
      filter.events_every.date_lt = input ? (input.date_lte || new Date()) : new Date();
      break;
    }
    default: {}
  }
  return filter;
};

const changeQueryFilterCourse = (input = {}, type) => {
  // const filter = appendProperty(input, 'lectures_every');
  // filter.lectures_every = appendProperty(filter.lectures_every, 'events_every');

  const filter = {
    lectures_every: {
      events_every: {

      }
    }
  };

  if (input) {
    const {
      date_gte,
      date_lte,
      lecturersIds,
      locationIds,
      tagsIds,
      title,
    } = input;
    if (date_gte) filter.lectures_every.events_every.date_gte = date_gte || new Date();
    if (date_lte) filter.lectures_every.events_every.date_lte = date_lte || new Date();
    if (title) filter.title_contains = title;
    if (lecturersIds && lecturersIds.length > 0) filter.lectures_every.events_every.lecturers_every = { id_in: lecturersIds };
    if (locationIds && locationIds.length > 0) filter.lectures_every.events_every.location = { id_in: locationIds };
    if (tagsIds && tagsIds.length > 0) filter.tags_every = { id_in: tagsIds };
  }

  switch (type) {
    case 'active': {
      if (!input.date_gte) {
        filter.lectures_every.events_some = {
          date_gte: new Date(),
        };
      }
      filter.public = true;
      return filter;
    }
    case 'not_public': {
      filter.public = false;
      return filter;
    }
    case 'archive': {
      filter.lectures_every.events_every.date_lt = input ? (input.date_lte || new Date()) : new Date();
      return filter;
    }
    default: {
      return filter;
    }
  }
};

const changeQueryFilterCycle = (input = {}, type) => changeQueryFilterCourse(input, type);

const changeQueryFilter = (type) => {
  switch (type) {
    case 'lecture': {
      return changeQueryFilterLecture;
    }
    case 'subscription': {
      // return changeQueryFilterSubscription;
      return input => input || {};
    }
    case 'certificate': {
      // return changeQueryFilterCertificate;
      return input => input || {};
    }
    // case 'lectureForCourse': {
    //   return changeQueryFilterLecture;
    // }
    // case 'lectureForCycles': {
    //   return changeQueryFilterLecture;
    // }
    // case 'course': {
    //
    // }
    // case 'cycle': {
    //
    // }
    default: {
      return changeQueryFilterCourse;
    }
  }
};

export default changeQueryFilter;
