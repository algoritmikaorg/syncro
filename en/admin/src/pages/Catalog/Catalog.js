import React from 'react';

import gql from './gql';

import {
  LECTURE_QUERY,
  COURSE_QUERY,
  CYCLE_QUERY,

  SUBSCRIPTION_PRODUCT_QUERY,
  CERTIFICATE_PRODUCT_QUERY,

  ALL_LECTURES_QUERY,
  ALL_COURSE_QUERY,
  ALL_CYCLES_QUERY,

  ALL_SUBSCRIPTION_PRODUCTS_QUERY,
  ALL_CERTIFICATES_PRODUCT_QUERY,

  EDIT_LECTURE_MUTATION,
  EDIT_COURSE_MUTATION,
  EDIT_CYCLE_MUTATION,

  EDIT_SUBSCRIPTION_PRODUCT_MUTATION,
  EDIT_CERTIFICATE_PRODUCT_MUTATION,

  CREATE_LECTURE_MUTATION,
  CREATE_COURSE_MUTATION,
  CREATE_CYCLE_MUTATION,

  CREATE_SUBSCRIPTION_PRODUCT_MUTATION,
  CREATE_CERTIFICATE_PRODUCT_MUTATION,

  DELETE_LECTURE_MUTATION,
  DELETE_COURSE_MUTATION,
  DELETE_CYCLE_MUTATION,

  DELETE_CERTIFICATE_PRODUCT_MUTATION,
  DELETE_SUBSCRIPTION_PRODUCT_MUTATION,
} from './../../constants';

import { format as formatUtil } from './../../utils';

import ListMaterials from './../../components/ListMaterials';
import replaceMutationAndViewLecturelLink from './replaceMutationAndViewLecturelLink';

import {
  lecture as lectureFields,
  lectureForCourse as lectureForCourseFields,
  lectureForCycle as lectureForCycleFields,
  course as courseFields,
  cycle as cycleFields,
  certificate as certificateFields,
  subscription as subscriptionFields,
} from './fields';

import {
  lectures as lecturesActions,
  courses as coursesActions,
  cycles as cyclesActions,
  subscriptions as subscriptionsActions,
  certificates as certificatesActions,
} from './actions';

import {
  lectures as lecturesColumns,
  lectureForCourse as lectureForCourseColumns,
  lectureForCycle as lectureForCycleColumns,
  courses as coursesColumns,
  cycles as cyclesColumns,
  subscriptions as subscriptionsColumns,
  certificates as certificatesColumns,
} from './columnData';

import changeQueryFilter from './filter';

import {
  lectures as tabsLectures,
  courses as tabsCourses,
  cycles as tabsCycles,
  subscriptions as tabsSubscriptions,
  certificates as tabsCertificates,
} from './tabs';

const filterModifierForLecture = changeQueryFilter('lecture');
const filterModifierForCourse = changeQueryFilter('course');
const filterModifierForCycle = changeQueryFilter('cycle');
const filterModifierForSubscription = changeQueryFilter('subscription');
const filterModifierForCertificate = changeQueryFilter('certificate');

const Catalog = (props) => {
  const {
    typeMaterial,
  } = props;

  let actions = null;
  let columns = null;
  let gqlAllQueryName = null;
  let gqlAllQueryItemsName = '';
  let gqlMutationDelete = null;
  let gqlMutationDeleteItemName = '';
  let filterModifier = null;
  let fields = null;
  let tabs = null;
  let replaceMutationAndViewMaterialLink = null;
  let formatInputData = null;

  switch (typeMaterial) {
    case 'lecture': {
      actions = lecturesActions;
      columns = lecturesColumns;
      gqlAllQueryName = ALL_LECTURES_QUERY;
      gqlAllQueryItemsName = 'allLectures';
      gqlMutationDelete = DELETE_LECTURE_MUTATION;
      gqlMutationDeleteItemName = 'deleteLecture';
      filterModifier = filterModifierForLecture;
      fields = lectureFields;
      tabs = tabsLectures;
      formatInputData = formatUtil.lectureListInputData;
      replaceMutationAndViewMaterialLink = replaceMutationAndViewLecturelLink;
      break;
    }
    case 'course': {
      actions = coursesActions;
      columns = coursesColumns;
      gqlAllQueryName = ALL_COURSE_QUERY;
      gqlAllQueryItemsName = 'allCourses';
      gqlMutationDelete = DELETE_COURSE_MUTATION;
      gqlMutationDeleteItemName = 'deleteCourse';
      filterModifier = filterModifierForCourse;
      fields = courseFields;
      tabs = tabsCourses;
      formatInputData = formatUtil.courseListInputData;
      break;
    }
    case 'cycle': {
      actions = cyclesActions;
      columns = cyclesColumns;
      gqlAllQueryName = ALL_CYCLES_QUERY;
      gqlAllQueryItemsName = 'allCycles';
      gqlMutationDelete = DELETE_CYCLE_MUTATION;
      gqlMutationDeleteItemName = 'deleteCycle';
      filterModifier = filterModifierForCycle;
      fields = cycleFields;
      formatInputData = formatUtil.cycleListInputData;
      tabs = tabsCycles;
      break;
    }
    case 'subscription': {
      actions = subscriptionsActions;
      columns = subscriptionsColumns;
      gqlAllQueryName = ALL_SUBSCRIPTION_PRODUCTS_QUERY;
      gqlAllQueryItemsName = 'allSubscriptionProducts';
      gqlMutationDelete = DELETE_SUBSCRIPTION_PRODUCT_MUTATION;
      gqlMutationDeleteItemName = 'deleteSubscriptionProduct';
      filterModifier = filterModifierForSubscription;
      fields = subscriptionFields;
      tabs = tabsSubscriptions;
      break;
    }
    case 'certificate': {
      actions = certificatesActions;
      columns = certificatesColumns;
      gqlAllQueryName = ALL_CERTIFICATES_PRODUCT_QUERY;
      gqlAllQueryItemsName = 'allCertificateProducts';
      gqlMutationDelete = DELETE_CERTIFICATE_PRODUCT_MUTATION;
      gqlMutationDeleteItemName = 'deleteCertificateProduct';
      filterModifier = filterModifierForCertificate;
      fields = certificateFields;
      tabs = tabsCertificates;
      break;
    }
    default: {
      actions = lecturesActions;
      columns = lecturesColumns;
      gqlAllQueryName = ALL_LECTURES_QUERY;
      gqlAllQueryItemsName = 'allLectures';
      gqlMutationDelete = DELETE_LECTURE_MUTATION;
      gqlMutationDeleteItemName = 'deleteLecture';
      filterModifier = filterModifierForLecture;
      fields = lectureFields;
      tabs = tabsLectures;
    }
  }

  return (
    <div>
      <ListMaterials
        {...props}
        formatInputData={formatInputData}
        actionsButtons={actions}
        columnData={columns}
        tabs={tabs}
        fields={fields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        filterModifier={filterModifier}
        showTitle={props.showTitle}
        showActions={props.showActions === undefined ? true : props.showActions}
        replaceMutationAndViewMaterialLink={replaceMutationAndViewMaterialLink}
      />
    </div>
  );
};

export default Catalog;
