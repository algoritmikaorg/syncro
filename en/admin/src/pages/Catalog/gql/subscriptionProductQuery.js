import gql from 'graphql-tag';

export default gql`
  query subscriptionProductQuery($id: ID!) {
    SubscriptionProduct(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lectures,
      months,
      price,
      prefix,
      itemType,
      image,
      type,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
      subscriptions {
        id,
        title,
        start,
        end,
        user {
          id,

          firstName,
          lastName,
        }
      }
      _subscriptionsMeta {
        count
      }
      _discountsMeta {
        count
      }
      #
    }
  }
`;
