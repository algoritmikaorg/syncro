import gql from 'graphql-tag';

export default gql`
  mutation createSubscriptionProductMutation(
    $lectures: Int!,
    $months: Int!,
    $price: Float!,
    $title: String!,
    $itemType: String!
    $image: String!
    $type: SubProductTypes,
    $discountsIds: [ID!],
  ){
    createSubscriptionProduct(
      lectures: $lectures,
      months: $months,
      price: $price,
      title: $title,
      itemType: $itemType,
      image: $image,
      type: $type,
      discountsIds: $discountsIds,
    ) {
      id
    }
  }
`;
