import gql from 'graphql-tag';

export default gql `
  mutation UpdateSubscriptionProductMutation(
      $id: ID!,
      $lectures: Int!,
      $months: Int!,
      $price: Float!,
      $title: String!,
      $prefix: String!,
      $itemType: String!,
      $image: String!,
      $type: SubProductTypes,
      $discountsIds: [ID!],
    ){
    updateSubscriptionProduct(
      id: $id,

      lectures: $lectures,
      months: $months,
      price: $price,
      prefix: $prefix,
      title: $title,
      type: $type,
      discountsIds: $discountsIds,
      itemType: $itemType,
      image: $image,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lectures,
      months,
      price,
      type,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
      subscriptions {
        id,
        title,
        start,
        end,
        user {
          id,

          firstName,
          lastName,
        }
      }
      _subscriptionsMeta {
        count
      }
      _discountsMeta {
        count
      }
      #
    }
  }
`;
