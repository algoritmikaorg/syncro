import gql from 'graphql-tag';

export default gql`
  query AllLecturesQuery($filter: LectureFilter, $first: Int, $skip: Int, $orderBy: LectureOrderBy) {
    allLectures(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,

      lectureType,
      title,
      subTitle,
      titleForCourse,
      titleForCycle,
      anons,
      anonsForCourse,
      anonsForCycle,
      public,
      description,
      themes,
      duration,
      img1240x349,
      img340x192,
      img340x340,
      metaDescription,
      metaKeywords,
      #
      author {
        id,

        firstName,
        lastName,
        email,
        phone,
        role,
      },
      #
      alias {
        id,
        alias,
      },
      #
      course {
        id,
        title,
      },
      #
      cycle {
        id,
        title,
      },
      #
      events {
        id,
        date,
        price,
        lecturers {
          id,
          firstName,
          lastName,
        },
        location {
          id,
          title,
          address,
          metro,
        },
      },
      #
      reviews {
        id,
        user {
          id,
          firstName,
          lastName,
        },
      },
      #
      tags {
        id,
        title,
        color,
        textColor,
      },
      discounts {
        id,
        title,
      }
      #
      metaTags {
        id,
        metaKeywords,
        metaDescription,
        title,
      },
      #
      recommendedCourses {
        id,
        title,
      },
      #
      recommendedCycles {
        id,
        title,
      },
      #
      recommendedLectures {
        id,
        title,
      },
      #
      inRecommendationsOfCourses {
        id,
        title,
      },
      #
      inRecommendationsOfCycles {
        id,
        title,
      },
      #
      inRecommendationsOfLectures {
        id,
        title,
      },
      #
    },
    _allLecturesMeta {
      count,
    },
  }
`;
