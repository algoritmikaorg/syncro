import { format } from './../../../utils';

const createLecture = [
  {
    id: 'author',
    label: 'Автор',
    type: 'author',
    disabled: true,
    hidden: true,
    format: format.authorToSting,
  },
  // {
  //   id: 'authorId',
  //   label: 'Автор',
  //   type: 'text',
  //   disabled: true,
  //   hidden: true,
  // },
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'text',
    disabled: true,
    required: true,
    value: 'SINGLE_LECTURE',
  },
  {
    id: 'title',
    label: 'Заголовок',
    required: true,
    type: 'multiline',
  },
  {
    id: 'subTitle',
    label: 'Подзаголовок',
    type: 'multiline',
  },
  {
    id: 'anons',
    label: 'Анонс',
    type: 'multiline',
  },
  {
    id: 'description',
    label: 'Описание',
    type: 'multiline',
  },
  {
    id: 'themes',
    label: 'Темы',
    type: 'array',
    helperText: 'Каждая тема с новой строки',
  },
  {
    id: 'duration',
    label: 'Продолжительность',
    required: true,
    type: 'number',
  },
  {
    id: 'events',
    label: 'Даты',
    relation: 'events',
    type: 'relation',
    required: true,
    disabled: true,
    format: format.eventsToDateArray,
  },
  {
    id: 'metaDescription',
    label: 'Meta - Description',
    type: 'multiline',
  },
  {
    id: 'metaKeywords',
    label: 'Meta - Keywords',
    type: 'multiline',
  },
  //
  {
    id: 'img1240x349',
    label: 'Картинка 1240х349',
    value: 'no-photo',
    type: 'image',
  },
  {
    id: 'img340x192',
    label: 'Картинка 340х192',
    value: 'no-photo',
    type: 'image',
  },
  {
    id: 'img340x340',
    label: 'Картинка 340x340',
    value: 'no-photo',
    type: 'image',
  },
  {
    id: 'tagsIds',
    label: 'Направления',
    relation: 'tags',
    type: 'relation',
    required: true,
    disabled: true,
  },
  {
    id: 'alias',
    label: 'URL alias',
    type: 'alias',
    required: true,
  },
  {
    id: 'discountsIds',
    label: 'Скидки',
    relation: 'discounts-select',
    type: 'relation',
    disabled: true,
  },
  // {
  //   id: 'recommendedCoursesIds',
  //   label: 'Рекомендованные курсы',
  //   relation: 'recommendedCourses',
  //   type: 'relation',
  //   disabled: true,
  // },
  // {
  //   id: 'recommendedCyclesIds',
  //   label: 'Рекомендованные циклы',
  //   relation: 'recommendedCycles',
  //   type: 'relation',
  //   disabled: true,
  // },
  {
    id: 'recommendedLecturesIds',
    label: 'Рекомендованные лекции',
    relation: 'recommendedLectures',
    type: 'relation',
    disabled: true,
  },
  {
    id: 'public',
    label: 'Опубликовать',
    type: 'checkbox',
    value: false,
  },
];

const createLectureForCourse = createLecture.slice();
createLectureForCourse.splice(1, 1,
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'text',
    disabled: true,
    required: true,
    value: 'LECTURE_FOR_COURSE',
  },
);
createLectureForCourse.splice(5, 0,
  {
    id: 'titleForCourse',
    label: 'Заголовок для Курса',
    type: 'multiline',
  },
  {
    id: 'anonsForCourse',
    label: 'Анонс для Курса',
    type: 'multiline',
  },
  {
    id: 'courseIds',
    label: 'Курс',
    type: 'relation',
    relation: 'courses',
    disabled: true,
    required: true,
    halperText: 'Выберите курс',
  },
);

const createLectureForCycle = createLecture.slice();
createLectureForCycle.splice(1, 1,
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'text',
    disabled: true,
    required: true,
    value: 'LECTURE_FOR_CYCLE',
  },
);
createLectureForCycle.splice(5, 0,
  {
    id: 'titleForCycle',
    label: 'Заголовок для Цикла',
    type: 'multiline',
  },
  {
    id: 'anonsForCycle',
    label: 'Анонс для Цикла',
    type: 'multiline',
  },
  {
    id: 'cycleIds',
    label: 'Цикл',
    type: 'relation',
    relation: 'cycles',
    halperText: 'Выберите цикл',
    disabled: true,
    required: true,
  },
);

export default {};
export { createLecture };
export { createLectureForCourse };
export { createLectureForCycle };
