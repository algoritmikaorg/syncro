import { fields as fieldsUtils } from './../../../utils';
import {
  createLecture,
  createLectureForCourse,
  createLectureForCycle,
} from './createLecture';

const idField = {
  id: 'id',
  label: 'id',
  type: 'multiline',
  required: true,
  disabled: true,
};

const updateLecture = fieldsUtils.cloneAndAddField(createLecture, idField);

const updateLectureForCourse = fieldsUtils.cloneAndAddField(createLectureForCourse, idField);

const updateLectureForCycle = fieldsUtils.cloneAndAddField(createLectureForCycle, idField);

export default {};
export { updateLecture }
export { updateLectureForCourse };
export { updateLectureForCycle };
