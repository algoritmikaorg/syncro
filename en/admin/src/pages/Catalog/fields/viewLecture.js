import {
  updateLecture,
  updateLectureForCourse,
  updateLectureForCycle,
} from './updateLecture';

const viewLecture = updateLecture.slice();

const viewLectureForCourse = updateLectureForCourse.slice();

const viewLectureForCycle = updateLectureForCycle.slice();

export default {};
export { viewLecture };
export { viewLectureForCourse };
export { viewLectureForCycle };
