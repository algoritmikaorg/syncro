import { fields as fieldsUtils } from './../../../utils';

import createCycle from './createCycle';

const idField = {
  id: 'id',
  label: 'id',
  type: 'multiline',
  required: true,
  disabled: true,
};

const updateCycle = fieldsUtils.cloneAndAddField(createCycle, idField);

export default updateCycle;
