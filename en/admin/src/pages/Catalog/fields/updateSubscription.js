import { fields as fieldsUtils } from './../../../utils';

import createSubscription from './createCertificate';

const idField = {
  id: 'id',
  label: 'id',
  type: 'multiline',
  required: true,
  disabled: true,
};

const updateSubscription = fieldsUtils.cloneAndAddField(createSubscription, idField);

export default updateSubscription;
