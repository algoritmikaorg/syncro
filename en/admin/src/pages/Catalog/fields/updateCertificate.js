import { fields as fieldsUtils } from './../../../utils';

import createCertificate from './createCertificate';

const idField = {
  id: 'id',
  label: 'id',
  type: 'multiline',
  required: true,
  disabled: true,
};

const updateCertificate = fieldsUtils.cloneAndAddField(createCertificate, idField);

export default updateCertificate;
