import {
  SUB_PRODUCT_TYPES,
} from './../../../enum';
import { format } from './../../../utils';

const createSubscription = [
  {
    id: 'author',
    label: 'Автор',
    type: 'author',
    disabled: true,
    hidden: true,
    format: format.authorToSting,
  },
  {
    id: 'type',
    label: 'Тип',
    type: 'enum',
    enum: SUB_PRODUCT_TYPES,
    value: 'SUBSCRIPTION',
    required: true,
    disabled: true,
  },
  {
    id: 'title',
    label: 'Название',
    required: true,
    type: 'multiline',
  },
  {
    id: 'itemType',
    required: true,
    label: 'Тип абонемента',
    type: 'multiline',
  },
  {
    id: 'lectures',
    required: true,
    label: 'Количество лекций',
    type: 'number',
  },
  {
    id: 'months',
    required: true,
    label: 'Срок действия(месяцев)',
    type: 'number',
  },
  {
    id: 'price',
    required: true,
    label: 'Цена',
    type: 'number',
  },
  {
    id: 'prefix',
    required: true,
    label: 'Префикс',
    type: 'multiline',
  },
  {
    id: 'discountsIds',
    label: 'Скидки',
    relation: 'discounts-select',
    type: 'relation',
    disabled: true,
  },
  {
    id: 'image',
    label: 'Картинка 610x338',
    value: 'no-photo',
    type: 'image',
  },
];

export default createSubscription;
