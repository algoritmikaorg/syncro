import {
  SUB_PRODUCT_TYPES,
} from './../../../enum';
import { format } from './../../../utils';

const createCertificate = [
  {
    id: 'author',
    label: 'Автор',
    type: 'author',
    disabled: true,
    hidden: true,
    format: format.authorToSting,
  },
  {
    id: 'type',
    label: 'Тип',
    type: 'enum',
    enum: SUB_PRODUCT_TYPES,
    value: 'CERTIFICATE',
    required: true,
    disabled: true,
  },
  {
    id: 'title',
    label: 'Название',
    required: true,
    type: 'multiline',
  },
  {
    id: 'lectures',
    required: true,
    label: 'Количество лекций',
    type: 'number',
  },
  {
    id: 'months',
    required: true,
    label: 'Срок действия(месяцев)',
    type: 'number',
  },
  {
    id: 'price',
    required: true,
    label: 'Цена',
    type: 'number',
  },
  {
    id: 'prefix',
    required: true,
    label: 'Префикс',
    type: 'multiline',
  },
  {
    id: 'discountsIds',
    label: 'Скидки',
    relation: 'discounts-select',
    type: 'relation',
    disabled: true,
  },
];

export default createCertificate;
