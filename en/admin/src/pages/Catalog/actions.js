const lectures = [
  {
    title: 'Создать одиночную лекцию',
    type: 'create',
    to: 'lectures/create',
  },
  {
    title: 'Создать лекцию для курса',
    type: 'create',
    to: 'lectures/create-lecture-for-course',
  },
  {
    title: 'Создать лекцию для цикла',
    type: 'create',
    to: 'lectures/create-lecture-for-cycle',
  },
];

const courses = [
  {
    title: 'Создать курс',
    type: 'create',
    to: 'courses/create',
  },
];

const cycles = [
  {
    title: 'Создать цикл',
    type: 'create',
    to: 'cycles/create',
  },
];

const subscriptions = [
  {
    title: 'Создать новый тип абонемента',
    type: 'create',
    to: 'subscriptions/create',
  },
];

const certificates = [
  {
    title: 'Создать новый тип сертификата',
    type: 'create',
    to: 'certificates/create',
  },
];

export default {};
export { lectures };
export { courses };
export { cycles };
export { subscriptions };
export { certificates };
