export default ({ lectureType }, link) => {
  switch (lectureType) {
    case 'SINGLE_LECTURE': {
      return link.replace(/lectureType/g, 'lectures');
    }
    case 'LECTURE_FOR_COURSE': {
      return link.replace(/lectureType/g, 'lectures-for-course');
    }
    case 'LECTURE_FOR_CYCLE': {
      return link.replace(/lectureType/g, 'lectures-for-cycle');
    }
    default: {
      return link;
    }
  }
};
