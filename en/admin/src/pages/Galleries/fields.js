import * as R from 'ramda';
import { format } from './../../utils';

const create = [
  {
    id: 'title',
    label: 'Название',
    required: true,
  },
  {
    id: 'type',
    label: 'Тип',
    required: true,
  },
];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const view = update.slice();
view.push(
  {
    id: 'images',
    label: 'Изображения',
    disabled: true,
    relation: 'images',
    type: 'relation',
  },
);

const filter = [];

export default {
  create,
  update,
  filter,
  view,
};
