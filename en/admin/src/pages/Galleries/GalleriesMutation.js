import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_GALLERY_MUTATION,
  EDIT_GALLERY_MUTATION,
  GALLERY_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import tagsFields from './fields';
import tagsActions from './actions';


const GalleriesMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_GALLERY_MUTATION;
    fields = tagsFields.create;
    mutationResultObjectName = 'createGallery';
    resulMutatiomtMessageSuccses = 'Галерея создана';
  } else {
    gqlMutationName = EDIT_GALLERY_MUTATION;
    fields = tagsFields.update;
    mutationResultObjectName = 'updateGallery';
    resulMutatiomtMessageSuccses = 'Галерея обновлена';
    gqlQueryName = GALLERY_QUERY;
    gqlQueryItemName = 'Gallery';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
        // showActions={false}
      />
    </div>
  );
};

export default GalleriesMutation;
