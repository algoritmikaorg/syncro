import gql from 'graphql-tag';

export default gql`
  mutation deleteGalleryMutation(
      $id: ID!
    ){
    deleteGallery(
      id: $id,
    ) {
      id
    }
  }
`;
