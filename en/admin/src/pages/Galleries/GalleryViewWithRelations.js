import React from 'react';

import gql from './gql';

import {
  GALLERY_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import fields from './fields';

const GalleryViewWithRelations = (props) => {

  const gqlQueryWithRelationsName = GALLERY_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Gallery';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={fields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
        // showActions={false}
      />
    </div>
  );
};

export default GalleryViewWithRelations;
