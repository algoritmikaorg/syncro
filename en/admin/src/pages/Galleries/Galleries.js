import React from 'react';

import gql from './gql';

import {
  ALL_GALLERIES_QUERY,
  DELETE_GALLERY_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import galleriesFields from './fields';
import galleriesActions from './actions';
import galleriesColumnData from './columnData';

const galleries = (props) => {

  const gqlAllQueryName = ALL_GALLERIES_QUERY;
  const gqlAllQueryItemsName = 'allGalleries';
  const gqlMutationDelete = DELETE_GALLERY_MUTATION;
  const gqlMutationDeleteItemName = 'deleteGallery';
  const resulMutatiomtMessageSuccses = 'Галерея удалено';

  return (
    <div>
      <ListMaterials
        {...props}
        actionsButtons={galleriesActions}
        columnData={galleriesColumnData}
        fields={galleriesFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showTitle={props.showTitle}
        showActions={props.showActions === undefined ? true : props.showActions}
      />
    </div>
  );
};

export default galleries;
