import React from 'react';

import gql from './gql';

import {
  TAG_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import tagsFields from './fields';

const TagViewWithRelations = (props) => {
  const gqlQueryWithRelationsName = TAG_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Tag';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={tagsFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
      />
    </div>
  );
};

export default TagViewWithRelations;
