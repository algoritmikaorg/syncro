import * as R from 'ramda';

import { format } from './../../utils';

const create = [
  {
    id: 'demo',
    label: '',
    disabled: true,
    type: 'demo-tags'
  },
  {
    id: 'title',
    label: 'Название',
    required: true,
    type: 'multiline',
  },
  {
    id: 'color',
    label: 'Цвет фона',
    required: true,
    type: 'colorPicker',
  },
  {
    id: 'textColor',
    label: 'Цвет текста',
    required: true,
    type: 'colorPicker',
  },
  {
    id: 'alias',
    label: 'URL alias',
    required: true,
    type: 'alias',
    // format: format.aliasToString,
  },
];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
  // {
  //   id: 'alias_id',
  //   label: 'alias id',
  //   required: true,
  //   disabled: true,
  //   type: 'alias',
  // },
);

const view = update.slice();
view.push(
  {
    id: 'lecturers',
    label: 'Лекторы связанные с направлением',
    disabled: true,
    relation: 'lecturers',
    type: 'relation',
  },
  {
    id: 'lectures',
    label: 'Лекции связанные с направлением',
    disabled: true,
    relation: 'lectures',
    type: 'relation',
  },
  {
    id: 'courses',
    label: 'Курсы связанные с направлением',
    disabled: true,
    relation: 'courses',
    type: 'relation',
  },
  {
    id: 'cycles',
    label: 'Циклы связанные с направлением',
    disabled: true,
    relation: 'cycles',
    type: 'relation',
  },
);

const filter = [
  {
    id: 'title_contains',
    value: '',
    label: 'Название',
    type: 'text',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
