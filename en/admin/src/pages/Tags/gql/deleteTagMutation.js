import gql from 'graphql-tag';

export default gql`
  mutation deleteTagMutation(
      $id: ID!
    ){
    deleteTag(
      id: $id,
    ) {
      id
    }
  }
`;
