import { format } from './../../utils';

const columnData = [
  {
    id: 'title',
    numeric: false,
    disablePadding: false,
    label: 'Название',
  },
  {
    id: 'color',
    label: 'Цвет фона',
  },
  {
    id: 'textColor',
    label: 'Цвет текста',
  },
  {
    id: 'alias',
    numeric: false,
    disablePadding: false,
    label: 'Алиас',
    format: format.aliasToString,
  },
];
export default columnData;
