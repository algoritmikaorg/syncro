import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_TAG_MUTATION,
  EDIT_TAG_MUTATION,
  TAG_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import tagsFields from './fields';
import tagsActions from './actions';


const TagsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_TAG_MUTATION;
    fields = tagsFields.create;
    mutationResultObjectName = 'createTag';
    resulMutatiomtMessageSuccses = 'Направление создано';
  } else {
    gqlMutationName = EDIT_TAG_MUTATION;
    fields = tagsFields.update;
    mutationResultObjectName = 'updateTag';
    resulMutatiomtMessageSuccses = 'Направление обновлено';
    gqlQueryName = TAG_QUERY;
    gqlQueryItemName = 'Tag';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default TagsMutation;
