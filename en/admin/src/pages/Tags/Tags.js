import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_TAGS_QUERY,
  DELETE_TAG_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import tagsFields from './fields';
import tagsActions from './actions';
import tagsColumnData from './columnData';

const Tags = (props) => {

  const gqlAllQueryName = ALL_TAGS_QUERY;
  const gqlAllQueryItemsName = 'allTags';
  const gqlMutationDelete = DELETE_TAG_MUTATION;
  const gqlMutationDeleteItemName = 'deleteTag';
  const resulMutatiomtMessageSuccses = 'Направление удалено';

  return (
    <div>
      <ListMaterials
        {...props}
        actionsButtons={tagsActions}
        columnData={tagsColumnData}
        fields={tagsFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showActions={props.showActions === undefined ? true : props.showActions}
        // filterModifier={filterModifier}
      />
    </div>
  );
};

export default Tags;
