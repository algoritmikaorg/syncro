import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Tags from './Tags';
import TagsMutation from './TagsMutation';
import TagViewWithRelations from './TagViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const TagsRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Tags
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <TagsMutation {...props} {...rest} mutationType="create" title={'Новое направление'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <TagsMutation {...props} {...rest} mutationType="update" title={'Редактирование направления'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <TagViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(TagsRoutes, { title: 'Направления' });
export default TagsRoutes;
