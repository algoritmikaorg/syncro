import { AUTHENTICATE_USER_MUTATION } from './../../../constants';
import authenticateUserMutation from './authenticateUserMutation';

const gql = {
  [AUTHENTICATE_USER_MUTATION]: authenticateUserMutation,
};

export default gql;
