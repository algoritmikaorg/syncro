import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import * as R from 'ramda';

import gql from './gql';
import { view } from './fields';
import FormBase from './../../components/FormBase';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';
import { PARTNER_RELATIONS_QUERY } from './../../constants';

const Form = withProgress([PARTNER_RELATIONS_QUERY])(FormBase);


class EditPartner extends Component {
  render() {
    const partner = this.props[PARTNER_RELATIONS_QUERY].Partner;
    let fields = null;
    if (partner) {
      fields = R.clone(view);
      Object.keys(fields).forEach((key) => {
        fields[key].value = partner[key] || 'Нет данных';
      });
    }

    return (
      <div>
        <Form
          formTitle={partner ? partner.title : ''}
          fields={fields || view}
          type={'edit'}
          disabledAll={false}
          showActions={{}}
          req={this.props[PARTNER_RELATIONS_QUERY].loading}
          {...this.props}
        />
      </div>
    );
  }
}

export default compose(
  graphql(gql[PARTNER_RELATIONS_QUERY], {
    name: PARTNER_RELATIONS_QUERY,
    options(ownProps) {
      const id = ownProps.match.params.id;
      return (
        {
          variables: {
            id,
          },
        }
      );
    },
  }),
)(EditPartner);
