import * as R from 'ramda';

export const create = {
  title: {
    value: '',
    label: 'Название',
    required: true,
  },
  link: {
    value: '',
    label: 'Ссылка',
    required: false,
  },
  image: {
    value: '',
    label: 'Изображение',
    required: false,
  },
};

export const view = {
  link: {
    value: '',
    label: 'Ссылка',
    disabled: true,
  },
  image: {
    value: '',
    label: 'Изображение',
    disabled: true,
  },
};

export const edit = (() => {
  const output = R.clone(create);
  output.id = {
    value: '',
    label: 'ID',
    required: true,
    disabled: true,
  };
  return output;
})();

export const filter = {
  title: {
    value: '',
    label: 'Название',
  },
};
