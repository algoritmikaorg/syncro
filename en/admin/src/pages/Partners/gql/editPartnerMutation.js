import gql from 'graphql-tag';

export default gql`
  mutation UpdatePartnerMutation(
      $id: ID!,
      $image: String
      $link: String
      $title: String!
    ){
    updatePartner(
      id: $id,
      image: $image,
      link: $link,
      title: $title,
    ) {
      id,
      createdAt,
      updatedAt,
      image,
      link,
      title,
    }
  }
`;
