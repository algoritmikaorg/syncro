import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import CreatePartner from './CreatePartner';
import EditPartner from './EditPartner';
import Partner from './Partner';
import List from './List';
import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const Partners = (props) => {
  const { match } = props;
  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${match.url}`}
          component={rest => (
            <List {...props} {...rest} title={''} />
          )}
        />
        <Route
          exact
          path={`${match.url}/view/:id`}
          component={rest => (
            <Partner {...props} {...rest} title={''} />
          )}
        />
        <Route
          exact
          path={`${match.url}/create`}
          component={rest => (
            <CreatePartner {...props} {...rest} />
          )}
        />
        <Route
          exact
          path={`${match.url}/edit/:id`}
          component={rest => (
            <EditPartner {...props} {...rest} />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(Partners, { title: 'Партнеры' });
export default Partners;
