import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import * as R from 'ramda';

import gql from './gql';
import { create } from './fields';
import FormBase from './../../components/FormBase';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';

import { CREATE_PARTNER_MUTATION } from './../../constants';

const Form = withProgress([CREATE_PARTNER_MUTATION])(FormBase);

const createPartnerMutationResult = `${CREATE_PARTNER_MUTATION}Result`;
const createPartnerMutationError = `${CREATE_PARTNER_MUTATION}Error`;
const createPartnerMutationLoading = `${CREATE_PARTNER_MUTATION}Loading`;
const createPartnerMutationReset = `${CREATE_PARTNER_MUTATION}Reset`;

class CreatePartner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: null,
      clearFields: [],
      req: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[createPartnerMutationResult]) {
      const id = nextProps[createPartnerMutationResult].data.createPartner.id
      this.props.snackbar({ message: `Партнер создан - id: ${id}`, name: 'Success' });
      this.props.reqSuccess(id);
    }
    if (nextProps[createPartnerMutationError]) {
      this.props.snackbar(nextProps[createPartnerMutationError]);
      this.props[createPartnerMutationReset]();
      this.setState({
        // clearFields: ['password', 'repassword'],
        req: false,
      }, () => {
        this.props.reqError(nextProps[createPartnerMutationError]);
      });
    }
  }
  confirm = async () => {
    const { fields } = this.state;
    this.props[CREATE_PARTNER_MUTATION]({
      variables: fields,
    });
  }
  handleSubmit(fields) {
    const state = {
      fields: R.clone(fields),
      req: true,
    };
    this.setState(state, () => {
      this.confirm();
    });
  }
  render() {
    const { clearFields, req } = this.state;

    return (
      <Form
        formTitle="Создать партнера"
        fields={create}
        disabledAll={false}
        disabledActions={
          {
            save: false,
          }
        }
        showActions={
          {
            save: true,
          }
        }
        onSubmit={this.handleSubmit}
        clearFields={clearFields}
        req={req}
        {...this.props}
      />
    );
  }
}

CreatePartner.defaultProps = {
  reqSuccess: () => {},
  reqError: () => {},
};

export default compose(
  graphql(gql[CREATE_PARTNER_MUTATION], { name: CREATE_PARTNER_MUTATION }),
  withStateMutation({ name: CREATE_PARTNER_MUTATION }),
  withSnackbar(),
)(CreatePartner);
