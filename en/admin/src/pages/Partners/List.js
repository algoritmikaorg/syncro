import React from 'react';
import PropTypes from 'prop-types';
import Divider from 'material-ui/Divider';
import AddIcon from 'material-ui-icons/Add';
import Button from 'material-ui/Button';
import { graphql, compose } from 'react-apollo';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';

import Actions from './../../components/Actions/';
import PartnerList from './PartnerList';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});

const actionItems = ({ url }) => [
  {
    title: 'Создать партнера',
    to: `${url}/create`,
  },
];

const List = (props) => {
  const { classes, match } = props;

  const actions = actionItems(match);
  return (
    <div>
      <Typography type="title" gutterBottom>
        {props.title}
      </Typography>
      <Actions
        items={actions}
      />
      <PartnerList {...props} />
    </div>
  );
};

List.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(List);
