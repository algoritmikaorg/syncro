import React, { Component } from 'react';
import { graphql, compose, withApollo } from 'react-apollo';

import { format } from './../../utils';

import ExtTable from './../../components/ExtTable';
import Filter from './../../components/Filter';
import DialogDeleteItem from './../../components/DialogDeleteItem';

import { filter } from './fields';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';
import gql from './gql';
import { ALL_PARTNERS_QUERY, ITEMS_PER_PAGE, DELETE_PARTNER_MUTATION } from './../../constants';

const Table = withProgress([ALL_PARTNERS_QUERY])(ExtTable);

const columnData = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Название' },
  { id: 'link', numeric: false, disablePadding: false, label: 'Ссылка' },
  { id: 'image', numeric: false, disablePadding: false, label: 'Изображение' },
];

const deletePartnerMutationResult = `${DELETE_PARTNER_MUTATION}Result`;
const deletePartnerMutationError = `${DELETE_PARTNER_MUTATION}Error`;
const deletePartnerMutationLoading = `${DELETE_PARTNER_MUTATION}Loading`;
const deletePartnerMutationReset = `${DELETE_PARTNER_MUTATION}Reset`;

class PartnerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemToDelete: null,
      req: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.handleView = this.handleView.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[deletePartnerMutationResult]) {
      const id = nextProps[deletePartnerMutationResult].data.deletePartner.id
      this.props.snackbar({ message: `Партнер удален - id: ${id}`, name: 'Success' });
      this.setState({
        req: false,
        itemToDelete: null,
      }, () => {
        this.props[ALL_PARTNERS_QUERY].refetch();
      });
    }
    if (nextProps[deletePartnerMutationError]) {
      this.props.snackbar(nextProps[deletePartnerMutationError]);
      this.props[deletePartnerMutationReset]();
      this.setState({
        req: false,
      });
    }
  }
  handleView(id) {
    this.props.history.push(`${this.props.match.url}/view/${id}`)
  }
  handleSubmit(fields) {
    this.props[ALL_PARTNERS_QUERY].refetch({ filter: fields });
    // this.setState({ links })
  }
  handleEdit(id) {
    const {
      match,
      history,
    } = this.props;
    history.push(`${match.url}/edit/${id}`);
  }
  handleDelete(n) {
    this.setState({
      itemToDelete: n,
    });
  }
  handleCancelDelete() {
    this.setState({
      itemToDelete: null,
    });
  }
  handleDeleteConfirm = async () => {
    const { itemToDelete } = this.state;
    this.setState({
      req: true,
    }, () => {
      this.props[DELETE_PARTNER_MUTATION]({
        variables: {
          id: itemToDelete.id,
        },
      });
    });
  }
  render() {
    const partnersToRender = this.props[ALL_PARTNERS_QUERY].allPartners;
    const { itemToDelete, req } = this.state;

    return (
      <div>
        <Filter
          fields={filter}
          onChange={(fields) => { }}
          onSubmit={this.handleSubmit}
        />
        <Table
          {...this.props}
          columnData={columnData}
          data={partnersToRender}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
          onView={this.handleView}
        />
        <DialogDeleteItem
          open={itemToDelete ? true : false}
          title={itemToDelete ? itemToDelete.title : ''}
          onCancel={this.handleCancelDelete}
          onConfirm={this.handleDeleteConfirm}
          disabled={req}
        />
      </div>
    )
  }
}

PartnerList.defaultProps = {
  [ALL_PARTNERS_QUERY]: {
    allPartners: [],
  },
};

export default compose(
  graphql(gql[ALL_PARTNERS_QUERY], {
    name: [ALL_PARTNERS_QUERY],
  }),
  graphql(gql[DELETE_PARTNER_MUTATION], { name: DELETE_PARTNER_MUTATION }),
  withStateMutation({ name: DELETE_PARTNER_MUTATION }),
  withSnackbar(),
  // withProgress([ALL_PARTNERS_QUERY]),
)(PartnerList);
