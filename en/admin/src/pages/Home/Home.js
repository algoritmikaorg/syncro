import React from 'react';
import {
  Redirect,
} from 'react-router-dom';

import { GC_USER_ID } from './../../constants';

const Index = (props) => {
  // const userId = localStorage.getItem(GC_USER_ID);
  // if (!userId) return <Redirect to="/login" />;
  return <Redirect to="/catalog" />;
};

export default Index;
