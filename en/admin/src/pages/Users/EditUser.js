import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import * as R from 'ramda';

import gql from './gql';
import { edit } from './fields';
import FormBase from './../../components/FormBase';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';
import { EDIT_USER_MUTATION, USER_QUERY } from './../../constants';

const Form = withProgress([EDIT_USER_MUTATION, USER_QUERY])(FormBase);

const editUserMutationResult = `${EDIT_USER_MUTATION}Result`;
const editUserMutationError = `${EDIT_USER_MUTATION}Error`;
const editUserMutationLoading = `${EDIT_USER_MUTATION}Loading`;
const editUserMutationReset = `${EDIT_USER_MUTATION}Reset`;

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: null,
      req: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[editUserMutationResult]) {
      const id = nextProps[editUserMutationResult].data.updateUser.id
      this.props.snackbar({ message: `Пользователь обновлен - id: ${id}`, name: 'Success' });
      this.setState({
        req: false,
      });
      this.props.reqSuccess(id);
    }
    if (nextProps[editUserMutationError]) {
      this.props.snackbar(nextProps[editUserMutationError]);
      this.props[editUserMutationReset]();
      this.setState({
        req: false,
      }, () => {
        this.props.reqError(nextProps[editUserMutationError]);
      });
    }
  }
  confirm = async () => {
    const { fields } = this.state;
    this.props[EDIT_USER_MUTATION]({
      variables: fields,
    });
  }
  handleSubmit(fields) {
    const state = {
      fields: R.clone(fields),
      req: true,
    };
    this.setState(state, () => {
      this.confirm();
    });
  }
  render() {
    const { req } = this.state;
    let fields = null;
    if (this.props[USER_QUERY].User) {
      fields = R.clone(edit);
      const user = this.props[USER_QUERY].User;

      Object.keys(fields).forEach((key) => {
        fields[key].value = user[key];
      });
    }

    return (
      <Form
        formTitle="Редактировать пользователя"
        fields={fields || edit}
        type={'edit'}
        disabledAll={false}
        disabledActions={
          {
            save: false,
          }
        }
        showActions={
          {
            save: true,
          }
        }
        onSubmit={this.handleSubmit}
        req={req || this.props[USER_QUERY].loading}
        {...this.props}
      />
    );
  }
}

EditUser.defaultProps = {
  reqSuccess: () => {},
  reqError: () => {},
};

export default compose(
  graphql(gql[USER_QUERY], {
    name: USER_QUERY,
    options(ownProps) {
      const id = ownProps.match.params.id;
      return (
        {
          variables: {
            id,
          },
        }
      );
    },
  }),
  graphql(gql[EDIT_USER_MUTATION], { name: EDIT_USER_MUTATION }),
  withStateMutation({ name: EDIT_USER_MUTATION }),
  withSnackbar(),
)(EditUser);
