import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import CreateUser from './CreateUser';
import EditUser from './EditUser';
import List from './List';
import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const Users = (props) => {
  const { match } = props;
  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${match.url}`}
          component={rest => (
            <List {...props} {...rest} userRole={'CLIENT'} title={''} />
          )}
        />
        <Route
          exact
          path={`${match.url}/create`}
          component={rest => (
            <CreateUser {...props} {...rest} />
          )}
        />
        <Route
          exact
          path={`${match.url}/edit/:id`}
          component={rest => (
            <EditUser {...props} {...rest} />
          )}
        />
        <Route
          exact
          path={`${match.url}/administrators`}
          component={rest => (
            <List  {...props} {...rest} userRole={'ADMIN'} title={'Администраторы'} />
          )}
        />
        <Route
          exact
          path={`${match.url}/administrators/create`}
          component={rest => (
            <CreateUser {...props} {...rest} userRole={'ADMIN'} />
          )}
        />
        <Route
          exact
          path={`${match.url}/administrators/edit/:id`}
          component={rest => (
            <EditUser {...props} {...rest} userRole={'ADMIN'} />
          )}
        />
        <Route
          exact
          path={`${match.url}/moderators`}
          component={rest => (
            <List userRole={'MODERATOR'} {...props} {...rest} />
          )}
        />
        <Route
          exact
          path={`${match.url}/moderators/create`}
          component={rest => (
            <CreateUser {...props} {...rest} userRole={'MODERATOR'} />
          )}
        />
        <Route
          exact
          path={`${match.url}/moderators/edit/:id`}
          component={rest => (
            <EditUser {...props} {...rest} userRole={'MODERATOR'} />
          )}
        />
        <Route
          exact
          path={`${match.url}/curators`}
          component={rest => (
            <List userRole={'CURATOR'} {...props} {...rest} />
          )}
        />
        <Route
          exact
          path={`${match.url}/curators/create`}
          component={rest => (
            <CreateUser {...props} {...rest} userRole={'CURATOR'} />
          )}
        />
        <Route
          exact
          path={`${match.url}/curators/edit/:id`}
          component={rest => (
            <EditUser {...props} {...rest} userRole={'CURATOR'} />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(Users, { title: "Пользователи" });
export default Users;
