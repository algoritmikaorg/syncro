export const create = {
  firstName: {
    value: '',
    label: 'Имя',
    required: true,
  },
  lastName: {
    value: '',
    label: 'Фамилия',
    required: true,
  },
  email: {
    value: '',
    label: 'E-mail',
    required: true,
  },
  phone: {
    value: '',
    label: 'Телефон',
    required: true,
  },
  password: {
    value: '',
    label: 'Новый пароль',
    required: true,
    helperText: 'Минимальная длина 8 символов',
  },
  repassword: {
    value: '',
    label: 'Повторите пароль',
    required: true,
  },
  role: {
    value: '',
    label: 'Роль',
    required: true,
    disabled: true,
  },
};

export const edit = (() => {
  const output = {};
  const keys = Object.keys(create);
  keys.forEach((key) => {
    if (key === 'password' || key === 'repassword') {
      output[key] = Object.assign({}, create[key], { required: false });
    } else {
      output[key] = Object.assign({}, create[key]);
    }
  });
  output.id = {
    value: '',
    label: 'ID',
    required: true,
    disabled: true,
  };
  return output;
})();

export const filter = {
  firstName: {
    value: '',
    label: 'Имя',
  },
  lastName: {
    value: '',
    label: 'Фамилия',
  },
  email: {
    value: '',
    label: 'E-mail',
  },
  phone: {
    value: '',
    label: 'Телефон',
  },
  createdAt: {
    label: 'Дата регистрации',
    from: null,
    to: null,
    // max: new Date(),
    // min: new Date(),
    instance: Date,
  },
  // orders: {
  //   label: 'Количество заказов',
  //   from: '',
  //   to: '',
  //   min: 0,
  //   instance: Number,
  // },
  role: {
    value: 'CLIENT',
    label: 'Роль',
    hidden: true,
    disabled: true,
  },
};
