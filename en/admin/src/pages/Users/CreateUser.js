import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import * as R from 'ramda';

import gql from './gql';
import { create } from './fields';
import FormBase from './../../components/FormBase';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';

import { CREATE_USER_MUTATION } from './../../constants';

const Form = withProgress([CREATE_USER_MUTATION])(FormBase);

const createUserMutationResult = `${CREATE_USER_MUTATION}Result`;
const createUserMutationError = `${CREATE_USER_MUTATION}Error`;
const createUserMutationLoading = `${CREATE_USER_MUTATION}Loading`;
const createUserMutationReset = `${CREATE_USER_MUTATION}Reset`;

class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: null,
      clearFields: [],
      req: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[createUserMutationResult]) {
      const id = nextProps[createUserMutationResult].data.signupUserWithRole.id
      this.props.snackbar({ message: `Пользователь создан - id: ${id}`, name: 'Success' });
      this.props.reqSuccess(id);
    }
    if (nextProps[createUserMutationError]) {
      this.props.snackbar(nextProps[createUserMutationError]);
      this.props[createUserMutationReset]();
      this.setState({
        // clearFields: ['password', 'repassword'],
        req: false,
      }, () => {
        this.props.reqError(nextProps[createUserMutationError]);
      });
    }
  }
  confirm = async () => {
    const { fields } = this.state;
    this.props[CREATE_USER_MUTATION]({
      variables: fields,
    });
  }
  handleSubmit(fields) {
    const state = {
      fields: R.clone(fields),
      req: true,
    };
    this.setState(state, () => {
      this.confirm();
    });
  }
  render() {
    const { clearFields, req } = this.state;
    create.role.value = this.props.userRole;

    return (
      <Form
        formTitle="Создать пользователя"
        fields={create}
        disabledAll={false}
        disabledActions={
          {
            save: false,
          }
        }
        showActions={
          {
            save: true,
          }
        }
        onSubmit={this.handleSubmit}
        clearFields={clearFields}
        req={req}
        {...this.props}
      />
    );
  }
}

CreateUser.defaultProps = {
  reqSuccess: () => {},
  reqError: () => {},
};

export default compose(
  graphql(gql[CREATE_USER_MUTATION], { name: CREATE_USER_MUTATION }),
  withStateMutation({ name: CREATE_USER_MUTATION }),
  withSnackbar(),
)(CreateUser);
