import gql from 'graphql-tag';

export default gql`
  query userQuery($id: ID, $email: String) {
    User(
      id: $id,
      email: $email,
    ) {
      id,
      createdAt,
      updatedAt,
      email,
      firstName,
      lastName,
      phone,
      role,
    }
  }
`;
