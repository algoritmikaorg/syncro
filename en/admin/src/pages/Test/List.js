import React from 'react';
import { compose, graphql } from 'react-apollo';

import gql from './../Lecturers/gql';

import {
  ALL_LECTURERS_QUERY,
  DELETE_LECTURER_MUTATION,
} from './../../constants';

const modifyQueryFilter = () => { return {}; };
const gqlQueryName = ALL_LECTURERS_QUERY;
const queryResultObjectName = 'allLecturers';

const List = (props) => {
  const data = props[gqlQueryName];
  if (!data) return <div>No Data</div>;
  const { loading, error } = data;
  if (loading) return <div>Loading</div>;
  if (error) return <h1>ERROR</h1>;
  return (
    <div>
      {
        data[queryResultObjectName].map(item => (
          <div key={item.id}>{item.id}</div>
        ))
      }
    </div>
  );
};

export default compose(
  graphql(
    gql[gqlQueryName],
    {
      name: gqlQueryName,
      options: (ownProps) => {
        return {
          variables: {
            filter: modifyQueryFilter(ownProps.allQueryFilter, ownProps.filterType),
          },
        };
      },
    },
  )
)(List);
