import React from 'react';

import gql from './gql';

import {
  CERTIFICATE_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import certificatesFields from './fields';

const CertificateViewWithRelations = (props) => {
  const gqlQueryWithRelationsName = CERTIFICATE_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Certificate';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={certificatesFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
      />
    </div>
  );
};

export default CertificateViewWithRelations;
