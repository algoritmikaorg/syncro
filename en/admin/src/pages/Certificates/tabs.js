export default [
  {
    label: 'Активные',
    id: 'active',
  },
  {
    label: 'Срок действия закончен',
    id: 'expired',
  },
];
