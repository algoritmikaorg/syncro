import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_CERTIFICATES_QUERY,
  DELETE_CERTIFICATE_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import certificatesFields from './fields';
import certificatesActions from './actions';
import certificatesColumnData from './columnData';

import tabs from './tabs';

const Certificates = (props) => {

  const gqlAllQueryName = ALL_CERTIFICATES_QUERY;
  const gqlAllQueryItemsName = 'allCertificates';
  const gqlMutationDelete = DELETE_CERTIFICATE_MUTATION;
  const gqlMutationDeleteItemName = 'deleteCertificate';
  const resulMutatiomtMessageSuccses = 'Сертификат удален';

  return (
    <div>
      <ListMaterials
        {...props}
        tabs={tabs}
        actionsButtons={certificatesActions}
        columnData={certificatesColumnData}
        fields={certificatesFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showActions={props.showActions === undefined ? true : props.showActions}
        // filterModifier={filterModifier}
      />
    </div>
  );
};

export default Certificates;
