import gql from 'graphql-tag';

export default gql`
  mutation deleteCertificateMutation(
      $id: ID!
    ){
    deleteCertificate(
      id: $id,
    ) {
      id
    }
  }
`;
