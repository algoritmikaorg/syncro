import gql from 'graphql-tag';

export default gql`
  query certificateQueryWithRelations($id: ID!) {
    Certificate(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

    }
  }
`;
