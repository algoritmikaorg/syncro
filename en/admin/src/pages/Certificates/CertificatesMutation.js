import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_CERTIFICATE_MUTATION,
  EDIT_CERTIFICATE_MUTATION,
  CERTIFICATE_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import certificatesFields from './fields';
import certificatesActions from './actions';


const CertificatesMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_CERTIFICATE_MUTATION;
    fields = certificatesFields.create;
    mutationResultObjectName = 'createCertificate';
    resulMutatiomtMessageSuccses = 'Сертификат создан';
  } else {
    gqlMutationName = EDIT_CERTIFICATE_MUTATION;
    fields = certificatesFields.update;
    mutationResultObjectName = 'updateCertificate';
    resulMutatiomtMessageSuccses = 'Сертификат обновлен';
    gqlQueryName = CERTIFICATE_QUERY;
    gqlQueryItemName = 'Certificate';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default CertificatesMutation;
