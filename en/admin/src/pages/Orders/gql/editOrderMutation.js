import gql from 'graphql-tag';

export default gql`
  mutation UpdateOrderMutation(
      $id: ID!,
    ){
    updateOrder(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
    }
  }
`;
