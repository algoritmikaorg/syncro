import gql from 'graphql-tag';

export default gql`
  query AllOrdersQuery($filter: OrderFilter, $first: Int, $skip: Int, $orderBy: OrderOrderBy) {
    allOrders(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
    }
    _allOrdersMeta {
      count
    }
  }
`;
