import gql from 'graphql-tag';

export default gql`
  query orderQueryWithRelations($id: ID!) {
    Order(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

    }
  }
`;
