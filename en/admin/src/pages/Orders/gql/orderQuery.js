import gql from 'graphql-tag';

export default gql`
  query orderQuery($id: ID!) {
    Order(
      id: $id
    ) {
      id,
      createdAt,
      updatedAt,
    }
  }
`;
