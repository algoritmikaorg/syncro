import { format } from './../../utils';

const columnData = [
  {
    id: 'userId',
    numeric: false,
    disablePadding: false,
    label: 'Покупатель',
  },
  {
    id: 'discountsIds',
    label: 'Скидка',
  },
  {
    id: 'eventsIds',
    label: 'Мероприятия',
  },
  {
    id: 'paymentId',
    label: 'Оплата',
  },
  {
    id: 'participantsIds',
    label: 'Участники',
  },
  {
    id: 'tickets',
    label: 'Количество билетов',
  },
];
export default columnData;
