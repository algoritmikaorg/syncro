const filterHowWokMainPage = () => ({ page: 'mainpage' });
const filterHowWokCourse = () => ({ page: 'course' });
const filterHowWokLecture = () => ({ page: 'lecture' });

export default {
  filterHowWokMainPage,
  filterHowWokCourse,
  filterHowWokLecture,
};
