import gql from 'graphql-tag';

export default gql`
  query AllMainPageQuery {
    allMainPages(
      first: 1,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      #
      recomendedTitle,
      recomendedDescription,
      #
      lecturersTitle,
      lecturersDescription,
      #
      howWorksTitle,
      #
      scheduleTitle,
      galleryTitle,
      reviewsTitle,
      #
      locationTitle,
      locationDescription,
      #
      partnerTitle,

      recommendedLectures {
        id,
        tags {
          id,
          title
        }
        events {
          id,
          date
          lecturers {
            id,
            firstName,
            lastName,
          }
          location {
            id,
            title,
            address,
            metro,
          }
        }
      }
    }
  }
`;
