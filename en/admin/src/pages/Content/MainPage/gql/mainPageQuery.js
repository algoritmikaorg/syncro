import gql from 'graphql-tag';

export default gql`
  query MainPageQuery($id: ID!) {
    MainPage(
      id: $id
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      #
      recomendedTitle,
      recomendedDescription,
      #
      lecturersTitle,
      lecturersDescription,
      #
      howWorksTitle,
      #
      scheduleTitle,
      galleryTitle,
      reviewsTitle,
      #
      locationTitle,
      locationDescription,
      #
      partnerTitle,
      #
      recommendedLectures {
        id,
        title,
        tags {
          id,
          title
        }
        events {
          id,
          date
          lecturers {
            id,
            firstName,
            lastName,
          }
          location {
            id,
            title,
            address,
            metro,
          }
        }
      }
    }
  }
`;
