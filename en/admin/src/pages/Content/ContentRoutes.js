import React from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Content from './Content';
import ContentMutation from './ContentMutation';

import MainGallery from './MainGallery';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const ContentRoutes = (props) => {
  const { match } = props;
  const { url } = match;
  return (
    <div>
      <Switch>
        {/* <Route
          exact
          path={`${url}`}
          component={rest => (
            <Redirect to={`${url}/main`} />
          )}
        /> */}
        <Route
          exact
          path={`${url}/main`}
          component={rest => (
            <Content
              {...props}
              {...rest}
              typeMaterial="main"
              title={'Главная страница'}
            />
          )}
        />

        <Route
          exact
          path={`${url}/main/create`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="main"
              mutationType="create"
              title={'Главная страница'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/main/update/:id`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="main"
              mutationType="update"
              title={'Главная страница'}
            />
          )}
        />

        <Route
          exact
          path={`${url}/about`}
          component={rest => (
            <Content
              {...props}
              {...rest}
              typeMaterial="about"
              title={'О проекте'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/about/create`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="about"
              mutationType="create"
              title={'О проекте'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/about/update/:id`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="about"
              mutationType="update"
              title={'О проекте'}
            />
          )}
        />

        <Route
          path={`${url}/maingallery`}
          component={rest => (
            <MainGallery
              {...props}
              {...rest}
              typeMaterial="maingallery"
              title={'Фотографии с мероприятий'}
            />
          )}
        />

        <Route
          exact
          path={`${url}/howwork-mainpage`}
          component={rest => (
            <Content
              {...props}
              {...rest}
              typeMaterial="howwork-mainpage"
              title={'Как проходит синхронизация'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/howwork-mainpage/create`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="howwork-mainpage"
              mutationType="create"
              title={'Как проходит синхронизация'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/howwork-mainpage/update/:id`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="howwork-mainpage"
              mutationType="update"
              title={'Как проходит синхронизация'}
            />
          )}
        />

        <Route
          exact
          path={`${url}/howwork-mainpage`}
          component={rest => (
            <Content
              {...props}
              {...rest}
              typeMaterial="howwork-mainpage"
              title={'Как проходит синхронизация - Главная страница'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/howwork-mainpage/create`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="howwork-mainpage"
              mutationType="create"
              title={'Как проходит синхронизация - Главная страница'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/howwork-mainpage/update/:id`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="howwork-mainpage"
              mutationType="update"
              title={'Как проходит синхронизация - Главная страница'}
            />
          )}
        />

        <Route
          exact
          path={`${url}/howwork-lecture`}
          component={rest => (
            <Content
              {...props}
              {...rest}
              typeMaterial="howwork-lecture"
              title={'Как проходит синхронизация - Страница Лекции'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/howwork-lecture/create`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="howwork-lecture"
              mutationType="create"
              title={'Как проходит синхронизация - Страница Лекции'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/howwork-lecture/update/:id`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="howwork-lecture"
              mutationType="update"
              title={'Как проходит синхронизация - Страница Лекции'}
            />
          )}
        />

        <Route
          exact
          path={`${url}/howwork-course`}
          component={rest => (
            <Content
              {...props}
              {...rest}
              typeMaterial="howwork-course"
              title={'Как проходит синхронизация - Страница Курс'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/howwork-course/create`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="howwork-course"
              mutationType="create"
              title={'Как проходит синхронизация - Страница Курс'}
            />
          )}
        />
        <Route
          exact
          path={`${url}/howwork-course/update/:id`}
          component={rest => (
            <ContentMutation
              {...props}
              {...rest}
              typeMaterial="howwork-course"
              mutationType="update"
              title={'Как проходит синхронизация - Страница Курс'}
            />
          )}
        />

        {/* <Route
          exact
          path={`${url}/about/view/:id`}
          component={rest => (
            <ContentViewWithRelations
              {...props}
              {...rest}
              typeMaterial="about"
            />
          )} */}
        />

        <Route
          component={rest => (
            <NotFound {...props} {...rest} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(ContentRoutes, { title: 'Содержимое сайта' });
export default ContentRoutes;
