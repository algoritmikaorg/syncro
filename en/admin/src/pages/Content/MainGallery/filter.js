const filterModifier = (input = {}) => ({
  gallery: {
    type: "maingallery",
  },
  description_contains: input.description_contains,
});

export default filterModifier;
