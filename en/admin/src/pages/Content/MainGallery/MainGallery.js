import React from 'react';

import gql from './gql';

import {
  ALL_IMAGES_QUERY,
  DELETE_IMAGE_MUTATION,

} from './../../../constants';

import ListMaterials from './../../../components/ListMaterials';

import mainGalleriesFields from './fields';
import mainGalleriesActions from './actions';
import mainGalleriesColumnData from './columnData';
import mainGalleriesFilterModifier from './filter';

const MainGallery = (props) => {

  const gqlAllQueryName = ALL_IMAGES_QUERY;
  const gqlAllQueryItemsName = 'allImages';
  const gqlMutationDelete = DELETE_IMAGE_MUTATION;
  const gqlMutationDeleteItemName = 'deleteImage';
  const resulMutatiomtMessageSuccses = 'Изображение удалено';

  return (
    <div>
      <ListMaterials
        {...props}
        actionsButtons={mainGalleriesActions}
        columnData={mainGalleriesColumnData}
        fields={mainGalleriesFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showTitle={props.showTitle}
        showActions={props.showActions === undefined ? true : props.showActions}
        filterModifier={mainGalleriesFilterModifier}
      />
    </div>
  );
};

export default MainGallery;
