import gql from 'graphql-tag';

export default gql`
  mutation CreateMainGalleryMutation(
      $secret: String!,
      $galleryId: ID,
      $description: String,
    ){
    createImage(
      secret: $secret,
      galleryId: $galleryId,
      description: $description,
    ) {
      id
      createdAt
      updatedAt

      secret
      description
    }
  }
`;
