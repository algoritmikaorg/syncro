import {
  ALL_IMAGES_QUERY,
  DELETE_IMAGE_MUTATION,

  CREATE_IMAGE_MUTATION,
  UPDATE_IMAGE_MUTATION,
  IMAGE_QUERY,
  IMAGE_QUERY_WITH_RELATIONS,
} from './../../../../constants';

import allImagesQuery from './allImagesQuery';
import deleteImageMutation from './deleteImageMutation';
import createImageMutation from './createImageMutation';
import updateImageMutation from './updateImageMutation';
import imageQuery from './imageQuery';
import imageQueryWithRelations from './imageQueryWithRelations';


const gql = {
  [ALL_IMAGES_QUERY]: allImagesQuery,
  [DELETE_IMAGE_MUTATION]: deleteImageMutation,

  [CREATE_IMAGE_MUTATION]: createImageMutation,
  [UPDATE_IMAGE_MUTATION]: updateImageMutation,

  [IMAGE_QUERY]: imageQuery,
  [IMAGE_QUERY_WITH_RELATIONS]: imageQueryWithRelations,
};

export default gql;
