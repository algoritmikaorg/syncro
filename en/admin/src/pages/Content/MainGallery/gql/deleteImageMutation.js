import gql from 'graphql-tag';

export default gql`
  mutation deleteImageMutation(
      $id: ID!
    ){
    deleteImage(
      id: $id,
    ) {
      id
    }
  }
`;
