import {
  ALL_ABOUTPAGE_QUERY,
  CREATE_ABOUTPAGE_MUTATION,
  EDIT_ABOUTPAGE_MUTATION,
  ABOUTPAGE_QUERY,
} from './../../../../constants';

import allAboutPageQuery from './allAboutPageQuery';
import createAboutPageMutation from './createAboutPageMutation';
import updateAboutPageMutation from './updateAboutPageMutation';
import aboutPageQuery from './aboutPageQuery';

const gql = {
  [ALL_ABOUTPAGE_QUERY]: allAboutPageQuery,
  [CREATE_ABOUTPAGE_MUTATION]: createAboutPageMutation,
  [EDIT_ABOUTPAGE_MUTATION]: updateAboutPageMutation,
  [ABOUTPAGE_QUERY]: aboutPageQuery,
};

export default gql;
