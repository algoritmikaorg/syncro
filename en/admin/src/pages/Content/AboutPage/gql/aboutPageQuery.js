import gql from 'graphql-tag';

export default gql`
  query AboutPageQuery($id: ID!) {
    AboutPage(
      id: $id
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      description,
    }
  }
`;
