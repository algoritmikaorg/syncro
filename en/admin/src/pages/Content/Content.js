import React from 'react';

import {
  ALL_ABOUTPAGE_QUERY,
  ALL_MAINPAGE_QUERY,
  ALL_HOWWORKPAGES_QUERY,
} from './../../constants';

import gqlAboutPage from './AboutPage/gql';
import gqlMainPage from './MainPage/gql';
import gqlHowWorks from './HowWorks/gql';

import filterHowWorks from './HowWorks/filter';


import MaterialQueryAndMutationRedirect from './../../components/MaterialQueryAndMutationRedirect';

const Content = (props) => {
  const { typeMaterial } = props;
  let gqlAllQueryName = null;
  let gqlAllQueryItemsName = null;
  let gql = null;
  let filterModifier = null;

  switch (typeMaterial) {
    case 'about': {
      gqlAllQueryName = ALL_ABOUTPAGE_QUERY;
      gqlAllQueryItemsName = 'allAboutPages';
      gql = gqlAboutPage;
      break;
    }
    case 'howwork-mainpage': {
      gqlAllQueryName = ALL_HOWWORKPAGES_QUERY;
      gqlAllQueryItemsName = 'allHowWorkPages';
      gql = gqlHowWorks;
      filterModifier = filterHowWorks.filterHowWokMainPage;
      break;
    }
    case 'howwork-course': {
      gqlAllQueryName = ALL_HOWWORKPAGES_QUERY;
      gqlAllQueryItemsName = 'allHowWorkPages';
      gql = gqlHowWorks;
      filterModifier = filterHowWorks.filterHowWokCourse;
      break;
    }
    case 'howwork-lecture': {
      gqlAllQueryName = ALL_HOWWORKPAGES_QUERY;
      gqlAllQueryItemsName = 'allHowWorkPages';
      gql = gqlHowWorks;
      filterModifier = filterHowWorks.filterHowWokLecture;
      break;
    }
    default: {
      // main page
      gqlAllQueryName = ALL_MAINPAGE_QUERY;
      gqlAllQueryItemsName = 'allMainPages';
      gql = gqlMainPage;
    }
  }

  return (
    <div>
      <MaterialQueryAndMutationRedirect
        {...props}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        filterModifier={filterModifier}
      />
    </div>
  );
};

export default Content;
