import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_EVENT_MUTATION,
  EDIT_EVENT_MUTATION,
  EVENT_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import tagsFields from './fields';
import tagsActions from './actions';


const EventsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_EVENT_MUTATION;
    fields = tagsFields.create;
    mutationResultObjectName = 'createEvent';
    resulMutatiomtMessageSuccses = 'Скидка создана';
  } else {
    gqlMutationName = EDIT_EVENT_MUTATION;
    fields = tagsFields.update;
    mutationResultObjectName = 'updateEvent';
    resulMutatiomtMessageSuccses = 'Скидка обновлена';
    gqlQueryName = EVENT_QUERY;
    gqlQueryItemName = 'Event';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
        // showActions={false}
      />
    </div>
  );
};

export default EventsMutation;
