import gql from 'graphql-tag';

export default gql`
  mutation UpdateEventMutation(
      $id: ID!
      $date: DateTime!,
      $price: Int,
      $quantityOfTickets: Int,
      $lectureId: ID,
      $locationId: ID,
      $lecturersIds: [ID!],
    ){
    updateEvent(
      id: $id,
      date: $date,
      price: $price,
      quantityOfTickets: $quantityOfTickets,
      lectureId: $lectureId,
      locationId: $locationId,
      lecturersIds: $lecturersIds,
    ) {
      id,
      createdAt,
      updatedAt,

      date,
      quantityOfTickets,
      price,
      tickets {
        user {
          id,

          firstName,
          lastName,
        },
      },
      lecture {
        id,

        title,
      },
      lecturers {
        id,

        firstName,
        lastName,
      },
      location {
        id,

        title,
        address,
        metro,
      },
      orders {
        payment {
          id,

          createdAt,
          commission,
          certificate,
          subscription,
          type,

        },
        user {
          id,

          firstName,
          lastName,
        },
      },
      participants {
        user {
          id,

          firstName,
          lastName,
        },
      },
      reviews {
        user {
          id,

          firstName,
          lastName,
        },
      },
      _attendingsMeta {
        count
      },
      _lecturersMeta {
        count
      },
      _participantsMeta {
        count
      },
      _reviewsMeta {
        count
      }
    }
  }
`;
