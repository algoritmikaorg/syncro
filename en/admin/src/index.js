import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink, split } from 'apollo-client-preset';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

import config from './config';
import SnackbarProvider from './provider/SnackbarProvider';
import BrokerSnackbar from './components/Snackbar/brokerSnackbar';
import registerServiceWorker from './registerServiceWorker';
import App from './App';

import { GC_AUTH_TOKEN } from './constants';

const wsLink = new WebSocketLink({
  uri: config.SUBSCRIPTIONS__API_ENDPOINT,
  options: {
    reconnect: true,
    connectionParams: {
      authToken: localStorage.getItem(GC_AUTH_TOKEN),
    },
  },
});

const httpLink = new HttpLink({ uri: config.SIMPLE_API_ENDPOINT });

const middlewareAuthLink = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem(GC_AUTH_TOKEN);
  const authorizationHeader = token ? `Bearer ${token}` : null;
  operation.setContext({
    headers: {
      authorization: authorizationHeader,
    },
  });
  return forward(operation);
});

const httpLinkWithAuthToken = middlewareAuthLink.concat(httpLink);

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLinkWithAuthToken,
);

const client = new ApolloClient({
  link,
  cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
  // link: httpLink,
  // cache: new InMemoryCache({ dataIdFromObject: object => object.id }),
});

const brokerSnackbar = new BrokerSnackbar();

ReactDOM.render(
  <BrowserRouter basename="/synhro-admin/">
    <ApolloProvider client={client}>
      <SnackbarProvider brokerSnackbar={brokerSnackbar}>
        <App brokerSnackbar={brokerSnackbar} />
      </SnackbarProvider>
    </ApolloProvider>
  </BrowserRouter>,
  document.getElementById('root'),
);

registerServiceWorker();
