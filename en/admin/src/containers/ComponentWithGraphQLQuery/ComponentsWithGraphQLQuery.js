import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';

// const ComponentsWithGraphQLQuery = (props) => {
//   const {
//     gql,
//     gqlQueryName,
//     filterModifier,
//   } = props;
//
//   const InnerChild = (
//     compose(
//       graphql(gql[gqlQueryName], {
//         name: gqlQueryName,
//         options: (ownProps) => {
//           return {
//             variables: {
//               filter: filterModifier(ownProps),
//             },
//           };
//         },
//       }),
//     )(props.component)
//   );
//
//   return (
//     <InnerChild
//       {...props}
//     />
//   );
// };

class ComponentsWithGraphQLQuery extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }
  render() {
    const {
      gql,
      gqlQueryName,
      filterModifier,
    } = this.props;

    const InnerChild = (
      compose(
        graphql(gql[gqlQueryName], {
          name: gqlQueryName,
          options: (ownProps) => {
            return {
              variables: {
                filter: filterModifier(ownProps),
              },
            };
          },
        }),
      )(this.props.component)
    );

    return (
      <InnerChild
        {...this.props}
      />
    );
  }
}

ComponentsWithGraphQLQuery.defaultProps = {
  filterModifier: () => {},
};

export default ComponentsWithGraphQLQuery;
