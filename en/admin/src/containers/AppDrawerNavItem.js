import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { withStyles } from 'material-ui/styles';
import { ListItem } from 'material-ui/List';
import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';

const styles = theme => ({
  button: theme.mixins.gutters({
    borderRadius: 0,
    justifyContent: 'flex-start',
    textTransform: 'none',
    width: '100%',
    transition: theme.transitions.create('background-color', {
      duration: theme.transitions.duration.shortest,
    }),
    '&:hover': {
      textDecoration: 'none',
    },
  }),
  navItem: {
    ...theme.typography.body2,
    display: 'block',
    paddingTop: 0,
    paddingBottom: 0,
  },
  navLink: {
    fontWeight: theme.typography.fontWeightRegular,
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
  },
  navChild: {
    paddingLeft: 20,
  },
  navLinkButton: {
    color: theme.palette.text.secondary,
    textIndent: theme.spacing.unit * 3,
    fontSize: theme.typography.pxToRem(13),
  },
  activeButton: {
    color: theme.palette.text.primary,
  },
});

class AppDrawerNavItem extends React.Component<any, any> {
  static defaultProps = {
    openImmediately: false,
  };

  state = {
    open: false,
  };

  componentWillMount() {
    if (this.props.openImmediately) {
      this.setState({ open: true });
    }
  }

  handleClick = () => {
    this.setState({ open: !this.state.open });
  };

  render() {
    const { children, classes, to, openImmediately, title } = this.props;

    if (to) {
      return (
        // <ListItem className={classes.navLink} disableGutters>
        <ListItem className={classes.navItem} disableGutters>
          <Button
            variant="button"
            // activeClassName={classes.activeButton}
            // className={classNames(classes.button, classes.navLinkButton)}
            className={classNames(classes.button)}
            disableRipple
            onClick={this.props.onClick}
            component={Link}
            to={to}
          >
            {title}
            {/* <Link to={to}>{title}</Link> */}
          </Button>
        </ListItem>
      );
    }

    return (
      <ListItem className={classes.navItem} disableGutters>
        <Button
          classes={{
            root: classes.button,
            label: openImmediately ? 'algolia-lvl0' : '',
          }}
          onClick={this.handleClick}
        >
          {title}
        </Button>
        <Collapse
          in={this.state.open}
          timeout="auto"
          unmountOnExit
          className={classes.navChild}
        >
          {children}
        </Collapse>
      </ListItem>
    );
  }
}

AppDrawerNavItem.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
  to: PropTypes.string,
  onClick: PropTypes.func,
  openImmediately: PropTypes.bool,
  title: PropTypes.string.isRequired,
};

export default withStyles(styles)(AppDrawerNavItem);
