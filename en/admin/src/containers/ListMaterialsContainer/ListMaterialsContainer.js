import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';

import withStateMutation from './../../hoc/withStateMutation';

// const ListMaterialsContainer = (props) => {
//   const {
//     gql,
//     gqlMutationDelete,
//     gqlAllQueryName,
//     filterModifier,
//   } = props;
//
//   const InnerChild = (
//     compose(
//       graphql(gql[gqlMutationDelete], { name: gqlMutationDelete }),
//       graphql(gql[gqlAllQueryName], {
//         name: gqlAllQueryName,
//         options: (ownProps) => {
//           return {
//             variables: {
//               filter: filterModifier(ownProps.allQueryFilter, ownProps.filterType),
//             },
//           };
//         },
//       }),
//       withStateMutation({ name: gqlMutationDelete }),
//     )(props.component)
//   );
//
//   return (
//     <InnerChild {...props} />
//   );
// };

class ListMaterialsContainer extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }
  render() {
    const {
      gql,
      gqlMutationDelete,
      gqlAllQueryName,
      filterModifier,
    } = this.props;

    const InnerChild = (
      compose(
        graphql(gql[gqlMutationDelete], { name: gqlMutationDelete }),
        graphql(gql[gqlAllQueryName], {
          name: gqlAllQueryName,
          options: (ownProps) => {
            return {
              variables: {
                filter: filterModifier(ownProps.allQueryFilter, ownProps.filterType),
              },
            };
          },
        }),
        withStateMutation({ name: gqlMutationDelete }),
      )(this.props.component)
    );

    return (
      <InnerChild {...this.props} />
    );
  }
}

ListMaterialsContainer.defaultProps = {
  gqlAllQuery: '',
  gqlMutationDelete: '',
  gql: {},
  filterModifier: input => input,
};

export default ListMaterialsContainer;
