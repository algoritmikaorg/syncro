import FileSaver from 'file-saver';
import aoaToXLSX from './aoaToXLSX';
import s2ab from './s2ab';
import XLSX from 'xlsx';

const writeXLSX = (wb, defaultCellStyle) => {
  const wopts = {
    bookType: 'xlsx',
    bookSST: false,
    type: 'binary',
    // defaultCellStyle,
  };
  return XLSX.write(wb, wopts);
};

const generateOutputFile = (input, inputHead) => {
  const idKey = inputHead.map(item => item.id);
  const head = inputHead.map(item => item.label);

  const data = input.map(item => idKey.map(key => item[key]));
  data.unshift(head);

  const wb = aoaToXLSX(data);
  const blob = new Blob([s2ab(writeXLSX(wb))], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  FileSaver.saveAs(blob, 'output.xlsx');
};

export default generateOutputFile;
