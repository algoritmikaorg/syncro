import React, { Component } from 'react'
import PropTypes from 'prop-types'

const withSnackbar = () => {
  return (BaseComponent) => {
    const ComponentWithSnackbar = (props, context) => {
      return (
        <BaseComponent
          snackbar={context.snackbar}
          {...props}
        />
      );
    }

    ComponentWithSnackbar.contextTypes = {
      snackbar: PropTypes.func
    }

    return ComponentWithSnackbar;
  }
};

export default withSnackbar;
