import React, { PureComponent } from 'react';
import wrapDisplayName from 'recompose/wrapDisplayName';
import hoistNonReactStatic from 'hoist-non-react-statics';

import {
  Login
} from './../pages';

import { GC_USER_ID, GC_USER_ROLE, GC_AUTH_TOKEN } from './../constants';
import Progress from './../components/Progress';

const withAuthAndPermission = () => (BaseComponent) => {
  const WithAuthAndPermission = props => {
    const userId = localStorage.getItem(GC_USER_ID);
    if (!userId) return <Login {...props} />;
    <BaseComponent {...props} />
  };

  WithAuthAndPermission.displayName = wrapDisplayName(BaseComponent, 'withAuthAndPermission');
  hoistNonReactStatic(WithAuthAndPermission, BaseComponent);

  return WithAuthAndPermission;
};

export default withAuthAndPermission;
