import React from 'react';
import wrapDisplayName from 'recompose/wrapDisplayName';
import hoistNonReactStatic from 'hoist-non-react-statics';

import {
  Login
} from './../pages';

import { GC_USER_ID, GC_USER_ROLE, GC_AUTH_TOKEN } from './../constants';

const authAndPermission = (props) => (BaseComponent) => {
  const userId = localStorage.getItem(GC_USER_ID);
  if (!userId) return <Login {...props} />;
  return BaseComponent;
};

export default authAndPermission;
