import React from 'react';
import wrapDisplayName from 'recompose/wrapDisplayName';
import gql from 'graphql-tag';
import { withApollo } from 'react-apollo';

import {
  Login
} from './../pages';

import { GC_USER_ID, GC_USER_ROLE, GC_AUTH_TOKEN } from './../constants';

const authAndPermission = (props) => (BaseComponent) => {
  const userId = localStorage.getItem(GC_USER_ID);
  if (!userId) return <Login {...props} />;
  return BaseComponent;
};

export default authAndPermission;
