import gql from 'graphql-tag';

export default gql`
  query partnerQuery($id: ID) {
    Partner(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
      image,
      link,
      title,
    }
  }
`;
