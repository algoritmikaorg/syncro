import {
  CREATE_PARTNER_MUTATION,
  ALL_PARTNERS_QUERY,
  EDIT_PARTNER_MUTATION,
  PARTNER_QUERY,
  DELETE_PARTNER_MUTATION,
  PARTNER_QUERY_WITH_RELATIONS,
} from './../../../constants';

import createPartnerMutation from './createPartnerMutation';
import allPartnersQuery from './allPartnersQuery';
import editPartnerMutation from './editPartnerMutation';
import partnerQuery from './partnerQuery';
import deletePartnerMutation from './deletePartnerMutation';
import partnerRelationQuery from './partnerRelationQuery';

const gql = {
  [CREATE_PARTNER_MUTATION]: createPartnerMutation,
  [DELETE_PARTNER_MUTATION]: deletePartnerMutation,
  [EDIT_PARTNER_MUTATION]: editPartnerMutation,
  [ALL_PARTNERS_QUERY]: allPartnersQuery,
  [PARTNER_QUERY]: partnerQuery,
  [PARTNER_QUERY_WITH_RELATIONS]: partnerRelationQuery,
};

export default gql;
