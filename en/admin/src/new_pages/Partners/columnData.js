const columnData = [
  {
    id: 'title',
    label: 'Название'
  }, {
    id: 'link',
    label: 'Ссылка'
  }
];

export default columnData;
