import React from 'react';

import gql from './gql';

import {
  PARTNER_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const PartnerViewWithRelations = (props) => {
  const gqlQueryName = PARTNER_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Partner';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default PartnerViewWithRelations;
