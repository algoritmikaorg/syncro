import gql from 'graphql-tag';

export default gql`
  mutation deleteReviewMutation(
      $id: ID!
    ){
    deleteReview(
      id: $id,
    ) {
      id
    }
  }
`;
