import gql from 'graphql-tag';

export default gql`
  query reviewQueryWithRelations($id: ID!) {
    Review(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

    }
  }
`;
