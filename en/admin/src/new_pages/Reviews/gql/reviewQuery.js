import gql from 'graphql-tag';

export default gql`
  query reviewQuery($id: ID!) {
    Review(
      id: $id
    ) {
      id
      createdAt
      updatedAt

      anons
      description
      public
      user {
        id
        firstName
        lastName
      }
      lecture {
        id
        title
      }
    }
  }
`;
