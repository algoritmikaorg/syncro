import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_REVIEWS_QUERY,
  DELETE_REVIEW_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';
import { tabsModerate } from './../../new_utils';

const Reviews = (props) => {

  const gqlQueryName = ALL_REVIEWS_QUERY;
  const queryResultObjectName = 'allReviews';
  const gqlMutationName = DELETE_REVIEW_MUTATION;
  const mutationResultObjectName = 'deleteReview';
  const resulMutationMessageSuccses = 'Отзыв удален';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
        tabs={tabsModerate}
      />
    </div>
  );
};

export default Reviews;
