import { appendProp } from './../../new_utils';

const modifyQueryFilter = (input = {}) => {
  let filter = {};
  const {
    email,
    firstName,
    lastName,
    phone,
    tab,
    description,
  } = input;

  filter.public = true;
  if (input.tab) {
    switch (input.tab.id) {
      case 'active': {
        filter.public = true;
        break;
      }
      case 'not_moderated': {
        filter.public = false;
        break;
      }
      default: {}
    }
  }

  if (email) {
    filter = appendProp(filter, 'user');
    filter.user.email_contains = email;
  }
  if (firstName) {
    filter = appendProp(filter, 'user');
    filter.user.firstName_contains = firstName;
  }
  if (lastName) {
    filter = appendProp(filter, 'user');
    filter.user.lastName_contains = lastName;
  }
  if (phone) {
    filter = appendProp(filter, 'user');
    filter.user.filter.phone_contains = phone;
  }
  if (description) {
    filter.description_contains = description;
  }

  return filter;
};

export default modifyQueryFilter;
