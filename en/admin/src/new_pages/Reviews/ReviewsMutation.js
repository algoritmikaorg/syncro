import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_REVIEW_MUTATION,
  EDIT_REVIEW_MUTATION,
  REVIEW_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import reviewsFields from './fields';
import reviewsActions from './actions';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const ReviewsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_REVIEW_MUTATION;
    fields = reviewsFields.create;
    mutationResultObjectName = 'createReview';
    resultMutationMessageSuccses = 'Отзыв создан';
  } else {
    gqlMutationName = EDIT_REVIEW_MUTATION;
    fields = reviewsFields.update;
    mutationResultObjectName = 'updateReview';
    resultMutationMessageSuccses = 'Отзыв обновлен';
    gqlQueryName = REVIEW_QUERY;
    queryResultObjectName = 'Review';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default ReviewsMutation;
