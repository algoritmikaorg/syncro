import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllEventsQuery($filter: EventFilter, $first: Int, $skip: Int, $orderBy: EventOrderBy) {
    allEvents(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...EventPayload
    }
  }
  ${fragments.eventPayload}
`;
