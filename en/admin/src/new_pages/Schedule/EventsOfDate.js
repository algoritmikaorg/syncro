import React from 'react';
import moment from 'moment';
import 'moment/locale/ru';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';

import Event from './Event';

moment.locale();

const styles = theme => ({
  paper: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
  }),
});

const EventsOfDate = (props) => {
  const {
    data,
    date,
    classes,
  } = props;
  return (
    <div>
      <Typography type="headline" gutterBottom>
        {moment(date).format('dddd, D MMMM')}
      </Typography>
      <Divider />
      {
        data.map(event => (
          <Paper key={event.id} className={classes.paper}>
            <Event data={event} />
          </Paper>
        ))
      }
    </div>
  );
};

export default withStyles(styles)(EventsOfDate);
