import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import Typography from 'material-ui/Typography';

import FilterOfQuery from './../../new_components/FilterOfQuery';
import withProgress from './../../new_hoc/withProgress';
import { formatInputDataArrayForTable, formatOutputFormData } from './../../new_utils';

import Events from './Events';

import gql from './gql';
import {
  ALL_EVENTS_QUERY,
} from './../../constants';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';

class Schedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data || null,
      isRequested: true,
    };
    this.handleFilterQuerySubmit = this.handleFilterQuerySubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
    let isRequested = false
    if (data && data.loading) {
      isRequested = true;
    }
    if (data && data.error) {
      isRequested = true;
      this.props.snackbar(data.error);
      this.setState({
        isRequested: false,
      });
      return false;
    }
    if (data && data.allEvents) {
      isRequested = false;
    }

    this.setState({
      isRequested,
      data: data.allEvents || [],
    });
  }
  handleFilterQuerySubmit(inputFormFilterData) {
    const formFilterData = Object.assign({}, this.state.formFilterData, inputFormFilterData);
    this.setState({
      isRequested: true,
      formFilterData: formatOutputFormData(formFilterData, fields.filter),
    }, this.refetch);
  }
  refetch = async () => {
    const { formFilterData } = this.state;
    const result = await this.props.data.refetch({ filter: modifyQueryFilter(formFilterData || {}) });
    if (result.data) {
      // const data = formatInputDataArrayForTable(result.data.allEvents);
      const data = result.data.allEvents;
      this.setState({
        data,
        isRequested: false,
      });
    } else {
      this.setState({
        isRequested: false,
      });
    }

  }
  render() {
    const { title } = this.props;
    const { data, isRequested } = this.state;

    return (
      <div>
        <Typography type="display1" gutterBottom>
          {title}
        </Typography>
        <FilterOfQuery
          formTitle="Фильтр"
          fields={fields.filter}
          isRequested={isRequested}
          onSubmit={this.handleFilterQuerySubmit}
        />
        {
          data && data.length > 0 &&
          <Events {...this.props} data={data} />
        }
      </div>
    );
  }
}

export default compose(
  graphql(gql[ALL_EVENTS_QUERY], {
    options: (ownProps) => {
      return {
        variables: {
          filter: modifyQueryFilter(),
        },
      };
    },
  }),
  withProgress([ALL_EVENTS_QUERY]),
)(Schedule);
