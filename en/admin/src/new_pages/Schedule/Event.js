import React from 'react';
import moment from 'moment';
import 'moment/locale/ru';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import Button from 'material-ui/Button';
import { Link } from 'react-router-dom';

moment.locale();

const Event = (props) => {
  const { data } = props;
  const {
    id,
    date,
    price,
    quantityOfTickets,
    quantityOfTicketsAvailable,
    lecture,
    lecturers,
    location,
    curator,
    _ordersMeta,
    orders,
  } = data;
  return (
    <div>
      <Typography type="title" gutterBottom>
        {moment(date).format('HH:mm')}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Лекция: </strong>{lecture.title}
      </Typography>
      {
        lecture.course && lecture.course.length > 0 &&
        (
          <Typography type="body1" gutterBottom>
            <strong>Курс: </strong>{lecture.course[0].title}
          </Typography>
        )
      }
      {
        lecture.cycle && lecture.cycle.length > 0 &&
        (
          <Typography type="body1" gutterBottom>
            <strong>Цикл: </strong>{lecture.cycle[0].title}
          </Typography>
        )
      }
      <Typography type="body1" gutterBottom>
        <strong>Лекторы: </strong>
        {
          lecturers
            .map(({ firstName, lastName }) => `${firstName} ${lastName}`)
            .join(', ')
        }
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Локация: </strong>{location.title} (м. {location.metro}, {location.address})
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Координатор: </strong>{curator.firstName} {curator.lastName}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Цена: </strong>{price}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>
          Количество билетов:
        </strong> {quantityOfTickets}
        {/* (доступно: {quantityOfTicketsAvailable}) */}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Количество заказов: </strong>{_ordersMeta.count}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Количество оплаченных заказов: </strong>{orders.length}
      </Typography>
      <Button
        // className={classes.button}
        raised
        dense
        color="primary"
        component={Link}
        to={`/events/view/${id}`}
      >
        Подробнее
      </Button>
    </div>
  );
};

export default Event;
