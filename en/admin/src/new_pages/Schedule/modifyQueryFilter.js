const modifyQueryFilter = (input = {}) => {
  let filter = {};
  const {
    date,
    lecturersIds,
    locationsIds,
    lectureType,
    curatorIds,
  } = input;

  if (date && new Date(date) >= new Date()) {
    filter = {
      date_gte: date,
    };
  } else {
    filter = {
      date_gte: new Date(),
    };
  }

  if (lecturersIds && lecturersIds.length > 0) {
    filter.lecturers_some = {
      id_in: lecturersIds,
    };
  }
  if (locationsIds && locationsIds.length > 0) {
    filter.location = {
      id_in: locationsIds,
    };
  }
  if (lectureType) {
    filter.lecture = {
      lectureType,
    };
  }

  if (curatorIds && curatorIds.length > 0) {
    filter.curator = {
      id_in: curatorIds,
    };
  }

  return filter;
};

export default modifyQueryFilter;
