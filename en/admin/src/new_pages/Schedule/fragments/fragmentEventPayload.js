import gql from 'graphql-tag';

export default gql`
  fragment EventPayload on Event {
    id
    createdAt
    updatedAt

    date
    price
    quantityOfTickets
    quantityOfTicketsAvailable
    lecture {
      id
      title
      course {
        id
        title
      }
      cycle {
        id
        title
      }
    }
    lecturers {
      id
      firstName
      lastName
    }
    location {
      id
      title
      address
      metro
    }
    curator {
      id
      firstName
      lastName
      email
    }
    orders (
      filter: {
        payment: {
          status: PAID
        }
      }
    ) {
      id
    }
    _ordersMeta {
      count
    }
  }
`;
