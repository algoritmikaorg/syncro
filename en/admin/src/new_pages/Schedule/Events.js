import React from 'react';
import moment from 'moment';
import EventsOfDate from './EventsOfDate';

const groupOfDate = (events) => {
  const output = {};
  events.forEach((event) => {
    const { date } = event;
    const formatedDate = moment(date).format('YYYY-MM-DD');
    if (!output[formatedDate]) output[formatedDate] = [];
    output[formatedDate].push(event);
  });
  return output;
};

const Events = (props) => {
  const { data } = props;
  let grouped = null;
  if (data && data.length > 0) {
    grouped = groupOfDate(data);
  } else {
    grouped = {};
  }
  return (
    <div style={{ marginTop: 40 }}>
      {
        Object.keys(grouped).map(key => (
          <EventsOfDate key={key} data={grouped[key]} date={key} />
        ))
      }
    </div>
  );
};

export default Events;
