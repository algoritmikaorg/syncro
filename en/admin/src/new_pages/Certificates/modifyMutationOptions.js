export default () => {
  return (
    {
      refetchQueries: [
        'AllCertificatesQuery',
        'AllCertificatesOfUserQuery',
      ],
    }
  );
};
