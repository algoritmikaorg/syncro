import { appendProp } from './../../new_utils';

const modifyQueryCertificates = (input = {}) => {
  let filter = {};
  const {
    tab,
    title,
    lecturesCount,
    firstName,
    lastName,
    email,
    start,
    end,
  } = input;

  if (end) filter.end_lte = end;

  if (tab) {
    switch (input.tab.id) {
      case 'active': {
        filter.unusedLectures_gt = 0;
        filter.end_gt = new Date();
        filter.payment = {
          status: 'PAID',
        };
        break;
      }
      case 'expired': {
        filter.unusedLectures_lte = 0;
        filter.end_lte = new Date();
        filter.payment = {
          status: 'PAID',
        };
        break;
      }
      case 'no_payment': {
        filter.payment = {
          status: 'PENDING',
        };
        break;
      }
      default: {}
    }
  } else {
    filter.unusedLectures_gt = 0;
    filter.end_gt = new Date();
    filter.payment = {
      status: 'PAID',
    };
  }

  if (title) filter.title_contains = title;
  if (lecturesCount) filter.lecturesCount = lecturesCount;

  if (firstName) {
    filter = appendProp(filter, 'user');
    filter.user.firstName_contains = firstName;
  }
  if (lastName) {
    filter = appendProp(filter, 'user');
    filter.user.lastName_contains = lastName;
  }
  if (email) {
    filter = appendProp(filter, 'user');
    filter.user.email_contains = email;
  }
  if (start) filter.start_gte = start;

  return filter;
};

const modifyQueryCertificatesOfUser = ({ id }) => (input = {}) => {
  const filter = modifyQueryCertificates(input);
  filter.user = {
    id,
  };
  return filter;
};

export default {
  modifyQueryCertificates,
  modifyQueryCertificatesOfUser,
};
