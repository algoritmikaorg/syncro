import gql from 'graphql-tag';

export default gql`
  fragment CertificateShortPayload on Certificate {
    id
    createdAt
    updatedAt

    user {
      id
      firstName
      lastName
    }
    draft
    gift
    title
    price
    lecturesCount
    unusedLectures
    start
    end
    personsCount
    discounts {
      id
      title
    }
    code
    payment {
      id
      status
    }
  }
`;
