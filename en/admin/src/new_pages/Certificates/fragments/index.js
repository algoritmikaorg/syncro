import certificatePayload from './fragmentCertificatePayload';
import certificateShortPayload from './fragmentCertificateShortPayload';

export default {
  certificatePayload,
  certificateShortPayload,
};
