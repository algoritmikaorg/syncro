import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query certificateQuery($id: ID!) {
    Certificate(
      id: $id
    ) {
      ...CertificatePayload
    }
  }
  ${fragments.certificatePayload}
`;
