import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllCertificatesOfUserQuery($filter: CertificateFilter, $first: Int, $skip: Int, $orderBy: CertificateOrderBy) {
    allCertificates(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...CertificatePayload
    }
  }
  ${fragments.certificatePayload}
`;
