import gql from 'graphql-tag';

export default gql`
  mutation CreateCertificateMutation(
      $userId: ID,
      $draft: Boolean!,
      $gift: Boolean!,
      $title: String!,
      $discountsIds: [ID!],
      $additionalDiscountsIds: [ID!],
      $productId: ID!,
      $payment: CertificatepaymentPayment,
    ){
    createCertificate(
      userId: $userId,
      draft: $draft
      gift: $gift
      title: $title
      discountsIds: $discountsIds
      additionalDiscountsIds: $additionalDiscountsIds
      productId: $productId
      payment: $payment
    ) {
      id
    }
  }
`;
