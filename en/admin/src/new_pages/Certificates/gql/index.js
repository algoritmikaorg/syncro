import {
  CREATE_CERTIFICATE_MUTATION,
  ALL_CERTIFICATES_QUERY,
  ALL_CERTIFICATES_OF_USER_QUERY,
  EDIT_CERTIFICATE_MUTATION,
  CERTIFICATE_QUERY,
  DELETE_CERTIFICATE_MUTATION,
  CERTIFICATE_QUERY_WITH_RELATIONS,
  CHECK_ORDER_CERT_AND_SUB,
} from './../../../constants';
import createCertificateMutation from './createCertificateMutation';
import allCertificatesQuery from './allCertificatesQuery';
import editCertificateMutation from './editCertificateMutation';
import certificateQuery from './certificateQuery';
import deleteCertificateMutation from './deleteCertificateMutation';
import certificateQueryWithRelations from './certificateQueryWithRelations';
import allCertificatesOfUserQuery from './allCertificatesOfUserQuery';
import checkOrderCertAndSub from './checkOrderCertAndSub';

const gql = {
  [CREATE_CERTIFICATE_MUTATION]: createCertificateMutation,
  [ALL_CERTIFICATES_QUERY]: allCertificatesQuery,
  [EDIT_CERTIFICATE_MUTATION]: editCertificateMutation,
  [CERTIFICATE_QUERY]: certificateQuery,
  [DELETE_CERTIFICATE_MUTATION]: deleteCertificateMutation,
  [CERTIFICATE_QUERY_WITH_RELATIONS]: certificateQueryWithRelations,
  [ALL_CERTIFICATES_OF_USER_QUERY]: allCertificatesOfUserQuery,
  [CHECK_ORDER_CERT_AND_SUB]: checkOrderCertAndSub,
};

export default gql;
