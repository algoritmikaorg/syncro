import React from 'react';

import gql from './gql';

import {
  CERTIFICATE_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const CertificateViewWithRelations = (props) => {
  const gqlQueryName = CERTIFICATE_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Certificate';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default CertificateViewWithRelations;
