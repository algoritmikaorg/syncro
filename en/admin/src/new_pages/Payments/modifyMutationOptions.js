export default () => {
  return (
    {
      refetchQueries: [
        'AllPaymentsQuery',
        'AllPaymentsShortQuery',
      ],
    }
  );
};
