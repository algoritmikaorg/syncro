import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Payments from './Payments';
import PaymentsMutation from './PaymentsMutation';
import PaymentViewWithRelations from './PaymentViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../new_hoc/withWrapper';

const PaymentsRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Payments
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <PaymentsMutation {...props} {...rest} mutationType="update" title={'Редактирование платежа'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <PaymentViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(PaymentsRoutes, { title: 'Скидки' });
export default PaymentsRoutes;
