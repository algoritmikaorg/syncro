import React from 'react';

import gql from './gql';

import {
  ALL_PAYMENTS_QUERY,
  DELETE_PAYMENT_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import { tabsPayment } from './../../new_utils';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Payments = (props) => {
  const gqlQueryName = ALL_PAYMENTS_QUERY;
  const queryResultObjectName = 'allPayments';
  const gqlMutationName = DELETE_PAYMENT_MUTATION;
  const mutationResultObjectName = 'deletePayment';
  const resulMutationMessageSuccses = 'Направление удалено';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
        tabs={tabsPayment}
      />
    </div>
  );
};

export default Payments;
