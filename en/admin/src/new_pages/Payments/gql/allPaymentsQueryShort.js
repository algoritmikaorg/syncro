import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllPaymentsShortQuery($filter: PaymentFilter, $first: Int, $skip: Int, $orderBy: PaymentOrderBy) {
    allPayments(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...PaymentShortPayload
    }
  }
  ${fragments.paymentShortPayload}
`;
