import {
  CREATE_PAYMENT_MUTATION,
  ALL_PAYMENTS_QUERY,
  EDIT_PAYMENT_MUTATION,
  PAYMENT_QUERY,
  DELETE_PAYMENT_MUTATION,
  PAYMENT_QUERY_WITH_RELATIONS,
  ALL_PAYMENTS_QUERY_SHORT,
  ALL_PAYMENTS_RELAIONS_QUERY,
} from './../../../constants';
import createPaymentMutation from './createPaymentMutation';
import allPaymentsQuery from './allPaymentsQuery';
import editPaymentMutation from './editPaymentMutation';
import paymentQuery from './paymentQuery';
import deletePaymentMutation from './deletePaymentMutation';
import paymentQueryWithRelations from './paymentQueryWithRelations';
import allPaymentsQueryShort from './allPaymentsQueryShort';
import allPaymentsRelationsQuery from './allPaymentsRelationsQuery';

const gql = {
  [CREATE_PAYMENT_MUTATION]: createPaymentMutation,
  [ALL_PAYMENTS_QUERY]: allPaymentsQuery,
  [ALL_PAYMENTS_QUERY_SHORT]: allPaymentsQueryShort,
  [EDIT_PAYMENT_MUTATION]: editPaymentMutation,
  [PAYMENT_QUERY]: paymentQuery,
  [DELETE_PAYMENT_MUTATION]: deletePaymentMutation,
  [PAYMENT_QUERY_WITH_RELATIONS]: paymentQueryWithRelations,
  [ALL_PAYMENTS_RELAIONS_QUERY]: allPaymentsRelationsQuery,
};

export default gql;
