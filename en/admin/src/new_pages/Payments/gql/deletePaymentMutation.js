import gql from 'graphql-tag';

export default gql`
  mutation deletePaymentMutation(
      $id: ID!
    ){
    deletePayment(
      id: $id,
    ) {
      id
    }
  }
`;
