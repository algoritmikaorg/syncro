import paymentPayload from './fragmentPaymentPayload';
import paymentShortPayload from './fragmentPaymentShortPayload';
import paymentFullPayload from './fragmentPaymentFullPayload';
import paymentReationsPayload from './fragmentPaymentRelationsPayload';
export default {
  paymentPayload,
  paymentShortPayload,
  paymentFullPayload,
  paymentReationsPayload,
};
