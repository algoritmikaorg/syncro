import gql from 'graphql-tag';

export default gql`
  fragment PaymentPayload on Payment {
    id
    createdAt
    updatedAt

    commission
    orderPrice
    discountPrice
    totalPrice
    type
    status
    acquiring {
      id
      title
    }
    discounts {
      id
      title
    }
    additionalDiscounts {
      id
      title
    }

    order {
      id
      events {
        id
        date
        lecture {
          id
          title
          course {
            id
            title
          }
          cycle {
            id
            title
          }
        }
      }
      # courses {
      #   title
      # }
      # cycles {
      #   title
      # }
      # lectures {
      #   title
      #   events {
      #     date
      #   }
      # }
    }

    certificate {
      id
      title
    }
    certificatesInPayments {
      id
      title
    }
    subscription {
      id
      title
    }
    subscriptionsInPayments {
      id
      title
    }
    user {
      id
      firstName
      lastName
      email
    }
  }
`;
