import gql from 'graphql-tag';

export default gql`
  fragment PaymentRelationsPayload on Payment {
    id
    title
    promocode
    rate
    allProducts
    buyingLecturesCount
    participantsCount
    useCount
    validityPeriodFrom
    validityPeriodTo
    lectures {
      id
      title
    }
    courses {
      id
      title
    }
    cycles {
      id
      title
    }
    tags {
      id
      title
    }
  }
`;
