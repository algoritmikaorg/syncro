import gql from 'graphql-tag';

export default gql`
  fragment PaymentShortPayload on Payment {
    id
    title
    promocode
    rate
    allProducts
    buyingLecturesCount
    participantsCount
    useCount
    validityPeriodFrom
    validityPeriodTo
  }
`;
