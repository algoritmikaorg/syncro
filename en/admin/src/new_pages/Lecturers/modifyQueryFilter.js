const modifyQueryFilter = (input = {}, type) => {
  const filter = {};
  if (!input) return {};
  const {
    firstName,
    lastName,
    tagsIds,
  } = input;

  if (firstName) filter.firstName_contains = firstName;
  if (lastName) filter.lastName_contains = lastName;

  if (tagsIds && tagsIds.length > 0) {
    filter.tags_some = {
      id_in: tagsIds,
    };
  }

  return filter;
};

export default modifyQueryFilter;
