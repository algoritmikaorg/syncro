import gql from 'graphql-tag';

export default gql`
  mutation deleteLecturerMutation(
      $id: ID!,
      $aliasId: ID!
    ){
    deleteLecturer(
      id: $id,
    ) {
      id
    }
    deleteAlias(
      id: $aliasId
    ) {
      id
    }
  }
`;
