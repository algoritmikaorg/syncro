import gql from 'graphql-tag';

export default gql`
  query AllLecturersQuery($filter: LecturerFilter, $first: Int, $skip: Int, $orderBy: LecturerOrderBy) {
    allLecturers(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
      firstName,
      lastName,
      anons,
      anonsForLecture,
      description,
      events {
        id,
        date,
        lecture {
          id,
          title,
        }
      },
      img160x160,
      img340x225,
      img337x536,
      skills,
      specialization,
      tags {
        id,
        title
      }

      _eventsMeta {
        count
      }
      _tagsMeta{
        count
      }
    }
    _allLecturersMeta {
      count
    }
  }
`;
