import gql from 'graphql-tag';

export default gql`
  query lecturerQueryWithRelations($id: ID!) {
    Lecturer(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
      firstName,
      lastName,
      anons,
      anonsForLecture,
      description,
      events {
        id,
        date,
        lecture {
          id,
          title,
          tags {
            title
          }
          # author {
          #   firstName
          #   lastName
          # }
        }
      },
      img160x160,
      img340x225,
      img337x536,
      skills,
      specialization,
      tags {
        id,
        title,
        color,
        textColor,
        alias {
          id,
          alias,
        }
      }
      alias {
        id,
        alias
      }

      _eventsMeta {
        count
      }
      _tagsMeta{
        count
      }
    }
    allLectures(
      filter: {
          events_some: {
            lecturers_some: {
              id: $id
            }
          }
        }
    ) {
      id,
      title
    }
  }
`;
