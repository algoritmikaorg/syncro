import gql from 'graphql-tag';

export default gql`
  query AllLecturersQueryShort(
    $filter: LecturerFilter,
    $first: Int,
    $skip: Int,
    $orderBy: LecturerOrderBy
  ) {
    allLecturers(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      updatedAt
      firstName
      lastName
      img160x160
      tags {
        title
      }
    }
    _allLecturersMeta {
      count
    }
  }
`;
