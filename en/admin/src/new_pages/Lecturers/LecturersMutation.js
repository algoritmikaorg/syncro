import React from 'react';

import gql from './gql';

import {
  CREATE_LECTURER_MUTATION,
  EDIT_LECTURER_MUTATION,
  LECTURER_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import lecturersFields from './fields';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const LecturersMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_LECTURER_MUTATION;
    fields = lecturersFields.create;
    mutationResultObjectName = 'createLecturer';
    resultMutationMessageSuccses = 'Лектор создан';
  } else {
    gqlMutationName = EDIT_LECTURER_MUTATION;
    fields = lecturersFields.update;
    mutationResultObjectName = 'updateLecturer';
    resultMutationMessageSuccses = 'Лектор обновлен';
    gqlQueryName = LECTURER_QUERY;
    queryResultObjectName = 'Lecturer';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default LecturersMutation;
