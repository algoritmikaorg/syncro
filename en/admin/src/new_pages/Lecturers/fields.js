const create = [
  {
    id: 'firstName',
    label: 'Имя',
    required: true,
  },
  {
    id: 'lastName',
    label: 'Фамилия',
    required: true,
  },
  {
    id: 'anons',
    label: 'Анонс',
    type: 'multiline',
  },
  {
    id: 'anonsForLecture',
    label: 'Анонс для лекции',
    type: 'multiline',
  },
  {
    id: 'description',
    label: 'Описание',
    type: 'multiline',
  },
  {
    id: 'skills',
    label: 'Навыки',
    required: true,
    type: 'multiline',
    helperText: 'Через запятую',
    outputType: 'array',
    outputSubType: ',',
  },
  {
    id: 'specialization',
    value: '',
    label: 'Специализация',
    type: 'multiline',
  },
  {
    id: 'img160x160',
    label: 'Фото 160х160',
    defaultValue: 'no-photo',
    type: 'image',
    // size: '160x!/0x0:160x160',
    size: '160x160',
  },
  {
    id: 'img340x225',
    label: 'Фото 340х225',
    type: 'image',
    defaultValue: 'no-photo',
    // size: '340x!/0x0:340x225',
    size: '340x225',
  },
  {
    id: 'img337x536',
    label: 'Фото 337x536',
    defaultValue: 'no-photo',
    type: 'image',
    // size: '337x!/0x0:337x536',
    size: '337x536',
  },
  {
    id: 'alias',
    label: 'URL alias',
    required: true,
    type: 'alias',
    outputType: 'alias',
  },
  {
    id: 'tags',
    label: 'Направления',
    required: true,
    disabled: true,
    relation: 'tags',
    type: 'relation',
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
];


const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const view = update.slice();
view.push({
  id: 'lectures',
  label: 'Лекции связанные с лектором',
  disabled: true,
  relation: 'lecturesOfEvents',
  type: 'relation',
});

const filter = [
  {
    id: 'firstName',
    label: 'Имя',
    type: 'text',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
    type: 'text',
  },
  {
    id: 'tagsIds',
    label: 'Направления',
    relation: 'tags-select',
    type: 'relation',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
