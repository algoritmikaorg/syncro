import React from 'react';

import gql from './gql';

import {
  LECTURER_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const LecturerViewWithRelations = (props) => {
  const gqlQueryName = LECTURER_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Lecturer';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default LecturerViewWithRelations;
