import React from 'react';

import gql from './gql';

import {
  ALL_LECTURERS_QUERY,
  DELETE_LECTURER_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';


const Lecturers = (props) => {

  const gqlQueryName = ALL_LECTURERS_QUERY;
  const queryResultObjectName = 'allLecturers';
  const gqlMutationName = DELETE_LECTURER_MUTATION;
  const mutationResultObjectName = 'deleteLecturer';
  const resulMutationMessageSuccses = 'Лектор удален';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
      />
    </div>
  );
};

export default Lecturers;
