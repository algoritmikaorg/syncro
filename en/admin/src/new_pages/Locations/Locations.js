import React from 'react';

import gql from './gql';

import {
  ALL_LOCATIONS_QUERY,
  DELETE_LOCATION_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Locations = (props) => {

  const gqlQueryName = ALL_LOCATIONS_QUERY;
  const queryResultObjectName = 'allLocations';
  const gqlMutationName = DELETE_LOCATION_MUTATION;
  const mutationResultObjectName = 'deleteLocation';
  const resulMutationMessageSuccses = 'Направление удалено';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
      />
    </div>
  );
};

export default Locations;
