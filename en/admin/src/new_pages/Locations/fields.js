const create = [
  {
    id: 'title',
    label: 'Название',
    type: 'multiline',
    required: true,
  },
  {
    id: 'metro',
    label: 'Метро',
    type: 'multiline',
    required: true,
  },
  {
    id: 'address',
    label: 'Адрес',
    type: 'multiline',
    required: true,
  },
  {
    id: 'image',
    label: 'Фото',
    type: 'image',
    defaultValue: 'no-photo',
    size: '340x220',
    required: false,
  },
];

const update = create.slice();
update.unshift({
  id: 'id',
  label: 'ID',
  type: 'multiline',
  required: true,
  disabled: true,
});

// const view = update.slice();
// view.push(
//   {
//     id: 'lectures',
//     label: 'Лекции связанные с локацией',
//     disabled: true,
//     relation: 'lectures',
//     type: 'relation',
//   },
// );

const view = update.slice();
view.push(
  {
    id: 'events',
    label: 'Мероприятия связанные с локацией',
    disabled: true,
    relation: 'events',
    type: 'relation',
  },
);

const filter = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'address',
    label: 'Адрес',
  },
  {
    id: 'metro',
    label: 'Метро',
  },
];

export default {
  create,
  update,
  view,
  filter,
};
