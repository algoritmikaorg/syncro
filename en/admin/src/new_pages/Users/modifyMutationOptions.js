export default () => {
  return (
    {
      refetchQueries: [
        'AllUsersQuery',
        'AllUsersQueryShort',
      ],
    }
  );
};
