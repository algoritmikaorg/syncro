import gql from 'graphql-tag';

export default gql`
  fragment UserPayload on User {
    id
    createdAt
    updatedAt

    email,
    firstName,
    lastName,
    phone,
    role,
    blocked,
  }
`;
