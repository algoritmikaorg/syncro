import React from 'react';
import Typography from 'material-ui/Typography';

import gql from './gql';

import {
  ALL_USERS_QUERY,
  DELETE_USER_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Users = (props) => {
  const { userRole, title } = props;
  const gqlQueryName = ALL_USERS_QUERY;
  const queryResultObjectName = 'allUsers';
  const gqlMutationName = DELETE_USER_MUTATION;
  const mutationResultObjectName = 'deleteUser';
  const resulMutationMessageSuccses = 'Пользователь удален';

  let currentActions = null;
  let currentFields = null;

  switch (userRole) {
    case 'ADMIN': {
      currentActions = actions.admin;
      currentFields = fields.admin;
      break;
    }
    case 'MODERATOR': {
      currentActions = actions.moderator;
      currentFields = fields.moderator;
      break;
    }
    case 'CURATOR': {
      currentActions = actions.curator;
      currentFields = fields.curator;
      break;
    }
    default: {
      currentFields = fields.client;
      currentActions = actions.client;
    }
  }

  return (
    <div>
      {
        userRole !== 'CLIENT' &&
        <Typography type="title" gutterBottom>
         {title}
        </Typography>
      }

      <ListEntities
        {...props}
        actionsButtons={currentActions}
        columnData={columnData}
        fields={currentFields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter(userRole)}
      />
    </div>
  );
};

export default Users;
