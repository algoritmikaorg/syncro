export default {
  client: [
    {
      title: 'Создать пользователя',
      type: 'create',
      to: '/users/create',
    },
  ],
  admin: [
    {
      title: 'Создать администратора',
      type: 'create',
      to: '/users/administrators/create',
    },
  ],
  moderator: [
    {
      title: 'Создать модератора',
      type: 'create',
      to: '/users/moderators/create',
    },
  ],
  curator: [
    {
      title: 'Создать Координатора',
      type: 'create',
      to: '/users/curators/create',
    },
  ],
};
