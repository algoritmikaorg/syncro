const modifyQueryFilter = role => (input = {}) => {
  const filter = {};
  filter.role = role;
  const {
    firstName,
    lastName,
    email,
    phone,
    createdAtFrom,
    createdAtTo,
  } = input || {};

  if (firstName) filter.firstName_contains = firstName
  if (lastName) filter.lastName_contains = lastName;
  if (email) filter.email_contains = email;
  if (phone) filter.phone_contains = phone;
  if (createdAtFrom) filter.createdAt_gte = createdAtFrom;
  if (createdAtTo) filter.createdAt_lte = createdAtTo;
  return filter;
};

export default modifyQueryFilter;
