import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_USER_MUTATION,
  EDIT_USER_MUTATION,
  USER_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import fields from './fields';
import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const UserMutation = (props) => {
  const {
    mutationType,
    userRole,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let currentFields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_USER_MUTATION;
    mutationResultObjectName = 'signupUserWithRole';
  } else {
    gqlMutationName = EDIT_USER_MUTATION;
    mutationResultObjectName = 'updateUser';
    gqlQueryName = USER_QUERY;
    queryResultObjectName = 'User';
  }

  switch (userRole) {
    case 'ADMIN': {
      if (mutationType === 'create') {
        currentFields = fields.admin.create;
        resultMutationMessageSuccses = 'Администратор создан';
      } else {
        currentFields = fields.admin.update;
        resultMutationMessageSuccses = 'Администратор обновлен';
      }
      break;
    }
    case 'MODERATOR': {
      if (mutationType === 'create') {
        currentFields = fields.moderator.create;
        resultMutationMessageSuccses = 'Модератор создан';
      } else {
        currentFields = fields.moderator.update;
        resultMutationMessageSuccses = 'Модератор обновлен';
      }
      break;
    }
    case 'CURATOR': {
      if (mutationType === 'create') {
        currentFields = fields.curator.create;
        resultMutationMessageSuccses = 'Координатор создан';
      } else {
        currentFields = fields.curator.update;
        resultMutationMessageSuccses = 'Координатор обновлен';
      }
      break;
    }
    default: {
      if (mutationType === 'create') {
        currentFields = fields.client.create;
        resultMutationMessageSuccses = 'Пользователь создан';
      } else {
        currentFields = fields.client.update;
        resultMutationMessageSuccses = 'Пользователь обновлен';
      }
    }
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={currentFields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default UserMutation;
