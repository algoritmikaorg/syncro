import gql from 'graphql-tag';

export default gql`
  mutation SignupUserWithRoleAndGenPass(
      $email: String!,
      $firstName: String!,
      $lastName: String!,
      $phone: String!,
      $role: String!,
    ){
    signupUserWithRoleAndGenPass(
      email: $email,
      firstName: $firstName,
      lastName: $lastName,
      phone: $phone,
      role: $role,
    ) {
      id
      createdAt
      updatedAt

      email
      firstName
      lastName
      phone
      role
    }
  }
`;
