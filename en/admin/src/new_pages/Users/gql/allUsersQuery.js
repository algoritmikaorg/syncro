import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllUsersQuery($filter: UserFilter, $first: Int, $skip: Int, $orderBy: UserOrderBy) {
    allUsers(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...UserPayload
    }
    _allUsersMeta {
      count
    }
  }
  ${fragments.userPayload}
`;
