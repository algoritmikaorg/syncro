import gql from 'graphql-tag';

export default gql`
  mutation UpdateUserPasswordMutation(
      $id: ID!,
      $password: String,
    ){
    updateUser(
      id: $id,
      password: $password,
    ) {
      id,
    }
  }
`;
