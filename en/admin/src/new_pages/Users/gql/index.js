import {
  CREATE_USER_MUTATION,
  ALL_USERS_QUERY,
  EDIT_USER_MUTATION,
  USER_QUERY,
  USER_CERTIFICATE_AND_SUBSCRIPTION_QUERY,
  DELETE_USER_MUTATION,
  USER_CLIENT_QUERY_WITH_RELATIONS,
  USER_SYSTEM_QUERY_WITH_RELATIONS,
  LOGGED_IN_USER_WITH_ROLE,
  CREATE_USER_WITHOUT_PASS_MUTATION,
  AUTHENTICATE_USER_MUTATION,
  USER_RESET_PASS_MUTATION,
  USER_RESET_PASS_SEND_EMAIL_MUTATION,
} from './../../../constants';

import createUserMutation from './createUserMutation';
import allUsersQuery from './allUsersQuery';
import editUserMutation from './editUserMutation';
import userQuery from './userQuery';
import userCeriticateAndSubscriptionQuery from './userCeriticateAndSubscriptionQuery';
import deleteUserMutation from './deleteUserMutation';
import userClientQueryWithRelations from './userClientQueryWithRelations';
import userSystemQueryWithRelations from './userSystemQueryWithRelations';
import loggedInUserWithRole from './loggedInUserWithRole';
import createUserWithoutPassMutation from './createUserWithoutPassMutation';
import authenticateUserMutation from './authenticateUserMutation';

import userResetPassMutation from './userResetPassMutation';
import userResetPassSendEmailMutation from './userResetPassSendEmailMutation';

const gql = {
  [CREATE_USER_MUTATION]: createUserMutation,
  [ALL_USERS_QUERY]: allUsersQuery,
  [EDIT_USER_MUTATION]: editUserMutation,
  [USER_QUERY]: userQuery,
  [USER_CERTIFICATE_AND_SUBSCRIPTION_QUERY]: userCeriticateAndSubscriptionQuery,
  [DELETE_USER_MUTATION]: deleteUserMutation,
  [USER_CLIENT_QUERY_WITH_RELATIONS]: userClientQueryWithRelations,
  [USER_SYSTEM_QUERY_WITH_RELATIONS]: userSystemQueryWithRelations,
  [LOGGED_IN_USER_WITH_ROLE]: loggedInUserWithRole,
  [CREATE_USER_WITHOUT_PASS_MUTATION]: createUserWithoutPassMutation,
  [AUTHENTICATE_USER_MUTATION]: authenticateUserMutation,
  [USER_RESET_PASS_MUTATION]: userResetPassMutation,
  [USER_RESET_PASS_SEND_EMAIL_MUTATION]: userResetPassSendEmailMutation,
};

export default gql;
