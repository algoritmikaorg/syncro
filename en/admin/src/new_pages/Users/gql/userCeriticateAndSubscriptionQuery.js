import gql from 'graphql-tag';

export default gql`
  query userCeriticateAndSubscriptionQuery($id: ID, $email: String, $end: DateTime) {
    User(
      id: $id,
      email: $email,
    ) {
      id
      createdAt
      updatedAt
      blocked
      email
      firstName
      lastName
      phone
      role
      certificates(
        filter: {
          end_gt: $end
          lectures_gt: 0
        }
      ) {
        title
        lectures
        end
      }
      subscriptions(
        filter: {
          end_gt: $end
          lectures_gt: 0
        }
      ) {
        title
        lectures
        end
      }
    }
  }
`;
