import gql from 'graphql-tag';

export default gql`
  mutation UserResetPassTokenMutation(
      $email: String!,
      $host: String!,
    ){
    userResetPassToken(
      email: $email,
      host: $host,
    ) {
      id,
    }
  }
`;
