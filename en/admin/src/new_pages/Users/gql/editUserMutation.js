import gql from 'graphql-tag';

export default gql`
  mutation UpdateUserMutation(
      $id: ID!,
      $email: String!,
      $firstName: String!,
      $lastName: String!,
      $password: String,
      $phone: String!,
      $role: UserRole,
      $blocked: Boolean,
    ){
    updateUser(
      id: $id,
      email: $email,
      firstName: $firstName,
      lastName: $lastName,
      password: $password,
      phone: $phone,
      role: $role,
      blocked: $blocked,
    ) {
      id,
      # createdAt,
      # updatedAt,
      # email,
      # firstName,
      # lastName,
      # phone,
      # role,
    }
  }
`;
