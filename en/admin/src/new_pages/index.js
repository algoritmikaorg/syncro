export default {};
// export { default as Home } from './Home';
// export { default as Login } from './Login';
export { default as Messages } from './Messages';
export { default as Catalog } from './Catalog';
// export { default as NotFound } from './NotFound';
// // export { default as Lecturers } from './Lecturers';
// // export { default as Tags } from './Tags';
export { default as Locations } from './Locations';
export { default as Certificates } from './Certificates';
export { default as Subscriptions } from './Subscriptions';
export { default as Partners } from './Partners';
export { default as Users } from './Users';
// export { default as Others } from './Others';
export { default as Orders } from './Orders';
export { default as Payments } from './Payments';
export { default as Settings } from './Settings';
export { default as Reviews } from './Reviews';
//
export { default as Tags } from './Tags';
export { default as Lecturers } from './Lecturers';
export { default as Discounts } from './Discounts';
export { default as Content } from './Content';
export { default as Template } from './Template';
export { default as Acquirings } from './Acquirings';
//
export { default as Galleries } from './Galleries';
//
// export { default as Imports } from './Imports';
// export { default as Test } from './Test';

export { default as Events } from './Events';
export { default as Schedule } from './Schedule';
