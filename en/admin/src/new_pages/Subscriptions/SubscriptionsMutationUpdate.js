import React from 'react';

import gql from './gql';

import {
  EDIT_SUBSCRIPTION_MUTATION,
  SUBSCRIPTION_QUERY,
} from './../../constants';

import { UpdateSubscriptionOrder } from './../../new_components/SubscriptionOrder';
import QueryWithQueryOptions from './../../new_containers/QueryWithQueryOptions';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const modifyQueryOptions = (ownProps) => {
  const { id } = ownProps.match.params;
  return (
    {
      variables: {
        id,
      },
    }
  );
};

const SubscriptionsMutationUpdate = (props) => {
  const {
    match,
    history,
  } = props;
  const gqlMutationName = EDIT_SUBSCRIPTION_MUTATION;
  const mutationResultObjectName = 'updateSubscription';
  const resultMutationMessageSuccses = 'Абонемент обновлен';
  const gqlQueryName = SUBSCRIPTION_QUERY;
  const queryResultObjectName = 'Subscription';

  return (
    <div>
      <QueryWithQueryOptions
        {...props}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryOptions={modifyQueryOptions}
        match={match}
        history={history}
      >
        <UpdateSubscriptionOrder
          {...props}
        />
      </QueryWithQueryOptions>
    </div>
  );
};

export default SubscriptionsMutationUpdate;
