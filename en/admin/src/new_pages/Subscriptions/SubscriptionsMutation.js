import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_SUBSCRIPTION_MUTATION,
  EDIT_SUBSCRIPTION_MUTATION,
  SUBSCRIPTION_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';
import subscriptionsFields from './fields';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

import SubscriptionOrder from './../../new_components/SubscriptionOrder';

const SubscriptionsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_SUBSCRIPTION_MUTATION;
    fields = subscriptionsFields.create;
    mutationResultObjectName = 'createSubscription';
    resultMutationMessageSuccses = 'Абонемент создан';
  } else {
    gqlMutationName = EDIT_SUBSCRIPTION_MUTATION;
    fields = subscriptionsFields.update;
    mutationResultObjectName = 'updateSubscription';
    resultMutationMessageSuccses = 'Абонемент обновлен';
    gqlQueryName = SUBSCRIPTION_QUERY;
    queryResultObjectName = 'Subscription';
  }

  return (
    <div>
      <SubscriptionOrder
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default SubscriptionsMutation;
