import subscriptionPayload from './fragmentSubscriptionPayload';
import subscriptionShortPayload from './fragmentSubscriptionShortPayload';

export default {
  subscriptionPayload,
  subscriptionShortPayload,
};
