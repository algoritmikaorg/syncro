import gql from 'graphql-tag';

export default gql`
  query CheckOrderCertAndSub($order: Json!) {
    checkOrderCertAndSub(
      order: $order,
    ) {
      resultOfChecking
      resultOfCalcOrder
    }
    allSettings (
      first: 1
    ) {
      acquiring {
        id
        commission
      }
    }
  }
`;
