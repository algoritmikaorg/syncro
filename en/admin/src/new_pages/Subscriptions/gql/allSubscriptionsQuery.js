import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllSubscriptionsQuery($filter: SubscriptionFilter, $first: Int, $skip: Int, $orderBy: SubscriptionOrderBy) {
    allSubscriptions(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...SubscriptionPayload
    }
    _allSubscriptionsMeta {
      count
    }
  }
  ${fragments.subscriptionPayload}
`;
