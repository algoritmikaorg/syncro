import gql from 'graphql-tag';

export default gql`
  mutation CreateSubscriptionMutation(
      $userId: ID,
      $draft: Boolean!,
      $title: String!,
      $productId: ID!,
      $payment: SubscriptionpaymentPayment
    ){
    createSubscription(
      userId: $userId,
      draft: $draft
      title: $title
      productId: $productId
      payment: $payment
    ) {
      id
    }
  }
`;
