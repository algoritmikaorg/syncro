import gql from 'graphql-tag';

export default gql`
  mutation UpdateSubscriptionMutation(
      $id: ID!,
      $userId: ID,
      $productId: ID!,

      $paymentId: ID!,
      $totalPrice: Float!
      $orderPrice: Float!,
      $discountPrice: Float!,
      $type: PaymentsTypes!,
      $status: PaymentStatus!,
      $discountsIds: [ID!],
      $additionalDiscountsIds: [ID!],
      $data: Json!
    ){
    updateSubscription(
      id: $id,
      userId: $userId,
      productId: $productId
    ) {
      id,
    }
    updatePayment(
      id: $paymentId,
      orderPrice: $orderPrice,
      discountPrice: $discountPrice,
      totalPrice: $totalPrice,
      type: $type,
      userId: $userId,
      status: $status,
      discountsIds: $discountsIds,
      additionalDiscountsIds: $additionalDiscountsIds,
      data: $data,
    ) {
      id
    }
  }
`;
