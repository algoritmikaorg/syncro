import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query subscriptionQuery($id: ID!) {
    Subscription(
      id: $id
    ) {
      ...SubscriptionPayload
    }
  }
  ${fragments.subscriptionPayload}
`;
