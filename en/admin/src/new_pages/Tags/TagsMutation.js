import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_TAG_MUTATION,
  EDIT_TAG_MUTATION,
  TAG_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import tagsFields from './fields';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const TagsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_TAG_MUTATION;
    fields = tagsFields.create;
    mutationResultObjectName = 'createTag';
    resultMutationMessageSuccses = 'Направление создано';
  } else {
    gqlMutationName = EDIT_TAG_MUTATION;
    fields = tagsFields.update;
    mutationResultObjectName = 'updateTag';
    resultMutationMessageSuccses = 'Направление обновлено';
    gqlQueryName = TAG_QUERY;
    queryResultObjectName = 'Tag';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default TagsMutation;
