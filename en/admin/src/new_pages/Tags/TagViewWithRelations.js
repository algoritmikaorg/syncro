import React from 'react';

import gql from './gql';

import {
  TAG_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import tagsFields from './fields';

const TagViewWithRelations = (props) => {
  const gqlQueryName = TAG_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Tag';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={tagsFields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default TagViewWithRelations;
