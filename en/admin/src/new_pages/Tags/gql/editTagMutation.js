import gql from 'graphql-tag';

export default gql`
  mutation UpdateTagMutation(
      $id: ID!,
      $alias: TagaliasAlias,
      $color: String!
      $textColor: String!
      $title: String!
    ){
    updateTag(
      id: $id,
      alias: $alias,
      color: $color,
      textColor: $textColor,
      title: $title,
    ) {
      id,
      createdAt,
      updatedAt,
      alias {
        id,
        alias,
      },
      color,
      textColor,
      title,
    }
  }
`;
