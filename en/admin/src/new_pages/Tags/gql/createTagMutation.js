import gql from 'graphql-tag';

export default gql`
  mutation CreateTagMutation(
      $alias: TagaliasAlias!
      $color: String!
      $textColor: String!
      $title: String!
    ){
    createTag(
      alias: $alias,
      color: $color,
      textColor: $textColor,
      title: $title,
    ) {
      id
    }
  }
`;
