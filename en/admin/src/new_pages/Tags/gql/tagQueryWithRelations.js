import gql from 'graphql-tag';

export default gql`
  query tagQueryWithRelations($id: ID!) {
    Tag(
      id: $id,
    ) {
      id
      createdAt
      updatedAt

      title
      alias {
        id
        alias
      }
      color
      textColor

      courses {
        id
        title
        price
        buyFull
      }
      cycles {
        id
        title
        price
        buyFull
      }
      lectures {
        id
        title
      }
      lecturers {
        id
        firstName
        lastName
      }

      _coursesMeta {
        count
      }
      _cyclesMeta {
        count
      }
      # _lecturersMeta {
      #   count
      # }
      _lecturersMeta {
        count
      }
      _lecturesMeta {
        count
      }
    }
  }
`;
