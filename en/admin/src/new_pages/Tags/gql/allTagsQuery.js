import gql from 'graphql-tag';

export default gql`
  query AllTagsQuery($filter: TagFilter, $first: Int, $skip: Int, $orderBy: TagOrderBy) {
    allTags(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
      alias {
        id,
        alias,
      },
      color,
      textColor,
      title,
    }
    _allTagsMeta {
      count
    }
  }
`;
