import gql from 'graphql-tag';

export default gql`
  query AllTagsQueryShort($filter: TagFilter, $first: Int, $skip: Int, $orderBy: TagOrderBy) {
    allTags(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      updatedAt,
      title,
    }
    _allTagsMeta {
      count
    }
  }
`;
