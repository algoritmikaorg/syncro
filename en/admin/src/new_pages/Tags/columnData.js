const columnData = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'color',
    label: 'Цвет фона',
  },
  {
    id: 'textColor',
    label: 'Цвет текста',
  },
  {
    id: 'alias',
    label: 'Алиас',
  },
];
export default columnData;
