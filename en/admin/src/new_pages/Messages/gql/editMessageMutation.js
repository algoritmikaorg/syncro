import gql from 'graphql-tag';

export default gql`
  mutation UpdateMessageMutation(
      $text: String
      $title: String!
    ){
    updateMessage(
      id: $id,
      text: $text,
      title: $title,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      text
    }
  }
`;
