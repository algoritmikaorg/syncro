import gql from 'graphql-tag';

export default gql`
  query AllMessagesQuery($filter: MessageFilter, $first: Int, $skip: Int, $orderBy: MessageOrderBy) {
    allMessages(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      text
    }
  }
`;
