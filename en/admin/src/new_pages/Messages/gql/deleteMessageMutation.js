import gql from 'graphql-tag';

export default gql`
  mutation deleteMessageMutation(
      $id: ID!
    ){
    deleteMessage(
      id: $id,
    ) {
      id
    }
  }
`;
