import gql from 'graphql-tag';

export default gql`
  query messageQuery($id: ID) {
    Message(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      text
    }
  }
`;
