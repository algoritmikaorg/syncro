import React from 'react';

import gql from './gql';

import {
  CREATE_MESSAGE_MUTATION,
  EDIT_MESSAGE_MUTATION,
  MESSAGE_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import messagesFields from './fields';

const MessagesMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_MESSAGE_MUTATION;
    fields = messagesFields.create;
    mutationResultObjectName = 'createMessage';
    resultMutationMessageSuccses = 'Партнер создан';
  } else {
    gqlMutationName = EDIT_MESSAGE_MUTATION;
    fields = messagesFields.update;
    mutationResultObjectName = 'updateMessage';
    resultMutationMessageSuccses = 'Партнер обновлен';
    gqlQueryName = MESSAGE_QUERY;
    queryResultObjectName = 'Message';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
      />
    </div>
  );
};

export default MessagesMutation;
