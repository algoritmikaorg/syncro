import React from 'react';

import gql from './gql';

import {
  MESSAGE_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const MessageViewWithRelations = (props) => {
  const gqlQueryName = MESSAGE_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Message';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default MessageViewWithRelations;
