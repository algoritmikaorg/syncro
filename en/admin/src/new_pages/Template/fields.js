const createScript = [
  {
    id: 'scriptHead',
    label: 'Скрипты в <head>',
    helperText: '<script>Ваш код</script> или <script src=\'\'></script>',
    type: 'multiline',
  },
  {
    id: 'scriptBody',
    label: 'Скрипты в <body>',
    helperText: '<script>Ваш код</script> или <script src=""></script>',
    type: 'multiline',
  },
];

const createStyle = [
  {
    id: 'styleHead',
    label: 'Стили в <head>',
    helperText: '<style>Ваш стиль</style> или <link rel="stylesheet" href="">',
    type: 'multiline',
  },
];

const createBlocks = [
  {
    id: 'contactTitle',
    label: 'Заголовок блока "Контакты"',
    helperText: 'Блок Контакты',
    type: 'multiline',
  },
  {
    id: 'contactOfficeTitle',
    label: 'Контакты - Название',
    helperText: 'Блок Контакты',
    type: 'multiline',
  },
  {
    id: 'contactOfficeAddress',
    label: 'Контакты - Адрес',
    helperText: 'Блок Контакты',
    type: 'multiline',
  },
  {
    id: 'contactOfficePhone',
    label: 'Контакты - Телефон',
    helperText: 'Блок Контакты',
    type: 'multiline',
  },
  {
    id: 'contactOfficeEmail',
    label: 'Контакты - Email',
    helperText: 'Блок Контакты',
    type: 'multiline',
  },
  {
    id: 'contactMapCenter',
    label: 'Контакты - Центр Карты Координаты',
    helperText: 'Блок Контакты, (определить координаты - https://yandex.ru/map-constructor/location-tool/)',
    type: 'multiline',
  },
  {
    id: 'contactMapPlacemark',
    label: 'Контакты - Координаты отметки',
    helperText: 'Блок Контакты, определить координаты - https://yandex.ru/map-constructor/location-tool/',
    type: 'multiline',
  },

  {
    id: 'metaKeywords',
    label: 'META Ключевые слова',
    helperText: '<meta name="keywords" content="ключевые слова" />',
    type: 'multiline',
  },

  {
    id: 'metaDescription',
    label: 'МЕТА Описание',
    helperText: '<meta name="description" content="описание" />',
    type: 'multiline',
  },

  {
    id: 'titleHead',
    label: 'HEAD Заголовок',
    helperText: '<head><title>Заголовок</title></head>',
    type: 'multiline',
  },
];

const updateScript = createScript.slice();
updateScript.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
);

const updateStyle = createStyle.slice();
updateStyle.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
);

const updateBlocks = createBlocks.slice();
updateBlocks.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
);

export default {
  createScript,
  updateScript,
  createStyle,
  updateStyle,
  createBlocks,
  updateBlocks,
};
