import {
  ALL_TEMPLATES_QUERY,
  CREATE_TEMPLATE_MUTATION,
  EDIT_TEMPLATE_MUTATION,
  TEMPLATE_QUERY,
} from './../../../constants';

import allTemplatesQuery from './allTemplatesQuery';
import createTemplateMutation from './createTemplateMutation';
import updateTemplateMutation from './updateTemplateMutation';
import templateQuery from './templateQuery';

const gql = {
  [ALL_TEMPLATES_QUERY]: allTemplatesQuery,
  [CREATE_TEMPLATE_MUTATION]: createTemplateMutation,
  [EDIT_TEMPLATE_MUTATION]: updateTemplateMutation,
  [TEMPLATE_QUERY]: templateQuery,
};

export default gql;
