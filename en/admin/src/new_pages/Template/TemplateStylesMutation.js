import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_TEMPLATE_MUTATION,
  EDIT_TEMPLATE_MUTATION,
  TEMPLATE_QUERY,
  ALL_TEMPLATES_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import templateFields from './fields';

const TemplateStylesMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_TEMPLATE_MUTATION;
    fields = templateFields.createStyle;
    mutationResultObjectName = 'createTemplate';
    resulMutatiomtMessageSuccses = 'Создано';
  } else {
    gqlMutationName = EDIT_TEMPLATE_MUTATION;
    fields = templateFields.updateStyle;
    mutationResultObjectName = 'updateTemplate';
    resulMutatiomtMessageSuccses = 'Обновлено';
    gqlQueryName = TEMPLATE_QUERY;
    gqlQueryItemName = 'Template';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        showActions
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default TemplateStylesMutation;
