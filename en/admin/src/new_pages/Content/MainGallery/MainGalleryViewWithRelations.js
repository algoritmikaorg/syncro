import React from 'react';

import gql from './gql';

import {
  IMAGE_QUERY_WITH_RELATIONS,
} from './../../../constants';

import ViewEntityWithRelations from './../../../new_components/ViewEntityWithRelations';

import fields from './fields';

const MainGalleryViewWithRelations = (props) => {

  const gqlQueryName = IMAGE_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Image';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default MainGalleryViewWithRelations;
