import React from 'react';
import { graphql } from 'react-apollo';

import gql from './gql';

import {
  CREATE_IMAGE_MUTATION,
  UPDATE_IMAGE_MUTATION,
  IMAGE_QUERY,
  GALLERY_OF_TYPE_QUERY,
} from './../../../constants';

import MutationEntities from './../../../new_components/MutationEntities';

import imagesFields from './fields';
import imagesActions from './actions';
import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';
// class MainGalleryMutation {
//   constructor(props) {
//     super(props);
//     this.state = { galleryId};
//   }
// }

const MainGalleryMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_IMAGE_MUTATION;
    fields = imagesFields.create;
    mutationResultObjectName = 'createImage';
    resultMutationMessageSuccses = 'Изображение создано';
  } else {
    gqlMutationName = UPDATE_IMAGE_MUTATION;
    fields = imagesFields.update;
    mutationResultObjectName = 'updateImage';
    resultMutationMessageSuccses = 'Изображение обновлено';
    gqlQueryName = IMAGE_QUERY;
    queryResultObjectName = 'Image';
  }

  if (props.data.Gallery) {
    fields = fields.map((field) => {
      if (field.id === 'galleryId') {
        return Object.assign({}, field, { defaultValue: props.data.Gallery.id });
      }
      return field;
    });
  }


  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        onSuccessMutation={handleSuccessMutation}
        modifyMutationOptions={modifyMutationOptions}
      />
    </div>
  );
};

export default graphql(gql[GALLERY_OF_TYPE_QUERY], {
  options: { variables: { type: 'maingallery' } },
})(MainGalleryMutation);
