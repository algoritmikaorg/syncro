import * as R from 'ramda';
import { format } from './../../../utils';

const create = [
  {
    id: 'secret',
    label: 'Изображение',
    defaultValue: 'no-photo',
    size: 'x271',
    type: 'image',
  },
  {
    id: 'description',
    label: 'Описание',
    type: 'multiline',
  },
  {
    id: 'galleryId',
    defaultValue: '',
    label: 'ID Галереи',
    type: 'multiline',
    required: true,
    disabled: true,
  },
];


const update = create.slice();
// update.splice(-1, 1)
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const view = update.slice();

const filter = [
  {
    id: 'description',
    label: 'Описание',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
