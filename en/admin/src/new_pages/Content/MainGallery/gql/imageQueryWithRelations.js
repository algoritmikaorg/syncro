import gql from 'graphql-tag';

export default gql`
  query($id: ID!) {
    Image(id: $id){
      id
      secret
      description
    }
  }
`;
