const modifyQueryFilter = (input = {}) => {
  const filter = {
    gallery: {
      type: 'maingallery',
    },
  };

  if (input.description) filter.description_contains = input.description;

  return filter;
};

export default modifyQueryFilter;
