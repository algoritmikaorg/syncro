import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllAboutPageQuery {
    allAboutPages(
      first: 1,
    ) {
      ...AboutPagePayload
    }
  }
  ${fragments.aboutPagePayload}
`;
