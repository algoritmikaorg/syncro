import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  mutation UpdateAboutPageMutation(
      $id: ID!
      $title: String!
      $subTitle: String

      $img1240x430: String

      $description: String

      $title1: String
      $description1: String
      $title2: String
      $description2: String
      $title3: String
      $description3: String

      $titleFigures: String
      $fig1: String
      $caption1: String
      $fig2: String
      $caption2: String
      $fig3: String
      $caption3: String
      $fig4: String
      $caption4: String

      $titleImportant: String
      $subTitleImportant1: String
      $descriptionImportant1: String
      $subTitleImportant2: String
      $descriptionImportant2: String
      $subTitleImportant3: String
      $descriptionImportant3: String

      $titleTeam: String

      $titleThanks: String
      $peopleThanks: [String!]
    ){
    updateAboutPage(
      id: $id
      title: $title
      subTitle: $subTitle

      img1240x430: $img1240x430

      description: $description

      title1: $title1
      description1: $description1
      title2: $title2
      description2: $description2
      title3: $title3
      description3: $description3

      titleFigures: $titleFigures
      fig1: $fig1
      caption1: $caption1
      fig2: $fig2
      caption2: $caption2
      fig3: $fig3
      caption3: $caption3
      fig4: $fig4
      caption4: $caption4

      titleImportant: $titleImportant
      subTitleImportant1: $subTitleImportant1
      descriptionImportant1: $descriptionImportant1
      subTitleImportant2: $subTitleImportant2
      descriptionImportant2: $descriptionImportant2
      subTitleImportant3: $subTitleImportant3
      descriptionImportant3: $descriptionImportant3

      titleTeam: $titleTeam

      titleThanks: $titleThanks
      peopleThanks: $peopleThanks
    ) {
      ...AboutPagePayload
    }
  }
  ${fragments.aboutPagePayload}
`;
