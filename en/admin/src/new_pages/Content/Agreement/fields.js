const create = [
  {
    id: 'title',
    label: 'Заголовок',
    type: 'multiline',
    required: true,
  },
  {
    id: 'description',
    label: 'Основной текст',
    type: 'multiline',
    required: true,
  },
  // {
  //   id: 'type',
  //   label: 'Тип',
  //   required: true,
  //   defaultValue: "agreement",
  // },
];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
);

export default {
  create,
  update,
};
