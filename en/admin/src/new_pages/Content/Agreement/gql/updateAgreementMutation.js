import gql from 'graphql-tag';

export default gql`
  mutation UpdateagreementMutation(
      $id: ID!,
      $title: String,
      $description: String,
    ){
    updateSimpleContent(
      id: $id,
      title: $title,
      description: $description,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      description,
    }
  }
`;
