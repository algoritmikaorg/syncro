import gql from 'graphql-tag';

export default gql`
  mutation CreateAgreementMutation(
      $title: String!,
      $description: String!,
    ){
    createSimpleContent(
      type: "agreement",
      title: $title,
      description: $description,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      description,
    }
  }
`;
