import gql from 'graphql-tag';

export default gql`
  mutation UpdateMainPageMutation(
      $id: ID!
      $galleryTitle: String
      $howWorksTitle: String
      $lecturersDescription: String
      $lecturersTitle: String
      $locationDescription: String
      $locationTitle: String
      $partnerTitle: String
      $recomendedDescription: String
      $recomendedTitle: String
      $recommendedLecturesIds: [ID!]
      $reviewsTitle: String
      $scheduleTitle: String
      $subTitle: String
      $title: String
    ){
    updateMainPage(
      id: $id,
      galleryTitle: $galleryTitle,
      howWorksTitle: $howWorksTitle,
      lecturersDescription: $lecturersDescription,
      lecturersTitle: $lecturersTitle,
      locationDescription: $locationDescription,
      locationTitle: $locationTitle,
      partnerTitle: $partnerTitle,
      recomendedDescription: $recomendedDescription,
      recomendedTitle: $recomendedTitle,
      recommendedLecturesIds: $recommendedLecturesIds,
      reviewsTitle: $reviewsTitle,
      scheduleTitle: $scheduleTitle,
      subTitle: $subTitle,
      title: $title,
    ) {
      id
    }
  }
`;
