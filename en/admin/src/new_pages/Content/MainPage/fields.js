const create = [
  {
    id: 'title',
    label: 'Заголовок',
    helperText: 'Блок под меню',
    type: 'multiline',
  },
  {
    id: 'subTitle',
    label: 'Подзаголовок',
    helperText: 'Блок под меню',
    type: 'multiline',
  },
  {
    id: 'recomendedTitle',
    label: 'Заголовок блока "Синхронизация рекомендует"',
    type: 'multiline',
  },
  {
    id: 'recomendedDescription',
    label: 'Описание блока "Синхронизация рекомендует"',
    type: 'multiline',
  },

  {
    id: 'lecturersTitle',
    label: 'Заголовок блока "Познакомьтесь с нашими лекторами"',
    type: 'multiline',
  },
  {
    id: 'lecturersDescription',
    label: 'Описание блока "Познакомьтесь с нашими лекторами"',
    type: 'multiline',
  },

  {
    id: 'howWorksTitle',
    label: 'Заголовок блока "Как проходит синхронизация"',
    type: 'multiline',
  },
  {
    id: 'scheduleTitle',
    label: 'Заголовок блока "Расписание на эту неделю"',
    type: 'multiline',
  },
  {
    id: 'galleryTitle',
    label: 'Заголовок блока "Фотографии с мероприятий"',
    type: 'multiline',
  },
  {
    id: 'reviewsTitle',
    label: 'Заголовок блока "Отзывы"',
    type: 'multiline',
  },

  {
    id: 'locationTitle',
    label: 'Заголовок блока "Наши площадки"',
    type: 'multiline',
  },
  {
    id: 'locationDescription',
    label: 'Описание блока "Наши площадки"',
    type: 'multiline',
  },

  {
    id: 'partnerTitle',
    label: 'Заголовок блока "Наши партнеры"',
    type: 'multiline',
  },
  {
    id: 'recommendedLectures',
    label: 'Рекомендованные лекции',
    relation: 'recommendedLectures',
    type: 'relation',
    multiple: 'true',
    disabled: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
);

export default {
  create,
  update,
};
