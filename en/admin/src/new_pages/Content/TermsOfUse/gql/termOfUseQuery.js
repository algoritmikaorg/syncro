import gql from 'graphql-tag';

export default gql`
  query SimpleContentQuery {
    SimpleContent(
      type: "termOfUse"
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      description,
    }
  }
`;
