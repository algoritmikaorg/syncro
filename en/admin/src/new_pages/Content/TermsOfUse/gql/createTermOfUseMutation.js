import gql from 'graphql-tag';

export default gql`
  mutation CreateTermOfUseMutation(
      $title: String!,
      $description: String!,
    ){
    createSimpleContent(
      type: "termOfUse",
      title: $title,
      description: $description,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      description,
    }
  }
`;
