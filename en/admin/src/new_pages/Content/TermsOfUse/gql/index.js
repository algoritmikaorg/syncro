import {
  TERM_OF_USE_QUERY,
  CREATE_TERM_OF_USE_MUTATION,
  EDIT_TERM_OF_USE_MUTATION,
} from './../../../../constants';

import termOfUseQuery from './termOfUseQuery';
import createTermOfUseMutation from './createTermOfUseMutation';
import updateTermOfUseMutation from './updateTermOfUseMutation';

const gql = {
  [TERM_OF_USE_QUERY]: termOfUseQuery,
  [CREATE_TERM_OF_USE_MUTATION]: createTermOfUseMutation,
  [EDIT_TERM_OF_USE_MUTATION]: updateTermOfUseMutation,
};

export default gql;
