export default {
  simpleContent() {
    return (
      {
        refetchQueries: [
          'SimpleContentQuery',
        ],
      }
    );
  },
  aboutPage() {
    return (
      {
        refetchQueries: [
          'AllAboutPageQuery',
          'AboutPageQuery',
        ],
      }
    );
  },
  howWorkPage() {
    return (
      {
        refetchQueries: [
          'AllHowWorkPages',
        ],
      }
    );
  },
  images() {
    return (
      {
        refetchQueries: [
          'AllImagesQuery',
        ],
      }
    );
  },
  mainPage() {
    return (
      {
        refetchQueries: [
          'AllMainPageQuery',
        ],
      }
    );
  },
};
