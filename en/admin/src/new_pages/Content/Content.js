import React from 'react';

import {
  ALL_ABOUTPAGE_QUERY,
  ALL_MAINPAGE_QUERY,
  ALL_HOWWORKPAGES_QUERY,
  TERM_OF_USE_QUERY,
  AGREEMENT_QUERY,
} from './../../constants';

import gqlAboutPage from './AboutPage/gql';
import gqlMainPage from './MainPage/gql';
import gqlHowWorks from './HowWorks/gql';
import gqlTermOfUse from './TermsOfUse/gql';
import gqlagreement from './Agreement/gql';

import filterHowWorks from './HowWorks/filter';

import EntityQueryAndMutationRedirect from './../../new_components/EntityQueryAndMutationRedirect';

const defaultFilter = () => ({});

const Content = (props) => {
  const { typeMaterial } = props;
  let gqlQueryName = null;
  let queryResultObjectName = null;
  let gql = null;
  let modifyQueryFilter = null;

  switch (typeMaterial) {
    case 'about': {
      gqlQueryName = ALL_ABOUTPAGE_QUERY;
      queryResultObjectName = 'allAboutPages';
      gql = gqlAboutPage;
      modifyQueryFilter = defaultFilter;
      break;
    }
    case 'howwork-mainpage': {
      gqlQueryName = ALL_HOWWORKPAGES_QUERY;
      queryResultObjectName = 'allHowWorkPages';
      gql = gqlHowWorks;
      modifyQueryFilter = filterHowWorks.filterHowWokMainPage;
      break;
    }
    case 'howwork-course': {
      gqlQueryName = ALL_HOWWORKPAGES_QUERY;
      queryResultObjectName = 'allHowWorkPages';
      gql = gqlHowWorks;
      modifyQueryFilter = filterHowWorks.filterHowWokCourse;
      break;
    }
    case 'howwork-lecture': {
      gqlQueryName = ALL_HOWWORKPAGES_QUERY;
      queryResultObjectName = 'allHowWorkPages';
      gql = gqlHowWorks;
      modifyQueryFilter = filterHowWorks.filterHowWokLecture;
      break;
    }
    case 'termOfUse': {
      gqlQueryName = TERM_OF_USE_QUERY;
      queryResultObjectName = 'SimpleContent';
      gql = gqlTermOfUse;
      modifyQueryFilter = defaultFilter;
      break;
    }
    case 'agreement': {
      gqlQueryName = AGREEMENT_QUERY;
      queryResultObjectName = 'SimpleContent';
      gql = gqlagreement;
      modifyQueryFilter = defaultFilter;
      break;
    }
    default: {
      // main page
      gqlQueryName = ALL_MAINPAGE_QUERY;
      queryResultObjectName = 'allMainPages';
      gql = gqlMainPage;
      modifyQueryFilter = defaultFilter;
    }
  }

  return (
    <div>
      <EntityQueryAndMutationRedirect
        {...props}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryFilter={modifyQueryFilter}
      />
    </div>
  );
};

export default Content;
