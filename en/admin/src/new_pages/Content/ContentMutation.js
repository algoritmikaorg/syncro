import React from 'react';

import {
  CREATE_ABOUTPAGE_MUTATION,
  EDIT_ABOUTPAGE_MUTATION,
  ABOUTPAGE_QUERY,
  ALL_ABOUTPAGE_QUERY,

  CREATE_MAINPAGE_MUTATION,
  EDIT_MAINPAGE_MUTATION,
  MAINPAGE_QUERY,
  ALL_MAINPAGE_QUERY,

  ALL_HOWWORKPAGES_QUERY,
  CREATE_HOWWORKPAGE_MUTATION,
  EDIT_HOWWORKPAGE_MUTATION,
  HOWWORKPAGE_QUERY,

  AGREEMENT_QUERY,
  CREATE_AGREEMENT_MUTATION,
  EDIT_AGREEMENT_MUTATION,

  TERM_OF_USE_QUERY,
  CREATE_TERM_OF_USE_MUTATION,
  EDIT_TERM_OF_USE_MUTATION,
} from './../../constants';

import gqlAboutPage from './AboutPage/gql';
import gqlMainPage from './MainPage/gql';
import gqlHowWorks from './HowWorks/gql';
import gqlagreement from './Agreement/gql';
import gqlTermOfUse from './TermsOfUse/gql';

import MutationEntities from './../../new_components/MutationEntities';

import aboutPageFields from './AboutPage/fields';
import mainPageFields from './MainPage/fields';
import howWorkPageFields from './HowWorks/fields';
import termOfUseFields from './TermsOfUse/fields';
import agreementFields from './Agreement/fields';

import modifyMutationOptions from './modifyMutationOptions';
import handleSuccessMutation from './handleSuccessMutation';

const ContentMutation = (props) => {
  const {
    mutationType,
    typeMaterial,
  } = props;

  let currentModifyMutationOptions = null;
  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;
  let gql = null;
  let formTitle = null;

  if (mutationType === 'create') {
    resultMutationMessageSuccses = 'Создано';
  } else {
    resultMutationMessageSuccses = 'Обновлено';
  }

  switch (typeMaterial) {
    case 'about': {
      gql = gqlAboutPage;
      formTitle = 'О проекте';
      currentModifyMutationOptions = modifyMutationOptions.aboutPage;

      if (mutationType === 'create') {
        gqlMutationName = CREATE_ABOUTPAGE_MUTATION;
        fields = aboutPageFields.create;
        mutationResultObjectName = 'createAboutPage';
      } else {
        gqlMutationName = EDIT_ABOUTPAGE_MUTATION;
        fields = aboutPageFields.update;
        mutationResultObjectName = 'updateAboutPage';
        gqlQueryName = ABOUTPAGE_QUERY;
        queryResultObjectName = 'AboutPage';
      }
      break;
    }
    case 'howwork-mainpage': {
      gql = gqlHowWorks;
      formTitle = 'Как мы работаем - Главная страница';
      currentModifyMutationOptions = modifyMutationOptions.howWorkPage;

      if (mutationType === 'create') {
        gqlMutationName = CREATE_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.createHWMainPage;
        mutationResultObjectName = 'createHowWorkPage';
      } else {
        gqlMutationName = EDIT_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.updateHWMainPage;
        mutationResultObjectName = 'updateHowWorkPage';
        gqlQueryName = HOWWORKPAGE_QUERY;
        queryResultObjectName = 'HowWorkPage';
      }
      break;
    }
    case 'howwork-course': {
      gql = gqlHowWorks;
      formTitle = 'Как мы работаем - Курс/Цикл';
      currentModifyMutationOptions = modifyMutationOptions.howWorkPage;

      if (mutationType === 'create') {
        gqlMutationName = CREATE_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.createHWCourse;
        mutationResultObjectName = 'createHowWorkPage';
      } else {
        gqlMutationName = EDIT_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.updateHWCourse;
        mutationResultObjectName = 'updateHowWorkPage';
        gqlQueryName = HOWWORKPAGE_QUERY;
        queryResultObjectName = 'HowWorkPage';
      }
      break;
    }
    case 'howwork-lecture': {
      gql = gqlHowWorks;
      formTitle = 'Как мы работаем - Лекция';
      currentModifyMutationOptions = modifyMutationOptions.howWorkPage;

      if (mutationType === 'create') {
        gqlMutationName = CREATE_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.createHWLecture;
        mutationResultObjectName = 'createHowWorkPage';
      } else {
        gqlMutationName = EDIT_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.updateHWLecture;
        mutationResultObjectName = 'updateHowWorkPage';
        gqlQueryName = HOWWORKPAGE_QUERY;
        queryResultObjectName = 'HowWorkPage';
      }
      break;
    }
    case 'agreement': {
      gql = gqlagreement;
      formTitle = 'Соглашение об обработке персональных данных';
      currentModifyMutationOptions = modifyMutationOptions.simpleContent;

      if (mutationType === 'create') {
        gqlMutationName = CREATE_AGREEMENT_MUTATION;
        fields = agreementFields.create;
        mutationResultObjectName = 'createSimpleContent';
      } else {
        gqlMutationName = EDIT_AGREEMENT_MUTATION;
        fields = agreementFields.update;
        mutationResultObjectName = 'updateSimpleContent';
        gqlQueryName = AGREEMENT_QUERY;
        queryResultObjectName = 'SimpleContent';
      }
      break;
    }
    case 'termOfUse': {
      gql = gqlTermOfUse;
      formTitle = 'Пользовательское соглашение';
      currentModifyMutationOptions = modifyMutationOptions.simpleContent;

      if (mutationType === 'create') {
        gqlMutationName = CREATE_TERM_OF_USE_MUTATION;
        fields = termOfUseFields.create;
        mutationResultObjectName = 'createSimpleContent';
      } else {
        gqlMutationName = EDIT_TERM_OF_USE_MUTATION;
        fields = termOfUseFields.update;
        mutationResultObjectName = 'updateSimpleContent';
        gqlQueryName = TERM_OF_USE_QUERY;
        queryResultObjectName = 'SimpleContent';
      }
      break;
    }
    default: {
      gql = gqlMainPage;
      formTitle = 'Главная страница';
      currentModifyMutationOptions = modifyMutationOptions.mainPage;
      if (mutationType === 'create') {
        gqlMutationName = CREATE_MAINPAGE_MUTATION;
        fields = mainPageFields.create;
        mutationResultObjectName = 'createMainPage';
      } else {
        gqlMutationName = EDIT_MAINPAGE_MUTATION;
        fields = mainPageFields.update;
        mutationResultObjectName = 'updateMainPage';
        gqlQueryName = MAINPAGE_QUERY;
        queryResultObjectName = 'MainPage';
      }
    }
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        formTitle={formTitle}
        modifyMutationOptions={currentModifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default ContentMutation;
