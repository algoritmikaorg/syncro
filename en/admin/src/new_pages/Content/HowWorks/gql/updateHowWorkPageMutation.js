import gql from 'graphql-tag';

export default gql`
  mutation UpdateHowWorkMutation(
      $id: ID!
      $page: String!,
      $titleFirstBlock: String,
      $textFirstBlock: String,
      $titleSecondBlock: String,
      $textSecondBlock: String,
      $titleThreeBlock: String,
      $textThreeBlock: String,
      $titleFourBlock: String,
      $textFourBlock: String,
    ){
    updateHowWorkPage(
      id: $id,
      page: $page,
      titleFirstBlock: $titleFirstBlock,
      textFirstBlock: $textFirstBlock,
      titleSecondBlock: $titleSecondBlock,
      textSecondBlock: $textSecondBlock,
      titleThreeBlock: $titleThreeBlock,
      textThreeBlock: $textThreeBlock,
      titleFourBlock: $titleFourBlock,
      textFourBlock: $textFourBlock,
    ) {
      id
      createdAt
      updatedAt

      page

      titleFirstBlock
      textFirstBlock

      titleSecondBlock
      textSecondBlock

      titleThreeBlock
      textThreeBlock

      titleFourBlock
      textFourBlock
    }
  }
`;
