import gql from 'graphql-tag';

export default gql`
  query HowWorkPageQuery($id: ID!) {
    HowWorkPage(
      id: $id
    ) {
      id,
      createdAt,
      updatedAt,

      page

      titleFirstBlock
      textFirstBlock

      titleSecondBlock
      textSecondBlock

      titleThreeBlock
      textThreeBlock

      titleFourBlock
      textFourBlock
    }
  }
`;
