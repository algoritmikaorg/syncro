import {
  ALL_HOWWORKPAGES_QUERY,
  CREATE_HOWWORKPAGE_MUTATION,
  EDIT_HOWWORKPAGE_MUTATION,
  HOWWORKPAGE_QUERY,
} from './../../../../constants';

import allHowWorkPageQuery from './allHowWorkPageQuery';
import createHowWorkPageMutation from './createHowWorkPageMutation';
import updateHowWorkPageMutation from './updateHowWorkPageMutation';
import howWorkPageQuery from './howWorkPageQuery';

const gql = {
  [ALL_HOWWORKPAGES_QUERY]: allHowWorkPageQuery,
  [CREATE_HOWWORKPAGE_MUTATION]: createHowWorkPageMutation,
  [EDIT_HOWWORKPAGE_MUTATION]: updateHowWorkPageMutation,
  [HOWWORKPAGE_QUERY]: howWorkPageQuery,
};

export default gql;
