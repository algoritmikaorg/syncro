import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import Typography from 'material-ui/Typography';

import FilterOfQuery from './../../new_components/FilterOfQuery';
import withProgress from './../../new_hoc/withProgress';
import { formatInputDataArrayForTable, formatOutputFormData } from './../../new_utils';

import Events from './Events';

import gql from './gql';
import {
  EVENT_QUERY,
} from './../../constants';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';

class EventsViewWithRelations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data || null,
      isRequested: true,
    };
  }
  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
    let isRequested = false
    if (data && data.loading) {
      isRequested = true;
    }
    if (data && data.error) {
      isRequested = true;
      this.props.snackbar(data.error);
      this.setState({
        isRequested: false,
      });
      return false;
    }
    if (data && data.Event) {
      isRequested = false;
    }

    this.setState({
      isRequested,
      data: data.Event || null,
    });
  }

  render() {
    const { title } = this.props;
    const { data, isRequested } = this.state;

    return (
      <div>
        <Typography type="display1" gutterBottom>
          Мероприятие
        </Typography>
      </div>
    );
  }
}

export default compose(
  graphql(gql[EVENT_QUERY], {
    options: (ownProps) => {
      const { match } = ownProps;
      const { id } = match.params;
      return {
        variables: {
          id,
        },
      };
    },
  }),
  withProgress([EVENT_QUERY]),
)(EventsViewWithRelations);
