import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';

import withProgress from './../../new_hoc/withProgress';
import withSnackbar from './../../new_hoc/withSnackbar';

const Query = (props) => {
  const {
    gql,
    gqlQueryName,
    children,
    modifyQueryFilter
  } = props;
  const query = gql[gqlQueryName];

  // Enhancer function.
  const withQuery = graphql(
    query,
    {
      name: gqlQueryName,
      options: (ownProps) => {
        return {
          variables: {
            filter: modifyQueryFilter(ownProps.allQueryFilter, ownProps.filterType),
          },
        };
      },
    },
  );
  // Enhance our component.
  const WithData = compose(
    withQuery,
    withSnackbar(),
    withProgress([gqlQueryName])
  )(ownProps => React.cloneElement(children, ownProps));

  // Return the enhanced component.
  return (
    // <WithData />
    <WithData {...props} />
  );
};

Query.defaultProps = {
  modifyQueryFilter() { return { variables: {} }; },
};

export default Query;
