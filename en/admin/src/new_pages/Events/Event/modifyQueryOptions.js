import { appendProp } from './../../../new_utils';

const registeredParticipants = (ownProps, formFilterData) => {
  const { match } = ownProps;
  const { id } = match.params;
  return { variables: { id } };
};

const arrivingParticipants = (ownProps, formFilterData) => {
  const { match } = ownProps;
  const { id } = match.params;
  return { variables: { filter: { event: { id } } } };
};

export default {
  registeredParticipants,
  arrivingParticipants,
};
