import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';

import EventData from './EventData';
import RegisteredParticipants from './RegisteredParticipants';
import ArrivingParticipants from './ArrivingParticipants';
import TabsContainer from './../../../new_containers/TabsContainer';

import tabs from './tabs';

const Event = (props) => {
  return (
    <div>
      <EventData {...props} />
      <TabsContainer
        tabs={tabs}
      >
        {
          [
            <RegisteredParticipants {...props} key={tabs[0].id} />,
            <ArrivingParticipants {...props} key={tabs[0].id} />,
          ]
        }
      </TabsContainer>
    </div>
  );
};

export default Event;
