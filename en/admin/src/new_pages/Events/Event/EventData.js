import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import Typography from 'material-ui/Typography';
import moment from 'moment';

import withProgress from './../../../new_hoc/withProgress';
import Actions from './../../../new_components/Actions';

import gql from './../gql';
import {
  PARTICIPANTS_OF_EVENT_QUERY,
} from './../../../constants';
import fields from './fields';


import actions from './actions';

const EventData = (props) => {
  const { data } = props;

  if (!data.Event) return <div></div>;

  const {
    date,
    lecture,
    lecturers,
    location,
    curator,
    price,
    quantityOfTickets,
    quantityOfTicketsAvailable,
    _ticketsMeta,
    _ordersMeta,
  } = data.Event;

  return (
    <div>
      <Typography type="title" gutterBottom>
        {moment(date).format('LLLL')}
      </Typography>
      <Typography type="title" gutterBottom>
        <strong>Лекция: </strong>{lecture.title}
      </Typography>
      {
        lecture.course && lecture.course.length > 0 &&
        (
          <Typography type="body1" gutterBottom>
            <strong>Курс: </strong>{lecture.course[0].title}
          </Typography>
        )
      }
      {
        lecture.cycle && lecture.cycle.length > 0 &&
        (
          <Typography type="body1" gutterBottom>
            <strong>Цикл: </strong>{lecture.cycle[0].title}
          </Typography>
        )
      }
      <Typography type="body1" gutterBottom>
        <strong>Лекторы: </strong>
        {
          lecturers
            .map(({ firstName, lastName }) => `${firstName} ${lastName}`)
            .join(', ')
        }
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Локация: </strong>{location.title} (м. {location.metro}, {location.address})
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Координатор: </strong>{curator.firstName} {curator.lastName}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Цена: </strong>{price}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>
          Количество билетов:
        </strong> {quantityOfTickets}
        {/* (доступно: {quantityOfTicketsAvailable}) */}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>
          Количество доступных билетов:
        </strong> {quantityOfTickets}
        {/* (доступно: {quantityOfTicketsAvailable}) */}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Количество заказов: </strong>{_ordersMeta.count}
      </Typography>
      <Typography type="body1" gutterBottom>
        <strong>Количество купленных билетов: </strong>{_ticketsMeta.count}
      </Typography>
      <Actions items={actions} />
    </div>
  );
};

export default compose(
  graphql(gql[PARTICIPANTS_OF_EVENT_QUERY], {
    options: (ownProps) => {
      const { match } = ownProps;
      const { id } = match.params;
      return {
        variables: {
          id,
        },
      };
    },
  }),
  withProgress([PARTICIPANTS_OF_EVENT_QUERY]),
)(EventData);
