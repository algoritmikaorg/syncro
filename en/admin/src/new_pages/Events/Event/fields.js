import { PAYMENT_STATUS } from './../../../enum';

const filter = [
  {
    id: 'firstName',
    label: 'Имя',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
  },
  {
    id: 'phone',
    label: 'Телефон',
  },
  {
    id: 'email',
    label: 'email',
  },
];

// оплатил на месте, оплата через сайт, сертификат, абонемент, без оплаты, подарок
const registeredParticipantsFilter = filter.slice();
registeredParticipantsFilter.push(
  {
    id: 'paymentType',
    label: 'Тип оплаты',
    type: 'select',
    enum: [
      {
        value: 'CASH',
        label: 'Оплатил на месте',
      },
      {
        value: 'INTERNET_ACQUIRING',
        label: 'Оплата через сайт',
      },
      {
        value: 'certificate',
        label: 'Сертификат',
      },
      {
        value: 'subscription',
        label: 'Абонемент',
      },
      {
        value: 'pending',
        label: 'Без оплаты',
      },
      {
        value: 'gift',
        label: 'Подарок(скидка 100%)',
      },
    ],
  },
  {
    id: 'paymentStatus',
    label: 'Статус платежа',
    type: 'select',
    enum: PAYMENT_STATUS,
  },
);

const registeredParticipants = {
  filter: registeredParticipantsFilter,
};

const arrivingParticipants = {
  filter: filter.slice(),
};

export default {
  registeredParticipants,
  arrivingParticipants,
};
