import React from 'react';

import gql from './gql';

import {
  ALL_EVENTS_QUERY,
} from './../../constants';

import ListEntitiesQuery from './../../new_components/ListEntitiesQuery';

import { tabsExp } from './../../new_utils';

import fields from './fields';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Discounts = (props) => {
  const gqlQueryName = ALL_EVENTS_QUERY;
  const queryResultObjectName = 'allEvents';

  return (
    <div>
      <ListEntitiesQuery
        {...props}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryFilter={modifyQueryFilter}
        tabs={tabsExp}
        orderBy="date"
      />
    </div>
  );
};

export default Discounts;
