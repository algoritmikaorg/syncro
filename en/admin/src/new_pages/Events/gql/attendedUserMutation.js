import gql from 'graphql-tag';

export default gql`
  mutation AttendedUserMutation(
      $id: ID!,
      $attended: Boolean!,
    ){
    updateTicket(
      id: $id,
      attended: $attended,
    ) {
      id
      number
      attended
      user {
        id
        firstName
        lastName
        email
        phone
      }
    }
  }
`;
