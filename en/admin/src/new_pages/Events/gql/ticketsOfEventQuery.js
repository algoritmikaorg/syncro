import gql from 'graphql-tag';

export default gql`
  query TicketsOfEventQuery(
    $filter: TicketFilter,
  ) {
    allTickets(
      filter: $filter,
    ) {
      id
      number
      attended
      user {
        id
        firstName
        lastName
        email
        phone
      }
    }
  }
`;
