import gql from 'graphql-tag';

export default gql`
  query ParticipantsOfEventQuery(
    $id: ID!,
    $participantsFilter: UserFilter,
    $orderFilter: OrderFilter,
  ) {
    Event (
      id: $id
    ) {
      orders (
        filter: $orderFilter
      ) {
        id
        participants (
          filter: $participantsFilter
        ) {
          id
          email
          firstName
          lastName
          phone
        }
        payment {
          id
          status
          type
          subscriptionsInPayments {
            title
          }
          certificatesInPayments {
            title
          }
          discounts {
            title
            rate
          }
          additionalDiscounts {
            title
            rate
          }
        }
      }
      date
      price
      lecture {
        id
        title
        course {
          id
          title
        }
        cycle {
          id
          title
        }
      }
      lecturers {
        id
        firstName
        lastName
      }
      location {
        id
        title
        address
        metro
      }
      curator {
        id
        firstName
        lastName
        email
      }
      quantityOfTickets
      quantityOfTicketsAvailable
      _ticketsMeta {
        count
      }
      _ordersMeta {
        count
      }
    }
  }
`;
