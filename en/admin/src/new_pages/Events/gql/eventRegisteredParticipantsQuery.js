import gql from 'graphql-tag';

export default gql`
  query EventRegisteredParticipantsQuery(
    $id: ID!,
    $orderFilter: OrderFilter,
  ) {
    Event(
      id: $id,
    ) {
      id
      orders (
        filter: $orderFilter
      ) {
        id
        participants {
          id
          firstName
          lastName
        }
        payment {
          status
          discountPrice
          totalPrice
          orderPrice
          additionalDiscounts {
            rate
          }
          discounts {
            rate
          }
          subscriptionsInPayments {
            title
          }
          certificatesInPayments {
            title
          }
        }
      }
    }
  }
`;
