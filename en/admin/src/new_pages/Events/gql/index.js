import {
  ALL_EVENTS_QUERY_SHORT,
  ALL_EVENTS_QUERY,
  EVENT_QUERY,
  EVENT_REGISTERED_PARTICIPANTS_QUERY,
  PARTICIPANTS_OF_EVENT_QUERY,
  TICKETS_OF_EVENT_QUERY,
  ATTENDED_USER_MUTATION,
} from './../../../constants';

import allEventsQuery from './allEventsQuery';
import allEventsQueryShort from './allEventsQueryShort';
import eventQuery from './eventQuery';
import eventRegisteredParticipantsQuery from './eventRegisteredParticipantsQuery';
import participantsOfEventQuery from './participantsOfEventQuery';
import ticketsOfEventQuery from './ticketsOfEventQuery';
import attendedUserMutation from './attendedUserMutation';

export default {
  [ALL_EVENTS_QUERY]: allEventsQuery,
  [ALL_EVENTS_QUERY_SHORT]: allEventsQueryShort,
  [EVENT_QUERY]: eventQuery,
  [EVENT_REGISTERED_PARTICIPANTS_QUERY]: eventRegisteredParticipantsQuery,
  [PARTICIPANTS_OF_EVENT_QUERY]: participantsOfEventQuery,
  [TICKETS_OF_EVENT_QUERY]: ticketsOfEventQuery,
  [ATTENDED_USER_MUTATION]: attendedUserMutation,
};
