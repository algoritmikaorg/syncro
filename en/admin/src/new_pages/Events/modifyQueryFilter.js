import moment from 'moment';

const modifyQueryFilter = (props, input = {}) => {
  const filter = {};
  const {
    date,
    lecturersIds,
    locationsIds,
    lectureType,
    curatorIds,
    tab,
  } = input;

  // if (date && new Date(date) >= new Date()) {
  //   filter = {
  //     date_gte: date,
  //   };
  // } else {
  //   filter = {
  //     date_gte: new Date(),
  //   };
  // }

  if (lecturersIds && lecturersIds.length > 0) {
    filter.lecturers_some = {
      id_in: lecturersIds,
    };
  }
  if (locationsIds && locationsIds.length > 0) {
    filter.location = {
      id_in: locationsIds,
    };
  }
  if (lectureType) {
    filter.lecture = {
      lectureType,
    };
  }

  const nextDate = moment(date).add(1, 'days');

  if (tab && tab.id === 'expired') {
    if (date && nextDate <= new Date()) {
      filter.date_gte = new Date(date);
      filter.date_lte = nextDate;
    } else {
      filter.date_lt = new Date();
    }
  } else {
    if (date && nextDate >= new Date()) {
      filter.date_gte = new Date(date);
      filter.date_lte = nextDate
    } else {
      filter.date_gte = new Date();
    }
  }

  if (curatorIds && curatorIds.length > 0) {
    filter.curator = {
      id_in: curatorIds,
    };
  }

  return filter;
};

export default modifyQueryFilter;
