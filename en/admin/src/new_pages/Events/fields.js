import { LECTYRE_TYPES } from './../../enum';

const filter = [
  {
    id: 'date',
    label: 'Дата',
    type: 'date',
  },
  {
    id: 'curator',
    label: 'Координатор',
    relation: 'curator-select',
    type: 'relation',
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
    relation: 'lecturers-select',
    type: 'relation',
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'locations',
    label: 'Локации',
    relation: 'locations-select',
    type: 'relation',
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'select',
    enum: LECTYRE_TYPES,
    multiple: true,
  },
  // {
  //   id: 'curator',
  //   label: 'Координатор',
  // },
];

export default {
  filter,
};
