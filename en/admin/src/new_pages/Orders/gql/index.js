import {
  ALL_ORDERS_QUERY,
  ORDER_QUERY,
  DELETE_ORDER_MUTATION,
  CREATE_ORDER_MUTATION,
  ORDER_QUERY_WITH_RELATIONS,
  CHECK_ORDER_EVENTS,
  EDIT_ORDER_MUTATION,
} from './../../../constants';

import allOrdersQuery from './allOrdersQuery';
import orderQuery from './orderQuery';
import deleteOrderQuery from './deleteOrderQuery';
import createOrderMutation from './createOrderMutation';
import orderQueryWithRelations from './orderQueryWithRelations';
import checkOrderEvents from './checkOrderEvents';
import updateOrderMutation from './updateOrderMutation';

const gql = {
  [ALL_ORDERS_QUERY]: allOrdersQuery,
  [ORDER_QUERY]: orderQuery,
  [DELETE_ORDER_MUTATION]: deleteOrderQuery,
  [CREATE_ORDER_MUTATION]: createOrderMutation,
  [ORDER_QUERY_WITH_RELATIONS]: orderQueryWithRelations,
  [CHECK_ORDER_EVENTS]: checkOrderEvents,
  [EDIT_ORDER_MUTATION]: updateOrderMutation,
};

export default gql;
