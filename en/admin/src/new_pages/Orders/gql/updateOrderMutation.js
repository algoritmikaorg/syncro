import gql from 'graphql-tag';

export default gql`
  mutation UpdateOrderMutation(
      $id: ID!,
      $description: [String!]
      $totalPrice: Float!
      $userId: ID
      $eventsIds: [ID!]
      $participantsIds: [ID!]

      $paymentId: ID!,
      $orderPrice: Float!,
      $discountPrice: Float!,
      $type: PaymentsTypes!,
      $status: PaymentStatus!,
      $discountsIds: [ID!],
      $additionalDiscountsIds: [ID!],
      $certificatesInPaymentsIds: [ID!],
      $subscriptionsInPaymentsIds: [ID!],
      $data: Json!
    ){
    updateOrder(
      id: $id,
      description: $description
      totalPrice: $totalPrice
      paymentId: $paymentId
      userId: $userId
      eventsIds: $eventsIds
      participantsIds: $participantsIds
    ) {
      id
    }
    updatePayment(
      id: $paymentId,
      orderPrice: $orderPrice,
      discountPrice: $discountPrice,
      totalPrice: $totalPrice,
      type: $type,
      userId: $userId,
      status: $status,
      discountsIds: $discountsIds,
      additionalDiscountsIds: $additionalDiscountsIds,
      certificatesInPaymentsIds: $certificatesInPaymentsIds,
      subscriptionsInPaymentsIds: $subscriptionsInPaymentsIds,
      data: $data,
    ) {
      id
    }
  }
`;
