import gql from 'graphql-tag';

export default gql`
  mutation CreateOrderMutation(
      $description: [String!]
      $draft: Boolean
      $totalPrice: Float!
      $payment: OrderpaymentPayment
      $userId: ID
      $eventsIds: [ID!]
      $participantsIds: [ID!]
    ){
    createOrder(
      description: $description
      draft: $draft
      totalPrice: $totalPrice
      payment: $payment
      userId: $userId
      eventsIds: $eventsIds
      participantsIds: $participantsIds
    ) {
      id
    }
  }
`;
