import gql from 'graphql-tag';

export default gql`
  query OrderQuery($id: ID!) {
    Order(
      id: $id,
    ) {
      id
      totalPrice
      events {
        id
        date
        price
        quantityOfTickets
        quantityOfTicketsAvailable
        location {
          title
          address
          metro
        }
        curator {
          firstName
          lastName
          email
        }
        lecture {
          id
          title
          tags {
            title
          }
          course {
            id
            title
          }
          cycle {
            id
            title
          }
        }
        _ticketsMeta {
          count
        }
      }
      participants {
        id
        createdAt
        firstName
        lastName
        email
        phone
      }
      payment {
        id
        updatedAt
        totalPrice
        discountPrice
        orderPrice
        commission
        discounts {
          id
          title
          rate
          validityPeriodFrom
          validityPeriodTo
          promocode
          useCount
          unused
          buyingLecturesCount
          participantsCount
        }
        additionalDiscounts {
          id
          promocode
          title
          rate
          validityPeriodFrom
          validityPeriodTo
          useCount
          unused
          buyingLecturesCount
          participantsCount
        }
        acquiring {
          id
          title
        }
        certificatesInPayments {
          id
          title
          code
          gift
          lecturesCount
          unusedLectures
          personsCount
          start
          end
          price
        }
        subscriptionsInPayments {
          id
          title
          code
          lecturesCount
          unusedLectures
          personsCount
          start
          end
          price
        }
      }
      user {
        id
        createdAt
        firstName
        lastName
        email
        phone
      }
    }
  }
`;
