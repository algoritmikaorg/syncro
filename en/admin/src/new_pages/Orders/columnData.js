const columnData = [
  {
    id: 'userFullNameEmail',
    label: 'Пользователь',
  },
  {
    id: 'discounts',
    label: 'Скидки',
  },
  {
    id: 'coursesTitles',
    label: 'Курс',
  },
  {
    id: 'cyclesTitles',
    label: 'Цикл',
  },
  {
    id: 'lecturesTitle',
    label: 'Лекции',
  },
  {
    id: 'dates',
    label: 'Даты',
  },
  {
    id: 'participants',
    label: 'Участники',
  },
  {
    id: 'paymentType',
    label: 'Оплата',
  },
  // {
  //   id: 'certificatesAndSubscriptions',
  //   label: 'Абонементы и сертификаты',
  // },
];
export default columnData;
