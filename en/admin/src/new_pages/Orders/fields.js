const filter = [
  {
    id: 'firstName',
    label: 'Имя',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
  },
  {
    id: 'email',
    label: 'E-mail',
  },
  {
    id: 'lectureTitle',
    label: 'Название лекции',
    // type: 'multiline',
  },
  {
    id: 'courseTitle',
    label: 'Название курса',
    // type: 'multiline',
  },
  {
    id: 'cycleTitle',
    label: 'Название цикла',
    // type: 'multiline',
  },
  {
    id: 'totalPriceMin',
    label: 'Минимальная сумма заказа',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'totalPriceMax',
    label: 'Максимальная сумма заказа',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'orderDate',
    label: 'Дата заказа',
    type: 'date',
  },
  {
    id: 'paymentDate',
    label: 'Дата оплаты',
    type: 'date',
  },
];

const view = [
  {
    id: 'events',
    label: 'Мероприятия',
    type: 'relation',
    relation: 'eventsOfLectures',
  },
  {
    id: 'user',
    label: 'Пользователь',
    type: 'relation',
    relation: 'users',
  },
  {
    id: 'discounts',
    label: 'Скидки',
    type: 'relation',
    relation: 'discounts',
  },
  {
    id: 'certificates',
    label: 'Сертификаты',
    type: 'relation',
    relation: 'certificates',
  },
  {
    id: 'subscriptions',
    label: 'Абонементы',
    type: 'relation',
    relation: 'subscriptions',
  },
  // {
  //   id: 'courses',
  //   label: 'Курс',
  //   type: 'relation',
  //   relation: 'courses',
  // },
  // {
  //   id: 'cycles',
  //   label: 'Цикл',
  //   type: 'relation',
  //   relation: 'cycles',
  // },
  // {
  //   id: 'lectures',
  //   label: 'Лекции',
  //   type: 'relation',
  //   relation: 'lectures',
  // },
  {
    id: 'participants',
    label: 'Участники',
    type: 'relation',
    relation: 'participants',
  },
  {
    id: 'payment',
    label: 'Оплата',
    type: 'relation',
    relation: 'payments',
  },
];

export default {
  filter,
  view,
};
