import React from 'react';

import gql from './gql';

import {
  EDIT_ORDER_MUTATION,
  ORDER_QUERY,
} from './../../constants';

import { UpdateOrder } from './../../new_components/Order';
import QueryWithQueryOptions from './../../new_containers/QueryWithQueryOptions';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const modifyQueryOptions = (ownProps) => {
  const { id } = ownProps.match.params;
  return (
    {
      variables: {
        id,
      },
    }
  );
};

const OrderMutationUpdate = (props) => {
  const {
    match,
    history,
  } = props;
  const gqlMutationName = EDIT_ORDER_MUTATION;
  const mutationResultObjectName = 'updateOrder';
  const resultMutationMessageSuccses = 'Заказ обновлен';
  const gqlQueryName = ORDER_QUERY;
  const queryResultObjectName = 'Order';

  return (
    <div>
      <QueryWithQueryOptions
        {...props}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryOptions={modifyQueryOptions}
        match={match}
        history={history}
      >
        <UpdateOrder
          {...props}
        />
      </QueryWithQueryOptions>
    </div>
  );
};

export default OrderMutationUpdate;
