import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Orders from './Orders';
import OrderMutation from './OrderMutation';
import OrderMutationUpdate from './OrderMutationUpdate';
import OrderViewWithRelations from './OrderViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../new_hoc/withWrapper';

const OrdersRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Orders
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <OrderMutation {...props} {...rest} mutationType="create" title={'Новый заказ'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <OrderMutationUpdate {...props} {...rest} mutationType="update" title={'Редактирование заказа'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <OrderViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(OrdersRoutes, { title: 'Заказы' });
export default OrdersRoutes
