import { checkPublic } from './../../new_utils';

const modifyQueryFilter = (input = {}, type) => {
  let filter = {};
  filter.public = true;

  if (input.tab) {
    switch (input.tab.id) {
      case 'public': {
        filter.public = true;
        break;
      }
      case 'not_public': {
        filter.public = false;
        break;
      }
      default: {}
    }
  }

  filter = checkPublic(filter, input);

  if (input.title) filter.title_contains = input.title;
  if (input.promocode) filter.promocode_contains = input.promocode;
  if (input.rate) filter.rate = input.rate;
  return filter;
};

export default modifyQueryFilter;
