import gql from 'graphql-tag';

export default gql`
  fragment DiscountPayload on Discount {
    id
    createdAt
    updatedAt

    title
    promocode
    rate
    allProducts
    buyingLecturesCount
    participantsCount
    useCount
    validityPeriodFrom
    validityPeriodTo  
  }
`;
