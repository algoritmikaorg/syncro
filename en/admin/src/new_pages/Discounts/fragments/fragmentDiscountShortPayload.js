import gql from 'graphql-tag';

export default gql`
  fragment DiscountShortPayload on Discount {
    id
    title
    promocode
    rate
    allProducts
    buyingLecturesCount
    participantsCount
    useCount
    validityPeriodFrom
    validityPeriodTo
  }
`;
