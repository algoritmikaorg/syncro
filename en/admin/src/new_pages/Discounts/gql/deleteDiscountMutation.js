import gql from 'graphql-tag';

export default gql`
  mutation deleteDiscountMutation(
      $id: ID!
    ){
    deleteDiscount(
      id: $id,
    ) {
      id
    }
  }
`;
