import React from 'react';

import gql from './gql';

import {
  ALL_DISCOUNTS_QUERY,
  DELETE_DISCOUNT_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import { tabsPublic } from './../../new_utils';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Discounts = (props) => {
  const gqlQueryName = ALL_DISCOUNTS_QUERY;
  const queryResultObjectName = 'allDiscounts';
  const gqlMutationName = DELETE_DISCOUNT_MUTATION;
  const mutationResultObjectName = 'deleteDiscount';
  const resulMutationMessageSuccses = 'Скидка удалена';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
        tabs={tabsPublic}
      />
    </div>
  );
};

export default Discounts;
