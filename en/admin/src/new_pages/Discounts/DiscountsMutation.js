import React from 'react';

import gql from './gql';

import {
  CREATE_DISCOUNT_MUTATION,
  EDIT_DISCOUNT_MUTATION,
  DISCOUNT_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import discountsFields from './fields';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const DiscountsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_DISCOUNT_MUTATION;
    fields = discountsFields.create;
    mutationResultObjectName = 'createDiscount';
    resultMutationMessageSuccses = 'Скидка создана';
  } else {
    gqlMutationName = EDIT_DISCOUNT_MUTATION;
    fields = discountsFields.update;
    mutationResultObjectName = 'updateDiscount';
    resultMutationMessageSuccses = 'Скидка обновлена';
    gqlQueryName = DISCOUNT_QUERY;
    queryResultObjectName = 'Discount';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default DiscountsMutation;
