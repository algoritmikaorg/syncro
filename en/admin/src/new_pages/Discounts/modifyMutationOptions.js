export default () => {
  return (
    {
      refetchQueries: [
        'AllDiscountsQuery',
        'AllDiscountsShortQuery',
      ],
    }
  );
};
