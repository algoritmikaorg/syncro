import React from 'react';

import gql from './gql';

import {
  DISCOUNT_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const DiscountViewWithRelations = (props) => {
  const gqlQueryName = DISCOUNT_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Discount';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default DiscountViewWithRelations;
