import React from 'react';

import gql from './gql';

import {
  LECTURE_QUERY_WITH_RELATIONS,
  LECTURE_FOR_COURSE_QUERY_WITH_RELATIONS,
  LECTURE_FOR_CYCLE_QUERY_WITH_RELATIONS,
  COURSE_QUERY_WITH_RELATIONS,
  CYCLE_QUERY_WITH_RELATIONS,
  CERTIFICATE_PRODUCT_QUERY_WITH_RELATIONS,
  SUBSCRIPTION_PRODUCT_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import {
  lecture as lectureFields,
  lectureForCourse as lectureForCourseFields,
  lectureForCycle as lectureForCycleFields,
  course as courseFields,
  cycle as cycleFields,
  subscription as subscriptionFields,
  certificate as certificateFields,
} from './fields';

const LecturerViewWithRelations = (props) => {
  const {
    typeMaterial,
    lectureType,
  } = props;

  let gqlQueryName = LECTURE_QUERY_WITH_RELATIONS;
  let queryResultObjectName = 'Lecture';
  let currentFileds = lectureFields;

  switch (typeMaterial) {
    case 'lecture': {
      gqlQueryName = LECTURE_FOR_COURSE_QUERY_WITH_RELATIONS;
      queryResultObjectName = 'Lecture';
      currentFileds = lectureFields;
      switch (lectureType) {
        case 'lectureForCourse': {
          gqlQueryName = LECTURE_FOR_CYCLE_QUERY_WITH_RELATIONS;
          currentFileds = lectureForCourseFields;
          break;
        }
        case 'lectureForCycle': {
          currentFileds = lectureForCycleFields;
          break;
        }
        default: {
          currentFileds = lectureFields;
        }
      }
      break;
    }
    case 'course': {
      gqlQueryName = COURSE_QUERY_WITH_RELATIONS;
      queryResultObjectName = 'Course';
      currentFileds = courseFields;
      break;
    }
    case 'cycle': {
      gqlQueryName = CYCLE_QUERY_WITH_RELATIONS;
      queryResultObjectName = 'Cycle';
      currentFileds = cycleFields;
      break;
    }
    case 'certificate': {
      gqlQueryName = CERTIFICATE_PRODUCT_QUERY_WITH_RELATIONS;
      queryResultObjectName = 'CertificateProduct';
      currentFileds = certificateFields;
      break;
    }
    case 'subscription': {
      gqlQueryName = SUBSCRIPTION_PRODUCT_QUERY_WITH_RELATIONS;
      queryResultObjectName = 'SubscriptionProduct';
      currentFileds = subscriptionFields;
      break;
    }
    default: {
    }
  }

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={currentFileds.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default LecturerViewWithRelations;
