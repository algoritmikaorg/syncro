import React from 'react';

import gql from './gql';

import {
  LECTURE_QUERY,
  COURSE_QUERY,
  CYCLE_QUERY,

  SUBSCRIPTION_PRODUCT_QUERY,
  CERTIFICATE_PRODUCT_QUERY,

  ALL_LECTURES_QUERY,
  ALL_COURSE_QUERY,
  ALL_CYCLES_QUERY,

  ALL_SUBSCRIPTION_PRODUCTS_QUERY,
  ALL_CERTIFICATES_PRODUCT_QUERY,

  EDIT_LECTURE_MUTATION,
  EDIT_COURSE_MUTATION,
  EDIT_CYCLE_MUTATION,

  EDIT_SUBSCRIPTION_PRODUCT_MUTATION,
  EDIT_CERTIFICATE_PRODUCT_MUTATION,

  CREATE_LECTURE_MUTATION,
  CREATE_COURSE_MUTATION,
  CREATE_CYCLE_MUTATION,

  CREATE_SUBSCRIPTION_PRODUCT_MUTATION,
  CREATE_CERTIFICATE_PRODUCT_MUTATION,

  DELETE_LECTURE_MUTATION,
  DELETE_COURSE_MUTATION,
  DELETE_CYCLE_MUTATION,

  DELETE_CERTIFICATE_PRODUCT_MUTATION,
  DELETE_SUBSCRIPTION_PRODUCT_MUTATION,
} from './../../constants';


import ListEntities from './../../new_components/ListEntities';
import replaceMutationAndViewLecturelLink from './replaceMutationAndViewLecturelLink';

import {
  lecture as lectureFields,
  lectureForCourse as lectureForCourseFields,
  lectureForCycle as lectureForCycleFields,
  course as courseFields,
  cycle as cycleFields,
  certificate as certificateFields,
  subscription as subscriptionFields,
} from './fields';

import {
  lectures as lecturesActions,
  courses as coursesActions,
  cycles as cyclesActions,
  subscriptions as subscriptionsActions,
  certificates as certificatesActions,
} from './actions';

import {
  lectures as lecturesColumns,
  lectureForCourse as lectureForCourseColumns,
  lectureForCycle as lectureForCycleColumns,
  courses as coursesColumns,
  cycles as cyclesColumns,
  subscriptions as subscriptionsColumns,
  certificates as certificatesColumns,
} from './columnData';

import modifyQueryFilterAll from './modifyQueryFilter';
import { tabsDate, tabsPublic } from './../../new_utils';

const Catalog = (props) => {
  const {
    typeMaterial,
  } = props;

  let actions = null;
  let columns = null;
  let gqlQueryName = null;
  let queryResultObjectName = '';
  let gqlMutationName = null;
  let mutationResultObjectName = '';
  let filterModifier = null;
  let fields = null;
  let tabs = null;
  let replaceMutationAndViewMaterialLink = null;
  let formatInputData = null;
  let resulMutationMessageSuccses = null;
  let modifyQueryFilter = null;

  switch (typeMaterial) {
    case 'course': {
      actions = coursesActions;
      columns = coursesColumns;
      gqlQueryName = ALL_COURSE_QUERY;
      queryResultObjectName = 'allCourses';
      gqlMutationName = DELETE_COURSE_MUTATION;
      mutationResultObjectName = 'deleteCourse';
      modifyQueryFilter = modifyQueryFilterAll.courseCatalog;
      fields = courseFields;
      tabs = tabsDate;

      resulMutationMessageSuccses = 'Курс удален';
      break;
    }
    case 'cycle': {
      actions = cyclesActions;
      columns = cyclesColumns;
      gqlQueryName = ALL_CYCLES_QUERY;
      queryResultObjectName = 'allCycles';
      gqlMutationName = DELETE_CYCLE_MUTATION;
      mutationResultObjectName = 'deleteCycle';
      modifyQueryFilter = modifyQueryFilterAll.cycleCatalog;
      fields = cycleFields;

      tabs = tabsDate;
      resulMutationMessageSuccses = 'Цикл удален';
      break;
    }
    case 'subscription': {
      actions = subscriptionsActions;
      columns = subscriptionsColumns;
      gqlQueryName = ALL_SUBSCRIPTION_PRODUCTS_QUERY;
      queryResultObjectName = 'allSubscriptionProducts';
      gqlMutationName = DELETE_SUBSCRIPTION_PRODUCT_MUTATION;
      mutationResultObjectName = 'deleteSubscriptionProduct';
      modifyQueryFilter = modifyQueryFilterAll.subscriptionCatalog;
      fields = subscriptionFields;

      resulMutationMessageSuccses = 'Тип абонемента удален';
      tabs = tabsPublic;
      break;
    }
    case 'certificate': {
      actions = certificatesActions;
      columns = certificatesColumns;
      gqlQueryName = ALL_CERTIFICATES_PRODUCT_QUERY;
      queryResultObjectName = 'allCertificateProducts';
      gqlMutationName = DELETE_CERTIFICATE_PRODUCT_MUTATION;
      mutationResultObjectName = 'deleteCertificateProduct';
      modifyQueryFilter = modifyQueryFilterAll.certificateCatalog;
      fields = certificateFields;

      resulMutationMessageSuccses = 'Тип сертификата удален';
      tabs = tabsPublic;
      break;
    }
    default: {
      actions = lecturesActions;
      columns = lecturesColumns;
      gqlQueryName = ALL_LECTURES_QUERY;
      queryResultObjectName = 'allLectures';
      gqlMutationName = DELETE_LECTURE_MUTATION;
      mutationResultObjectName = 'deleteLecture';
      modifyQueryFilter = modifyQueryFilterAll.lectureCatalog;
      fields = lectureFields;
      tabs = tabsDate;

      resulMutationMessageSuccses = 'Лекция удалена';
      replaceMutationAndViewMaterialLink = replaceMutationAndViewLecturelLink;
      break;
    }
  }

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columns}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
        replaceMutationAndViewMaterialLink={replaceMutationAndViewMaterialLink}
        tabs={tabs}
      />
    </div>
  );
};

export default Catalog;
