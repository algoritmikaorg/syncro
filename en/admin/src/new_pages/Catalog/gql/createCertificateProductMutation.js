import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  mutation createCertificateProductMutation(
    $lecturesCount: Int!,
    $personsCount: Int!,
    $months: Int!,
    $price: Float!,
    $title: String!,
    $type: SubProductTypes,
    $prefix: String,
    $public: Boolean,
    $discountsIds: [ID!],
  ){
    createCertificateProduct(
      lecturesCount: $lecturesCount,
      months: $months,
      price: $price,
      title: $title,
      type: $type,
      personsCount: $personsCount,
      prefix: $prefix,
      public: $public,
      discountsIds: $discountsIds,
    ) {
      ...CertificateProductPayload
    }
  }
  ${fragments.certificateProductPayload}
`;
