import gql from 'graphql-tag';

export default gql`
  query AllLecturesQueryShort($filter: LectureFilter, $first: Int, $skip: Int, $orderBy: LectureOrderBy) {
    allLectures(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      title
      public
      duration
      tags {
        id
        title
      }
      events {
        location {
          title
        }
        lecturers {
          firstName,
          lastName,
        }
      }
    }
  }
`;
