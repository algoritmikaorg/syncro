import gql from 'graphql-tag';

export default gql `
  mutation UpdateCertificateProductMutation(
      $id: ID!,
      $lecturesCount: Int!,
      $months: Int!,
      $price: Float!,
      $title: String!,
      $prefix: String!,
      $type: SubProductTypes,
      $personsCount: Int!,
      $discountsIds: [ID!],
      $public: Boolean,
    ){
    updateCertificateProduct(
      id: $id,

      lecturesCount: $lecturesCount,
      months: $months,
      price: $price,
      prefix: $prefix,
      title: $title,
      type: $type,
      discountsIds: $discountsIds,
      personsCount: $personsCount,
      public: $public,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lecturesCount,
      months,
      price,
      type,
      personsCount,
      prefix,
      public,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
    }
  }
`;
