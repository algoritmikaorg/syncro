import gql from 'graphql-tag';

export default gql`
  query subscriptionProductQuery($id: ID!) {
    SubscriptionProduct(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lecturesCount,
      months,
      price,
      prefix,
      personsCount,
      image,
      type,
      public,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
    }
  }
`;
