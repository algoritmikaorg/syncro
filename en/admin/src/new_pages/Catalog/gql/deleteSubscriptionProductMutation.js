import gql from 'graphql-tag';

export default gql`
  mutation deleteSubscriptionProductMutation(
      $id: ID!
    ){
    deleteSubscriptionProduct(
      id: $id,
    ) {
      id
    }
  }
`;
