import {
  LECTURE_QUERY,
  COURSE_QUERY,
  CYCLE_QUERY,

  ALL_LECTURES_QUERY,
  ALL_COURSE_QUERY,
  ALL_CYCLES_QUERY,

  ALL_LECTURES_QUERY_SHORT,
  ALL_COURSES_QUERY_SHORT,
  ALL_CYCLES_QUERY_SHORT,

  ALL_LECTURES_FOR_RECOMENDATIONS_QUERY_SHORT,

  EDIT_LECTURE_MUTATION,
  EDIT_COURSE_MUTATION,
  EDIT_CYCLE_MUTATION,

  CREATE_LECTURE_MUTATION,
  CREATE_COURSE_MUTATION,
  CREATE_CYCLE_MUTATION,

  DELETE_LECTURE_MUTATION,
  DELETE_COURSE_MUTATION,
  DELETE_CYCLE_MUTATION,

  SUBSCRIPTION_PRODUCT_QUERY,
  CERTIFICATE_PRODUCT_QUERY,
  ALL_SUBSCRIPTION_PRODUCTS_QUERY,
  ALL_CERTIFICATES_PRODUCT_QUERY,
  ALL_SUBSCRIPTION_PRODUCTS_QUERY_SHORT,
  ALL_CERTIFICATE_PRODUCTS_QUERY_SHORT,
  EDIT_SUBSCRIPTION_PRODUCT_MUTATION,
  EDIT_CERTIFICATE_PRODUCT_MUTATION,
  CREATE_SUBSCRIPTION_PRODUCT_MUTATION,
  CREATE_CERTIFICATE_PRODUCT_MUTATION,
  DELETE_CERTIFICATE_PRODUCT_MUTATION,
  DELETE_SUBSCRIPTION_PRODUCT_MUTATION,

  LECTURE_QUERY_WITH_RELATIONS,
  LECTURE_FOR_COURSE_QUERY_WITH_RELATIONS,
  LECTURE_FOR_CYCLE_QUERY_WITH_RELATIONS,
  COURSE_QUERY_WITH_RELATIONS,
  CYCLE_QUERY_WITH_RELATIONS,
  CERTIFICATE_PRODUCT_QUERY_WITH_RELATIONS,
  SUBSCRIPTION_PRODUCT_QUERY_WITH_RELATIONS,
  ALL_CURATOR_QUERY_SHORT,
} from './../../../constants';

import lectureQuery from './lectureQuery';
import courseQuery from './courseQuery';
import cycleQuery from './cycleQuery';

import allLecturesQuery from './allLecturesQuery';
import allCoursesQuery from './allCoursesQuery';
import allCyclesQuery from './allCyclesQuery';

import allLecturesQueryShort from './allLecturesQueryShort';
import allCoursesQueryShort from './allCoursesQueryShort';
import allCyclesQueryShort from './allCyclesQueryShort';

import editLectureMutation from './editLectureMutation';
import editCourseMutation from './editCourseMutation';
import editCycleMutation from './editCycleMutation';

import createLectureMutation from './createLectureMutation';
import createCourseMutation from './createCourseMutation';
import createCycleMutation from './createCycleMutation';

import deleteLectureMutation from './deleteLectureMutation';
import deleteCourseMutation from './deleteCourseMutation';
import deleteCycleMutation from './deleteCycleMutation';

import subscriptionProductQuery from './subscriptionProductQuery';
import certificateProductQuery from './certificateProductQuery';
import allSubscriptionsProductQuery from './allSubscriptionsProductQuery';
import allCertificatesProductQuery from './allCertificatesProductQuery';
import editSubscriptionProductMutation from './editSubscriptionProductMutation';
import editCertificateProductMutation from './editCertificateProductMutation';
import createSubscriptionProductMutation from './createSubscriptionProductMutation';
import createCertificateProductMutation from './createCertificateProductMutation';
import deleteCertificateProductMutation from './deleteCertificateProductMutation';
import deleteSubscriptionProductMutation from './deleteSubscriptionProductMutation';

import lectureQueryWithRelations from './lectureQueryWithRelations';
import lectureForCourseQueryWithRelations from './lectureForCourseQueryWithRelations';
import lectureForCycleQueryWithRelations from './lectureForCycleQueryWithRelations';
import courseQueryWithRelations from './courseQueryWithRelations';
import cycleQueryWithRelations from './cycleQueryWithRelations';
import certificateProductQueryWithRelations from './certificateProductQueryWithRelations';
import subscriptionProductQueryWithRelations from './subscriptionProductQueryWithRelations';
import allSubscriptionsProductQueryShort from './allSubscriptionsProductQueryShort';
import allCertificatesProductQueryShort from './allCertificatesProductQueryShort';
import allCuratorQueryShort from './allCuratorQueryShort';

import allLecturesForRecomendationsQuery from './allLecturesForRecomendationsQuery';

const gql = {
  [LECTURE_QUERY]: lectureQuery,
  [COURSE_QUERY]: courseQuery,
  [CYCLE_QUERY]: cycleQuery,
  [ALL_LECTURES_QUERY]: allLecturesQuery,
  [ALL_COURSE_QUERY]: allCoursesQuery,
  [ALL_CYCLES_QUERY]: allCyclesQuery,
  [ALL_LECTURES_QUERY_SHORT]: allLecturesQueryShort,
  [ALL_LECTURES_FOR_RECOMENDATIONS_QUERY_SHORT]: allLecturesForRecomendationsQuery,
  [ALL_COURSES_QUERY_SHORT]: allCoursesQueryShort,
  [ALL_CYCLES_QUERY_SHORT]: allCyclesQueryShort,
  [EDIT_LECTURE_MUTATION]: editLectureMutation,
  [EDIT_COURSE_MUTATION]: editCourseMutation,
  [EDIT_CYCLE_MUTATION]: editCycleMutation,
  [CREATE_LECTURE_MUTATION]: createLectureMutation,
  [CREATE_COURSE_MUTATION]: createCourseMutation,
  [CREATE_CYCLE_MUTATION]: createCycleMutation,
  [DELETE_LECTURE_MUTATION]: deleteLectureMutation,
  [DELETE_COURSE_MUTATION]: deleteCourseMutation,
  [DELETE_CYCLE_MUTATION]: deleteCycleMutation,
  [SUBSCRIPTION_PRODUCT_QUERY]: subscriptionProductQuery,
  [CERTIFICATE_PRODUCT_QUERY]: certificateProductQuery,
  [ALL_SUBSCRIPTION_PRODUCTS_QUERY_SHORT]: allSubscriptionsProductQueryShort,
  [ALL_CERTIFICATE_PRODUCTS_QUERY_SHORT]: allCertificatesProductQueryShort,
  [ALL_SUBSCRIPTION_PRODUCTS_QUERY]: allSubscriptionsProductQuery,
  [ALL_CERTIFICATES_PRODUCT_QUERY]: allCertificatesProductQuery,
  [EDIT_SUBSCRIPTION_PRODUCT_MUTATION]: editSubscriptionProductMutation,
  [EDIT_CERTIFICATE_PRODUCT_MUTATION]: editCertificateProductMutation,
  [CREATE_SUBSCRIPTION_PRODUCT_MUTATION]: createSubscriptionProductMutation,
  [CREATE_CERTIFICATE_PRODUCT_MUTATION]: createCertificateProductMutation,
  [DELETE_CERTIFICATE_PRODUCT_MUTATION]: deleteCertificateProductMutation,
  [DELETE_SUBSCRIPTION_PRODUCT_MUTATION]: deleteSubscriptionProductMutation,

  [LECTURE_QUERY_WITH_RELATIONS]: lectureQueryWithRelations,
  [LECTURE_FOR_COURSE_QUERY_WITH_RELATIONS]: lectureForCourseQueryWithRelations,
  [LECTURE_FOR_CYCLE_QUERY_WITH_RELATIONS]: lectureForCycleQueryWithRelations,
  [COURSE_QUERY_WITH_RELATIONS]: courseQueryWithRelations,
  [CYCLE_QUERY_WITH_RELATIONS]: cycleQueryWithRelations,
  [CERTIFICATE_PRODUCT_QUERY_WITH_RELATIONS]: certificateProductQueryWithRelations,
  [SUBSCRIPTION_PRODUCT_QUERY_WITH_RELATIONS]: subscriptionProductQueryWithRelations,
  [ALL_CURATOR_QUERY_SHORT]: allCuratorQueryShort,
};

export default gql;
