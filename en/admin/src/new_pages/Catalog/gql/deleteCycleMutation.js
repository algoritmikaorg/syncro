import gql from 'graphql-tag';

export default gql`
  mutation deleteCycleMutation(
      $id: ID!,
      $aliasId: ID!
    ){
    deleteCycle(
      id: $id,
    ) {
      id
    }
    deleteAlias(
      id: $aliasId
    ) {
      id
    }
  }
`;
