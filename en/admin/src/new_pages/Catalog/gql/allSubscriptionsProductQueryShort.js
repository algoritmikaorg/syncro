import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllSubscriptionProductQueryShort($filter: SubscriptionProductFilter, $first: Int, $skip: Int, $orderBy: SubscriptionProductOrderBy) {
    allSubscriptionProducts(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...SubscriptionProductPayload
    }
  }
  ${fragments.subscriptionProductPayload}
`;
