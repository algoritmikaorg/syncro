import gql from 'graphql-tag';

export default gql`
  mutation CreateCycleMutation(
      $authorId: ID,
      $title: String!,
      $subTitle: String,
      $anons: String,
      $description: String,
      $price: Float!,
      $public: Boolean,
      $buyFull: Boolean,
      $when: String,
      $whenStrong: String,
      $metaDescription: String,
      $metaKeywords: String,

      $img1240x349: String,
      $img340x192: String,
      # $aliasId: ID,
      $tagsIds: [ID!],
      $alias: CyclealiasAlias,
      # $metaTagsId: ID,
      # $inRecommendationsOfCoursesIds: [ID!],
      # $inRecommendationsOfCyclesIds: [ID!],
      # $inRecommendationsOfLecturesIds: [ID!],
      $lecturesIds: [ID!],
      $recommendedCoursesIds: [ID!],
      $recommendedCyclesIds: [ID!],
      $recommendedLecturesIds: [ID!],
      # $reviewsIds: [ID!],
    ){
    createCycle(
      anons: $anons,
      buyFull: $buyFull,
      description: $description,
      img1240x349: $img1240x349,
      img340x192: $img340x192,
      metaDescription: $metaDescription,
      metaKeywords: $metaKeywords,
      price: $price,
      public: $public,
      subTitle: $subTitle,
      title: $title,
      when: $when,
      whenStrong: $whenStrong,
      # aliasId: $aliasId,
      alias: $alias
      authorId: $authorId,
      # metaTagsId: $metaTagsId,
      # inRecommendationsOfCoursesIds: $inRecommendationsOfCoursesIds,
      # inRecommendationsOfCyclesIds: $inRecommendationsOfCyclesIds,
      # inRecommendationsOfLecturesIds: $inRecommendationsOfLecturesIds,
      lecturesIds: $lecturesIds,
      recommendedCoursesIds: $recommendedCoursesIds,
      recommendedCyclesIds: $recommendedCyclesIds,
      recommendedLecturesIds: $recommendedLecturesIds,
      # reviewsIds: $reviewsIds,
      tagsIds: $tagsIds,
    ) {
      id
    }
  }
`;
