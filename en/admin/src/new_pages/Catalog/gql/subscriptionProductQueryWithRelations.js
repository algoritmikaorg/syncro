import gql from 'graphql-tag';

export default gql`
  query subscriptionProductQuery($id: ID!) {
    SubscriptionProduct(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lecturesCount,
      months,
      price,
      type,
      personsCount,
      public,
      image,
      prefix,
      discounts {
        id,
        title,
        promocode,
        rate,
        allProducts,
        buyingLecturesCount,
        participantsCount,
        useCount,
        validityPeriodFrom,
        validityPeriodTo,
      }
    }
  }
`;
