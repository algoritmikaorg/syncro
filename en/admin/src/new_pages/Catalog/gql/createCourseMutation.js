import gql from 'graphql-tag';

export default gql`
  mutation CreateCourseMutation(
      $authorId: ID,
      $title: String!,
      $subTitle: String,
      $anons: String,
      $description: String,
      $price: Float!,
      $public: Boolean,
      $buyFull: Boolean,
      $metaKeywords: String,
      $metaDescription: String,
      $img1240x349: String,
      $img340x192: String,
      $alias: CoursealiasAlias,
      $tagsIds: [ID!],
      $lecturesIds: [ID!],
      # $aliasId: ID,
      # $metaTagsId: ID,
      # $inRecommendationsOfCoursesIds: [ID!],
      # $inRecommendationsOfCyclesIds: [ID!],
      # $inRecommendationsOfLecturesIds: [ID!],
      $recommendedCoursesIds: [ID!],
      $recommendedCyclesIds: [ID!],
      $recommendedLecturesIds: [ID!],
      # $reviewsIds: [ID!],
    ){
    createCourse(
      authorId: $authorId,
      anons: $anons,
      buyFull: $buyFull,
      description: $description,
      img1240x349: $img1240x349,
      img340x192: $img340x192,
      metaDescription: $metaDescription,
      metaKeywords: $metaKeywords,
      price: $price,
      public: $public,
      subTitle: $subTitle,
      title: $title,
      # aliasId: $aliasId,
      alias: $alias,
      # metaTagsId: $metaTagsId,
      # inRecommendationsOfCoursesIds: $inRecommendationsOfCoursesIds,
      # inRecommendationsOfCyclesIds: $inRecommendationsOfCyclesIds,
      # inRecommendationsOfLecturesIds: $inRecommendationsOfLecturesIds,
      lecturesIds: $lecturesIds,
      recommendedCoursesIds: $recommendedCoursesIds,
      recommendedCyclesIds: $recommendedCyclesIds,
      recommendedLecturesIds: $recommendedLecturesIds,
      # reviewsIds: $reviewsIds,
      tagsIds: $tagsIds,
    ) {
      id
    }
  }
`;
