import gql from 'graphql-tag';

export default gql`
  query certificateProductQuery($id: ID!) {
    CertificateProduct(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lecturesCount,
      months,
      price,
      prefix,
      personsCount,
      image,
      type,
      public,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
    }
  }
`;
