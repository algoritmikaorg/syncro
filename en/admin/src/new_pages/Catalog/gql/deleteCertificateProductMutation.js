import gql from 'graphql-tag';

export default gql`
  mutation deleteCertificateProductMutation(
      $id: ID!
    ){
    deleteCertificateProduct(
      id: $id,
    ) {
      id
    }
  }
`;
