import React from 'react';
import Icon from 'material-ui/Icon';
import NoteAdd from 'material-ui-icons/NoteAdd';
import Button from 'material-ui/Button';
import { Link } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});

const LectureAppendActions = (props) => {
  const { classes, lectureType } = props;

  switch (lectureType) {
    case 'lectureForCourse': {
      return (
        <Button
          className={classes.button}
          raised
          dense
          color="primary"
          component={Link}
          to="/catalog/courses/create"
        >
          <NoteAdd className={classes.leftIcon} />
          {'Создать курс'}
        </Button>
      );
    }
    case 'lectureForCycle': {
      return (
        <Button
          className={classes.button}
          raised
          dense
          color="primary"
          component={Link}
          to="/catalog/cycles/create"
        >
          <NoteAdd className={classes.leftIcon} />
          {'Создать цикл'}
        </Button>
      );
    }
    default: {
      return null;
    }
  }
};

export default withStyles(styles)(LectureAppendActions);
