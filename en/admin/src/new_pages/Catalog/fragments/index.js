import certificateProductPayload from './fragmentCertificateProductPayload';
import subscriptionProductPayload from './fragmentSubscriptionProductPayload';
import certificateProductShortPayload from './fragmentCertificateProductShortPayload';

export default {
  certificateProductPayload,
  certificateProductShortPayload,
  subscriptionProductPayload,
}
