import gql from 'graphql-tag';

export default gql`
  fragment CertificateProductPayload on CertificateProduct {
    id
    createdAt
    updatedAt

    title
    lecturesCount
    months
    price
    prefix
    type
    personsCount
  }
`;
