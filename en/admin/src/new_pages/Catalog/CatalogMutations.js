import React from 'react';

import EventsOfLectureMutations from './../../new_components/EventsOfLectureMutations';

import gql from './gql';
import {
  LECTURE_QUERY,
  COURSE_QUERY,
  CYCLE_QUERY,
  CERTIFICATE_PRODUCT_QUERY,
  SUBSCRIPTION_PRODUCT_QUERY,

  EDIT_LECTURE_MUTATION,
  EDIT_COURSE_MUTATION,
  EDIT_CYCLE_MUTATION,

  CREATE_LECTURE_MUTATION,
  CREATE_COURSE_MUTATION,
  CREATE_CYCLE_MUTATION,

  EDIT_SUBSCRIPTION_PRODUCT_MUTATION,
  EDIT_CERTIFICATE_PRODUCT_MUTATION,

  CREATE_SUBSCRIPTION_PRODUCT_MUTATION,
  CREATE_CERTIFICATE_PRODUCT_MUTATION,
  GC_USER_ID,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import LectureTypeChange from './../../new_components/LectureTypeChange';
import LectureAppendActions from './LectureAppendActions';
import CourseAppendActions from './CourseAppendActions';
import CycleAppendActions from './CycleAppendActions';

import {
  lecture as lectureFields,
  lectureForCourse as lectureForCourseFields,
  lectureForCycle as lectureForCycleFields,
  course as courseFields,
  cycle as cycleFields,
  subscription as subscriptionFields,
  certificate as certificateFields,
} from './fields';

import modifyMutationOptions from './modifyMutationOptions';
import handleSuccessMutation from './handleSuccessMutation';
// import {
//   lectures as lecturesActions,
//   courses as coursesActions,
//   cycles as cyclesActions,
// } from './actions';

const CatalogMutationMaterial = (props) => {
  const {
    typeMaterial,
    mutationType,
    lectureType,
    match,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;
  let showLectureTypeChange = null;
  let formTitle = null;
  let modifyMutationOptionsCurrent = null;
  let currentHandleSuccessMutation = null;

  switch (typeMaterial) {
    case 'lecture': {
      let currentFileds = null;
      currentHandleSuccessMutation = handleSuccessMutation.lectures;
      switch (lectureType) {
        case 'lectureForCourse': {
          currentFileds = lectureForCourseFields;
          break;
        }
        case 'lectureForCycle': {
          currentFileds = lectureForCycleFields;
          break;
        }
        default: {
          currentFileds = lectureFields;
        }
      }
      if (mutationType === 'create') {
        modifyMutationOptionsCurrent = modifyMutationOptions.lecture;
        gqlMutationName = CREATE_LECTURE_MUTATION;
        fields = currentFileds.create;
        mutationResultObjectName = 'createLecture';
        resultMutationMessageSuccses = 'Лекция создана';
        formTitle = 'Создание новой лекции';
      } else {
        showLectureTypeChange = true;
        gqlMutationName = EDIT_LECTURE_MUTATION;
        fields = currentFileds.update;
        mutationResultObjectName = 'updateLecture';
        resultMutationMessageSuccses = 'Лекция обновлена';
        formTitle = 'Редактирование лекции';
        gqlQueryName = LECTURE_QUERY;
        queryResultObjectName = 'Lecture';
      }

      break;
    }
    case 'course': {
      currentHandleSuccessMutation = handleSuccessMutation.courses;
      if (mutationType === 'create') {
        modifyMutationOptionsCurrent = modifyMutationOptions.course;
        gqlMutationName = CREATE_COURSE_MUTATION;
        fields = courseFields.create;
        mutationResultObjectName = 'createCourse';
        resultMutationMessageSuccses = 'Курс создан';
        formTitle = 'Новый курс';
      } else {
        gqlMutationName = EDIT_COURSE_MUTATION;
        fields = courseFields.update;
        mutationResultObjectName = 'updateCourse';
        resultMutationMessageSuccses = 'Курс обновлен';
        gqlQueryName = COURSE_QUERY;
        queryResultObjectName = 'Course';
        formTitle = 'Редактирование курса';
      }
      break;
    }
    case 'cycle': {
      currentHandleSuccessMutation = handleSuccessMutation.cycles;
      if (mutationType === 'create') {
        modifyMutationOptionsCurrent = modifyMutationOptions.cycle;
        gqlMutationName = CREATE_CYCLE_MUTATION;
        fields = cycleFields.create;
        mutationResultObjectName = 'createCycle';
        resultMutationMessageSuccses = 'Цикл создан';
        formTitle = 'Новый цикл';
      } else {
        gqlMutationName = EDIT_CYCLE_MUTATION;
        fields = cycleFields.update;
        mutationResultObjectName = 'updateCycle';
        resultMutationMessageSuccses = 'Цикл обновлен';
        gqlQueryName = CYCLE_QUERY;
        queryResultObjectName = 'Cycle';
        formTitle = 'Редактирование цикла';
      }
      break;
    }
    case 'certificate': {
      currentHandleSuccessMutation = handleSuccessMutation.certificates;
      if (mutationType === 'create') {
        modifyMutationOptionsCurrent = modifyMutationOptions.certificate;
        gqlMutationName = CREATE_CERTIFICATE_PRODUCT_MUTATION;
        fields = certificateFields.create;
        mutationResultObjectName = 'createCertificateProduct';
        resultMutationMessageSuccses = 'Новый тип сертификата создан';
        formTitle = 'Новый тип сертификата';
      } else {
        gqlMutationName = EDIT_CERTIFICATE_PRODUCT_MUTATION;
        fields = certificateFields.update;
        mutationResultObjectName = 'updateCertificateProduct';
        resultMutationMessageSuccses = 'Сертификата обновлен';
        gqlQueryName = CERTIFICATE_PRODUCT_QUERY;
        queryResultObjectName = 'CertificateProduct';
        formTitle = 'Редактирование сертификата';
      }
      break;
    }
    case 'subscription': {
      currentHandleSuccessMutation = handleSuccessMutation.subscriptions;
      if (mutationType === 'create') {
        modifyMutationOptionsCurrent = modifyMutationOptions.subscription;
        gqlMutationName = CREATE_SUBSCRIPTION_PRODUCT_MUTATION;
        fields = subscriptionFields.create;
        mutationResultObjectName = 'createSubscriptionProduct';
        resultMutationMessageSuccses = 'Новый тип абонемента создан';
        formTitle = 'Новый тип абонемента';
      } else {
        gqlMutationName = EDIT_SUBSCRIPTION_PRODUCT_MUTATION;
        fields = subscriptionFields.update;
        mutationResultObjectName = 'updateSubscriptionProduct';
        resultMutationMessageSuccses = 'Абонемент обновлен';
        gqlQueryName = SUBSCRIPTION_PRODUCT_QUERY;
        queryResultObjectName = 'SubscriptionProduct';
        formTitle = 'Радатирование абонемента';
      }
      break;
    }
    default: {
      gqlMutationName = '';
      mutationResultObjectName = '';
    }
  }

  fields = fields.map((field) => {
    if (field.id === 'authorId') {
      field.value = localStorage.getItem(GC_USER_ID);
    }
    return field;
  })

  return (
    <div>
      {
        typeMaterial === 'lecture' &&
        <LectureAppendActions
          lectureType={lectureType}
        />
      }
      {
        typeMaterial === 'course' &&
        <CourseAppendActions />
      }
      {
        typeMaterial === 'cycle' &&
        <CycleAppendActions />
      }
      {
        showLectureTypeChange &&
        <LectureTypeChange {...props} />
      }
      {
        typeMaterial === 'lecture' && mutationType === 'update' &&
        <EventsOfLectureMutations
          mutationType={mutationType}
          match={match}
        />
      }
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptionsCurrent}
        onSuccessMutation={currentHandleSuccessMutation}
        formTitle={formTitle}
        actions={['save', 'preview']}
      />
    </div>
  );
};

export default CatalogMutationMaterial;
