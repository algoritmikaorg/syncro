import filterLectures from './filterLectures';

const filterCycles = filterLectures.slice();

export default filterCycles;
