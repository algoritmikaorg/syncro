import { LECTYRE_TYPES } from './../../../enum';

const filterLectures = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'tags',
    label: 'Направления',
    relation: 'tags-select',
    type: 'relation',
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
    relation: 'lecturers-select',
    type: 'relation',
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'locations',
    label: 'Локации',
    relation: 'locations-select',
    type: 'relation',
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'select',
    enum: LECTYRE_TYPES,
    multiple: true,
  },
  // {
  //   id: 'date',
  //   label: 'Даты проведения',
  //   type: 'date',
  //   from: '_gte',
  //   to: '_lte',
  //   format: format.date,
  // },
];

export default filterLectures;
