import { LECTYRE_TYPES, PUBLIC } from './../../../enum';

const filterLecturesShort = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'tags',
    label: 'Направления',
    relation: 'tags-select',
    type: 'relation',
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
    relation: 'lecturers-select',
    type: 'relation',
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
  {
    id: 'locations',
    label: 'Локации',
    relation: 'locations-select',
    type: 'relation',
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
  {
    id: 'public',
    label: 'Опубликовано',
    type: 'select',
    enum: PUBLIC,
    defaultValue: 'true',
  },
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'select',
    enum: LECTYRE_TYPES,
    defaultValue: LECTYRE_TYPES[0].value,
  },
];

export default filterLecturesShort;
