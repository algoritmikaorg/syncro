// дате, направлению, лектору, локации.
import filterLectures from './filterLectures';
const filterCourses = filterLectures.filter(({ id }) => id !== 'lectureType');

export default filterCourses;
