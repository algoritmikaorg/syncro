import { fields as fieldsUtils } from './../../../utils';

import createCourse from './createCourse';

const idField = {
  id: 'id',
  label: 'id',
  type: 'multiline',
  required: true,
  disabled: true,
};

const updateCourse = fieldsUtils.cloneAndAddField(createCourse, idField);

export default updateCourse;
