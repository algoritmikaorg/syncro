import filterCertificatesShort from './filterCertificatesShort';

const filterSubscriptionsShort = filterCertificatesShort.slice();

export default filterSubscriptionsShort;
