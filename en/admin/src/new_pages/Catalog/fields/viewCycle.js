import updateCycle from './updateCycle';

const viewCycle = updateCycle.slice();

export default viewCycle;
