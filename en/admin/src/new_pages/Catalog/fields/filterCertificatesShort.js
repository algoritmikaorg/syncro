import { PUBLIC } from './../../../enum';
import filterCertificates from './filterCertificates';

const filterCertificatesShort = filterCertificates.slice();
filterCertificatesShort.push({
  id: 'public',
  label: 'Опубликовано',
  type: 'select',
  enum: PUBLIC,
  defaultValue: 'true',
});

export default filterCertificatesShort;
