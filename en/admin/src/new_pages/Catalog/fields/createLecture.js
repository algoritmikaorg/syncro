import { GC_USER_ID } from './../../../constants';
const userId = localStorage.getItem(GC_USER_ID);

const createLecture = [
  {
    id: 'author',
    label: 'Автор',
    type: 'author',
    disabled: true,
    hidden: true,
    defaultValue: userId,
    outputType: 'id',
  },
  // {
  //   id: 'authorId',
  //   label: 'Автор',
  //   type: 'text',
  //   disabled: true,
  //   hidden: true,
  // },
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'text',
    disabled: true,
    required: true,
    defaultValue: 'SINGLE_LECTURE',
  },
  {
    id: 'title',
    label: 'Заголовок',
    required: true,
    type: 'multiline',
  },
  {
    id: 'subTitle',
    label: 'Подзаголовок',
    type: 'multiline',
  },
  {
    id: 'anons',
    label: 'Анонс',
    type: 'multiline',
  },
  {
    id: 'description',
    label: 'Описание',
    type: 'multiline',
  },
  {
    id: 'themes',
    label: 'Темы',
    type: 'multiline',
    helperText: 'Каждая тема с новой строки',
    outputType: 'array',
    outputSubType: '\n',
  },
  {
    id: 'duration',
    label: 'Продолжительность',
    required: true,
    type: 'number',
    subType: 'float',
    outputType: 'number',
    outputSubType: 'float',
  },
  {
    id: 'events',
    label: 'Даты',
    defaultValue: [],
    relation: 'events',
    type: 'relation',
    required: true,
    disabled: true,
    outputType: 'events',
  },
  {
    id: 'metaDescription',
    label: 'Meta - Description',
    type: 'multiline',
  },
  {
    id: 'metaKeywords',
    label: 'Meta - Keywords',
    type: 'multiline',
  },
  //
  {
    id: 'img1240x349',
    label: 'Картинка 1240х349',
    defaultValue: 'no-photo',
    type: 'image',
    // size: '1240x!/0x0:1240x349',
    size: '400x113',
  },
  {
    id: 'img340x192',
    label: 'Картинка 340х192',
    defaultValue: 'no-photo',
    type: 'image',
    // size: '340x!/0x0:340x192',
    size: '340x192',
  },
  {
    id: 'img340x340',
    label: 'Картинка 340x340',
    defaultValue: 'no-photo',
    type: 'image',
    // size: '340x!/0x0:340x340',
    size: '340x340',
  },
  {
    id: 'tags',
    label: 'Направления',
    relation: 'tags',
    type: 'relation',
    required: true,
    disabled: true,
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
  {
    id: 'alias',
    label: 'URL alias',
    type: 'alias',
    required: true,
    outputType: 'alias',
  },
  {
    id: 'discounts',
    label: 'Скидки',
    relation: 'discounts',
    type: 'relation',
    disabled: true,
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
  // {
  //   id: 'recommendedCoursesIds',
  //   label: 'Рекомендованные курсы',
  //   relation: 'recommendedCourses',
  //   type: 'relation',
  //   disabled: true,
  // },
  // {
  //   id: 'recommendedCyclesIds',
  //   label: 'Рекомендованные циклы',
  //   relation: 'recommendedCycles',
  //   type: 'relation',
  //   disabled: true,
  // },
  {
    id: 'recommendedLectures',
    label: 'Рекомендованные лекции',
    relation: 'recommendedLectures',
    type: 'relation',
    multiple: 'true',
    disabled: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
  {
    id: 'public',
    label: 'Опубликовать',
    type: 'checkbox',
    defaultValue: false,
    outputType: 'boolean',
  },
  {
    id: 'presentation',
    label: 'Презентация',
    type: 'fileUpload',
    defaultValue: [],
    multiple: false,
    // outputType: 'array',
  },
];

const createLectureForCourse = createLecture.slice();
createLectureForCourse.splice(1, 1, {
  id: 'lectureType',
  label: 'Тип лекции',
  type: 'text',
  disabled: true,
  required: true,
  defaultValue: 'LECTURE_FOR_COURSE',
});

createLectureForCourse.splice(5, 0,
  {
    id: 'titleForCourse',
    label: 'Заголовок для Курса',
    type: 'multiline',
  },
  {
    id: 'anonsForCourse',
    label: 'Анонс для Курса',
    type: 'multiline',
  },
  {
    id: 'course',
    label: 'Курс',
    type: 'relation',
    relation: 'course',
    disabled: true,
    required: true,
    halperText: 'Выберите курс',
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
);

const createLectureForCycle = createLecture.slice();
createLectureForCycle.splice(1, 1,
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'text',
    disabled: true,
    required: true,
    defaultValue: 'LECTURE_FOR_CYCLE',
  },
);

createLectureForCycle.splice(5, 0,
  {
    id: 'titleForCycle',
    label: 'Заголовок для Цикла',
    type: 'multiline',
  },
  {
    id: 'anonsForCycle',
    label: 'Анонс для Цикла',
    type: 'multiline',
  },
  {
    id: 'cycle',
    label: 'Цикл',
    type: 'relation',
    relation: 'cycle',
    halperText: 'Выберите цикл',
    disabled: true,
    required: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
);

export default {};
export { createLecture };
export { createLectureForCourse };
export { createLectureForCycle };
