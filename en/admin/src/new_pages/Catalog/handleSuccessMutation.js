export default {
  certificates: (data, { history }) => { history.push('/catalog/certificates') },
  subscriptions: (data, { history }) => { history.push('/catalog/subscriptions') },
  lectures: (data, { history }) => { history.push('/catalog/lectures') },
  courses: (data, { history }) => { history.push('/catalog/courses') },
  cycles: (data, { history }) => { history.push('/catalog/cycles') },
}
