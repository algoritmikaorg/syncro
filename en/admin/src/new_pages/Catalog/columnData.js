const columnData = [
  {
    id: 'title',
    label: 'Заголовок',
  },
  {
    id: 'tags',
    label: 'Направление',
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
  },
  {
    id: 'locations',
    label: 'Локации',
  },
  {
    id: 'dates',
    label: 'Даты',
  },
  {
    id: 'prices',
    label: 'Цена',
  },
  {
    id: 'author',
    label: 'Автор',
  },
  {
    id: 'createdAt',
    label: 'Дата создания',
  },
];

const lectures = columnData.slice();
const lecturesAll = columnData.slice();
const lecturesForCourse = columnData.slice();
const lecturesForCycle = columnData.slice();
const courses = columnData.slice().map((field) => {
  switch (field.id) {
    case 'lecturers': {
      return Object.assign({}, field);
    }
    case 'location': {
      return Object.assign({}, field);
    }
    case 'date': {
      return Object.assign({}, field);
    }
    default: {
      return field;
    }
  }
});
courses.splice(1, 0, {
  id: 'lectures',
  label: 'Лекции',
});
const cycles = courses.slice();

lecturesForCourse.push({
  id: 'courses',
  label: 'Курс',
});

lecturesForCycle.push({
  id: 'cycles',
  label: 'Цикл',
});

lecturesAll.push({
  id: 'courses',
  label: 'Курс',
});

lecturesAll.push({
  id: 'cycles',
  label: 'Цикл',
});

// cycles.push({
//   id: 'lectures',
//   numeric: false,
//   disablePadding: false,
//   label: 'Лекции',
// });

const subscriptionsShort = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'lecturesCount',
    label: 'Количество лекций',
  },
  {
    id: 'months',
    label: 'Срок действия(месяцев)',
  },
];

const subscriptions = subscriptionsShort.slice();
subscriptions.push({
  id: 'discounts',
  label: 'Скидки',
});

const certificatesShort = subscriptionsShort.slice();
const certificates = subscriptions.slice();

const columnDataShort = [
  {
    id: 'title',
    label: 'Заголовок',
  },
  {
    id: 'tags',
    label: 'Направление',
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
  },
  {
    id: 'locations',
    label: 'Локации',
  },
  {
    id: 'price',
    label: 'Цена',
  },
];

const lecturesShort = columnDataShort.slice();

const coursesShort = columnDataShort.slice();
coursesShort.push({
  id: 'buyFull',
  label: 'Покупка полного курса',
});

const cyclesShort = columnDataShort.slice();
cyclesShort.push({
  id: 'buyFull',
  label: 'Покупка полного цикла',
});

export default {};
export { lectures };
export { lecturesShort };
export { lecturesAll };
export { lecturesForCourse };
export { lecturesForCycle };
export { courses };
export { cycles };
export { coursesShort };
export { cyclesShort };
export { subscriptions };
export { certificates };
export { subscriptionsShort };
export { certificatesShort };
