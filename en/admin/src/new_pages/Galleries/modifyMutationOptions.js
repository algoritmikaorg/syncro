export default () => {
  return (
    {
      refetchQueries: [
        'AllGalleriesQuery',
      ],
    }
  );
};
