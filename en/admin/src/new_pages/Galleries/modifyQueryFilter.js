const modifyQueryFilter = (input = {}, type) => {
  const filter = {};
  if (!input) return {};
  const {
    title,
  } = input;

  if (title) filter.title_contains = title;

  return filter;
};

export default modifyQueryFilter;
