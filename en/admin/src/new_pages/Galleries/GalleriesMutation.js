import React from 'react';

import gql from './gql';

import {
  CREATE_GALLERY_MUTATION,
  EDIT_GALLERY_MUTATION,
  GALLERY_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import galleriesFields from './fields';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const GallerysMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_GALLERY_MUTATION;
    fields = galleriesFields.create;
    mutationResultObjectName = 'createGallery';
    resultMutationMessageSuccses = 'Галерея создана';
  } else {
    gqlMutationName = EDIT_GALLERY_MUTATION;
    fields = galleriesFields.update;
    mutationResultObjectName = 'updateGallery';
    resultMutationMessageSuccses = 'Галерея обновлена';
    gqlQueryName = GALLERY_QUERY;
    queryResultObjectName = 'Gallery';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default GallerysMutation;
