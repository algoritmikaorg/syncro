import gql from 'graphql-tag';

export default gql`
  query galleryQueryWithRelations($id: ID!) {
    Gallery(
      id: $id,
    ) {
      id
      createdAt
      updatedAt

      title
      type
      images {
        id
        secret
        description
      }
    }
  }
`;
