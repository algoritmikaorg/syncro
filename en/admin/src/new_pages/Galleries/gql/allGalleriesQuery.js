import gql from 'graphql-tag';

export default gql`
  query AllGalleriesQuery($filter: GalleryFilter, $first: Int, $skip: Int, $orderBy: GalleryOrderBy) {
    allGalleries(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,

      title
      type
      images {
        description
        id
        secret
      }
      _imagesMeta {
        count
      }
    }
    _allGalleriesMeta {
      count
    }
  }
`;
