import gql from 'graphql-tag';

export default gql`
  query galleryQuery($id: ID) {
    Gallery(
      id: $id,
    ) {
      id
      createdAt
      updatedAt

      title
      type
    }
  }
`;
