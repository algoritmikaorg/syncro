const modifyQueryFilter = (input = {}) => {
  const filter = {};
  if (!input) return filter;
  const {
    title,
  } = input;
  if (title) filter.title_contains = title;
  return filter;
};

export default modifyQueryFilter;
