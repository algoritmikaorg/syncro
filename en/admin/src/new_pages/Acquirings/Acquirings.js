import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_ACQUIRINGS_QUERY,
  DELETE_ACQUIRING_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const Acquirings = (props) => {

  const gqlQueryName = ALL_ACQUIRINGS_QUERY;
  const queryResultObjectName = 'allAcquirings';
  const gqlMutationName = DELETE_ACQUIRING_MUTATION;
  const mutationResultObjectName = 'deleteAcquiring';
  const resulMutationMessageSuccses = 'Эквайринг удален';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
      />
    </div>
  );
};

export default Acquirings;
