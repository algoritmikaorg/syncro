import React from 'react';

import gql from './gql';

import {
  ACQUIRING_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import acquiringsFields from './fields';

const AcquiringViewWithRelations = (props) => {
  const gqlQueryName = ACQUIRING_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Acquiring';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={acquiringsFields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default AcquiringViewWithRelations;
