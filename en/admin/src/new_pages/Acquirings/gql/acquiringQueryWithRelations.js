import gql from 'graphql-tag';

export default gql`
  query acquiringQueryWithRelations($id: ID!) {
    Acquiring(
      id: $id,
    ) {
      id
      createdAt
      updatedAt

      commission
      password
      shopId
      title
      token
    }
  }
`;
