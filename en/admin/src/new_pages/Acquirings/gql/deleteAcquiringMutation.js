import gql from 'graphql-tag';

export default gql`
  mutation deleteAcquiringMutation(
      $id: ID!
    ){
    deleteAcquiring(
      id: $id,
    ) {
      id
    }
  }
`;
