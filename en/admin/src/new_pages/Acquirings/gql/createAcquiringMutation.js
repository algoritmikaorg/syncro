import gql from 'graphql-tag';

export default gql`
  mutation CreateAcquiringMutation(
      $commission: Float!
      $password: String!
      $shopId: String!
      $title: String!
      $token: String!
    ){
    createAcquiring(
      commission: $commission
      password: $password
      shopId: $shopId
      title: $title
      token: $token
    ) {
      id
      createdAt
      updatedAt

      commission
      password
      shopId
      title
      token
    }
  }
`;
