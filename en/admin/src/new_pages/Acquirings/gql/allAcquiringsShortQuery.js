import gql from 'graphql-tag';

export default gql`
  query AllAcquiringsQuery($filter: AcquiringFilter, $first: Int, $skip: Int, $orderBy: AcquiringOrderBy) {
    allAcquirings(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      title
    }
  }
`;
