import {
  CREATE_ACQUIRING_MUTATION,
  ALL_ACQUIRINGS_QUERY,
  EDIT_ACQUIRING_MUTATION,
  ACQUIRING_QUERY,
  DELETE_ACQUIRING_MUTATION,
  ACQUIRING_QUERY_WITH_RELATIONS,
  ALL_ACQUIRINGS_QUERY_SHORT,
} from './../../../constants';
import createAcquiringMutation from './createAcquiringMutation';
import allAcquiringsQuery from './allAcquiringsQuery';
import editAcquiringMutation from './editAcquiringMutation';
import acquiringQuery from './acquiringQuery';
import deleteAcquiringMutation from './deleteAcquiringMutation';
import acquiringQueryWithRelations from './acquiringQueryWithRelations';
import allAcquiringsShortQuery from './allAcquiringsShortQuery';

const gql = {
  [CREATE_ACQUIRING_MUTATION]: createAcquiringMutation,
  [ALL_ACQUIRINGS_QUERY]: allAcquiringsQuery,
  [EDIT_ACQUIRING_MUTATION]: editAcquiringMutation,
  [ACQUIRING_QUERY]: acquiringQuery,
  [DELETE_ACQUIRING_MUTATION]: deleteAcquiringMutation,
  [ACQUIRING_QUERY_WITH_RELATIONS]: acquiringQueryWithRelations,
  [ALL_ACQUIRINGS_QUERY_SHORT]: allAcquiringsShortQuery,
};

export default gql;
