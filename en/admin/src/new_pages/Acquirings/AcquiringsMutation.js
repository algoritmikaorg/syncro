import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_ACQUIRING_MUTATION,
  EDIT_ACQUIRING_MUTATION,
  ACQUIRING_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import acquiringsFields from './fields';
import acquiringsActions from './actions';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const AcquiringsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_ACQUIRING_MUTATION;
    fields = acquiringsFields.create;
    mutationResultObjectName = 'createAcquiring';
    resultMutationMessageSuccses = 'Эквайринг создан';
  } else {
    gqlMutationName = EDIT_ACQUIRING_MUTATION;
    fields = acquiringsFields.update;
    mutationResultObjectName = 'updateAcquiring';
    resultMutationMessageSuccses = 'Экваринг обновлен';
    gqlQueryName = ACQUIRING_QUERY;
    queryResultObjectName = 'Acquiring';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default AcquiringsMutation;
