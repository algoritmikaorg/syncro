import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Settings from './Settings';
import SettingsMutation from './SettingsMutation';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const SettingsRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        {/* <Route
          exact
          path={`${url}`}
          component={rest => (
            <Settings
              {...props}
              {...rest}
            />
          )}
        /> */}
        <Route
          exact
          path={`${url}/mailchimp`}
          component={rest => (
            <Settings {...props} {...rest} title={'MailChimp API'} />
          )}
        />
        <Route
          exact
          path={`${url}/mailchimp/create`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'MailChimp API'}
              mutationType="create"
              typeMaterial="mailchimp"
            />
          )}
        />
        <Route
          exact
          path={`${url}/mailchimp/update/:id`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'MailChimp API'}
              mutationType="update"
              typeMaterial="mailchimp"
            />
          )}
        />
        <Route
          exact
          path={`${url}/mandrill`}
          component={rest => (
            <Settings {...props} {...rest} title={'MailChimp API'} />
          )}
        />
        <Route
          exact
          path={`${url}/mandrill/create`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'Mandrill API'}
              mutationType="create"
              typeMaterial="mandrill"
            />
          )}
        />
        <Route
          exact
          path={`${url}/mandrill/update/:id`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'Mandrill API'}
              mutationType="update"
              typeMaterial="mandrill"
            />
          )}
        />
        <Route
          exact
          path={`${url}/smtp`}
          component={rest => (
            <Settings {...props} {...rest} title={'SMTP'} />
          )}
        />
        <Route
          exact
          path={`${url}/smtp/create`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'SMTP'}
              mutationType="create"
              typeMaterial="smtp"
            />
          )}
        />
        <Route
          exact
          path={`${url}/smtp/update/:id`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'SMTP'}
              mutationType="update"
              typeMaterial="smtp"
            />
          )}
        />
        <Route
          exact
          path={`${url}/contacts`}
          component={rest => (
            <Settings {...props} {...rest} title={'Контакты'} />
          )}
        />
        <Route
          exact
          path={`${url}/contacts/create`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'Контакты'}
              mutationType="create"
              typeMaterial="contacts"
            />
          )}
        />
        <Route
          exact
          path={`${url}/contacts/update/:id`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'Контакты'}
              mutationType="update"
              typeMaterial="contacts"
            />
          )}
        />
        <Route
          exact
          path={`${url}/acquiring`}
          component={rest => (
            <Settings {...props} {...rest} title={'Эквайринг'} />
          )}
        />
        <Route
          exact
          path={`${url}/acquiring/create`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'Эквайринг'}
              mutationType="create"
              typeMaterial="acquiring"
            />
          )}
        />
        <Route
          exact
          path={`${url}/acquiring/update/:id`}
          component={rest => (
            <SettingsMutation
              {...props}
              {...rest}
              title={'Эквайринг'}
              mutationType="update"
              typeMaterial="acquiring"
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(SettingsRoutes, { title: 'Настройки' });
export default SettingsRoutes;
