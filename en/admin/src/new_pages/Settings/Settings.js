import React from 'react';

import gql from './gql';

import {
  ALL_SETTINGS_QUERY,
} from './../../constants';

import EntityQueryAndMutationRedirect from './../../new_components/EntityQueryAndMutationRedirect';

const Settings = (props) => {

  return (
    <div>
      <EntityQueryAndMutationRedirect
        {...props}
        gql={gql}
        gqlQueryName={ALL_SETTINGS_QUERY}
        queryResultObjectName={'allSettings'}
      />
    </div>
  );
};

export default Settings;
