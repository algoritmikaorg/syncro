const сreateMailchimp = [
  {
    id: 'mailChimpHost',
    label: 'Хост',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mailChimpAPIKey',
    label: 'API key',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mailChimpMainList',
    label: 'ID лист контактов - основной',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mailChimpReactivationList',
    label: 'ID лист контактов - реактивации',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mailChimpNewSchedule',
    label: 'ID лист контактов - новое расписание',
    type: 'multiline',
    required: true,
  },
];

const createMaindrill = [
  {
    id: 'senderName',
    label: 'Отправитель',
    type: 'multiline',
    required: true,
  },
  {
    id: 'senderEmail',
    label: 'E-mail',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillHost',
    label: 'Хост',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillAPIKey',
    label: 'API key',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugResetPass',
    label: 'ID шаблона - Сброс пароля',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugOptRegistration',
    label: 'ID шаблона - Опосредованная Регистрация',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugRegistration',
    label: 'ID шаблона - Регистрация',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugConfirmEmail',
    label: 'ID шаблона - Подтверждения E-mail',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugMaterialsOfLecture',
    label: 'ID шаблона - Материалы лекции',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugOrder',
    label: 'ID шаблона - Новый заказ',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugOrderWithoutPayment',
    label: 'ID шаблона - Неоплаченный заказ',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugPayment',
    label: 'ID шаблона - Успешный платеж',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugCertificate',
    label: 'ID шаблона - Сертификат',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugSubscription',
    label: 'ID шаблона - Абонемент',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugUserBlock',
    label: 'ID шаблона - Пользователь заблокирован',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugUserUnblock',
    label: 'ID шаблона - Пользователь разблокирован',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugCertificateAsGift',
    label: 'ID шаблона - Сертификат в подарок',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugReminderOfLecture',
    label: 'ID шаблона - Напоминание о лекции',
    type: 'multiline',
    required: true,
  },
  {
    id: 'mandrillTemplateSlugTicket',
    label: 'ID шаблона - Билет',
    type: 'multiline',
    required: true,
  },
];

const createSmtp = [
  {
    id: 'smtpHost',
    label: 'Хост',
    type: 'text',
    required: true,
  },
  {
    id: 'smtpLogin',
    label: 'Логин',
    type: 'text',
    required: true,
  },
  {
    id: 'smtpPassword',
    label: 'Пароль',
    type: 'text',
    required: true,
  },
];

const createContacts = [
  {
    id: 'contactOfficeTitle',
    label: 'Название',
    type: 'multiline',
  },

  {
    id: 'contactOfficeAddress',
    label: 'Адрес',
    type: 'multiline',
  },

  {
    id: 'contactOfficePhone',
    label: 'Телефон',
    type: 'multiline',
  },
];

const createAcquiring = [
  {
    id: 'acquiring',
    label: 'Система эквайринга',
    relation: 'acquirings-select',
    type: 'relation',
    disabled: true,
    required: true,
    outputType: 'relation',
    outputSubType: 'id',
  },
];

const updateMailchimp = сreateMailchimp.slice();
updateMailchimp.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const updateMandrill = createMaindrill.slice();
updateMandrill.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const updateSmtp = createSmtp.slice();
updateSmtp.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const updateContacts = createContacts.slice();
updateContacts.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const updateAcquiring = createAcquiring.slice();
updateAcquiring.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

export default {
  сreateMailchimp,
  createMaindrill,
  createSmtp,
  createContacts,
  createAcquiring,
  updateAcquiring,
  updateMailchimp,
  updateMandrill,
  updateSmtp,
  updateContacts,
};
