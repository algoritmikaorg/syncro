export const USER_ROLES = [
  {
    value: 'CLIENT',
    label: 'Клиент',
  },
  {
    value: 'ADMIN',
    label: 'Администратор',
  },
  {
    value: 'MODERATOR',
    label: 'Модератор',
  },
  {
    value: 'CURATOR',
    label: 'Координатор',
  },
];

export const SUB_PRODUCT_TYPES = [
  {
    value: 'CERTIFICATE',
    label: 'Сертификат',
  },
  {
    value: 'SUBSCRIPTION',
    label: 'Абонемент',
  },
];

export const LECTYRE_TYPES = [
  {
    value: 'SINGLE_LECTURE',
    label: 'Одиночная лекция',
  },
  {
    value: 'LECTURE_FOR_COURSE',
    label: 'Лекция для курса',
  },
  {
    value: 'LECTURE_FOR_CYCLE',
    label: 'Лекция для цикла',
  },
];

export const PUBLIC = [
  {
    value: 'true',
    label: 'Да',
  },
  {
    value: 'false',
    label: 'Нет',
  },
];

export const PAYMENT_STATUS = [
  {
    value: 'PAID',
    label: 'Оплачен',
  },
  {
    value: 'PENDING',
    label: 'Ожидает оплаты',
  },
];
