import React from 'react';

const TabContainer = props => (
  <div style={{ padding: 8 * 3 }}>{props.children}</div>
);

export default TabContainer;
