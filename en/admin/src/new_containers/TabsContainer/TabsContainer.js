import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import TabContainer from './TabContainer';

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit * 3,
    backgroundColor: theme.palette.background.paper,
  },
});

class TabsContainer extends Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    const { tabs, onChange } = this.props;
    this.setState({ value }, onChange(tabs[value]));
  };
  render() {
    const { classes, tabs } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={this.handleChange}
            scrollable
            scrollButtons="on"
          >
            {
              tabs.map((tab, index) => (
                <Tab  key={index} label={tab.label} />
              ))
            }
          </Tabs>
        </AppBar>
        <TabContainer>
          {this.props.children[value]}
        </TabContainer>
      </div>
    );
  }
}

TabsContainer.defaultProps = {
  tabs: [],
  onChange() {},
};

export default withStyles(styles)(TabsContainer);
