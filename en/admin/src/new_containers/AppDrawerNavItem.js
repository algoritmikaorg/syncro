import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { withStyles } from 'material-ui/styles';
import { ListItem } from 'material-ui/List';
import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
import {
  Route,
} from 'react-router-dom';

const styles = theme => ({
  button: theme.mixins.gutters({
    borderRadius: 0,
    justifyContent: 'flex-start',
    textTransform: 'none',
    width: '100%',
    transition: theme.transitions.create('background-color', {
      duration: theme.transitions.duration.shortest,
    }),
    '&:hover': {
      textDecoration: 'none',
    },
    // color: theme.palette.text.secondary,
    fontWeight: 500,
  }),
  navItem: {
    ...theme.typography.body2,
    display: 'block',
    paddingTop: 0,
    paddingBottom: 0,
  },
  navLink: {
    fontWeight: theme.typography.fontWeightRegular,
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
  },
  navChild: {
    paddingLeft: 20,
  },
  navLinkButton: {
    color: theme.palette.text.secondary,
    textIndent: theme.spacing.unit * 3,
    fontSize: theme.typography.pxToRem(13),
  },
  activeButton: {
    color: theme.palette.text.primary,
    fontWeight: 900
  },
});

class AppDrawerNavItem extends Component {
  constructor(props) {
    super(props);
    const { id } = props;
    this.state = {
      open: false,
    };
  }
  componentWillMount() {
    if (this.props.openImmediately) {
      this.setState({ open: true });
    }
  }

  handleClick = () => {
    const open = !this.state.open;
    this.setState({ open });
  };

  render() {
    const {
      children,
      classes,
      to,
      openImmediately,
      title,
      inner,
      activeOnlyWhenExact,
    } = this.props;

    if (to) {
      return (
        // <ListItem className={classes.navLink} disableGutters>
        <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
          <ListItem className={classes.navLink} disableGutters>
            <Button
              variant="button"
              className={classNames(
                {
                  [classes.button]: true,
                }, {
                  [classes.navLinkButton]: inner || false,
                }, {
                  [classes.activeButton]: match || false
                })}
              disableRipple
              onClick={this.props.onClick}
              component={Link}
              to={to}
            >
              {title}
            </Button>
          </ListItem>
        )}/>
      );
    }

    return (
      <ListItem className={classes.navItem} disableGutters>
        <Button
          classes={{
            root: classes.button,
            label: openImmediately ? 'algolia-lvl0' : '',
          }}
          onClick={this.handleClick}
        >
          {title}
        </Button>
        <Collapse
          in={this.state.open}
          timeout="auto"
          unmountOnExit
          // className={classes.navChild}
        >
          {children}
        </Collapse>
      </ListItem>
    );
  }
}

AppDrawerNavItem.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
  to: PropTypes.string,
  onClick: PropTypes.func,
  openImmediately: PropTypes.bool,
  title: PropTypes.string.isRequired,
};

AppDrawerNavItem.defaultProps = {
  openImmediately: false,
};

export default withStyles(styles)(AppDrawerNavItem);
