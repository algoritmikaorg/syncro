// @flow

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  root: {
    marginTop: 20,
  },
});

const AppContentInner = (props) => {
  const { className, classes, children } = props;
  return <div className={classNames(classes.root, className)}>{children}</div>;
}

AppContentInner.propTypes = {
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default withStyles(styles)(AppContentInner);
