import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';

import withProgress from './../../new_hoc/withProgress';
import withStateMutation from './../../new_hoc/withStateMutation';
import withSnackbar from './../../new_hoc/withSnackbar';

const QueryWithOptionsAndMutation = (props) => {
  const {
    gql,
    gqlQueryName,
    gqlMutationName,
    queryOptions,
    children,
    modifyQueryFilter,
  } = props;
  const query = gql[gqlQueryName];

  // Enhancer function.
  const withQuery = graphql(
    query,
    {
      name: gqlQueryName,
      options: queryOptions,
    },
  );

  const withMutation = graphql(gql[gqlMutationName], { name: gqlMutationName });

  // Enhance our component.
  const WithData = compose(
    withMutation,
    withQuery,
    withStateMutation({ name: gqlMutationName }),
    withSnackbar(),
    withProgress([gqlQueryName, gqlMutationName]),
  )(ownProps => React.cloneElement(children, ownProps));

  // Return the enhanced component.
  return (
    <WithData {...props} />
    // <WithProgress />
  );
};

QueryWithOptionsAndMutation.defaultProps = {
  modifyQueryFilter() { return {}; },
};

export default QueryWithOptionsAndMutation;
