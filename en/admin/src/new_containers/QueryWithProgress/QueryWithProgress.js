import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import withProgress from './../../new_hoc/withProgress';

const QueryWithProgress = (props) => {
  const {
    gql,
    gqlQueryName,
    queryOptions,
    children,
    modifyQueryFilter,
    modifyQueryOptions,
  } = props;
  const query = gql[gqlQueryName];

  // Enhancer function.
  const withQuery = graphql(
    query,
    {
      name: gqlQueryName,
      options: modifyQueryOptions(modifyQueryFilter),
    },
  );
  // Enhance our component.
  const WithProgress = withProgress([gqlQueryName])(ownProps => React.cloneElement(children, ownProps));
  const WithData = withQuery(WithProgress);
  // Return the enhanced component.
  return (
    <WithData {...props} />
    // <WithProgress />
  );
};

QueryWithProgress.defaultProps = {
  modifyQueryFilter() { return {}; },
  modifyQueryOptions(modifyQueryFilter) {
    return (ownProps) => {
      return {
        variables: {
          filter: modifyQueryFilter(ownProps.allQueryFilter, ownProps.filterType),
        },
      };
    };
  },
};

export default QueryWithProgress;
