import React from 'react';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';
import AppDrawerNavItem from './AppDrawerNavItem';

const AppDrawerNavList = (props) => {
  const { onClose } = props;

  return (
    <List>
      <AppDrawerNavItem
        to={'/schedule'}
        title={'Расписание'}
      />
      <AppDrawerNavItem
        to={'/events'}
        title={'Мероприятия'}
      />
      <AppDrawerNavItem
        to={'/messages'}
        title={'Уведомления'}
        onClick={onClose}
      />
      <Divider />
      <AppDrawerNavItem
        title={'Каталог'}
      >
        <List>
          <AppDrawerNavItem
            to={'/catalog/lectures'}
            title={'Лекции'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/catalog/courses'}
            title={'Курсы'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/catalog/cycles'}
            title={'Циклы'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/catalog/certificates'}
            title={'Сертификаты'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/catalog/subscriptions'}
            title={'Абонементы'}
            onClick={onClose}
            inner
          />
        </List>
      </AppDrawerNavItem>

      <AppDrawerNavItem
        title={'Содержимое сайта'}
      >
        <List>
          <AppDrawerNavItem
            to={'/content/main'}
            title={'Главная страница'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/content/howwork-mainpage'}
            title={'Как проходит синхронизация - Главная страница'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/content/howwork-course'}
            title={'Как проходит синхронизация - Страница Курс'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/content/howwork-lecture'}
            title={'Как проходит синхронизация - Страница Лекции'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/content/maingallery'}
            title={'Галерея - Фотографии с мероприятий'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/content/about'}
            title={'О проекте'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/content/term-of-use'}
            title={'Пользовательское соглашение'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/content/agreement'}
            title={'Соглашение об обработке персональных данных'}
            onClick={onClose}
            inner
          />
        </List>
      </AppDrawerNavItem>

      <AppDrawerNavItem
        to={'/discounts'}
        title={'Скидки'}
        onClick={onClose}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/lecturers'}
        title={'Лекторы'}
        onClick={onClose}
      />
      <AppDrawerNavItem
        to={'/tags'}
        title={'Направления'}
        onClick={onClose}
      />
      <AppDrawerNavItem
        to={'/locations'}
        title={'Локации'}
        onClick={onClose}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/orders'}
        title={'Заказы'}
        onClick={onClose}
      />
      <AppDrawerNavItem
        to={'/payments'}
        title={'Платежи'}
        onClick={onClose}
      />
      <AppDrawerNavItem
        to={'/certificates'}
        title={'Сертификаты'}
        onClick={onClose}
      />
      <AppDrawerNavItem
        to={'/subscriptions'}
        title={'Абонементы'}
        onClick={onClose}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/users'}
        title={'Пользователи'}
        onClick={onClose}
        activeOnlyWhenExact
      />
      <AppDrawerNavItem
        to={'/reviews'}
        title={'Отзывы'}
        onClick={onClose}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/partners'}
        title={'Партнеры'}
        onClick={onClose}
      />
      <AppDrawerNavItem
        to={'/galleries'}
        title={'Галереи'}
        onClick={onClose}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/users/administrators'}
        title={'Администраторы  '}
        onClick={onClose}
        activeOnlyWhenExact
      />
      <AppDrawerNavItem
        to={'/users/moderators'}
        title={'Модераторы'}
        onClick={onClose}
        activeOnlyWhenExact
      />
      <AppDrawerNavItem
        to={'/users/curators'}
        title={'Координаторы'}
        onClick={onClose}
        activeOnlyWhenExact
      />
      <Divider />
      {/* <AppDrawerNavItem
        to={'/settings'}
        title={'Система'}
      /> */}
      <AppDrawerNavItem
        to={'/acquirings'}
        title={'Эквайринг'}
        onClick={onClose}
      />
      <AppDrawerNavItem
        // to={'/catalog'}
        title={'Настройки'}
      >
        <List>
          <AppDrawerNavItem
            to={'/settings/mailchimp'}
            title={'MailChimp API'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/settings/mandrill'}
            title={'Mandrill API'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/settings/smtp'}
            title={'SMTP'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/settings/contacts'}
            title={'Контакты'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/settings/acquiring'}
            title={'Эквайринг'}
            onClick={onClose}
            inner
          />
        </List>
      </AppDrawerNavItem>
      <AppDrawerNavItem
        // to={'/catalog'}
        title={'Шаблон'}
      >
        <List>
          <AppDrawerNavItem
            to={'/template/scripts'}
            title={'Скрипты'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/template/styles'}
            title={'Стили'}
            onClick={onClose}
            inner
          />
          <AppDrawerNavItem
            to={'/template/blocks'}
            title={'Блоки'}
            onClick={onClose}
            inner
          />
        </List>
      </AppDrawerNavItem>
    </List>
  );
};

export default AppDrawerNavList;
