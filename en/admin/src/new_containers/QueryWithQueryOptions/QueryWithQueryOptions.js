import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';

import withProgress from './../../new_hoc/withProgress';
import withSnackbar from './../../new_hoc/withSnackbar';

const QueryWithQueryOptions = (props) => {
  const {
    gql,
    gqlQueryName,
    queryOptions,
    children,
    modifyQueryOptions,
  } = props;
  const query = gql[gqlQueryName];

  // Enhancer function.
  const withQuery = graphql(
    query,
    {
      name: gqlQueryName,
      options: modifyQueryOptions,
    },
  );
  // Enhance our component.

  const WithData = compose(
    withQuery,
    withSnackbar(),
    withProgress([gqlQueryName]),
  )(ownProps => React.cloneElement(children, ownProps));
  // Return the enhanced component.
  return (
    <WithData {...props} />
    // <WithProgress />
  );
};

QueryWithQueryOptions.defaultProps = {
  modifyQueryOptions() { return { variables: {} }; },
};

export default QueryWithQueryOptions;
