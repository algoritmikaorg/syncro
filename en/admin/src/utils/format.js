import moment from 'moment';

export default {
  parseIntNumberField(input) {
    if (!input) return null;
    return parseInt(input, 10);
  },
  parseFloatNumberField(input) {
    if (!input) return null;
    return parseFloat(input, 10);
  },
  // parseIntNumberField: fn => input => {
  //   if (input && typeof input === 'string') {
  //     fn(parseInt(input, 10));
  //   } else {
  //     fn(input);
  //   }
  // },
  tags(input) {
    return input.tags.map(tag => tag.title).join(', ');
  },
  locationListInputData(input) {

  },
  courseListInputData(input) {
    const {
      id,
      title,
      price,
      buyFull,
      author,
      tags,
      lectures,
    } = input;
    const output = {
      id,
      title,
      price,
      buyFull,
    };
    output.id = id;
    output.date = [];
    output.lecturers = [];
    output.location = [];
    output.lectures = [];
    output.author = `${input.author.firstName} ${input.author.lastName}`;
    input.lectures.forEach(({ title, events }) => {
      output.lectures.push(title);
      events.forEach(({ date, price, lecturers, location }) => {
        output.lecturers.push(lecturers.map(({ firstName, lastName }) => `${firstName} ${lastName}`).join(', '));
        output.location.push(location.title);
        output.date.push(moment(date).format('YYYY-MM-DD hh:mm'));
      });
    });
    output.date = output.date.join(', ');
    output.lecturers = output.lecturers.join(', ');
    output.location = output.location.join(', ');
    output.lectures = output.lectures.join(', ');
    output.createdAt = moment(input.createdAt).format('YYYY-MM-DD hh:mm');
    return output;
  },
  cycleListInputData(input) {
    const {
      id,
      title,
      price,
      buyFull,
      author,
      tags,
      lectures,
    } = input;
    const output = {
      id,
      title,
      price,
      buyFull,
    };
    output.date = [];
    output.lecturers = [];
    output.location = [];
    output.lectures = [];
    output.author = `${input.author.firstName} ${input.author.lastName}`;
    input.lectures.forEach(({ title, events }) => {
      output.lectures.push(title);
      events.forEach(({ date, price, lecturers, location }) => {
        output.lecturers.push(lecturers.map(({ firstName, lastName }) => `${firstName} ${lastName}`).join(', '));
        output.location.push(location.title);
        output.date.push(moment(date).format('YYYY-MM-DD hh:mm'));
      });
    });
    output.date = output.date.join(', ');
    output.lecturers = output.lecturers.join(', ');
    output.location = output.location.join(', ');
    output.lectures = output.lectures.join(', ');
    output.createdAt = moment(input.createdAt).format('YYYY-MM-DD hh:mm');
    return output;
  },
  lectureListInputData(input) {
    const output = {};
    output.id = input.id;
    output.title = input.title;
    output.tags = input.tags.map(tag => tag.title).join(', ');
    output.date = [];
    output.price = [];
    output.lecturers = [];
    output.location = [];
    input.events.forEach(({ date, price, lecturers, location }) => {
      output.lecturers.push(lecturers.map(({ firstName, lastName }) => `${firstName} ${lastName}`).join(', '));
      output.location.push(location.title);
      output.date.push(moment(date).format('YYYY-MM-DD hh:mm'));
      output.price.push(price);
    });
    output.date = output.date.join(', ');
    output.price = output.price.join(', ');
    output.lecturers = output.lecturers.join(', ');
    output.location = output.location.join(', ');
    output.createdAt = moment(input.createdAt).format('YYYY-MM-DD hh:mm');
    output.author = `${input.author.firstName} ${input.author.lastName}`;
    return output;
  },
  date(date) {
    if (date) return moment(date).format('YYYY-MM-DD');
    return '';
  },
  time(time) {
    if (time) return moment(time).format('HH:mm');
    return '';
  },
  timeofDate(time, n) {
    return moment(n.date).format('HH:mm');
  },
  dateTime(date) {
    return moment(date).format('YYYY-MM-DD  HH:mm');
  },
  discountsToString(discounts) {
    return discounts.map(({ title }) => title).join(',\n');
  },
  lecturersToSting(lecturers) {
    if (!lecturers) return '';
    return (lecturers.filter(item => item).map(({ firstName, lastName }) => `${firstName} ${lastName}`)).join('\n');
  },
  locationToSting(location) {
    return location.title;
  },
  locationsToSting(locations) {
    return (locations.map(({ title }) => title)).join('\n');
  },
  labelDisplayedRows: ({ from, to, count }) => `${from}-${to} из ${count}`,
  tagsTitle: tags => tags.map(tag => tag.title).join(', '),
  arrayToStringComma: (input) => {
    if (Array.isArray(input)) {
      return input.join(', ');
    }
    return input;
  },
  tagsToString(input) {
    if (Array.isArray(input)) {
      return input.map(item => item.title).join(', ');
    }
    return input;
  },
  eventsToLecturersString: input => input.map(
    item => item.lecturers.map(
      ({ firstName, lastName }) => `${firstName} ${lastName}`).join('\n')),
  tagsToIdsArray(input) {
    if (Array.isArray(input)) {
      return input.map(item => item.title);
    }
    return [];
  },
  tagsToTitlesArray(input) {
    if (Array.isArray(input)) {
      return input.map(item => item.id);
    }
    return [];
  },
  tagsToTitleString(input) {
    if (Array.isArray(input)) {
      return input.map(item => item.title).join(', ');
    }
    return input;
  },
  url: url => actions => actions.map((actions) => {
    actions.to = `${url}/${actions.to}`;
    return actions;
  }),
  relationInputValueFormat(items) {
    if (!Array.isArray(items)) return items;

    const titles = items.map((item) => {
      const { title, firstName, lastName } = item;
      if (title) {
        return title;
      }
      if (item.firstName && item.lastName) {
        return `${firstName} ${lastName}`;
      }
      return '';
    });
    return titles.join(', ');
  },
  aliasToString(item) {
    if (!(item instanceof Object)) return item;
    return item.alias;
  },
  formatInnerData(obj) {

  },
  authorToSting(author) {
    if (author && author instanceof Object) return `${author.firstName} ${author.lastName}`;
    if (author) return author;
    return '';
  },
  authorId(author, n) {
    if (n.author) return author.id;
    return '';
  },
  // authorToSting(...args) {
  //   // return `${firstName} ${lastName}`;
  //   return '';
  // },
  createdAt(value) {
    return moment(value).format('YYYY-DD-MM HH:mm');
  },
  eventsToDateString(events) {
    if (events && Array.isArray(events)) {
      return (events.map(({ date, time }) => `${date} ${time}`)).join(', ');
    }
    return '';
  },
  eventsToDateArray(events) {
    if (Array.isArray(events)) {
      return events.map(({ date, time }) => `${date} ${time}`);
    }
    return [];
  },
  courseLecturersToTitleString(value, n) {
    if (!n) return '';
    if (!n.lectures) return '';
    const output = [];
    n.lectures.forEach((lecture) => {
      lecture.events.forEach((event) => {
        event.lecturers.forEach(({ firstName, lastName }) => {
          output.push(`${firstName} ${lastName}`);
        });
      });
    });
    return output.join('\n');
  },
  courseLocationToTitleString(value, n) {
    if (!n) return '';
    if (!n.lectures) return '';
    const output = [];
    n.lectures.forEach((lecture) => {
      lecture.events.forEach(({ location }) => {
        output.push(location.title);
      });
    });
    return output.join('\n');
  },
  courseDateToTitleString(value, n) {
    if (!n) return '';
    if (!n.lectures) return '';
    const output = [];
    n.lectures.forEach((lecture) => {
      lecture.events.forEach(({ date }) => {
        output.push(moment(date).format('YYYY-MM-DD HH:mm'));
      });
    });
    return output.join('\n');
  },
  courseLecturesToTitleString(value, n) {
    if (!n) return '';
    if (!n.lectures) return '';
    const output = n.lectures.map(({ title }) => title)
    return output.join('\n');
  },
  lectureLecturersToTitleString(value, n) {
    if (!n) return '';
    if (!n.events) return '';
    const output = [];
    n.events.forEach((event) => {
      event.lecturers.forEach(({ firstName, lastName }) => {
        output.push(`${firstName} ${lastName}`);
      });
    });
    return output.join(', ');
  },
  lectureLocationToTitleString(value, n) {
    if (!n) return '';
    if (!n.events) return '';
    const output = [];
    n.events.forEach(({ location }) => {
      output.push(location.title);
    });
    return output.join(', ');
  },
  lectureDateToTitleString(value, n) {
    if (!n) return '';
    if (!n.events) return '';
    const output = [];
    n.events.forEach(({ date }) => {
      output.push(moment(date).format('YYYY-MM-DD HH:mm'));
    });
    return output.join(', ');
  },

  // tagsToString: input => input.map(item => item.title).join(', '),
};
