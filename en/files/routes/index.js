const express = require('express');
const cuid = require('scuid');
const path = require('path');
const cors = require('cors');
const multer = require('multer');
const fs = require('fs-extra');

const replaceReqUrl = require('./../middlewares/replaceReqUrl');
const fetchFileQuery = require('./../middlewares/fetchFileQuery');
const createFile = require('./../middlewares/createFile');

const {
  WHITE_LIST,
  CONTENT_LENGTH,
  DIR,
  PROJECT_ID,
} = require('./../config');

const router = express.Router();
const serve = express.static('public/files', { index: false });

const dir = `${DIR}/${PROJECT_ID}`;

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    fs.ensureDir(dir, (err) => {
      callback(err, dir);
    });
  },
  filename: (req, file, callback) => {
    const secret = cuid();
    file.secret = secret;
    callback(null, `${secret}_${file.originalname}`);
  },
});

const upload = multer({
  storage,
  limits: {
    fileSize: CONTENT_LENGTH,
    files: 1,
  },
});

const corsOptions = {
  origin: (origin, callback) => {
    if (WHITE_LIST.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
};

/* GET home page. */
router.get('/', (req, res, next) => {
  res.send('OK');
});

router.get('/file/:secret', fetchFileQuery, replaceReqUrl, serve);

router.get('/file/:secret/:filename', fetchFileQuery, replaceReqUrl, serve);

router.options('/file', cors(corsOptions));
router.post('/file', cors(corsOptions), upload.single('data'), createFile);

module.exports = router;
