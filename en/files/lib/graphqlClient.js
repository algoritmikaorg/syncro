const { GraphQLClient } = require('graphql-request');

const {
  PROJECT_ID,
  SIMPLE_API_ENDPOINT,
} = require('./../config');

module.exports = token => new GraphQLClient(`${SIMPLE_API_ENDPOINT}/${PROJECT_ID}`, {
  headers: {
    Authorization: token,
  },
});
