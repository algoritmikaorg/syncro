const graphqlClient = require('./../lib/graphqlClient');

const {
  PROJECT_ID,
  DIR,
  SIMPLE_API_ENDPOINT,
} = require('./../config');

const createFileMutation = `
  mutation CreateFileMutation(
    $contentType: String!
    $name: String!
    $size: Int!
    $url: String!
    $secret: String!
  ){
    createFile(
      contentType: $contentType
      name: $name
      size: $size
      url: $url
      secret: $secret
    ) {
      id
      contentType
      name
      size
      url
      secret
    }
  }
`;

module.exports = (req, res, next) => {
  const {
    file,
  } = req;

  const {
    originalname,
    mimetype,
    secret,
    size,
  } = file;

  const token = req.get('Authorization');

  const variables = {
    contentType: mimetype,
    name: originalname,
    size,
    secret,
    url: `${req.originalUrl}/${secret}`,
  };

  const client = graphqlClient(token);
  client.request(createFileMutation, variables)
    .then(data => res.status(200).json(data.createFile))
    .catch(err => res.status(500).json({ errors: err.response.errors }));
};
