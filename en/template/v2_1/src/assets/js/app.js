import simpleLightbox from 'simplelightbox';

import Spin from './components/spin';
import map from './components/map';
import subscribe from './components/subscribe';
import form from './components/form';
import sticky from './components/sticky';
import showMore from './components/showMore';
import utils from './utils';

window.Spin = Spin;
window.simpleLightbox = simpleLightbox;
window.utils = utils;

const { $, Foundation } = window;

Foundation.Abide.defaults.patterns.alpha_ru = /^[а-яА-Я]+$/;
Foundation.Abide.defaults.patterns.alpha_numeric_ru = /^[а-яА-Я0-9]+$/;
// Foundation.Abide.defaults.patterns.phone = /^[0-9-+()]*$/;
// Foundation.Abide.defaults.patterns.phone = /^((\+?7|8)+([0-9-()]){10})$/;
Foundation.Abide.defaults.patterns.phone = {
  test: (text) => {
    const t = text.replace(/\+|\(|\)|-|_| |/g, '');
    return /^(7|8)[0-9]{10}$/.test(t);
  },
};
Foundation.Abide.defaults.patterns.password = {
  test: (text) => {
    if (/[а-яё"'\s]/i.test(text) || text.length < 8) return false;
    return true;
  },
};

Foundation.Abide.defaults.validators.avatar = (e) => {
  const el = $(e);
  const { size, type } = e[0].files[0];
  const maxSize = parseInt(el.data('max-size'), 10);
  const accept = el.prop('accept').split(', ');
  if (!maxSize) return false;
  return size < maxSize && accept.includes(type);
};

$(document).foundation();

utils.remCalcFn = utils.remCalc($('html')).bind(null);

$(window).ready(() => {
  $('.sticky-container').addClass('sticky-animate');
});

$('.event-dates .more-dates').on('click', (e) => {
  const el = $(e.target);
  const p = el.parent();
  utils.show(p.find('.hidden'));
  utils.hide(el);
});

window.MediaQueryCurrent = Foundation.MediaQuery.current;
$(window).on('changed.zf.mediaquery', (event, newSize, oldSize) => {
  window.MediaQueryCurrent = newSize;
});

sticky();
form();
subscribe();
showMore();
