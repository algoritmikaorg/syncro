import anchors from '../components/anchors';
import sortable from '../components/sortable';
import sortableDate from '../components/sortableDate';
import sortableSubDate from '../components/sortableSubDate';
import Search from '../components/search';

const { $ } = window;

anchors({
  stHeader: $('.st-inner'),
  elements: $('.calendar-block'),
  points: $('.anchor-date-point'),
  anchorClass: 'date-anc',
  findInContainer: true,
  itemsSelector: 'date-anc',
  qf: 60,
  qfCl: 60,
  findEl: (e, anchorClass) => {
    const c = $(e.target);
    let el = null;
    if (c.hasClass(anchorClass)) {
      el = c;
    }
    return el;
  },
});

sortable();
sortableDate();
sortableSubDate();
const search = new Search($('.search-container'), template_search);
