// import $ from 'jquery';
const $ = window.$;

const utils = {
  remCalc(rootEl) {
    return (
      n => (
        (parseFloat(rootEl.css('font-size')) * n) / 16
      )
    );
  },
  show: (el, inlineBlock) => {
    el.removeClass('hidden');
    if (inlineBlock) {
      el.addClass('visible-inline-block');
    } else {
      el.addClass('visible');
    }
  },
  hide: (el) => {
    el.removeClass('visible');
    el.addClass('hidden');
  },
  sortItems(items, alias, dataSelector) {
    const self = this;
    // const visibleElements = [];
    // const hiddenElements = [];
    if (alias === 'all') {
      self.show(items);
    } else {
      items.each((i, e) => {
        const el = $(e);
        const dateEl = el.parents('.sortable-date-item');
        if (el.data(dataSelector) === alias) {
          self.show(el);
          self.show(dateEl);
          // visibleElements.push(el);
        } else {
          self.hide(el);
          if (dateEl.find('.sortable-item:visible').length === 0) self.hide(dateEl);
          // hiddenElements.push(el);
        }
      });
    }
  },
  sortItemsMany(items, alias, dataSelector) {
    const self = this;
    // const visibleElements = [];
    // const hiddenElements = [];
    if (alias.length === 0) {
      self.show(items);
    } else {
      items.each((i, e) => {
        const el = $(e);
        const elAlias = el.data(dataSelector);
        const dateEl = el.parents('.sortable-date-item');

        if (alias.indexOf(elAlias) !== -1) {
          self.show(el);
          self.show(dateEl);
          // visibleElements.push(el);
        } else {
          self.hide(el);
          if (dateEl.find('.sortable-item:visible').length === 0) self.hide(dateEl);
          // hiddenElements.push(el);
        }
      });
    }
  },
  setActive: (container, el) => {
    container.find('.active').removeClass('active');
    el.addClass('active');
  },
  setActiveMany: (container, el, alias, firstEl) => {
    if (firstEl) {
      if (alias.length === 0) {
        container.find(firstEl).eq(0).addClass('active');
      } else {
        container.find(firstEl).eq(0).removeClass('active');
      }
    }

    if (el.hasClass('active')) {
      el.removeClass('active');
    } else {
      el.addClass('active');
    }
  },
  checkNoEvents(el, noEvens) {
    if (el.length === 0) {
      this.show(noEvens);
    } else {
      this.hide(noEvens);
    }
  },
};

export default utils;
