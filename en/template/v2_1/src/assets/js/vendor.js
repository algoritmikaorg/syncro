import $ from 'jquery';
import moment from 'moment';
// import Foundation from 'foundation-sites';
import whatInput from 'what-input';
import EventEmitter from 'events';
import Swiper from 'swiper';
import Foundation from './lib/foundation-explicit-pieces';

moment.locale('ru');

// window.jQuery = $;
// window.$ = $;
// window.Foundation = Foundation;
window.moment = moment;
window.whatInput = whatInput;
window.EventEmitter = EventEmitter;
window.Swiper = Swiper;
