// import EventEmitter from 'events';
// import moment from 'moment';
// import pug from 'pug';
// import $ from 'jquery';

const EventEmitter = require('events');
const moment = require('moment');
const pug = require('pug');
const $ = require('jquery');

class Calendar extends EventEmitter {
  constructor(date, calendarEvents, container, template) {
    super();
    if (!template) {
      throw new Error('No template');
    }

    this.activeDate = null;
    this.activeMonth = {
      firstDate: null,
      lastDate: null,
    };

    this.pugCompiledFunction = pug.compile(template);
    this.calendarEvents = calendarEvents || [];
    this.elements = {};
    this.handleChangeMonth = this.handleChangeMonth.bind();
    this.handleSelectDate = this.handleSelectDate.bind();
    this.setActiveDate();
    this.setDates();
    this.render();
  }

  setDates(d) {
    const date = d || this.activeDate;

    this.activeMonth = {
      firstDate: moment(date).utc().startOf('month'),
      lastDate: moment(date).utc().endOf('month'),
    };
  }

  setActiveDate(date) {
    this.activeDate = moment(date || new Date()).utc();
  }

  handleChangeMonth(e) {
    const el = $(e.target);
    let reRender = false;
    const currentMonth = moment(this.activeMonth.firstDate);
    const anotherMonth = moment(currentMonth);

    if (el.hasClass('next')) {
      this.setDates(anotherMonth.add(1, 'M'));
      this.emit('nextMonth', currentMonth, anotherMonth);
      reRender = true;
    } else if (el.hasClass('prev')) {
      this.setDates(anotherMonth.subtract(1, 'M'));
      this.emit('prevMonth', currentMonth, anotherMonth);
      reRender = true;
    }

    if (reRender) {
      this.render();
    }
  }

  handleSelectDate(e) {
    const el = $(e.target);
    if (el.hasClass('date-anc') && el.hasClass('event')) {
      container.find('.active').removeClass('active');
      el.parent().addClass('active');
      this.emit('selectDate', el.data('date'));
    }
  }

  actions() {
    const container = this.container;
    const navEl = container.find('.nav-calendar');
    const datesEl = container.find('.date-wrap');
    navEl.on('click', this.handleChangeMonth);
    datesEl.on('click', this.handleSelectDate);
  }

  render() {
    const date = moment(this.activeMonth.firstDate).utc();
    const lastDate = this.activeMonth.lastDate;

    const dates = [];

    while (true) {
      const diff = date.diff(lastDate, 'h');

      if (diff > 0) {
        break;
      }

      const activeDateDiff = date.diff(this.activeDate, 'h');
      const item = {
        class: (activeDateDiff > -24 && activeDateDiff < 0) ? 'active' : '',
        date: date.toISOString(),
        text: date.format('D'),
        href: date.format('#MM/DD'),
      };

      dates.push(item);
      date.add('1', 'd');
    }

    const html = this.pugCompiledFunction({ dates });
  }
}

const template = 'each item in dates \n\tli.date(class=item.class, data-date=item.date)= item.text';

const calendar = new Calendar(new Date(), [], {}, template);
