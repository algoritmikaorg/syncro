const $ = window.$;
const Foundation = window.Foundation;
const moment = window.moment;

// moment.locale('ru');

const checkTickets = (val) => {
  const n = parseInt(val, 10);
  if (Number.isNaN(n)) return 1;
  return n;
};

const renderFormParticipants = (order, participantsList, abidePull) => () => {
  const { tickets } = order;
  const participantsItems = participantsList.find('li');
  const l = participantsItems.length;
  if (tickets !== l) {
    if (tickets > l) {
      for (let i = l; i < tickets; i += 1) {
        const els = $(template_participant({ n: i + 1 }));
        participantsList.append(els);
        const abide = new Foundation.Abide(els);
        abidePull.push(abide);
      }
    } else if (tickets < l) {
      const r = l - tickets;
      for (let i = l; i > tickets; i -= 1) {
        participantsItems.eq(i - 1).remove();
        abidePull.splice(i - 1, 1);
      }
    }
  }
};

const showError = (containerOrderError, containerAbideError, orderValid, abidePull) => {
  if (!orderValid) {
    containerOrderError.show();
  } else {
    containerOrderError.hide();
  }
  if (abidePull.length === 0) {
    containerAbideError.show();
  } else {
    containerOrderError.hide();
  }
};

const listnerLectureDateSlide = (slideContainerEl, orderEl, product) => {
  slideContainerEl.on('click', (e) => {
    const el = $(e.target);
    if (el.hasClass('slider-nav')) return false;
    let slideEl = null;
    if (el.hasClass('slide')) {
      slideEl = el;
    } else {
      slideEl = el.parents('.slide');
    }
    const lectureId = slideEl.data('lecture-id');
    const eventId = slideEl.data('event-id');
    let updateOrder = false;
    let setEventDate = false;
    let removeEventDate = false;

    if (product.buyFull) {
      if (!slideEl.hasClass('active')) {
        updateOrder = true;
        setEventDate = true;
        slideEl.parent().find('.active').removeClass('active');
        slideEl.addClass('active');
      }
    } else {
      updateOrder = true;
      if (slideEl.hasClass('active')) {
        removeEventDate = true;
        slideEl.removeClass('active');
      } else {
        setEventDate = true;
        slideEl.parent().find('.active').removeClass('active');
        slideEl.addClass('active');
      }
    }

    const orderLectureEl = orderEl.find(`[data-lecture-id="${lectureId}"]`);
    const date = slideEl.data('lecture-date');
  });
};

const clickBuy = (cantainerEl) => {
  container.find('.process-buy').on('click', (e) => {
    e.preventDefault();
    const el = $(e.target);
    const href = el.data('href');
    let abideValid = false;
    let orderValid = false;

    if (orderValid && abidePull.length > 0) {
      abideValid = abidePull.every(item => item.validateForm());
    }

    showError(containerOrderError, containerAbideError, orderValid, abidePull);

    if (orderValid && abideValid) {
      //
    }
  });
};

const listnerParticipiants = (order, ticketsEl, ticketsElInput) => {
  ticketsElInput.on('change', (e) => {
    const el = $(e.target);
    order.tickets = checkTickets(el.val());
    setParticipants();
  });

  ticketsEl.find('.title-r').on('click', (e) => {
    const el = $(e.target);
    if (el.hasClass('plus') || el.parent().hasClass('plus')) {
      order.tickets += 1;
    }
    if (el.hasClass('minus') || el.parent().hasClass('minus')) {
      if (order.tickets > 0) {
        order.tickets -= 1;
      }
    }
    ticketsElInput.val(order.tickets);
    setParticipants();
  });
};

const init = (slideContainerEl) => {
  slideContainerEl.each((i, e) => {
    $(e).find('.slide').eq(0).trigger('click');
  });

};

const buy = () => {
  const { product } = window;
  const order = {
    tickets: 0,
  };
  const containerEl = $('.buy');
  const lectures = containerEl.find('.lecture-item');
  const isCourse = $('.course-page').length > 0;
  const isCycle = $('.cycle-page').length > 0;
  const isLecture = $('.lecture-page').length > 0;

  const orderEl = containerEl.find('.order');
  const ticketsEl = containerEl.find('.tickets');
  const ticketsElInput = ticketsEl.find('input');
  const slideContainerEl = containerEl.find('.lecture-dates .slider-container');
  const participantsList = containerEl.find('.participants-list');
  const containerOrderError = containerEl.find('.order-error');
  const containerAbideError = containerEl.find('.abide-error');

  let tickets = checkTickets(ticketsElInput.val());

  orderEl.find('li').hide();

  // listner
  listnerLectureDateSlide(slideContainerEl, orderEl, product);
  listnerParticipiants(order, ticketsEl, ticketsElInput);

  // init
  init(slideContainerEl);

};

export default buy;
