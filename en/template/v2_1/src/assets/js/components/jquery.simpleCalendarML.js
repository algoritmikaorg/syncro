;
(function ($, window, moment) {
    $.SimpleCalendarML = function (options, element) {
        this.$el = $(element);
        this.$container = {};
        this._init(options);
    };

    $.SimpleCalendarML.defaults = {
        today: new Date(),
        activeDate: this.today
    };

    $.SimpleCalendarML.prototype = {
        _init: function (options) {
            var self = this;
            this.options = $.extend(true, {}, $.SimpleCalendarML.defaults, options);
            //
            // this._sendAjax(this.options.activeDate, this._renderEventsDate);
            var d = new Date(this.options.activeDate);
            var date = new Date(d.getFullYear() + '-' + (d.getMonth() + 1) + '-1');

            this._sendAjax(date, function (data) {
                self._renderTemplate(true, this.options.activeDate, data);
                self._renderEventsDate(data);
            });

            this._initEvents();
        },
        _initEvents: function () {
            var self = this;
            var options = self.options;
            var activeDate = options.activeDate;

            $('body').on('click', function (e) {
                var el = $(e.target);
                if (el.closest(self.$el.parent().find('*')).length === 0) {
                    if (self.$el.is(':visible')) {
                        self.$el.hide();
                    }
                }

            });

            this.$el.parent().find('.select-date span').on('click', function (e) {

                self.$el.toggle();
            });

            this.$el.find('.prev').on('click', function (e) {
                var el = $(e.target);
                activeDate = new Date(activeDate.getFullYear(), activeDate.getMonth() - 1, 1);
                doEvent(activeDate, 'no-render');
            });
            this.$el.find('.next').on('click', function (e) {
                var el = $(e.target);
                activeDate = new Date(activeDate.getFullYear(), activeDate.getMonth() + 1, 1);
                doEvent(activeDate, 'no-render');
            });

            function doEvent(date, type) {
                // self._sendAjax(date, self._renderEventsDate);
                // self._renderTemplate(false, date);
                self._sendAjax(date, function (data) {
                    self._renderTemplate(false, date, data, type);
                    self._renderEventsDate(data, type);
                });
            }
        },
        _renderTemplate: function (firstRun, date, data) {
            // simple-calendar-ml
            var options = this.options;
            var self = this;
            var i = 0;

            if (firstRun) {
                this.monthWrap = this.$el.find('.month');
                this.dateWrap = this.$el.find('ul.date-wrap');
            }

            this.dateElements = [];
            this.dateWrap.empty();

            var d = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDay = new Date(date.getFullYear(), d.getMonth(), 1);
            var firstDayM = moment(firstDay);

            this.monthWrap.text(moment(date).format('MMMM'));

            var day = null;
            var old = true;

            var firstDayWeek = firstDay.getDay();

            for (i = 1; i < firstDayWeek; i++) {
                day = $('<li/>', {
                    class: 'empty',
                });
                this.dateWrap.append(day);
            }

            for (i = 1; i <= d.getDate(); i++) {
                day = $('<li/>', {
                    class: 'date' + ((date.getDate() === i) ? ' active' : ''),
                    'data-date': i
                }).append($('<a/>', {
                    href: '#' + i,
                    'data-date': i,
                    class: 'date-anc',
                    text: i
                }));

                if (date.getDate() === i) {
                    day.addClass('active');
                    old = false;
                } else {
                    // if (old) {
                    //     day.addClass('old');
                    // }
                }

                if (data[i.toString()]) {
                    day.addClass('event');
                } else {
                    day.addClass('empty');
                }

                var eD = firstDayM.format('e');

                if (eD === '5' || eD === '6') {
                    day.addClass('red');
                }

                this.dateWrap.append(day);
                this.dateElements.push(day);
                day = null;
                firstDayM.add(1, 'd');
            }


        },
        _renderEventsDate: function (data, type) {
            // if (type === 'no-render') {
            //     return false;
            // }

            var self = this;
            self.options.dataWrap.empty();

            if (!$.isEmptyObject(data)) {
                $.each(data, function (index, item) {
                    var date = new Date(item.date);
                    var wrap = $('<div/>', {
                        class: 'row lecture-item'
                    });

                    wrap.append($('<a/>', {
                        name: index
                    }));

                    var top = $('<div/>', {
                        class: 'large-12 medium-12 columns'
                    });

                    var day = $('<span/>', {
                        class: (date.getDay() === 6 || date.getDay() === 7) ? 'red' : '',
                        text: ' ' + moment(item.date).format('dddd')
                    });
                    top.append($('<div/>', {
                            class: 'schedule-date',
                        })
                        .append('<span>' + moment(item.date).format('D MMMM') + '</span>')
                        .append(day));

                    var left = $('<div/>', {
                        class: 'large-3 medium-5 columns'
                    });
                    var right = $('<div/>', {
                        class: 'large-9 medium-7 columns'
                    });

                    self.options.dataWrap.append(wrap);
                    wrap.append(top);
                    wrap.append(left);
                    wrap.append(right);

                    left.append('<div class="photo">' +
                            '<a href="/content/lecture/' + item._id + '">' +
                                '<img src="' +
                                    item.photo + '">' +
                            '</a>' +
                            '<div class="time">в ' +
                            moment(item.date).format('LT') + '<div/>' +
                        '<div/>');

                    var anons = $(item.anons);
                    var location = '';
                    item.location.forEach(function (loc) {
                        location += '<div class="location clearfix">' +
                                '<div class="icon"></div>' +
                                '<h3 class="location-name">' + loc.name + '</h3>' +
                                '<address class="location-address">' + loc.address.street + ' ' + loc.address.house + ' ' + loc.address.room + '</address>' +
                            '</div>';
                    });

                    right.append('<div class="row">' +
                        '<div class="large-12 columns">' +
                            '<a href="/content/lecture/' + item._id + '">' +
                                '<h2 class="anons-title">' + item.name + '</h2>' +
                                '<div class="anons dot-ellipsis dot-resize-update">' + anons.html() +
                                '</div>' +
                            '</a>' +
                        '</div>' +

                        '<div class="large-5 columns">' + location + '</div>' +

                        '<div class="large-3 columns">' +
                            '<div class="buy-ticket float-left">' +
                                '<a href="/content/lecture/' + item._id + '">' +
                                    '<img src="/assets/img/buy-ticket-big.png">' +
                                '</a>' +
                            '</div>' +
                        '</div>' +
                        //
                        '<div class="large-4 columns">' +
                            '<a class="over-date" href="/content/lecture/' + item._id + '"> Другие даты ' +
                            '<span class="arrow-left"></span>' + '</a>' +
                        '</div>' +
                        //
                        '</div>');

                    wrap.find('.dot-ellipsis').dotdotdot();

                });
            } else {
                self.options.dataWrap.append($('<h2>Нет событий</h2>'));
            }
            $('.schedule-sticky').foundation('_calc', true);
            // new Foundation.Sticky($('.schedule-sticky'), {
            //     'data-anchor': 'schedule-content',
            //     'data-margin-top': 5.6
            // });
        },
        _sendAjax: function (date, callback) {
            var self = this;
            $.ajax({
                url: self.options.url,
                method: 'post',
                data: {
                    date: date
                },
                success: function (data) {
                    var output = {};
                    if (data.result) {
                        data.result.forEach(function (item) {
                            output[moment(item.date).format('D')] = item;
                        });
                    }
                    data.result = output;
                    callback.call(self, data.result);
                }
            });

        }
    };

    $.fn.simpleCalendarML = function (options) {
        var instance = $.data(this, 'simpleCalendarML');
        if (typeof options === 'string') {
            var args = Array.prototype.slice.call(arguments, 1);
            this.each(function () {
                instance[options].apply(instance, args);
            });
        } else {
            this.each(function () {
                if (instance) {
                    instance._init();
                } else {
                    instance = $.data(this, 'simpleCalendarML', new $.SimpleCalendarML(options, this));
                }
            });
        }
        return instance;
    };

})(jQuery, window, moment);
