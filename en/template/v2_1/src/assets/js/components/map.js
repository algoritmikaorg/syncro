const init = () => {
  let myMap = null;
  let myPlacemark = null;
  const {
    contactDetails: {
      address = '',
      phone = '',
    },
    mapDetails: {
      center = '',
      placemark = '',
    },
  } = window;

  myMap = new ymaps.Map('map', {
    center: center.split(','),
    zoom: 17,
    controls: [],
  });

  myPlacemark = new ymaps.Placemark(
    placemark.split(','),
    {
      hintContent: 'Синхронизация',
      balloonContent: `${address} <br> ${phone}`,
    }, {
      // Опции.
      // Необходимо указать данный тип макета.
      iconLayout: 'default#image',
      // Своё изображение иконки метки.
      iconImageHref: '/assets/img/location.png',
      // Размеры метки.
      iconImageSize: [
        23, 34,
      ],
    // Смещение левого верхнего угла иконки относительно
    // её "ножки" (точки привязки).
    // iconImageOffset: [-5, -38]
    },
  );

  myMap.geoObjects.add(myPlacemark);
};


window.mapOnload = () => {
  if ($('#map').length > 0) {
    window.ymaps.ready(init);
  }
};
