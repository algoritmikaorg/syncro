import utils from '../utils';
const { $ } = window;

const account = () => {
  const editProfile = $('h1 .edit');
  const saveProfile = $('h1 .save');
  const separator = $('h1 .separator');
  const cancelProfile = $('h1 .cancel');
  const profile = $('.form-profile');
  const values = profile.find('.value');
  const inputs = profile.find('input');
  const button = profile.find('button');

  editProfile.on('click', (e) => {
    const el = $(e.target);
    profile.addClass('edit');
    utils.show(saveProfile, true);
    utils.show(separator, true);
    utils.show(cancelProfile, true);
    utils.hide(editProfile);
    //
    utils.show(inputs, true);
    utils.hide(values);
  });

  cancelProfile.on('click', (e) => {
    const el = $(e.target);
    profile.removeClass('edit');
    utils.show(editProfile, true);
    utils.hide(separator);
    utils.hide(cancelProfile);
    utils.hide(saveProfile);
    //
    utils.hide(inputs);
    utils.show(values, true);
  });

  saveProfile.on('click', (e) => {
    button.trigger('click');

    const el = $(e.target);
    profile.removeClass('edit');
    inputs.each((i, e) => {
      const el = $(e);
      el.prev().text(el.val());
    });
    utils.show(editProfile, true);
    utils.hide(separator);
    utils.hide(cancelProfile);
    utils.hide(saveProfile);
    //
    utils.hide(inputs);
    utils.show(values, true);
  });

  $('.button.review').on('click', (e) => {
    const el = $(e.target);
    const lectureId = el.data('lecture-id');
    const eventId = el.data('event-id');
    const reviewFormEl = $('.form-review');
    reviewFormEl.find('[name="lectureId"]').val(lectureId);
    reviewFormEl.find('[name="eventId"]').val(eventId);
    return true;
  });
};

export default account;
