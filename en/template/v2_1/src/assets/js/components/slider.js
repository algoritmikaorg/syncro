const Swiper = window.Swiper;

const sliderBuy = (selector) => {
  const output = [];
  $(selector).find('.lecture-dates').each((i, e) => {
    const el = $(e);
    const navNext = el.find('.slider-button-next');
    const navPrev = el.find('.slider-button-prev');
    const container = el.find('.slider-container');

    const slider = new Swiper(container[0], {
      slidesPerView: 5,
      spaceBetween: 0,
      nextButton: navNext[0],
      prevButton: navPrev[0],
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 0,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 0,
        },
      },
    });
    output.push(slider);
  });
  return output;
};

const slider = () => {
  $(document).ready(() => {
    const selector1 = $('.lecturers .slider-container');
    if (selector1.length > 0) {
      const slider1 = new Swiper(selector1[0], {
        slidesPerView: 3,
        spaceBetween: 118,
        nextButton: '.lecturers .slider-button-next',
        prevButton: '.lecturers .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
          1030: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
        },
      });
    }

    const selector2 = $('.reviews .slider-container');
    if (selector2.length > 0) {
      const slider2 = new Swiper(selector2[0], {
        slidesPerView: 2,
        spaceBetween: 42,
        nextButton: '.reviews .slider-button-next',
        prevButton: '.reviews .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0,
          },
        },
      });
    }

    const selector3 = $('.locations .slider-container');
    if (selector3.length > 0) {
      const slider3 = new Swiper(selector3[0], {
        slidesPerView: 3,
        spaceBetween: 118,
        nextButton: '.locations .slider-button-next',
        prevButton: '.locations .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
          1030: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
        },
      });
    }

    const selector4 = $('.buy .lecture-dates');
    if (selector4.length > 0) {
      const slider4 = sliderBuy($('.buy'));
    }
  });
};

export default slider;
