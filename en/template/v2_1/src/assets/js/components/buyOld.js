// import moment from 'moment';
// import Foundation from 'foundation-sites';
const $ = window.$;
const Foundation = window.Foundation;
const moment = window.moment;

Foundation.Abide.defaults.patterns.alpha_ru = /^[а-яА-Я]+$/;
Foundation.Abide.defaults.patterns.alpha_numeric_ru = /^[а-яА-Я0-9]+$/;
// Foundation.Abide.defaults.patterns.phone = /^[0-9-+()]*$/;
Foundation.Abide.defaults.patterns.phone = /^((\+?7|8)+([0-9-()]){10})$/;

// moment.locale('ru');

const checkTickets = (val) => {
  const n = parseInt(val, 10);
  if (isNaN(n)) return 1;
  return n;
};

class Order {
  constructor(type, lecturesLength, coursePrice, totalPriceEl) {
    this.items = {};
    this.lecturesLength = lecturesLength;
    this.coursePrice = coursePrice || 0;
    this.totalPrice = 0;
    this.totalPriceEl = totalPriceEl;
    this.participants = 0;
    this.type = type;
  }
  addItem(el) {
    const input = el.find('.check-lecture');
    const id = input.data('lecture-id');
    const price = input.data('lecture-price');
    const title = input.data('lecture-title');
    const date = input.find('.swiper-slide.active').data('lecture-date');
    this.items[id] = {
      id,
      price,
      title,
      date: date || null,
    };
    this.calcPrice();
  }
  removeItem(id) {
    delete this.items[id];
    this.calcPrice();
  }
  changeDate(id, date) {
    if (Object.prototype.hasOwnProperty.call(this.items, id)) {
      this.items[id].date = date;
    }
  }
  valid() {
    return Object.keys(this.items).every(key => this.items[key].date);
  }
  addParticipants() {
    this.participants += 1;
    this.calcPrice();
  }
  removeParticipants() {
    if (this.participants > 0) {
      this.participants -= 1;
    }
    this.calcPrice();
  }
  calc(keys) {
    let output = 0;
    keys.forEach((key) => {
      output += this.items[key].price;
    });
    return output;
  }
  calcPrice() {
    this.totalPrice = 0;
    const keys = Object.keys(this.items);
    switch (this.type) {
      case 'course': {
        if (keys.length === this.lecturesLength) {
          this.totalPrice = this.coursePrice;
        } else {
          this.totalPrice = this.calc(keys);
        }
        break;
      }
      default: {
        this.totalPrice = this.calc(keys);
      }
    }
    this.totalPrice *= this.participants;
    this.totalPriceEl.text(this.totalPrice);
  }
}

const showError = (containerOrderError, containerAbideError, orderValid, abidePull) => {
  if (!orderValid) {
    containerOrderError.show();
  } else {
    containerOrderError.hide();
  }
  if (abidePull.length === 0) {
    containerAbideError.show();
  } else {
    containerOrderError.hide();
  }
};

const buy = () => {
  const abidePull = [];
  const container = $('.buy');
  const lectures = container.find('.lecture-item');
  const coursePage = $('.course-page').length > 0;
  const lecturePage = $('.lecture-page').length > 0;
  const cyclePage = $('.cycle-page').length > 0;
  const type = coursePage ? 'course' : 'other';
  const order = new Order(type, lectures.length, parseFloat($('[data-course-price]').data('course-price') || 0), container.find('.total .total-item'));

  const orderEl = container.find('.order');
  const ticketsEl = container.find('.tickets');
  const ticketsElInput = ticketsEl.find('input');
  const selDateEl = container.find('.lecture-dates .slider-container');
  const participantsList = container.find('.participants-list');
  const containerOrderError = container.find('.order-error');
  const containerAbideError = container.find('.abide-error');

  let tickets = checkTickets(ticketsElInput.val());

  const setParticipants = () => {
    const participantsItems = participantsList.find('li');
    const l = participantsItems.length;
    if (tickets !== l) {
      if (tickets > l) {
        for (let i = l; i < tickets; i += 1) {
          const els = $(template_participant({ n: i + 1 }));
          participantsList.append(els);
          const abide = new Foundation.Abide(els);
          abidePull.push(abide);
          order.addParticipants();
        }
      } else if (tickets < l) {
        const r = l - tickets;
        for (let i = l; i > tickets; i -= 1) {
          participantsItems.eq(i - 1).remove();
          abidePull.splice(i - 1, 1);
          order.removeParticipants();
        }
      }
    }
  };


  // init
  // if (window.MediaQueryCurrent !== 'small') {
  //   lectures.each((i, e) => {
  //     const el = $(e);
  //     order.addItem(el);
  //   });
  // } else {
  //   orderEl.find('li').hide();
  // }
  orderEl.find('li').hide();
  //

  container.find('.process-buy').on('click', (e) => {
    const el = $(e.target);
    const href = el.data('href');
    let orderValid = false;
    let abideValid = false;
    orderValid = order.valid();
    if (orderValid && abidePull.length > 0) {
      abideValid = abidePull.every(item => item.validateForm());
    }
    if (orderValid && abideValid) {
      window.location.href = `${window.location.origin}/${href}`;
    }
    showError(containerOrderError, containerAbideError, orderValid, abidePull);
    e.preventDefault();
  });

  selDateEl.on('click', (e) => {
    const el = $(e.target);
    if (el.hasClass('slider-nav')) return false;
    let p = null;
    let id = '';
    let set = false;
    if (el.hasClass('slide')) {
      p = el;
    } else {
      p = el.parents('.slide');
    }
    id = p.data('lecture-id');
    if (p.hasClass('active')) {
      p.removeClass('active');
    } else {
      p.parent().find('.active').removeClass('active');
      set = true;
      p.addClass('active');
    }
    const date = p.data('lecture-date');
    const orderLectureEl = orderEl.find(`[data-lecture-id="${id}"]`);

    // if (window.MediaQueryCurrent === 'small') {
    //   if (set) {
    //     order.addItem(lectures.find(`[data-lecture-id="${id}"]`).parents('.lecture-item'));
    //   } else {
    //     order.removeItem(id);
    //   }
    // }

    if (set) {
      order.addItem(lectures.find(`[data-lecture-id="${id}"]`).parents('.lecture-item'));
    } else {
      order.removeItem(id);
    }

    if (set) {
      orderLectureEl.show();
      orderLectureEl.find('.date').show();
      orderLectureEl.find('.date-empty').hide();
      orderLectureEl.find('.l-month').text(moment(date).format('D MMMM'));
      orderLectureEl.find('.l-time').text(moment(date).format('HH:mm'));
      order.changeDate(id, date);
    } else {
      // if (window.MediaQueryCurrent === 'small') {
      //   orderLectureEl.find('.date-empty').show();
      //   orderLectureEl.find('.date').hide();
      //   orderLectureEl.find('.l-month').text('');
      //   orderLectureEl.find('.l-time').text('');
      //   orderLectureEl.hide();
      // } else {
      //   orderLectureEl.find('.date-empty').show();
      //   orderLectureEl.find('.date').hide();
      //   orderLectureEl.find('.l-month').text('');
      //   orderLectureEl.find('.l-time').text('');
      //   order.changeDate(id, null);
      // }
      orderLectureEl.find('.date-empty').show();
      orderLectureEl.find('.date').hide();
      orderLectureEl.find('.l-month').text('');
      orderLectureEl.find('.l-time').text('');
      orderLectureEl.hide();
    }
  });

  ticketsElInput.on('change', (e) => {
    const el = $(e.target);
    tickets = checkTickets(el.val());
    setParticipants();
  });

  ticketsEl.find('.title-r').on('click', (e) => {
    const el = $(e.target);
    if (el.hasClass('plus') || el.parent().hasClass('plus')) {
      tickets += 1;
    }
    if (el.hasClass('minus') || el.parent().hasClass('minus')) {
      if (tickets > 0) {
        tickets -= 1;
      }
    }
    ticketsElInput.val(tickets);
    setParticipants();
  });

  if (!cyclePage) {
    selDateEl.each((i, e) => {
      const el = $(e);
      el.find('.slide').eq(0).trigger('click');
    });
  }
  ticketsEl.find('.plus').trigger('click');
  // lectures.find('.check-lecture').on('change', (e) => {
  //   if (window.MediaQueryCurrent !== 'small') {
  //     const el = $(e.target);
  //     const id = el.data('lecture-id');
  //     const price = parseFloat(el.data('lecture-price'));
  //     const parent = el.parents('.lecture-item');
  //
  //     if (el.prop('checked')) {
  //       orderEl.find(`[data-lecture-id="${id}"]`).show();
  //       totalPrice += price;
  //       order.addItem(parent);
  //     } else {
  //       orderEl.find(`[data-lecture-id="${id}"]`).hide();
  //       totalPrice -= price;
  //       order.removeItem(id);
  //     }
  //     setTotalPrice();
  //   }
  // });
};

export default buy;
