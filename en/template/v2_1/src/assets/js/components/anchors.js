// const $ = window.$;
const $ = window.$;

// const remCalc = (rootEl) => ((n) => n / parseFloat(rootEl.css('font-size')));
const remCalc = rootEl => (
  n => (
    (parseFloat(rootEl.css('font-size')) * n) / 16
  )
);

let remCalcFn = null;

const onScroll = (doc, menu, ids, qf, qfF, stHeader, findInContainer, itemsSelector) => {
  let menuItems = null;
  const scrollPos = doc.scrollTop();

  if (findInContainer) {
    menuItems = menu.find(itemsSelector);
  } else {
    menuItems = menu;
  }

  menuItems.each((i, e) => {
    const currLink = $(e);
    // const refElement = ids.filter(currLink.attr('href'));
    const refElement = $(currLink.attr('href'));
    const refElTop = Math.floor(refElement.position().top);
    const refHeight = refElement.height();
    if (
        refElTop - stHeader.height() - remCalcFn(qf) - 1 <= scrollPos
        && refElTop + refElement.height() > scrollPos
    ) {
      menu.removeClass('active');
      currLink.addClass('active');
      // return false;
    } else {
      currLink.removeClass('active');
    }
  });
};

const findEl = (e, anchorClass) => {
  const c = $(e.target);
  let el = null;
  if (c.hasClass(anchorClass)) {
    el = c;
  } else {
    el = c.parent();
  }
  return el;
};

const anchors = (options = {}) => {
  remCalcFn = remCalc($('html')).bind(null);
  const doc = $(document);
  const fnFindEL = options.findEl || findEl;
  const dynamic = options.dynamic || false;
  const container = options.container || $('html, body');
  const stHeader = options.stHeader || $('.anchors-sticky-inner');
  const elements = options.elements || $('.anchor');
  const findInContainer = options.findInContainer || false;
  const itemsSelector = options.itemsSelector || '';

  const menu = options.menu || elements;
  const qf = options.qf || 30;
  const qfF = options.qfF || 0;
  const qfCl = options.qfCl || 0;
  const points = options.points || $('.anchor-point');
  const anchorClass = options.anchorClass || 'anchor';

  const fnScroll = onScroll.bind(
    null,
    doc,
    menu,
    points,
    qf,
    qfF,
    stHeader,
    findInContainer,
    itemsSelector);

  doc.on('scroll', fnScroll);

  elements.on('click', (e) => {
    const el = fnFindEL(e, anchorClass);
    if (!el || el.length === 0) {
      return true;
    }

    e.preventDefault();
    doc.off('scroll');

    elements.removeClass('active');
    el.addClass('active');

    const href = el.attr('href');
    let scrollTop = 0;

    // if (stHeader.hasClass('is-stuck')) {
    //   scrollTop = $(href).offset().top - stHeader.outerHeight() - qf;
    // } else {
    //   scrollTop = $(href).offset().top - stHeader.outerHeight() - qfCl;
    // }

    if (stHeader.hasClass('is-stuck')) {
      scrollTop = $(href).offset().top - stHeader.outerHeight() - remCalcFn(qfCl);
    } else {
      scrollTop = ($(href).offset().top + remCalcFn(qfF)) - stHeader.outerHeight() - remCalcFn(qfCl);
    }
    container.stop().animate(
      {
        scrollTop,
      },
      500,
      'swing',
      () => {
        window.location.hash = href;
        doc.on('scroll', fnScroll);
      });
    return false;
  });
};

export default anchors;
