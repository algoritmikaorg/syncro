const { $, utils } = window;

const sticky = () => {
  const navMain = $('.st-wrap');
  const navMainSticky = navMain.find('.st-inner');
  const navAnchor = $('.anchors-sticky-container');
  const navAnchorSticky = navAnchor.find('.anchors-sticky-inner');
  const navAnchorStickyLogo = navAnchorSticky.find('.logo-container');
  const navAnchorStickyAnchors = navAnchorSticky.find('.anchors');
  const $window = $(window);
  let prev = 0;
  $window.on('scroll', () => {
    const scrollTop = $window.scrollTop();
    if (navMainSticky.hasClass('is-stuck')) {
      navMain.toggleClass('mobile-hidden', scrollTop > prev && scrollTop > 100);
    }
    if (navAnchorSticky.hasClass('is-stuck')) {
      // const conHeight = navAnchorSticky.outerHeight() - navAnchorStickyAnchors.outerHeight();
      //

      // if (navAnchorSticky.hasClass('mobile-hidden')) {
      //   navAnchorSticky.css({ transform: 'translateY(0)' });
      // } else {
      //   navAnchorSticky.css({ transform: `translateY(-${utils.remCalcFn(conHeight)}px)` });
      // }
      navAnchorSticky.toggleClass('mobile-hidden', scrollTop > prev && scrollTop > 100);
    }
    prev = scrollTop;
  });
};

export default sticky;
