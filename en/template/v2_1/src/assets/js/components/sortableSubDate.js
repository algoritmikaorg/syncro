const { $, utils } = window;

const sortableSubDate = () => {
  const container = $('.filter-data-sub-container');
  const items = $('.sortable-items');
  const dataSelector = 'filter-date-alias';
  const noEventsGlb = $('.no-events-global');

  container.each((i, e) => {
    const containerActual = $(e);
    const allDate = items.find('[data-filter-date-alias]');
    const tomorrowitems = items.find('[data-filter-date-sub-tomorrow]');
    const holidayItems = items.find('[data-filter-date-sub-holiday]');
    const sortingType = containerActual.data('sorting-type');

    containerActual.on('click', (event) => {
      const el = $(event.target);
      if (el.hasClass('filter-sub-item')) {
        event.preventDefault();
        const elAlias = el.data(dataSelector);
        switch (elAlias) {
          case 'all': {
            utils.show(allDate);
            utils.checkNoEvents(allDate, noEventsGlb);
            break;
          }
          case 'holiday': {
            utils.hide(allDate);
            utils.show(holidayItems);
            utils.checkNoEvents(holidayItems, noEventsGlb);
            break;
          }
          case 'tomorrow': {
            utils.hide(allDate);
            utils.show(tomorrowitems);
            utils.checkNoEvents(tomorrowitems, noEventsGlb);
            break;
          }
          default: {}
        }
      }
    });
  });
};

export default sortableSubDate;
