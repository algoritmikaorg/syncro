const { $, whatInput, Swiper, simpleLightbox } = window;

//
const resizeGallery = (el) => {
  const a = el.find('a');
  const width = el.find('.swiper-slide').eq(0).width();
  a.height(width / 1.44);
};

const gallery = () => {
  const container = $('.gallery-container');
  $(document).ready(() => {
    const slider = new Swiper('.gallery-container', {
      slidesPerView: 4,
      spaceBetween: 0,
      nextButton: '.nav-slider-next',
      prevButton: '.nav-slider-prev',
      breakpoints: {
        // // when window width is <= 320px
        // 320: {
        //   slidesPerView: 1,
        //   spaceBetween: 10
        // },
        // // when window width is <= 480px
        // 480: {
        //   slidesPerView: 2,
        //   spaceBetween: 20
        // },
        // // when window width is <= 640px
        640: {
          slidesPerView: 1,
          spaceBetween: 0,
        },
      },
      // autoHeight: true
    });

    $('.gallery-lightbox a').simpleLightbox({
      captionSelector: 'self',
      captionType: 'attr',
      captionsData: 'title',
      fileExt: null,
      alertError: false,
    });
    resizeGallery(container);
  });

  $(window).resize(() => {
    resizeGallery(container);
  });
};

export default gallery;
