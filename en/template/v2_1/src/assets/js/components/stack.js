class Stack {
  constructor() {
    this.store = [];
    this.push = this.push.bind(this);
    this.pop = this.pop.bind(this);
    this.peek = this.peek.bind(this);
    this.length = this.length.bind(this);
    this.isEmpty = this.isEmpty.bind(this);
    this.getStack = this.getStack.bind(this);
    this.clear = this.clear.bind(this);
  }
  push(...args) {
    return Array.prototype.push.apply(this.store, args);
  }
  pop() {
    return Array.prototype.pop.call(this.store);
  }
  peek() {
    const { store } = this;
    return store[store.length - 1];
  }
  length() {
    return this.store.length;
  }
  isEmpty() {
    return this.store.length === 0;
  }
  getStack() {
    return this.store.slice();
  }
  clear() {
    this.store = [];
  }
}

export default Stack;
