const { $ } = window;

class Spin {
  constructor() {
    this.el = $('.spin-glb');
    this.elements = {};
  }
  show(container) {
    if (!container || container.length === 0) return undefined;

    if (this.el.length === 0) this.el = $('.spin-glb');

    const id = Date.now().toString();
    const cEl = this.el.clone();
    this.elements[id] = cEl;
    container.append(cEl);
    cEl.show();
    return id;
  }
  hide(id) {
    if (id) {
      this.elements[id].hide();
      this.elements[id].remove();
      delete this.elements[id];
    }
  }
}

export default Spin;
