import Stack from './stack';

class Queue {
  constructor() {
    this.queueStack = new Stack();
    this.delay = 300;
    this.successCallback = null;
    this.errorCallback = null;
    this.workerFn = null;

    this.run = this.run.bind(this);
    this.push = this.push.bind(this);
    this.clear = this.clear.bind(this);
    this.next = this.next.bind(this);
  }

  set stack(stack) {
    this.queueStack = stack;
  }
  set success(success) {
    this.successCallback = success;
  }
  set error(error) {
    this.errorCallback = error;
  }
  set worker(fn) {
    this.workerFn = fn;
  }

  push(value) {
    const output = this.queueStack.push(value);
    this.run();
    return output;
  }

  clear() {
    this.queueStack.clear();
  }

  run() {
    const {
      delay,
      next,
      queueStack,
      workerFn,
    } = this;
    setTimeout(() => {
      const value = queueStack.pop();
      queueStack.clear();

      if (!value || !(workerFn instanceof Function)) return undefined;

      return workerFn(value, next);
    }, delay);
  }

  next(err, data) {
    const {
      queueStack,
      workerFn,
      errorCallback,
      successCallback,
      run,
    } = this;

    if (err) {
      if (errorCallback instanceof Function) return errorCallback(err);
      return undefined;
    }
    if (data) {
      queueStack.clear();
      if (successCallback instanceof Function) return successCallback(data);
      return undefined;
    }
    if (!queueStack || queueStack.isEmpty()) {
      return undefined;
    }

    return run();
  }
}

export default Queue;
