const moment = require('moment');
const mandrill = require('./../../mandrill');

exports.upcoming = (req) => {
  const { body: { data: { allEvents } } } = req;

  const templateName = 'mandrillTemplateSlugReminderOfLecture';
  const tags = ['reminder-upcoming-event'];

  // const subject = `Напоминание о лекции "${lecture.title}" ${moment(date).format('LLL')}`;
  const subject = 'Напоминание о лекции';

  let users = [];

  const mergeVars = allEvents.map(({
    date,
    lecture,
    course = { alias: {} },
    cycle = { alias: {} },
    location,
    tickets,
  }) => {
    const currentUsers = tickets.map(({ user }) => user);
    users = users.concat(currentUsers);
    if (!currentUsers || currentUsers.length === 0) return [];

    const formatData = moment(date).format('LLL');

    return currentUsers
      .map(({ email, firstName, lastName }) => ({
        rcpt: email,
        vars: [
          {
            name: 'DATE',
            content: formatData,
          },
          {
            name: 'LECTURE_TITLE',
            content: lecture.title,
          },
          {
            name: 'COURSE_TITLE',
            content: course.title,
          },
          {
            name: 'CYCLE_TITLE',
            content: cycle.title,
          },
          {
            name: 'LECTURE_ALIAS',
            content: lecture.alias.alias,
          },
          {
            name: 'COURSE_ALIAS',
            content: course.alias.alias,
          },
          {
            name: 'CYCLE_ALIAS',
            content: cycle.alias.alias,
          },
          {
            name: 'LOCATION_TITLE',
            content: location.title,
          },
          {
            name: 'LOCATION_ADDRESS',
            content: location.address,
          },
          {
            name: 'LOCATION_METRO',
            content: location.metro,
          },
        ],
      }));
  })
    .reduce((prev, curr) => prev.concat(curr));

  if (mergeVars.length === 0) return Promise.reject(new Error('No data'));

  return mandrill(users, {
    name: templateName,
    tags,
    subject,
    mergeVars,
  });
};

exports.nonPayment = (req) => {
  const { body: { data: { allEvents } } } = req;

  const templateName = 'mandrillTemplateSlugOrderWithoutPayment';
  const tags = ['reminder-non-payment'];
  // const subject = `Неоплаченный заказ "${lecture.title}" ${moment(date).format('LLL')}`;
  const subject = 'Неоплаченный заказ';

  let users = [];

  const mergeVars = allEvents.map(({
    date,
    lecture,
    course = { alias: {} },
    cycle = { alias: {} },
    location,
    orders,
  }) => {
    const payments = orders.map(({ payment }) => payment);
    const currentUsers = payments.map(({ user }) => user);
    users = users.concat(currentUsers);
    if (!currentUsers || currentUsers.length === 0) return [];

    const formatData = moment(date).format('LLL');

    return payments
      .map(({ user: { email, firstName, lastName } }, id) => ({
        rcpt: email,
        vars: [
          {
            name: 'DATE',
            content: formatData,
          },
          {
            name: 'LECTURE_TITLE',
            content: lecture.title,
          },
          {
            name: 'COURSE_TITLE',
            content: course.title,
          },
          {
            name: 'CYCLE_TITLE',
            content: cycle.title,
          },
          {
            name: 'LECTURE_ALIAS',
            content: lecture.alias.alias,
          },
          {
            name: 'COURSE_ALIAS',
            content: course.alias.alias,
          },
          {
            name: 'CYCLE_ALIAS',
            content: cycle.alias.alias,
          },
          {
            name: 'LOCATION_TITLE',
            content: location.title,
          },
          {
            name: 'LOCATION_ADDRESS',
            content: location.address,
          },
          {
            name: 'LOCATION_METRO',
            content: location.metro,
          },
        ],
      }));
  })
    .reduce((prev, curr) => prev.concat(curr));

  if (mergeVars.length === 0) return Promise.reject(new Error('No data'));

  return mandrill(users, {
    name: templateName,
    tags,
    subject,
    mergeVars,
  });
};

exports.after = (req) => {
  const { body: { data: { Event } } } = req;

  const templateName = 'mandrillTemplateSlugMaterialsOfLecture';
  const tags = ['after-event'];

  const subject = 'Материалы лекции';

  const {
    date,
    lecture,
    course = { alias: {} },
    cycle = { alias: {} },
    location,
    tickets,
  } = Event;

  if (tickets.length) return Promise.resolve();

  const formatData = moment(date).format('LLL');

  const users = tickets.map(({ user }) => user);

  const mergeVars = tickets
    .map(({ email, firstName, lastName }) => ({
      rcpt: email,
      vars: [
        {
          name: 'DATE',
          content: formatData,
        },
        {
          name: 'LECTURE_TITLE',
          content: lecture.title,
        },
        {
          name: 'COURSE_TITLE',
          content: course.title,
        },
        {
          name: 'CYCLE_TITLE',
          content: cycle.title,
        },
        {
          name: 'LECTURE_ALIAS',
          content: lecture.alias.alias,
        },
        {
          name: 'COURSE_ALIAS',
          content: course.alias.alias,
        },
        {
          name: 'CYCLE_ALIAS',
          content: cycle.alias.alias,
        },
        {
          name: 'LOCATION_TITLE',
          content: location.title,
        },
        {
          name: 'LOCATION_ADDRESS',
          content: location.address,
        },
        {
          name: 'LOCATION_METRO',
          content: location.metro,
        },
      ],
    }));

  if (mergeVars.length === 0) return Promise.reject(new Error('No data'));

  return mandrill(users, {
    name: templateName,
    tags,
    subject,
    mergeVars,
  });
};
