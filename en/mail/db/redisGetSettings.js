const redis = require('./redis');

module.exports = (input, template) => {
  const { name: templateName } = template;
  return redis.getAsync('setting')
    .then((reply) => {
      if (!reply) return Promise.reject(new Error('No settings found'));
      const settings = JSON.parse(reply);
      if (!settings[templateName]) return Promise.reject(new Error('Template not found'));
      if (!settings.senderEmail || !settings.senderName || !settings.mandrillAPIKey) return Promise.reject(new Error('settings param not found'));

      return settings;
    });
};
