const crypto = require('crypto');
const hash = crypto.createHash('md5');

module.exports = (data) => {
  if (!data) return undefined;
  return hash.update(data).digest('hex');
};
