const settingsAndUserByEmailQuery = require('./../graphql/settingsAndUserByEmailQuery');
const MailChimpApi = require('./libs/mailChimpApi');
const mailChimpApi = new MailChimpApi();

module.exports = (list, input) => {
  const { email } = input;

  return settingsAndUserByEmailQuery(email)
    .then((result) => {
      const [settings, user] = result;
      const { mailChimpMainList, mailChimpReactivationList, mailChimpNewSchedule } = settings;

      const subscriber = user ? user : { email };

      mailChimpApi.setConfig(settings);
      switch (list) {
        case 'common': {
          return mailChimpApi.subscribe(mailChimpMainList, subscriber);
        }
        case 'schedule': {
          return mailChimpApi.subscribe(mailChimpNewSchedule, subscriber);
        }
        default: {
          return Promise.reject();
        }
      }
    });
};
