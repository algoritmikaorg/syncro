const mandrillAPI = require('mandrill-api');

const sendTemplate = (data, template, settings) => new Promise((resolve, reject) => {
  const {
    mandrillAPIKey,
    senderEmail,
    senderName,
  } = settings;

  const {
    subject,
    content = [],
    mergeVars = [],
    tags,
    name,
  } = template;

  let to = [];
  if (Array.isArray(data)) {
    to = data.map(({ email, firstName, lastName }) => ({
      email,
      name: `${firstName} ${lastName}`,
      type: 'to',
    }));
  } else {
    const {
      email,
      firstName,
      lastName,
    } = data;
    to.push({
      email,
      name: `${firstName} ${lastName}`,
      type: 'to',
    });
  }

  const mandrillClient = new mandrillAPI.Mandrill(mandrillAPIKey);

  const message = {
    subject,
    from_email: senderEmail,
    from_name: senderName,
    // to: [{
    //   email: "recipient.email@example.com",
    //   name: "Recipient Name",
    //   type: "to",
    // }],
    to,
    important: false,
    track_opens: true,
    track_clicks: true,
    view_content_link: true,
    // bcc_address: settings.bccEmail,
    tracking_domain: null,
    signing_domain: null,
    return_path_domain: null,
    merge: true,
    merge_language: 'mailchimp',
    global_merge_vars: content,
    merge_vars: mergeVars,
    // "global_merge_vars": [{
    //   "name": "merge1",
    //   "content": "merge1 content"
    // }],
    // "merge_vars": [{
    //   "rcpt": "recipient.email@example.com",
    //   "vars": [{
    //     "name": "merge2",
    //     "content": "merge2 content"
    //   }]
    // }],
    tags,
    // "subaccount": "customer-123",
    // "google_analytics_domains": [
    //     "example.com"
    // ],
    // "google_analytics_campaign": "message.from_email@example.com",
    // "metadata": {
    //   "website": "www.example.com"
    // },
    // "recipient_metadata": [{
    //   "rcpt": "recipient.email@example.com",
    //   "values": {
    //     "user_id": 123456
    //   }
    // }],
  };
  const async = false;
  // const ip_pool = "Main Pool";
  // const send_at = "example send_at";
  mandrillClient
    .messages
    .sendTemplate(
      {
        template_name: settings[name],
        template_content: content,
        message,
        async,
        // ip_pool,
        send_at: new Date(Date.now() - 3600000),
      },
      result => resolve(result),
      err => reject(err),
    );
  // try {
  //   mandrillClient
  //     .messages
  //     .sendTemplate(
  //       {
  //         template_name: name,
  //         template_content: content,
  //         message,
  //         async,
  //         // ip_pool,
  //         send_at: new Date(Date.now() - 3600000),
  //       },
  //       result => Promise.resolve(result),
  //       err => Promise.reject(err),
  //     );
  // } catch (err) {
  //   return Promise.reject(err);
  // }
});

// const sendTemplate = (data, template, settings) => {
//   const {
//     mandrillAPIKey,
//     senderEmail,
//     senderName,
//   } = settings;
//
//   const {
//     subject,
//     content,
//     tags,
//     name,
//   } = template;
//
//   const {
//     email,
//     firstName,
//     lastName,
//   } = data;
//
//   const mandrillClient = new mandrillAPI.Mandrill(mandrillAPIKey);
//
//   const message = {
//     subject,
//     from_email: senderEmail,
//     from_name: senderName,
//     // to: [{
//     //   email: "recipient.email@example.com",
//     //   name: "Recipient Name",
//     //   type: "to",
//     // }],
//     to: [
//       {
//         email,
//         name: `${firstName} ${lastName}`,
//         type: 'to',
//       },
//     ],
//     important: false,
//     track_opens: true,
//     track_clicks: true,
//     view_content_link: true,
//     // bcc_address: settings.bccEmail,
//     tracking_domain: null,
//     signing_domain: null,
//     return_path_domain: null,
//     merge: true,
//     merge_language: 'mailchimp',
//     global_merge_vars: content,
//     // "global_merge_vars": [{
//     //   "name": "merge1",
//     //   "content": "merge1 content"
//     // }],
//     // "merge_vars": [{
//     //   "rcpt": "recipient.email@example.com",
//     //   "vars": [{
//     //     "name": "merge2",
//     //     "content": "merge2 content"
//     //   }]
//     // }],
//     tags,
//     // "subaccount": "customer-123",
//     // "google_analytics_domains": [
//     //     "example.com"
//     // ],
//     // "google_analytics_campaign": "message.from_email@example.com",
//     // "metadata": {
//     //   "website": "www.example.com"
//     // },
//     // "recipient_metadata": [{
//     //   "rcpt": "recipient.email@example.com",
//     //   "values": {
//     //     "user_id": 123456
//     //   }
//     // }],
//   };
//   const async = false;
//   // const ip_pool = "Main Pool";
//   // const send_at = "example send_at";
//   return mandrillClient
//     .messages
//     .sendTemplate(
//       {
//         template_name: name,
//         template_content: content,
//         message,
//         async,
//         // ip_pool,
//         send_at: new Date(Date.now() - 3600000),
//       },
//       result => Promise.resolve(result),
//       err => Promise.reject(err),
//     );
//   // try {
//   //   mandrillClient
//   //     .messages
//   //     .sendTemplate(
//   //       {
//   //         template_name: name,
//   //         template_content: content,
//   //         message,
//   //         async,
//   //         // ip_pool,
//   //         send_at: new Date(Date.now() - 3600000),
//   //       },
//   //       result => Promise.resolve(result),
//   //       err => Promise.reject(err),
//   //     );
//   // } catch (err) {
//   //   return Promise.reject(err);
//   // }
// };

module.exports = sendTemplate;
