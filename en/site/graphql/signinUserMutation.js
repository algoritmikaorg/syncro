// const { graphQlFetch } = require('./graphql');
//
// const signinUserMutation = (req) => {
//   return graphQlFetch({ req, query: 'signinUserMutation', variables: req.body })
//     .then(({ data, errors, extensions }) => {
//       return {
//         data,
//         errors,
//         extensions,
//       };
//     });
// };
//
// module.exports = signinUserMutation;

const { graphQlFetch } = require('./graphql');

const signinUserMutation = (username, password) => {
  const variables = {
    email: username,
    password,
  };
  return graphQlFetch({ req: null, query: 'signinUserMutation', variables })
    .then(({ data, errors, extensions }) => {
      return {
        data,
        errors,
        extensions,
      };
    });
};

module.exports = signinUserMutation;
