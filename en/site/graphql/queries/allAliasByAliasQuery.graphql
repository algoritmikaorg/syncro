query($alias: String!, $date_gte: DateTime!) {
  allAliases(
  	filter: {
      alias: $alias
    }
    first: 1
  ) {
    course (
      filter: {
        public: true
      }
    ) {
      ...CoursePayload
    }
    cycle (
      filter: {
        public: true
      }
    ) {
      ...CyclePayload
    }
    lecture (
      filter: {
        public: true
        course_every: {
          public: true
        }
        cycle_every: {
          public: true
        }
      }
    ) {
      ...LectureSinglePayload
    }
    lecturer {
      ...LecturerFullPayload
    }
    tag {
      id
      title
    }
  }
  allHowWorkPages(filter: { page_in: ["course", "lecture"]}) {
    titleFirstBlock
    textFirstBlock

    titleSecondBlock
    textSecondBlock

    titleThreeBlock
    textThreeBlock

    titleFourBlock
    textFourBlock
  }
}

fragment AliasPayload on Alias {
  alias
}

fragment ReviewPayload on Review {
  anons
  description
  user {
    firstName
    lastName
    avatar
  }
}

fragment DiscountPayload on Discount {
  id
  title
  rate
}

fragment LecturerPayload on Lecturer {
  skills
  firstName
  lastName
  alias {
    ...AliasPayload
  }
}

fragment LecturerFullPayload on Lecturer {
  id
  firstName
  lastName
  description
  anons
  specialization
  img337x536
  events {
    date,
    lecture (
      filter: {
        public: true,
        course_every: {
          public: true,
        }
        cycle_every: {
          public: true,
        }
      }
    ) {
      title
      img340x192
      anons
      course {
        ...CourseShortPayload
      }
      cycle {
        ...CycleShortPayload
      }
      alias {
        ...AliasPayload
      }
      events (
        orderBy: date_ASC
      ) {
        date
      }
      tags {
        title,
        color,
        textColor,
        alias {
          ...AliasPayload
        }
      }
      reviews (
        public: true
      ) {
        ...ReviewPayload
      }
    }
  }
}

fragment LocationPayload on Location {
  title
  address
  metro
}

fragment EventPayload on Event {
  id
  date
  price
  quantityOfTicketsAvailable
  lecturers {
    ...LecturerPayload
  }
  location {
    ...LocationPayload
  }
}

fragment RecomendedLecturePayload on Lecture {
  title
}

fragment LecturePayload on Lecture {
  id
  title
  subTitle
  anons
  anonsForCourse
  anonsForCycle
  description
  themes
  duration
  img1240x349
  img340x192
  img340x340

  reviews (
    filter: { public: true }
  ) {
    ...ReviewPayload
  }
  discounts (
    filter: {
      public: true
    }
  ) {
    ...DiscountPayload
  }
  alias {
    ...AliasPayload
  }
  events (
    filter: {
      date_gte: $date_gte
      quantityOfTicketsAvailable_gt: 0
    }
    orderBy: date_ASC
  ) {
    ...EventPayload
  }
}

fragment CoursePayload on Course {
  id
  title
  price
  alias {
    alias
  }
  anons
  buyFull
  description
  img340x192
  img1240x349
  lectures (
    filter: {
      public: true
    }
  ) {
    ...LecturePayload
  }
  discounts (
    filter: {
      public: true
    }
  ) {
    ...DiscountPayload
  }
  recommendedLectures {
    ...RecomendedLecturePayload
  }
}

fragment CyclePayload on Cycle {
  id
  title
  price
  when
  whenStrong
  alias {
    ...AliasPayload
  }
  anons
  buyFull
  description
  img340x192
  img1240x349
  lectures (
    filter: {
      public: true,
    }
  ) {
    ...LecturePayload
  }
  discounts (
    filter: {
      public: true
    }
  ) {
    ...DiscountPayload
  }
  recommendedLectures {
    ...RecomendedLecturePayload
  }
}

fragment CourseShortPayload on Course {
  id
  title
  buyFull
  alias {
    ...AliasPayload
  }
}

fragment CycleShortPayload on Cycle {
  id
  title
  buyFull
  alias {
    ...AliasPayload
  }
}

fragment LectureSinglePayload on Lecture {
  id
  title
  subTitle
  anons
  anonsForCourse
  anonsForCycle
  description
  themes
  duration
  img1240x349
  img340x192
  course {
    ...CourseShortPayload
  }
  cycle {
    ...CycleShortPayload
  }
  discounts (
    filter: {
      public: true
    }
  ) {
    ...DiscountPayload
  }
  events (
    filter: {
      date_gte: $date_gte
      quantityOfTicketsAvailable_gt: 0
    }
    orderBy: date_ASC
  ) {
    ...EventPayload
  }
  reviews {
    ...ReviewPayload
  }
  lectureType
  metaDescription
  metaKeywords
  metaTags {
    metaKeywords
    metaDescription
  }
}
