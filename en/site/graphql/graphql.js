const fs = require('fs');
const path = require('path');
const { createApolloFetch } = require('apollo-fetch');

const config = require('./../config');

const apolloFetch = createApolloFetch({ uri: config.SIMPLE_API_ENDPOINT });

apolloFetch.use(({ request, options }, next) => {
  if (request.user) {
    if (!options.headers) {
      options.headers = {};
    }
    options.headers['Authorization'] = `Bearer ${request.user.token}`;
  }

  next();
});

let queries = {};
let mutations = {};

const readFilesSync = (dir) => {
  const output = {};
  const filesPath = fs.readdirSync(`${__dirname}/${dir}`);
  filesPath.forEach((filepath) => {
    const buff = fs.readFileSync(`${__dirname}/${dir}/${filepath}`);
    const filename = path.basename(filepath, '.graphql');
    output[filename] = buff.toString();
  });

  return output;
};

const readGraphQlFiles = () => {
  if (Object.keys(queries).length === 0) {
    queries = readFilesSync('queries');
  }
  if (Object.keys(mutations).length === 0) {
    mutations = readFilesSync('mutations');
  }
};

const graphQlFetch = ({
  req,
  query,
  variables = {},
  operationName = '',
}) => {
  let queryString = '';
  const user = req ? req.user : null;
  if (Array.isArray(query)) {
    queryString = query
      .map((item) => {
        if (!queries[item] && !mutations[item]) throw new Error('No query');
        return queries[item] || mutations[item];
      })
      .reduce((prev, curr) => `${prev}${curr}`);
  } else {
    if (!queries[query] && !mutations[query]) return Promise.reject(new Error('No query'));
    queryString = queries[query] || mutations[query];
  }

  return apolloFetch({
    query: queryString,
    variables,
    operationName,
    user,
  });
};

module.exports = {
  readGraphQlFiles,
  graphQlFetch,
};
