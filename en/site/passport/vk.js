const passport = require('passport');
const VKontakteStrategy = require('passport-vkontakte').Strategy;

const signupUserSocialNetworks = require('./../graphql/signupUserSocialNetworks');

const { URL } = require('./../config');

const VKONTAKTE_APP_ID = '6307787';
const VKONTAKTE_APP_SECRET = 'Qh4vMRpFqlyykx4Rr4q0';
const vkServiceKey = '0491c9f20491c9f20491c9f2f904f1f639004910491c9f25eacd7eddc499cb04f33ec7c';
const vkCallback = 'vk/callback';

module.exports = () => {
  passport.use(new VKontakteStrategy(
    {
      clientID: VKONTAKTE_APP_ID,
      clientSecret: VKONTAKTE_APP_SECRET,
      callbackURL: `${URL}/users/${vkCallback}`,
      scope: ['email', 'first_name', 'last_name'],
      profileFields: ['id', 'first_name', 'last_name', 'email'],
    },
    (accessToken, refreshToken, params, profile, done) => {
      const user = {};
      user.socToken = accessToken;
      user.socUserId = profile.id.toString();
      user.firstName = profile.name.givenName;
      user.lastName = profile.name.familyName;
      user.email = params.email;
      user.socialNetwork = 'VKONTAKTE';

      signupUserSocialNetworks(null, user)
        .then(({ data, errors }) => {
          if (data) {
            const result = data.signupUserSocialNetworks;
            return done(null, result);
          }
          return done(null, false);
        })
        .catch(done);
    },
  ));
};
