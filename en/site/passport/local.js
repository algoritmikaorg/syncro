const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const signinUserMutation = require('./../graphql/signinUserMutation');

module.exports = () => {
  passport.use(new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    (username, password, done) => {
      signinUserMutation(username, password)
        .then(({ data, errors, extensions }) => {
          if (!data.authenticateUser) {
            return done(null, false, { message: 'Неверный E-mail или пароль' });
          }
          const user = data.authenticateUser;
          return done(null, user);
        })
        .catch(done);
    },
  ));
};
