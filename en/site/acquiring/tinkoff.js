const redis = require('./../db/redis');

const { other: fetcPostJson } = require('./../lib/fetcPostJson');

const url = 'https://securepay.tinkoff.ru/v2';
const urlInit = `${url}/Init`;

exports.init = (draft) => {
  return redis.getAsync('acquiring')
    .then((reply) => {
      if (!reply) return Promise.reject(new Error('Not find'));

      return JSON.parse(reply);
    })
    .then(({ token }) => {
      const {
        userAnOrder: {
          email,
          phone,
        },
        order: {
          totalPrice,
        },
        product,
        certificate,
        subscription,
        paymentId,
      } = draft;

      switch (true) {
        case Boolean(certificate): {
          title = `Сертификат ${certificate.title}`;
          break;
        }
        case Boolean(subscription): {
          title = `Абонемент ${subscription.title}`;
          break;
        }
        case Boolean(product): {
          title = `Билеты ${product.title}`;
          break;
        }
        default: {
          title = 'Билеты';
        }
      }
      const data = {
        TerminalKey: token.trim(),
        Amount: totalPrice * 100,
        OrderId: paymentId,
        Description: title,
        Receipt: {
          Email: email,
          Phone: `+${phone}`,
          Taxation: 'usn_income',
          Items: [
            {
              Name: title,
              Price: totalPrice * 100,
              Quantity: 1.00,
              Amount: totalPrice * 100,
              Tax: 'none',
            },
          ],
        },
      };

      return fetcPostJson(urlInit, data);
    })
    .then(res => res.json());
    // .then((data));
};
