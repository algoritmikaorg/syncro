/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 80);
/******/ })
/************************************************************************/
/******/ ({

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _slider = __webpack_require__(5);

var _slider2 = _interopRequireDefault(_slider);

var _gallery = __webpack_require__(60);

var _gallery2 = _interopRequireDefault(_gallery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _slider2.default)();
(0, _gallery2.default)();

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var Swiper = window.Swiper;

var sliderBuy = function sliderBuy(selector) {
  var output = [];
  $(selector).find('.lecture-dates').each(function (i, e) {
    var el = $(e);
    var navNext = el.find('.slider-button-next');
    var navPrev = el.find('.slider-button-prev');
    var container = el.find('.slider-container');

    var slider = new Swiper(container[0], {
      slidesPerView: 5,
      spaceBetween: 0,
      nextButton: navNext[0],
      prevButton: navPrev[0],
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 0
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 0
        }
      }
    });
    output.push(slider);
  });
  return output;
};

var slider = function slider() {
  $(document).ready(function () {
    var selector1 = $('.lecturers .slider-container');
    if (selector1.length > 0) {
      var slider1 = new Swiper(selector1[0], {
        slidesPerView: 3,
        spaceBetween: 118,
        nextButton: '.lecturers .slider-button-next',
        prevButton: '.lecturers .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30
          },
          1030: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        }
      });
    }

    var selector2 = $('.reviews .slider-container');
    if (selector2.length > 0) {
      var slider2 = new Swiper(selector2[0], {
        slidesPerView: 2,
        spaceBetween: 42,
        nextButton: '.reviews .slider-button-next',
        prevButton: '.reviews .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0
          }
        }
      });
    }

    var selector3 = $('.locations .slider-container');
    if (selector3.length > 0) {
      var slider3 = new Swiper(selector3[0], {
        slidesPerView: 3,
        spaceBetween: 118,
        nextButton: '.locations .slider-button-next',
        prevButton: '.locations .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30
          },
          1030: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        }
      });
    }

    var selector4 = $('.buy .lecture-dates');
    if (selector4.length > 0) {
      var slider4 = sliderBuy($('.buy'));
    }
  });
};

exports.default = slider;

/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var _window = window,
    $ = _window.$,
    whatInput = _window.whatInput,
    Swiper = _window.Swiper,
    simpleLightbox = _window.simpleLightbox;

//

var resizeGallery = function resizeGallery(el) {
  var a = el.find('a');
  var width = el.find('.swiper-slide').eq(0).width();
  a.height(width / 1.44);
};

var gallery = function gallery() {
  var container = $('.gallery-container');
  $(document).ready(function () {
    var slider = new Swiper('.gallery-container', {
      slidesPerView: 4,
      spaceBetween: 0,
      nextButton: '.nav-slider-next',
      prevButton: '.nav-slider-prev',
      breakpoints: {
        // // when window width is <= 320px
        // 320: {
        //   slidesPerView: 1,
        //   spaceBetween: 10
        // },
        // // when window width is <= 480px
        // 480: {
        //   slidesPerView: 2,
        //   spaceBetween: 20
        // },
        // // when window width is <= 640px
        640: {
          slidesPerView: 1,
          spaceBetween: 0
        }
      }
      // autoHeight: true
    });

    $('.gallery-lightbox a').simpleLightbox({
      captionSelector: 'self',
      captionType: 'attr',
      captionsData: 'title',
      fileExt: null,
      alertError: false
    });
    resizeGallery(container);
  });

  $(window).resize(function () {
    resizeGallery(container);
  });
};

exports.default = gallery;

/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(26);


/***/ })

/******/ });