/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 79);
/******/ })
/************************************************************************/
/******/ ({

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stack = function () {
  function Stack() {
    _classCallCheck(this, Stack);

    this.store = [];
    this.push = this.push.bind(this);
    this.pop = this.pop.bind(this);
    this.peek = this.peek.bind(this);
    this.length = this.length.bind(this);
    this.isEmpty = this.isEmpty.bind(this);
    this.getStack = this.getStack.bind(this);
    this.clear = this.clear.bind(this);
  }

  _createClass(Stack, [{
    key: "push",
    value: function push() {
      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return Array.prototype.push.apply(this.store, args);
    }
  }, {
    key: "pop",
    value: function pop() {
      return Array.prototype.pop.call(this.store);
    }
  }, {
    key: "peek",
    value: function peek() {
      var store = this.store;

      return store[store.length - 1];
    }
  }, {
    key: "length",
    value: function length() {
      return this.store.length;
    }
  }, {
    key: "isEmpty",
    value: function isEmpty() {
      return this.store.length === 0;
    }
  }, {
    key: "getStack",
    value: function getStack() {
      return this.store.slice();
    }
  }, {
    key: "clear",
    value: function clear() {
      this.store = [];
    }
  }]);

  return Stack;
}();

exports.default = Stack;

/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _queue = __webpack_require__(9);

var _queue2 = _interopRequireDefault(_queue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _window = window,
    $ = _window.$,
    moment = _window.moment,
    Foundation = _window.Foundation;


var bodyElm = $('body');
var spinFullElm = $('.spin-full');

// moment.locale('ru');

var maxRateDiscount = function maxRateDiscount() {
  var discounts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var lectures = arguments[1];
  var tickets = arguments[2];

  var lecturesIds = Object.keys(lectures);
  var output = null;
  var tmpRate = 0;
  var tmpDiscounts = discounts.filter(function (_ref) {
    var buyingLecturesCount = _ref.buyingLecturesCount,
        participantsCount = _ref.participantsCount;
    return buyingLecturesCount <= lecturesIds.length && participantsCount <= tickets;
  });

  tmpDiscounts.forEach(function (discount) {
    var rate = discount.rate;

    if (rate > tmpRate) {
      output = discount;
      tmpRate = rate;
    }
  });
  return output;
};

var defaultOptions = {
  promocodePath: '/promocode',
  userProfilePath: '/users/profile'
};

var Order = function () {
  _createClass(Order, null, [{
    key: 'checkTickets',
    value: function checkTickets(val) {
      var n = parseInt(val, 10);
      if (Number.isNaN(n)) return 1;
      return n;
    }
  }]);

  function Order(product, options) {
    _classCallCheck(this, Order);

    this.product = Object.assign({}, product);

    this.participants = {};
    this.events = {};
    this.lectures = {};
    this.promocode = null;
    this.discounts = null;
    this.orderPrice = 0;
    this.discountsPrice = 0;
    this.discountsRate = 0;
    this.totalPrice = 0;

    this.tickets = 0;
    this.abidePull = [];
    this.promocodeQueue = new _queue2.default();
    this.options = Object.assign({}, defaultOptions, options);
    var elements = {};
    this.elements = elements;

    var containerEl = $('.buy');
    elements.containerEl = containerEl;
    elements.lectures = containerEl.find('.lecture-item');
    this.isCourse = $('.course-page').length > 0;
    this.isCycle = $('.cycle-page').length > 0;
    this.isLecture = $('.lecture-page').length > 0;

    elements.errorEl = containerEl.find('.fetch-error');
    elements.orderEl = containerEl.find('.order');
    elements.ticketsEl = containerEl.find('.tickets');
    elements.ticketsElInput = elements.ticketsEl.find('input');
    elements.slideContainerEl = containerEl.find('.lecture-dates .slider-container');
    elements.participantsListEl = containerEl.find('.participants-list');
    elements.containerAbideErrorEl = containerEl.find('.order-error');
    elements.containerAbideErrorEl = containerEl.find('.abide-error');
    elements.promocodeEl = containerEl.find('.promocode');
    elements.promocodeInputEl = elements.promocodeEl.find('input');
    elements.promocodeOrderEl = elements.orderEl.find('.order-promocode');
    elements.totalPriceEl = containerEl.find('.total').find('.total-price');
    elements.orderPriceEl = containerEl.find('.total').find('.order-price');

    this.handlerSelectDate = this.handlerSelectDate.bind(this);
    this.handlerClickBuy = this.handlerClickBuy.bind(this);
    this.handlerTickets = this.handlerTickets.bind(this);
    this.handlerTicketsButton = this.handlerTicketsButton.bind(this);
    this.handlerPromocode = this.handlerPromocode.bind(this);
    this.setLectureEvents = this.setLectureEvents.bind(this);
    this.setParticipants = this.setParticipants.bind(this);
    this.renderOrderItems = this.renderOrderItems.bind(this);
    this.renderParticipants = this.renderParticipants.bind(this);

    this.promocodeWorker = this.promocodeWorker.bind(this);
    this.promocodeSuccess = this.promocodeSuccess.bind(this);
    this.promocodeError = this.promocodeError.bind(this);
    this.getUserProfile = this.getUserProfile.bind(this);
    this.setUserToFirstparticipants = this.setUserToFirstparticipants.bind(this);
    this.calcOrder = this.calcOrder.bind(this);
    this.renderOrderPrice = this.renderOrderPrice.bind(this);
    this.renderOrderDiscounts = this.renderOrderDiscounts.bind(this);
    this.showError = this.showError.bind(this);

    elements.slideContainerEl.on('click', this.handlerSelectDate);
    elements.containerEl.find('.process-buy').on('click', this.handlerClickBuy);
    elements.ticketsElInput.on('change', this.handlerTickets);
    elements.ticketsEl.find('.title-r').on('click', this.handlerTicketsButton);

    elements.promocodeInputEl.on('input change', this.handlerPromocode);

    this.promocodeQueue.worker = this.promocodeWorker;
    this.promocodeQueue.success = this.promocodeSuccess;
    this.promocodeQueue.error = this.promocodeError;

    // init
    elements.slideContainerEl.each(function (i, e) {
      $(e).find('.slide').eq(0).trigger('click');
    });
    elements.ticketsEl.find('.plus').trigger('click');
    this.getUserProfile();
  }

  _createClass(Order, [{
    key: 'calcOrder',
    value: function calcOrder() {
      var lectures = this.lectures,
          tickets = this.tickets,
          product = this.product,
          promocode = this.promocode,
          isCourse = this.isCourse,
          isCycle = this.isCycle;


      var discounts = null;

      var orderPrice = 0;
      var discountsRate = 0;
      var discountsPrice = 0;

      var isProductBuy = false;
      var lecturesIds = Object.keys(lectures);
      var isBuyAllLectures = lecturesIds.length === product.lectures.length;

      if ((isCourse || isCycle) && isBuyAllLectures) {
        orderPrice = product.price;
        isProductBuy = true;
      } else if (lecturesIds.length === 0) {
        orderPrice = 0;
      } else {
        orderPrice = lecturesIds.map(function (id) {
          return lectures[id].price;
        }).reduce(function (prev, curr) {
          return prev + curr;
        });
      }

      orderPrice *= tickets;

      // промокод
      if (promocode) {
        var rate = promocode.rate,
            __typename = promocode.__typename,
            unusedLectures = promocode.unusedLectures,
            personsCount = promocode.personsCount,
            buyingLecturesCount = promocode.buyingLecturesCount,
            participantsCount = promocode.participantsCount;
        // сертификат

        if (__typename === 'Certificate' || __typename === 'Subscription') {
          var cerDiscountPrice = 0;
          if (unusedLectures === -1) {
            // безлимит
            if (isProductBuy) {
              cerDiscountPrice = product.price;
              cerDiscountPrice *= tickets > personsCount ? personsCount : tickets;
            } else {
              cerDiscountPrice = lecturesIds.map(function (id) {
                return lectures[id].price;
              }).reduce(function (prev, curr) {
                return prev + curr;
              });

              cerDiscountPrice *= tickets > personsCount ? personsCount : tickets;
            }
          } else if (lecturesIds.length < unusedLectures) {
            if (isProductBuy) {
              (function () {
                var partialPrice = product.p / lecturesIds.length;
                var ost = lecturesIds.length - unusedLectures;
                var np = tickets > personsCount ? personsCount : tickets;
                var nl = unusedLectures;

                while (np > 0 && nl > 0) {
                  np -= 1;
                  lecturesIds.forEach(function (id) {
                    nl -= 1;
                    cerDiscountPrice += partialPrice;
                  });
                }
              })();
            } else {
              var ost = lecturesIds.length - unusedLectures;
              var np = tickets > personsCount ? personsCount : tickets;
              var nl = unusedLectures;
              while (np > 0 && nl > 0) {
                np -= 1;
                lecturesIds.forEach(function (id) {
                  nl -= 1;
                  cerDiscountPrice += lectures[id].price;
                });
              }
            }
          } else {
            if (isProductBuy) {
              cerDiscountPrice = orderPrice / lecturesIds.length * unusedLectures;
            } else {
              cerDiscountPrice = lecturesIds.slice(0, unusedLectures).map(function (id) {
                return lectures[id].price;
              }).reduce(function (prev, curr) {
                return prev + curr;
              });
            }
          }
          discountsRate += cerDiscountPrice * 100 / orderPrice;
        } else if (lecturesIds.length >= buyingLecturesCount && tickets >= participantsCount) {
          var checkLectures = promocode.lectures.filter(function (_ref2) {
            var id = _ref2.id;
            return lecturesIds.includes(id);
          });
          var checkCourse = promocode.lectures.filter(function (_ref3) {
            var id = _ref3.id;
            return product.id === id;
          });
          var checkCycles = promocode.cycles.filter(function (_ref4) {
            var id = _ref4.id;
            return product.id === id;
          });
          var checkTags = promocode.tags.filter(function (_ref5) {
            var id = _ref5.id;
            return product.tags.includes(id);
          });
          if (checkTags.length > 0 || checkLectures.length === 0 && checkCourse.length === 0 && checkCycles.length === 0 || isProductBuy && (checkCourse.length > 0 || checkCycles.length > 0)) {
            discountsRate += rate;
          } else if (checkLectures.length > 0) {

            var _cerDiscountPrice = checkLectures.map(function (_ref6) {
              var id = _ref6.id;
              return lectures[id].price;
            }).reduce(function (prev, curr) {
              return prev + curr;
            });

            discountsRate += _cerDiscountPrice * rate / 100 * 100 / orderPrice;
          }
        }
      }

      // скидки
      if (product.discounts && product.discounts.length > 0) {
        if ((isCourse || isCycle) && !isBuyAllLectures) {
          var dd = {};
          discounts = lecturesIds.map(function (id) {
            var discount = maxRateDiscount(product.discounts[id], lectures, tickets);
            dd[id] = discount;
            return discount;
          });

          if (Object.keys(dd).length > 0) {
            discountsRate += lecturesIds.map(function (id) {
              if (!dd[id]) return 0;
              var rate = dd[id].rate;

              var dp = rate * lectures[id].price / 100;
              return 100 * dp / orderPrice;
            }).reduce(function (prev, curr) {
              return prev + curr;
            });
          }
        } else {
          discounts = [maxRateDiscount(product.discounts, lectures, tickets)];

          if (discounts[0]) {
            discountsRate += discounts.map(function (_ref7) {
              var rate = _ref7.rate;
              return rate || 0;
            }).reduce(function (prev, curr) {
              return prev + curr;
            });
          }
        }
      }

      discountsRate = discountsRate > 100 ? 100 : discountsRate;
      discountsPrice = discountsRate * orderPrice / 100;

      this.orderPrice = orderPrice;
      this.discountsRate = discountsRate;
      this.discountsPrice = discountsPrice > orderPrice ? orderPrice : discountsPrice;
      this.totalPrice = orderPrice - discountsPrice;
      discounts = discounts ? discounts.filter(function (item) {
        return item;
      }) : null;
      this.discounts = discounts || [];

      this.renderOrderPrice();
      this.renderOrderDiscounts();
    }
  }, {
    key: 'getUserProfile',
    value: function getUserProfile() {
      var _this = this;

      var userProfilePath = this.options.userProfilePath;

      $.get(userProfilePath).done(function (result) {
        if (result.data && result.data.user) {
          _this.user = result.data.user;
          _this.setUserToFirstparticipants();
        }
      });
    }
  }, {
    key: 'setUserToFirstparticipants',
    value: function setUserToFirstparticipants() {
      var user = this.user,
          participantsListEl = this.elements.participantsListEl;

      if (!user) return undefined;
      var email = user.email,
          _user$phone = user.phone,
          phone = _user$phone === undefined ? '' : _user$phone,
          firstName = user.firstName,
          lastName = user.lastName;

      var participantEl = participantsListEl.find('li').eq(0);
      if (participantEl.length > 0) {
        var inputs = participantEl.find('input');
        var values = inputs.filter(function (i, e) {
          return $(e).val();
        });
        if (values.length === 0) {
          inputs.each(function (i, e) {
            var el = $(e);
            switch (el.prop('name')) {
              case 'email':
                {
                  el.val(email);
                  break;
                }
              case 'phone':
                {
                  el.val(phone);
                  break;
                }
              case 'firstName':
                {
                  el.val(firstName);
                  break;
                }
              case 'lastName':
                {
                  el.val(lastName);
                  break;
                }
              default:
                {}
            }
          });
        }
      }
    }
  }, {
    key: 'promocodeSuccess',
    value: function promocodeSuccess(result) {
      var promocodeQueue = this.promocodeQueue,
          calcOrder = this.calcOrder,
          _elements = this.elements,
          promocodeEl = _elements.promocodeEl,
          promocodeInputEl = _elements.promocodeInputEl,
          promocodeOrderEl = _elements.promocodeOrderEl;

      this.promocode = result;

      promocodeEl.removeClass('error');
      promocodeEl.addClass('success');
      promocodeInputEl.prop('disabled', true).off('input change');
      promocodeOrderEl.removeClass('hidden').addClass('visible').find('span').text(result.title);
      calcOrder();
    }
  }, {
    key: 'promocodeError',
    value: function promocodeError(err) {
      var promocodeQueue = this.promocodeQueue,
          promocodeEl = this.elements.promocodeEl;

      promocodeQueue.delay *= 2;
      promocodeEl.addClass('error');
    }
  }, {
    key: 'promocodeWorker',
    value: function promocodeWorker(value, done) {
      var product = this.product,
          promocodePath = this.options.promocodePath,
          _elements2 = this.elements,
          promocodeEl = _elements2.promocodeEl,
          promocodeInputEl = _elements2.promocodeInputEl;

      var entityIds = product.lectures ? product.lectures.concat(product.id) : [product.id];
      var body = { promocode: value, entityIds: entityIds, tagsIds: product.tags };
      $.post({
        url: promocodePath,
        data: { data: JSON.stringify(body) },
        dataType: 'json'
      }).done(function (result) {
        var data = result.data,
            error = result.error;

        if (error) {
          return done(error);
        }
        if (value !== promocodeInputEl.val()) return done(null, null);
        return done(null, data);
      }).always(function () {
        promocodeEl.removeClass('in-process');
      }).fail(function (err) {
        done(err);
      });
    }
  }, {
    key: 'setLectureEvents',
    value: function setLectureEvents(type, lectureId, eventId) {
      var renderOrderItems = this.renderOrderItems,
          calcOrder = this.calcOrder,
          events = this.events,
          lectures = this.lectures;

      var lectureEvent = this.product.events[eventId];
      var date = lectureEvent.date,
          price = lectureEvent.price;

      renderOrderItems(lectureId, date, type);

      if (type === 'add') {
        lectures[lectureId] = { price: price, date: date, eventId: eventId };
      } else {
        delete lectures[lectureId];
      }
      calcOrder();
    }
  }, {
    key: 'setParticipants',
    value: function setParticipants(type) {
      if (type === 'add') {} else {}
    }
  }, {
    key: 'handlerPromocode',
    value: function handlerPromocode(e) {
      var promocodeQueue = this.promocodeQueue,
          promocodeEl = this.elements.promocodeEl;

      var el = $(e.target);
      if (promocodeEl.hasClass('error')) {
        promocodeEl.removeClass('error');
      }
      promocodeEl.addClass('in-process');
      this.promocodeQueue.push(el.val());
    }
  }, {
    key: 'handlerSelectDate',
    value: function handlerSelectDate(e) {
      var setLectureEvents = this.setLectureEvents,
          orderEl = this.elements.orderEl;

      var el = $(e.target);

      if (el.hasClass('slider-nav')) return false;

      var slideEl = null;

      if (el.hasClass('slide')) {
        slideEl = el;
      } else {
        slideEl = el.parents('.slide');
      }

      var lectureId = slideEl.data('lecture-id');
      var eventId = slideEl.data('event-id');

      if (product.buyFull) {
        if (!slideEl.hasClass('active')) {
          setLectureEvents('add', lectureId, eventId);
          slideEl.parent().find('.active').removeClass('active');
          slideEl.addClass('active');
        }
      } else if (slideEl.hasClass('active')) {
        setLectureEvents('remove', lectureId, eventId);
        slideEl.removeClass('active');
      } else {
        setLectureEvents('add', lectureId, eventId);
        slideEl.parent().find('.active').removeClass('active');
        slideEl.addClass('active');
      }

      return true;
    }
  }, {
    key: 'handlerClickBuy',
    value: function handlerClickBuy(e) {
      e.preventDefault();
      var valid = this.valid,
          showError = this.showError,
          abidePull = this.abidePull,
          lectures = this.lectures;

      var el = $(e.target);
      var href = el.data('href');

      var orderValid = valid();
      var abideValid = false;

      if (orderValid && abidePull.length > 0) {
        abideValid = abidePull.every(function (item) {
          return item.validateForm();
        });
      }

      if (!orderValid || !abideValid) {
        return showError();
      }
      if (!lectures || Object.keys(lectures).length === 0) {
        return showError();
      }
      var events = this.events,
          promocode = this.promocode,
          discounts = this.discounts,
          tickets = this.tickets,
          orderPrice = this.orderPrice,
          discountsPrice = this.discountsPrice,
          discountsRate = this.discountsRate,
          totalPrice = this.totalPrice,
          _elements3 = this.elements,
          errorEl = _elements3.errorEl,
          participantsListEl = _elements3.participantsListEl;


      var participants = [];
      participantsListEl.find('li').each(function (i, e) {
        var el = $(e);
        participants.push({
          firstName: el.find('input[name="firstName"]').val(),
          lastName: el.find('input[name="lastName"]').val(),
          email: el.find('input[name="email"]').val(),
          phone: el.find('input[name="phone"]').val()
        });
      });

      spinFullElm.show();
      bodyElm.addClass('hide-scroll');

      $.post({
        url: '/order/draft',
        data: {
          order: JSON.stringify({
            product: product,
            participants: participants,
            events: events,
            lectures: lectures,
            promocode: promocode,
            discounts: discounts,
            tickets: tickets,
            order: {
              orderPrice: orderPrice,
              discountsPrice: discountsPrice,
              discountsRate: discountsRate,
              totalPrice: totalPrice
            }
          })
        },
        dataType: 'json'
      }).done(function (_ref8) {
        var data = _ref8.data,
            error = _ref8.error;

        if (data && data.hash) {
          window.location.href = '/order/draft/' + data.hash;
          return;
        }

        spinFullElm.hide();
        bodyElm.removeClass('hide-scroll');

        switch (true) {
          case error:
            {
              errorEl.text('Ошибка. Заказ не может быть создан.');
              break;
            }
          case Boolean(data.resultOfChecking):
            {
              if (data.resultOfChecking.EntityNoPublic) {
                errorEl.text('Ошибка. Материал снят с публикации.');
              }
              if (data.resultOfChecking.allEventsNoTicketsAvailable) {
                errorEl.text('\u041E\u0448\u0438\u0431\u043A\u0430. \u041D\u0435\u0442 \u0431\u0438\u043B\u0435\u0442\u043E\u0432:\n                ' + data.resultOfChecking.allEventsNoTicketsAvailable.map(function (_ref9) {
                  var date = _ref9.date,
                      title = _ref9.title;
                  return title + ' ' + date;
                }).join('\n') + '\n              .');
              }
              if (data.resultOfChecking.PromocodeWithErrors) {
                errorEl.text('Ошибка. Промокод не доступен.');
                break;
              }
              if (data.resultOfChecking.DiscountsWithErrors) {
                errorEl.text('Ошибка. Скидки не доступны.');
                break;
              }
              break;
            }
          case Boolean(data.resultOfCalcOrder):
            {
              errorEl.text('Ошибка. Сумма заказа расчитана не верно.');
              break;
            }
          default:
            {
              errorEl.text('Ошибка.');
            }
        }
      }).fail(function () {
        spinFullElm.hide();
        bodyElm.removeClass('hide-scroll');
        errorEl.text('Ошибка.');
      });
    }
  }, {
    key: 'handlerTickets',
    value: function handlerTickets(e) {
      var renderParticipants = this.renderParticipants,
          calcOrder = this.calcOrder;

      var el = $(e.target);
      this.tickets = Order.checkTickets(el.val());
      renderParticipants();
      calcOrder();
    }
  }, {
    key: 'handlerTicketsButton',
    value: function handlerTicketsButton(e) {
      var renderParticipants = this.renderParticipants,
          calcOrder = this.calcOrder,
          ticketsElInput = this.elements.ticketsElInput;

      var el = $(e.target);
      if (el.hasClass('plus') || el.parent().hasClass('plus')) {
        this.tickets += 1;
      }
      if (el.hasClass('minus') || el.parent().hasClass('minus')) {
        if (this.tickets > 0) {
          this.tickets -= 1;
        }
      }
      ticketsElInput.val(this.tickets);
      renderParticipants();
      calcOrder();
    }
  }, {
    key: 'valid',
    value: function valid() {
      return true;
    }
  }, {
    key: 'renderOrderPrice',
    value: function renderOrderPrice() {
      var orderPrice = this.orderPrice,
          totalPrice = this.totalPrice,
          discountsPrice = this.discountsPrice,
          _elements4 = this.elements,
          totalPriceEl = _elements4.totalPriceEl,
          orderPriceEl = _elements4.orderPriceEl;

      if (orderPrice !== totalPrice) {
        orderPriceEl.text(orderPrice);
        totalPriceEl.text(totalPrice);
        orderPriceEl.addClass('line-through');
      } else {
        orderPriceEl.text('');
        totalPriceEl.text(totalPrice);
        totalPriceEl.removeClass('line-through');
      }
    }
  }, {
    key: 'renderOrderDiscounts',
    value: function renderOrderDiscounts() {
      var discounts = this.discounts;
    }
  }, {
    key: 'renderOrderItems',
    value: function renderOrderItems(id, date, add) {
      var orderEl = this.elements.orderEl;

      var orderLectureEl = orderEl.find('[data-lecture-id="' + id + '"]');
      if (add) {
        orderLectureEl.show();
        orderLectureEl.find('.date').show();
        orderLectureEl.find('.date-empty').hide();
        orderLectureEl.find('.l-month').text(moment(date).format('D MMMM'));
        orderLectureEl.find('.l-time').text(moment(date).format('HH:mm'));
      } else {
        orderLectureEl.find('.date-empty').show();
        orderLectureEl.find('.date').hide();
        orderLectureEl.find('.l-month').text('');
        orderLectureEl.find('.l-time').text('');
        orderLectureEl.hide();
      }
    }
  }, {
    key: 'renderParticipants',
    value: function renderParticipants() {
      var setParticipants = this.setParticipants,
          tickets = this.tickets,
          abidePull = this.abidePull,
          participantsListEl = this.elements.participantsListEl;

      var participantsItems = participantsListEl.find('li');
      var l = participantsItems.length;

      if (tickets !== l) {
        if (tickets > l) {
          for (var i = l; i < tickets; i += 1) {
            var els = $(template_participant({ n: i + 1 }));
            participantsListEl.append(els);
            var abide = new Foundation.Abide(els);
            abidePull.push(abide);
            setParticipants('add');
          }
        } else if (tickets < l) {
          var r = l - tickets;
          for (var _i = l; _i > tickets; _i -= 1) {
            participantsItems.eq(_i - 1).remove();
            abidePull.splice(_i - 1, 1);
            setParticipants('remove');
          }
        }
      }
    }
  }, {
    key: 'showError',
    value: function showError() {
      var abidePull = this.abidePull,
          containerAbideErrorEl = this.elements.containerAbideErrorEl;

      if (!this.valid()) {
        containerAbideErrorEl.show();
      } else {
        containerAbideErrorEl.hide();
      }
      if (abidePull.length === 0) {
        containerAbideErrorEl.show();
      } else {
        containerAbideErrorEl.hide();
      }
    }
  }]);

  return Order;
}();

var buy = function buy() {
  var _window2 = window,
      product = _window2.product;

  var order = new Order(product);
  window.getUserProfile = order.getUserProfile.bind(order);
};

exports.default = buy;

/***/ }),

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _slider = __webpack_require__(5);

var _slider2 = _interopRequireDefault(_slider);

var _buy = __webpack_require__(16);

var _buy2 = _interopRequireDefault(_buy);

var _anchors = __webpack_require__(8);

var _anchors2 = _interopRequireDefault(_anchors);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _slider2.default)();
(0, _buy2.default)();
(0, _anchors2.default)();

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var Swiper = window.Swiper;

var sliderBuy = function sliderBuy(selector) {
  var output = [];
  $(selector).find('.lecture-dates').each(function (i, e) {
    var el = $(e);
    var navNext = el.find('.slider-button-next');
    var navPrev = el.find('.slider-button-prev');
    var container = el.find('.slider-container');

    var slider = new Swiper(container[0], {
      slidesPerView: 5,
      spaceBetween: 0,
      nextButton: navNext[0],
      prevButton: navPrev[0],
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 0
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 0
        }
      }
    });
    output.push(slider);
  });
  return output;
};

var slider = function slider() {
  $(document).ready(function () {
    var selector1 = $('.lecturers .slider-container');
    if (selector1.length > 0) {
      var slider1 = new Swiper(selector1[0], {
        slidesPerView: 3,
        spaceBetween: 118,
        nextButton: '.lecturers .slider-button-next',
        prevButton: '.lecturers .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30
          },
          1030: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        }
      });
    }

    var selector2 = $('.reviews .slider-container');
    if (selector2.length > 0) {
      var slider2 = new Swiper(selector2[0], {
        slidesPerView: 2,
        spaceBetween: 42,
        nextButton: '.reviews .slider-button-next',
        prevButton: '.reviews .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0
          }
        }
      });
    }

    var selector3 = $('.locations .slider-container');
    if (selector3.length > 0) {
      var slider3 = new Swiper(selector3[0], {
        slidesPerView: 3,
        spaceBetween: 118,
        nextButton: '.locations .slider-button-next',
        prevButton: '.locations .slider-button-prev',
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 0
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30
          },
          1030: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        }
      });
    }

    var selector4 = $('.buy .lecture-dates');
    if (selector4.length > 0) {
      var slider4 = sliderBuy($('.buy'));
    }
  });
};

exports.default = slider;

/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(25);


/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
// const $ = window.$;
var $ = window.$;

// const remCalc = (rootEl) => ((n) => n / parseFloat(rootEl.css('font-size')));
var remCalc = function remCalc(rootEl) {
  return function (n) {
    return parseFloat(rootEl.css('font-size')) * n / 16;
  };
};

var remCalcFn = null;

var onScroll = function onScroll(doc, menu, ids, qf, qfF, stHeader, findInContainer, itemsSelector) {
  var menuItems = null;
  var scrollPos = doc.scrollTop();

  if (findInContainer) {
    menuItems = menu.find(itemsSelector);
  } else {
    menuItems = menu;
  }

  menuItems.each(function (i, e) {
    var currLink = $(e);
    // const refElement = ids.filter(currLink.attr('href'));
    var refElement = $(currLink.attr('href'));
    var refElTop = Math.floor(refElement.position().top);
    var refHeight = refElement.height();
    if (refElTop - stHeader.height() - remCalcFn(qf) - 1 <= scrollPos && refElTop + refElement.height() > scrollPos) {
      menu.removeClass('active');
      currLink.addClass('active');
      // return false;
    } else {
      currLink.removeClass('active');
    }
  });
};

var findEl = function findEl(e, anchorClass) {
  var c = $(e.target);
  var el = null;
  if (c.hasClass(anchorClass)) {
    el = c;
  } else {
    el = c.parent();
  }
  return el;
};

var anchors = function anchors() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  remCalcFn = remCalc($('html')).bind(null);
  var doc = $(document);
  var fnFindEL = options.findEl || findEl;
  var dynamic = options.dynamic || false;
  var container = options.container || $('html, body');
  var stHeader = options.stHeader || $('.anchors-sticky-inner');
  var elements = options.elements || $('.anchor');
  var findInContainer = options.findInContainer || false;
  var itemsSelector = options.itemsSelector || '';

  var menu = options.menu || elements;
  var qf = options.qf || 30;
  var qfF = options.qfF || 0;
  var qfCl = options.qfCl || 0;
  var points = options.points || $('.anchor-point');
  var anchorClass = options.anchorClass || 'anchor';

  var fnScroll = onScroll.bind(null, doc, menu, points, qf, qfF, stHeader, findInContainer, itemsSelector);

  doc.on('scroll', fnScroll);

  elements.on('click', function (e) {
    var el = fnFindEL(e, anchorClass);
    if (!el || el.length === 0) {
      return true;
    }

    e.preventDefault();
    doc.off('scroll');

    elements.removeClass('active');
    el.addClass('active');

    var href = el.attr('href');
    var scrollTop = 0;

    // if (stHeader.hasClass('is-stuck')) {
    //   scrollTop = $(href).offset().top - stHeader.outerHeight() - qf;
    // } else {
    //   scrollTop = $(href).offset().top - stHeader.outerHeight() - qfCl;
    // }

    if (stHeader.hasClass('is-stuck')) {
      scrollTop = $(href).offset().top - stHeader.outerHeight() - remCalcFn(qfCl);
    } else {
      scrollTop = $(href).offset().top + remCalcFn(qfF) - stHeader.outerHeight() - remCalcFn(qfCl);
    }
    container.stop().animate({
      scrollTop: scrollTop
    }, 500, 'swing', function () {
      window.location.hash = href;
      doc.on('scroll', fnScroll);
    });
    return false;
  });
};

exports.default = anchors;

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stack = __webpack_require__(11);

var _stack2 = _interopRequireDefault(_stack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Queue = function () {
  function Queue() {
    _classCallCheck(this, Queue);

    this.queueStack = new _stack2.default();
    this.timeoutDelay = 500;
    this.successCallback = null;
    this.errorCallback = null;
    this.workerFn = null;

    this.lastPush = null;

    this.run = this.run.bind(this);
    this.push = this.push.bind(this);
    this.clear = this.clear.bind(this);
    this.next = this.next.bind(this);
  }

  _createClass(Queue, [{
    key: 'push',
    value: function push(value) {
      var output = this.queueStack.push(value);
      this.lastPush = new Date();
      this.run();
      return output;
    }
  }, {
    key: 'clear',
    value: function clear() {
      this.queueStack.clear();
    }
  }, {
    key: 'run',
    value: function run() {
      var _this = this;

      var timeoutDelay = this.timeoutDelay,
          next = this.next,
          queueStack = this.queueStack,
          workerFn = this.workerFn;

      var t = new Date();

      setTimeout(function () {
        if (_this.lastPush > t) return next();
        var value = queueStack.pop();
        queueStack.clear();
        if (!value || !(workerFn instanceof Function)) return undefined;
        return workerFn(value, next);
      }, timeoutDelay);
    }
  }, {
    key: 'next',
    value: function next(err, data) {
      var queueStack = this.queueStack,
          errorCallback = this.errorCallback,
          successCallback = this.successCallback,
          run = this.run;


      if (err) {
        if (errorCallback instanceof Function) return errorCallback(err);
        return undefined;
      }
      if (data) {
        queueStack.clear();
        if (successCallback instanceof Function) return successCallback(data);
        return undefined;
      }
      if (!queueStack || queueStack.isEmpty()) {
        return undefined;
      }

      return run();
    }
  }, {
    key: 'stack',
    set: function set(stack) {
      this.queueStack = stack;
    }
  }, {
    key: 'success',
    set: function set(success) {
      this.successCallback = success;
    }
  }, {
    key: 'error',
    set: function set(error) {
      this.errorCallback = error;
    }
  }, {
    key: 'worker',
    set: function set(fn) {
      this.workerFn = fn;
    }
  }, {
    key: 'delay',
    set: function set(value) {
      this.timeoutDelay = value;
    },
    get: function get() {
      return this.timeoutDelay;
    }
  }]);

  return Queue;
}();

exports.default = Queue;

/***/ })

/******/ });