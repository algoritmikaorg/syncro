/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 78);
/******/ })
/************************************************************************/
/******/ ({

/***/ 17:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _window = window,
    $ = _window.$;


var defaultOptions = {
  promocodePath: '/promocode',
  userProfilePath: '/users/profile'
};

var OrderOther = function () {
  function OrderOther(options) {
    _classCallCheck(this, OrderOther);

    this.user = null;
    this.options = Object.assign({}, defaultOptions, options);
    this.form = $('.buy-others form');
    this.items = this.form.find('.items');
    this.buttons = $('.buy-other-button');
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.getUserProfile = this.getUserProfile.bind(this);
    this.setUserToOrder = this.setUserToOrder.bind(this);
    //
    this.form.find('.items input').on('change', this.handleInputChange);
    this.buttons.on('click', this.handleButtonClick);
    this.getUserProfile();
  }

  _createClass(OrderOther, [{
    key: 'handleInputChange',
    value: function handleInputChange(e) {
      var items = this.items;

      var el = $(e.target);
      items.find('.active').removeClass('active');
      el.parent().addClass('active');
    }
  }, {
    key: 'handleButtonClick',
    value: function handleButtonClick(e) {
      var items = this.items;

      var el = $(e.target);
      var qnt = el.data('qnt');
      var input = items.find('#lecturesCount-' + qnt);
      input.trigger('click');
    }
  }, {
    key: 'setUserToOrder',
    value: function setUserToOrder() {
      var user = this.user,
          form = this.form;

      if (!user) return undefined;
      var email = user.email,
          _user$phone = user.phone,
          phone = _user$phone === undefined ? '' : _user$phone,
          firstName = user.firstName,
          lastName = user.lastName;


      form.find('input').each(function (i, e) {
        var el = $(e);
        switch (el.prop('name')) {
          case 'email':
            {
              el.val(email);
              break;
            }
          case 'phone':
            {
              el.val(phone);
              break;
            }
          case 'firstName':
            {
              el.val(firstName);
              break;
            }
          case 'lastName':
            {
              el.val(lastName);
              break;
            }
          default:
            {}
        }
      });
    }
  }, {
    key: 'getUserProfile',
    value: function getUserProfile() {
      var _this = this;

      var userProfilePath = this.options.userProfilePath;

      $.get(userProfilePath).done(function (result) {
        if (result.data && result.data.user) {
          _this.user = result.data.user;
          _this.setUserToOrder();
        }
      });
    }
  }]);

  return OrderOther;
}();

var buyOthers = function buyOthers() {
  var orderOther = new OrderOther();
  window.getUserProfile = orderOther.getUserProfile.bind(orderOther);
};

exports.default = buyOthers;

/***/ }),

/***/ 24:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _buyOthers = __webpack_require__(17);

var _buyOthers2 = _interopRequireDefault(_buyOthers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _buyOthers2.default)();

/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(24);


/***/ })

/******/ });