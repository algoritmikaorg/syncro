/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 87);
/******/ })
/************************************************************************/
/******/ ({

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var _window = window,
    $ = _window.$,
    utils = _window.utils;


var onScroll = function onScroll(doc) {};

var tagsList = function tagsList() {
  var container = $('.filter-container');
  var items = $('.sortable-items');
  var dataSelector = 'filter-alias';
  var doc = $(document);
  var fnScroll = onScroll.bind(null, doc);
  doc.on('scroll', fnScroll);

  items.each(function (i, e) {
    var itemsActual = $(e);
    var tagsEl = itemsActual.find('.tag');
    tagsEl.on('click', function (eT) {
      var el = $(eT.target);
      var alias = '';
      if (el.hasClass('tag')) {
        alias = el.data('alias');
      } else {
        alias = el.parents('.tag').data('alias');
      }

      var pos = el.offset().top - doc.scrollTop();
      container.eq(i).find('[data-filter-alias=' + alias + ']').trigger('click');
      doc.scrollTop(el.offset().top - pos);
    });
  });

  container.each(function (i, e) {
    var containerActual = $(e);
    var itemsActual = items.eq(i).find('.sortable-item');
    var sortingType = containerActual.data('sorting-type');
    var alias = [];
    containerActual.on('click', function (event) {
      var el = $(event.target);
      if (el.hasClass('filter-item')) {
        event.preventDefault();
        var elAlias = el.data(dataSelector);

        if (elAlias === 'all') {
          alias = [];
          utils.setActive(containerActual, el);
        } else {
          if (sortingType === 'single') {
            alias = [elAlias];
            utils.setActive(containerActual, el, alias, '.filter-item');
          } else {
            var ind = alias.indexOf(elAlias);
            if (ind === -1) {
              alias.push(elAlias);
            } else {
              alias.splice(ind, 1);
            }
            utils.setActiveMany(containerActual, el, alias, '.filter-item');
          }
        }
        if (sortingType === 'single') {
          utils.sortItems(itemsActual, alias, dataSelector);
        } else {
          utils.sortItemsMany(itemsActual, alias, dataSelector);
        }
      }
    });
  });
};

exports.default = tagsList;

/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stack = function () {
  function Stack() {
    _classCallCheck(this, Stack);

    this.store = [];
    this.push = this.push.bind(this);
    this.pop = this.pop.bind(this);
    this.peek = this.peek.bind(this);
    this.length = this.length.bind(this);
    this.isEmpty = this.isEmpty.bind(this);
    this.getStack = this.getStack.bind(this);
    this.clear = this.clear.bind(this);
  }

  _createClass(Stack, [{
    key: "push",
    value: function push() {
      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return Array.prototype.push.apply(this.store, args);
    }
  }, {
    key: "pop",
    value: function pop() {
      return Array.prototype.pop.call(this.store);
    }
  }, {
    key: "peek",
    value: function peek() {
      var store = this.store;

      return store[store.length - 1];
    }
  }, {
    key: "length",
    value: function length() {
      return this.store.length;
    }
  }, {
    key: "isEmpty",
    value: function isEmpty() {
      return this.store.length === 0;
    }
  }, {
    key: "getStack",
    value: function getStack() {
      return this.store.slice();
    }
  }, {
    key: "clear",
    value: function clear() {
      this.store = [];
    }
  }]);

  return Stack;
}();

exports.default = Stack;

/***/ }),

/***/ 33:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _anchors = __webpack_require__(8);

var _anchors2 = _interopRequireDefault(_anchors);

var _sortable = __webpack_require__(10);

var _sortable2 = _interopRequireDefault(_sortable);

var _sortableDate = __webpack_require__(68);

var _sortableDate2 = _interopRequireDefault(_sortableDate);

var _sortableSubDate = __webpack_require__(69);

var _sortableSubDate2 = _interopRequireDefault(_sortableSubDate);

var _search = __webpack_require__(66);

var _search2 = _interopRequireDefault(_search);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$;


(0, _anchors2.default)({
  stHeader: $('.st-inner'),
  elements: $('.calendar-block'),
  points: $('.anchor-date-point'),
  anchorClass: 'date-anc',
  findInContainer: true,
  itemsSelector: 'date-anc',
  qf: 60,
  qfCl: 60,
  findEl: function findEl(e, anchorClass) {
    var c = $(e.target);
    var el = null;
    if (c.hasClass(anchorClass)) {
      el = c;
    }
    return el;
  }
});

(0, _sortable2.default)();
(0, _sortableDate2.default)();
(0, _sortableSubDate2.default)();
var search = new _search2.default($('.search-container'), template_search);

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _window = window,
    $ = _window.$,
    EventEmitter = _window.EventEmitter,
    moment = _window.moment;

// moment.locale('ru');

var Calendar = function (_EventEmitter) {
  _inherits(Calendar, _EventEmitter);

  _createClass(Calendar, null, [{
    key: 'datesWithEvents',
    value: function datesWithEvents(dates) {
      var output = [];
      if (!dates || !Array.isArray(dates)) return output;
      dates.forEach(function (date) {
        var dateStr = moment(date).startOf('day').toString();
        if (output.indexOf(dateStr) === -1) {
          output.push(dateStr);
        }
      });
      return output;
    }
  }]);

  function Calendar(container, calendarEvents, template, startDate) {
    _classCallCheck(this, Calendar);

    var _this = _possibleConstructorReturn(this, (Calendar.__proto__ || Object.getPrototypeOf(Calendar)).call(this));

    if (!template) {
      throw new Error('No template');
    }

    _this.activeDate = null;
    _this.activeMonth = {
      firstDate: null,
      lastDate: null
    };

    _this.calendarEvents = Calendar.datesWithEvents(calendarEvents);
    _this.template = template;
    _this.container = $(container);
    _this.datesEl = _this.container.find('.date-wrap');
    _this.monthEl = _this.container.find('.month');
    _this.navEl = _this.container.find('.nav-calendar');
    _this.handleChangeMonth = _this.handleChangeMonth.bind(_this);
    _this.handleSelectDate = _this.handleSelectDate.bind(_this);

    _this.actions();
    _this.setActiveDate(startDate);
    _this.setDates();
    _this.render();
    return _this;
  }

  _createClass(Calendar, [{
    key: 'setDates',
    value: function setDates(d) {
      var date = d || this.activeDate;

      this.activeMonth = {
        firstDate: moment(date).startOf('month'),
        lastDate: moment(date).endOf('month')
      };
    }
  }, {
    key: 'setActiveDate',
    value: function setActiveDate(date) {
      this.activeDate = moment(date || new Date());
    }
  }, {
    key: 'handleChangeMonth',
    value: function handleChangeMonth(e) {
      var el = $(e.target);
      var reRender = false;
      var currentMonth = moment(this.activeMonth.firstDate);
      var anotherMonth = moment(currentMonth);

      if (el.hasClass('next')) {
        this.setDates(anotherMonth.add(1, 'M'));
        this.emit('nextMonth', currentMonth, anotherMonth);
        reRender = true;
      } else if (el.hasClass('prev')) {
        this.setDates(anotherMonth.subtract(1, 'M'));
        this.emit('prevMonth', currentMonth, anotherMonth);
        reRender = true;
      }

      if (reRender) {
        this.render();
      }
    }
  }, {
    key: 'handleSelectDate',
    value: function handleSelectDate(e) {
      var el = $(e.target);
      if (el.hasClass('date-anc') && el.parent().hasClass('event')) {
        this.container.find('.active').removeClass('active');
        el.parent().addClass('active');
        var date = el.data('date');
        this.setActiveDate(date);
        this.emit('selectDate', this.activeDate);
      }
    }
  }, {
    key: 'actions',
    value: function actions() {
      this.navEl.on('click', this.handleChangeMonth);
      this.datesEl.on('click', this.handleSelectDate);
    }
  }, {
    key: 'render',
    value: function render() {
      var end = false;
      var date = moment(this.activeMonth.firstDate);
      var lastDate = this.activeMonth.lastDate;
      var dd = date.day();
      var dates = [];
      dd = dd === 0 ? 7 : dd;

      while (dd > 1) {
        dates.push({
          class: 'empty'
        });
        dd -= 1;
      }

      while (!end) {
        var diff = date.diff(lastDate, 'h');
        if (diff >= 0) {
          end = true;
          break;
        }
        var activeDateDiff = date.diff(this.activeDate, 'h');
        var itemClass = '';
        var ed = date.day();
        itemClass += ed === 6 || ed === 0 ? ' holiday' : '';

        if (this.calendarEvents.indexOf(date.toString()) !== -1) {
          itemClass += ' event';
          itemClass += activeDateDiff > -24 && activeDateDiff < 0 ? ' active' : '';
        } else {
          itemClass += ' empty';
        }

        var item = {
          class: itemClass,
          date: date.toISOString(),
          text: date.format('D'),
          href: '#date-' + date.format('MM-DD')
        };

        dates.push(item);
        date.add('1', 'd');
      }

      var html = this.template({ dates: dates });
      this.datesEl.empty();
      this.datesEl.append(html);
      this.monthEl.text(this.activeMonth.firstDate.format('MMMM'));
    }
  }]);

  return Calendar;
}(EventEmitter);

exports.default = Calendar;

/***/ }),

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _queue = __webpack_require__(9);

var _queue2 = _interopRequireDefault(_queue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _window = window,
    $ = _window.$,
    Foundation = _window.Foundation,
    Spin = _window.Spin,
    IMAGE_HOST = _window.IMAGE_HOST;


var bodyEl = $('body');

var Search = function () {
  function Search(container, template) {
    var _this = this;

    _classCallCheck(this, Search);

    if (!template) {
      throw new Error('No template');
    }

    this.queue = new _queue2.default();

    this.id = null;
    this.timer = null;
    this.searchResult = null;
    this.template = template;
    this.spin = new Spin();
    this.container = container;
    this.searchEl = this.container.find('.search');
    this.resultSeacrhContainer = this.searchEl.find('.result');
    this.searchForm = this.searchEl.find('.search-form');
    this.btnToggle = $('.search-toggle-btn');
    this.btnClose = this.container.find('.close');
    this.input = this.searchForm.find('input');

    this.search = this.search.bind(this);
    this.success = this.success.bind(this);
    this.error = this.error.bind(this);
    this.worker = this.worker.bind(this);

    this.handlerToggleClick = this.handlerToggleClick.bind(this);
    this.handlerBodyClick = this.handlerBodyClick.bind(this);
    this.handlerCloseClick = this.handlerCloseClick.bind(this);
    this.handlerSearchFormSubmit = this.handlerSearchFormSubmit.bind(this);

    this.queue.worker = this.worker;
    this.queue.success = this.success;
    this.queue.error = this.error;

    this.showSpin = this.showSpin.bind(this);
    this.hideSpin = this.hideSpin.bind(this);

    this.btnToggle.on('click', this.handlerToggleClick);
    this.btnClose.on('click', this.handlerCloseClick);
    this.searchForm.submit(this.handlerSearchFormSubmit);
    this.input.on('input', function (e) {
      _this.search();
    });

    bodyEl.on('click', this.handlerBodyClick);
  }

  _createClass(Search, [{
    key: 'handlerBodyClick',
    value: function handlerBodyClick(e) {
      var el = $(e.target);
      if (el.closest(this.searchEl).length === 0 && this.searchEl.is(':visible')) {
        this.btnClose.trigger('click');
      }
    }
  }, {
    key: 'handlerToggleClick',
    value: function handlerToggleClick(e) {
      var _this2 = this;

      var el = '';
      var elTmp = $(e.target);
      if (elTmp.hasClass('search-toggle-btn')) {
        el = elTmp;
      } else {
        el = elTmp.parent();
      }
      this.container.addClass('border-right');
      Foundation.Motion.animateIn(this.searchEl, 'slide-in-right ease-in', function () {
        _this2.container.removeClass('overflow-hidden');
        _this2.container.removeClass('border-right');
        _this2.input.focus();
      });
    }
  }, {
    key: 'handlerCloseClick',
    value: function handlerCloseClick(e) {
      var _this3 = this;

      this.resultSeacrhContainer.empty();
      this.input.val('');
      this.container.addClass('border-right');
      this.container.addClass('overflow-hidden');
      Foundation.Motion.animateOut(this.searchEl, 'slide-out-right ease-out', function () {
        _this3.container.removeClass('border-right');
      });
    }
  }, {
    key: 'handlerSearchFormSubmit',
    value: function handlerSearchFormSubmit(e) {
      e.preventDefault();
      this.search();
    }
  }, {
    key: 'hideSpin',
    value: function hideSpin() {
      var id = this.id,
          spin = this.spin;

      if (id) {
        spin.hide(id);
        this.id = null;
      }
    }
  }, {
    key: 'showSpin',
    value: function showSpin() {
      var id = this.id,
          spin = this.spin;

      this.id = spin.show(this.resultSeacrhContainer);
    }
  }, {
    key: 'search',
    value: function search() {
      var id = this.id,
          input = this.input,
          queue = this.queue;


      this.resultSeacrhContainer.find('.no-found').remove();

      var val = input.val().toLowerCase();

      if (val.length > 2) {
        queue.push(val);
      }
    }
  }, {
    key: 'worker',
    value: function worker(value, done) {
      var _this4 = this;

      this.showSpin();
      $.post('/search/events', { input: value }).done(function (result) {
        _this4.hideSpin();
        done(null, result);
      }).fail(function (err) {
        _this4.hideSpin();
        done(err);
      });
    }
  }, {
    key: 'success',
    value: function success(result) {
      this.searchResult = result.allEvents;
      this.render();
    }
  }, {
    key: 'error',
    value: function error(err) {}
  }, {
    key: 'render',
    value: function render() {
      var html = this.template({ events: this.searchResult, IMAGE_HOST: IMAGE_HOST });
      this.resultSeacrhContainer.empty();
      this.resultSeacrhContainer.append(html);
    }
  }]);

  return Search;
}();

exports.default = Search;

/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _calendar = __webpack_require__(58);

var _calendar2 = _interopRequireDefault(_calendar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = window.$;

var sortableDate = function sortableDate() {
  var filterContainer = $('.filter-schedule');
  var calendarContainer = $('.simple-calendar-ml');
  var calendar = new _calendar2.default(calendarContainer, scheduleEventsDates, template_calendar);

  calendar.on('nextMonth', function (currentMonth, nextMonth) {});

  calendar.on('prevMonth', function (currentMonth, prevMonth) {});

  calendar.on('selectDate', function (activeDate) {});

  filterContainer.on('click', function (e) {
    var el = $(e.target);
    if (el.closest(calendarContainer).length === 0) {
      e.preventDefault();
      if (el.hasClass('filter-nav')) {
        filterContainer.find('.active').removeClass('active');
        el.addClass('active');
        if (el.hasClass('calendar')) {
          calendarContainer.toggle();
          e.stopPropagation();
        }
      }
    }
  });

  $('body').on('click', function (e) {
    var el = $(e.target);
    if (el.closest(calendarContainer).length === 0 && calendarContainer.is(':visible')) {
      calendarContainer.hide();
    }
  });
};

exports.default = sortableDate;

/***/ }),

/***/ 69:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var _window = window,
    $ = _window.$,
    utils = _window.utils;


var sortableSubDate = function sortableSubDate() {
  var container = $('.filter-data-sub-container');
  var items = $('.sortable-items');
  var dataSelector = 'filter-date-alias';
  var noEventsGlb = $('.no-events-global');

  container.each(function (i, e) {
    var containerActual = $(e);
    var allDate = items.find('[data-filter-date-alias]');
    var tomorrowitems = items.find('[data-filter-date-sub-tomorrow]');
    var holidayItems = items.find('[data-filter-date-sub-holiday]');
    var sortingType = containerActual.data('sorting-type');

    containerActual.on('click', function (event) {
      var el = $(event.target);
      if (el.hasClass('filter-sub-item')) {
        event.preventDefault();
        var elAlias = el.data(dataSelector);
        switch (elAlias) {
          case 'all':
            {
              utils.show(allDate);
              utils.checkNoEvents(allDate, noEventsGlb);
              break;
            }
          case 'holiday':
            {
              utils.hide(allDate);
              utils.show(holidayItems);
              utils.checkNoEvents(holidayItems, noEventsGlb);
              break;
            }
          case 'tomorrow':
            {
              utils.hide(allDate);
              utils.show(tomorrowitems);
              utils.checkNoEvents(tomorrowitems, noEventsGlb);
              break;
            }
          default:
            {}
        }
      }
    });
  });
};

exports.default = sortableSubDate;

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
// const $ = window.$;
var $ = window.$;

// const remCalc = (rootEl) => ((n) => n / parseFloat(rootEl.css('font-size')));
var remCalc = function remCalc(rootEl) {
  return function (n) {
    return parseFloat(rootEl.css('font-size')) * n / 16;
  };
};

var remCalcFn = null;

var onScroll = function onScroll(doc, menu, ids, qf, qfF, stHeader, findInContainer, itemsSelector) {
  var menuItems = null;
  var scrollPos = doc.scrollTop();

  if (findInContainer) {
    menuItems = menu.find(itemsSelector);
  } else {
    menuItems = menu;
  }

  menuItems.each(function (i, e) {
    var currLink = $(e);
    // const refElement = ids.filter(currLink.attr('href'));
    var refElement = $(currLink.attr('href'));
    var refElTop = Math.floor(refElement.position().top);
    var refHeight = refElement.height();
    if (refElTop - stHeader.height() - remCalcFn(qf) - 1 <= scrollPos && refElTop + refElement.height() > scrollPos) {
      menu.removeClass('active');
      currLink.addClass('active');
      // return false;
    } else {
      currLink.removeClass('active');
    }
  });
};

var findEl = function findEl(e, anchorClass) {
  var c = $(e.target);
  var el = null;
  if (c.hasClass(anchorClass)) {
    el = c;
  } else {
    el = c.parent();
  }
  return el;
};

var anchors = function anchors() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  remCalcFn = remCalc($('html')).bind(null);
  var doc = $(document);
  var fnFindEL = options.findEl || findEl;
  var dynamic = options.dynamic || false;
  var container = options.container || $('html, body');
  var stHeader = options.stHeader || $('.anchors-sticky-inner');
  var elements = options.elements || $('.anchor');
  var findInContainer = options.findInContainer || false;
  var itemsSelector = options.itemsSelector || '';

  var menu = options.menu || elements;
  var qf = options.qf || 30;
  var qfF = options.qfF || 0;
  var qfCl = options.qfCl || 0;
  var points = options.points || $('.anchor-point');
  var anchorClass = options.anchorClass || 'anchor';

  var fnScroll = onScroll.bind(null, doc, menu, points, qf, qfF, stHeader, findInContainer, itemsSelector);

  doc.on('scroll', fnScroll);

  elements.on('click', function (e) {
    var el = fnFindEL(e, anchorClass);
    if (!el || el.length === 0) {
      return true;
    }

    e.preventDefault();
    doc.off('scroll');

    elements.removeClass('active');
    el.addClass('active');

    var href = el.attr('href');
    var scrollTop = 0;

    // if (stHeader.hasClass('is-stuck')) {
    //   scrollTop = $(href).offset().top - stHeader.outerHeight() - qf;
    // } else {
    //   scrollTop = $(href).offset().top - stHeader.outerHeight() - qfCl;
    // }

    if (stHeader.hasClass('is-stuck')) {
      scrollTop = $(href).offset().top - stHeader.outerHeight() - remCalcFn(qfCl);
    } else {
      scrollTop = $(href).offset().top + remCalcFn(qfF) - stHeader.outerHeight() - remCalcFn(qfCl);
    }
    container.stop().animate({
      scrollTop: scrollTop
    }, 500, 'swing', function () {
      window.location.hash = href;
      doc.on('scroll', fnScroll);
    });
    return false;
  });
};

exports.default = anchors;

/***/ }),

/***/ 87:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(33);


/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stack = __webpack_require__(11);

var _stack2 = _interopRequireDefault(_stack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Queue = function () {
  function Queue() {
    _classCallCheck(this, Queue);

    this.queueStack = new _stack2.default();
    this.timeoutDelay = 500;
    this.successCallback = null;
    this.errorCallback = null;
    this.workerFn = null;

    this.lastPush = null;

    this.run = this.run.bind(this);
    this.push = this.push.bind(this);
    this.clear = this.clear.bind(this);
    this.next = this.next.bind(this);
  }

  _createClass(Queue, [{
    key: 'push',
    value: function push(value) {
      var output = this.queueStack.push(value);
      this.lastPush = new Date();
      this.run();
      return output;
    }
  }, {
    key: 'clear',
    value: function clear() {
      this.queueStack.clear();
    }
  }, {
    key: 'run',
    value: function run() {
      var _this = this;

      var timeoutDelay = this.timeoutDelay,
          next = this.next,
          queueStack = this.queueStack,
          workerFn = this.workerFn;

      var t = new Date();

      setTimeout(function () {
        if (_this.lastPush > t) return next();
        var value = queueStack.pop();
        queueStack.clear();
        if (!value || !(workerFn instanceof Function)) return undefined;
        return workerFn(value, next);
      }, timeoutDelay);
    }
  }, {
    key: 'next',
    value: function next(err, data) {
      var queueStack = this.queueStack,
          errorCallback = this.errorCallback,
          successCallback = this.successCallback,
          run = this.run;


      if (err) {
        if (errorCallback instanceof Function) return errorCallback(err);
        return undefined;
      }
      if (data) {
        queueStack.clear();
        if (successCallback instanceof Function) return successCallback(data);
        return undefined;
      }
      if (!queueStack || queueStack.isEmpty()) {
        return undefined;
      }

      return run();
    }
  }, {
    key: 'stack',
    set: function set(stack) {
      this.queueStack = stack;
    }
  }, {
    key: 'success',
    set: function set(success) {
      this.successCallback = success;
    }
  }, {
    key: 'error',
    set: function set(error) {
      this.errorCallback = error;
    }
  }, {
    key: 'worker',
    set: function set(fn) {
      this.workerFn = fn;
    }
  }, {
    key: 'delay',
    set: function set(value) {
      this.timeoutDelay = value;
    },
    get: function get() {
      return this.timeoutDelay;
    }
  }]);

  return Queue;
}();

exports.default = Queue;

/***/ })

/******/ });