const passport = require('passport');

const local = require('./../passport/local');
const facebook = require('./../passport/facebook');
const vk = require('./../passport/vk');

const loggedInUserQuery = require('./../graphql/loggedInUserQuery');

local();
facebook();
vk();

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  loggedInUserQuery({ user })
    .then(({ data, errors, extensions }, error) => {
      if (error || errors) return Promise.reject(new Error(error || errors));
      if (!data) return done(null, false);
      if (data.loggedInUser) return done(null, user);
      return done(null, false);
    })
    .catch(done);
});

module.exports = (app) => {
  app.use(passport.initialize());
  app.use(passport.session());
};
