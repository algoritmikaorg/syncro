const redis = require('./redis');

module.exports = () => redis.batch([
  ['get', 'template'],
  ['get', 'setting'],
]).execAsync();
