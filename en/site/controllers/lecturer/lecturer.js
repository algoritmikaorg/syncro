const petrovich = require('petrovich');

const redis = require('./../../db/redis');
const { graphQlFetch } = require('./../../graphql');
const { ROOT_TOKEN } = require('./../../config');

const uniqEventsLecturesOfEvents = (events) => {
  const lecturesIds = [];
  return events.filter(({ lecture }) => {
    const { id } = lecture;
    if (!lecturesIds.includes(id)) {
      lecturesIds.push(id);
      return true;
    }
    return false;
  });
};

const lecturerByIdQuery = (req, lecturer) => {
  const { id } = lecturer;
  return graphQlFetch({
    req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
    query: 'lecturerByIdQuery',
    variables: {
      id,
      date: new Date(),
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      const { Lecturer, allLectures } = data;

      const LecturerWithAdditionalData = Object.assign(
        {},
        Lecturer,
        {
          genitive: petrovich.male.first.genitive(Lecturer.firstName),
          lectures: allLectures,
        },
      );

      const dataWithAdditionalData = Object.assign(
        {},
        data,
        {
          Lecturer: LecturerWithAdditionalData,
          allReviews: allLectures && allLectures.length > 0 ? allLectures
            .map(({ reviews }) => reviews)
            .reduce((prev, curr) => prev.concat(curr)) : [],
        },
      );
      const currentPageData = Object.assign(
        {},
        dataWithAdditionalData,
      );
      return currentPageData;
      // return null;
    });
};

module.exports = (req, lecture) => {
  return Promise.all([
    redis.batch([
      ['get', 'template'],
    ]).execAsync(),
    lecturerByIdQuery(req, lecture),
  ])
    .then((result) => {
      const [[template, howWork, gallery], data] = result;
      return Object.assign(
        {},
        data,
        { templateData: JSON.parse(template) },
        { ___typename: 'Lecturer' },
      );
    });
};
