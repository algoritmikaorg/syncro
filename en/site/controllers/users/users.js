// const redis = require('./../../db/redis');
const { graphQlFetch } = require('./../../graphql');

const profileByIdQuery = (req) => {
  if (!(req.user && req.user.id && req.user.token)) return Promise.reject(new Error());
  return graphQlFetch({
    req,
    query: 'userProfileByIdQuery',
    variables: {
      id: req.user.id,
    },
  });
};

module.exports = () => {

};

module.exports.profile = (req) => {
  return profileByIdQuery(req)
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.User;
    });
};
