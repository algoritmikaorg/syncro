const redis = require('./../../db/redis');
const { graphQlFetch } = require('./../../graphql');
const { ROOT_TOKEN } = require('./../../config');

const accountQuery = (req) => {
  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'accountQuery',
    variables: {
      id: req.user.id,
      date: new Date(),
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};

const noAuth = (req) => {
  return redis.getAsync('template')
    .then((reply) => {
      // if (!reply) return Promise.reject();
      return {
        templateData: JSON.parse(reply),
      };
    });
};

module.exports = (req) => {
  if (!req.isAuthenticated()) {
    return noAuth(req);
  }

  return Promise.all([
    redis.getAsync('template'),
    accountQuery(req),
  ])
    .then((result) => {
      const [template, data] = result;
      const { User: { payments } } = data;
      let certificatesNotPayment = [];
      let subscriptionsNotPayment = [];

      if (payments && payments.length > 0) {
        certificatesNotPayment = payments
          .filter(({ certificate }) => certificate)
          .map((payment) => {
            const { certificate } = payment;
            return Object.assign({}, certificate, { payment });
          });

        subscriptionsNotPayment = payments
          .filter(({ subscription }) => subscription)
          .map((payment) => {
            const { subscription } = payment;
            return Object.assign({}, subscription, { payment });
          });
      }

      return Object.assign(
        {},
        data,
        {
          certificatesNotPayment,
          subscriptionsNotPayment,
          templateData: JSON.parse(template),
        },
      );
    });
};

module.exports.update = (req) => {
  const {
    avatar,
    firstName,
    lastName,
    phone,
    email,
  } = req;
};
