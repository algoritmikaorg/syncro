module.exports = (phone) => {
  if (!phone) return phone;
  return phone.replace(/[^0-9]/g, '').replace(/^/g, '+');
};
