const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/review');

router.post('/', (req, res, next) => {
  let { review } = req.body;
  const { lectureId, eventId } = req.body;
  review = review.trim();

  if (!review || !lectureId || !eventId) return res.json({ data: null, error: true });

  return controllerAsync(req)
    .then((result) => {
      return res.json({ data: {}, error: false });
    })
    .catch((err) => {
      return res.json({ data: null, error: true });
    });
});

module.exports = router;
