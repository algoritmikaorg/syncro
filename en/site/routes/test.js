const express = require('express');
const router = express.Router();
const cuid = require('cuid');

const { graphQlFetch } = require('./../graphql');

/* GET home page. */
router.get('/', (req, res, next) => {
  graphQlFetch({ req, query: 'mainPageQuery' })
    .then((...args) => {
    })
    .catch((err) => {
      console.log('err', err);
    });
  const query = {
    query: `
      {
        mainPage {
          lectures {
            id,
            title,
          }
        }
      }
    `,
    variables: null,
    operationName: null,
  };
});

router.get('/order', (req, res, next) => {
  res.render('pages/test-order', { id: cuid() });
});

module.exports = router;
