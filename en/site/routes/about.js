const express = require('express');
const router = express.Router();

const redis = require('./../db/redis');
const { graphQlFetch } = require('./../graphql');
// const templateQuery = require('./../graphql/templateQuery');

const allAboutPagesQuery = (req) => {
  return graphQlFetch({ req, query: 'allAboutPagesQuery' })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};

/* GET home page. */
router.get('/', (req, res, next) => {
  Promise.all([
    allAboutPagesQuery(req),
    redis.getAsync('template'),
  ])
    .then((result) => {
      const [data, template] = result;

      const currentPageData = Object.assign(
        {
          user: req.user,
          templateData: JSON.parse(template),
        },
        data,
      );

      res.render('pages/about', { currentPageData });
    })
    .catch((error) => {
      next(error);
    });
});

module.exports = router;
