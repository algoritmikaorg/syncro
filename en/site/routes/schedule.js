const express = require('express');
const router = express.Router();
const moment = require('moment');
const validator = require('validator');

const redis = require('./../db/redis');
const { graphQlFetch } = require('./../graphql');
// const templateQuery = require('./../graphql/templateQuery');

const groupSheduleOfDate = (events) => {
  const output = [];
  const obj = {};
  events.forEach((event) => {
    const key = moment(event.date).format('YYYY-MM-DD');
    if (!obj[key]) {
      obj[key] = [];
    }
    obj[key].push(event);
  });
  return Object.keys(obj).map(key => ({ date: new Date(key), events: obj[key] }));
};

const allEventsOfDates = (req) => {
  const date_gte = new Date();
  const date_lte = moment().add(30, 'days').endOf('day');
  return graphQlFetch({ req, query: 'allEventsOfDates', variables: { date_gte } })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return {
        groupSheduleOfDate: groupSheduleOfDate(data.allEvents),
      };
    });
};

const allEventsActiveDate = (req) => {
  const date_gte = new Date();

  return graphQlFetch({ req, query: 'allEventsActiveDate', variables: { date_gte } })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return {
        allEventsActiveDate: data.allEvents,
      };
    });
};

const allTagsQuery = (req) => {
  return graphQlFetch({ req, query: 'allTagsQuery' })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);

      data.allTags.unshift({
        title: 'Все темы',
        alias: {
          alias: 'all',
        },
      });
      return data;
    });
};

router.get('/', (req, res, next) => {
  Promise.all([
    allEventsOfDates(req),
    allEventsActiveDate(req),
    allTagsQuery(req),
    redis.getAsync('template'),
  ])
    .then((result) => {
      const [ data1, data2, data3, template ] = result;
      const currentPageData = Object.assign(
        {
          user: req.user,
          templateData: JSON.parse(template),
        },
        data1,
        data2,
        data3,
      );

      res.render('pages/schedule', { currentPageData });
    })
    .catch((error) => {
      next(error);
    });
});

router.post('/events', (req, res) => {
  const date = validator.toDate(req.body.date);
  if (!date) {
    res.json({ error: true });
    return null;
  }

  let date_gte = moment(date).subtract(7, 'days').startOf('day');
  const date_lte = moment(date).add(7, 'days').endOf('day');
  if (date_gte.valueOf() < Date.now()) {
    date_gte = new Date();
  }

  graphQlFetch({ req, query: 'allEventsOfDates', variables: { date_gte, date_lte } })
    .then(({ data, errors }) => {
      if (errors) {
        res.json({ error: true });
        return null;
      }
      res.json({
        data: {
          groupSheduleOfDate: groupSheduleOfDate(data.allEvents),
        },
      });
    })
    .catch((err) => { res.json({ error: true }); });
});

module.exports = router;
