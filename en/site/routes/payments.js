// Нотификации по http(s) - http://dev.synchronize.ru/payments/status
// Страница успешной оплаты - http://dev.synchronize.ru/payments/success
// Страница ошибки платежа - http://dev.synchronize.ru/payments/fail

const express = require('express');
const request = require('request');
const async = require('async');
const router = express.Router();

const controllerAsync = require('./../controllers/payment');

router.post('/status', (req, res, next) => {
  return controllerAsync.statusCallback(req)
    .then(() => res.status(200).send('OK'))
    .catch(next);
});

router.post('/success', (req, res, next) => {
  const { params } = req;
  const { body } = req;
  const { query } = req;
  res.status(200).send('OK');
});

router.post('/fail', (req, res, next) => {
  const { params } = req;
  const { body } = req;
  const { query } = req;
  res.status(200).send('OK');
});

router.get('/status', (req, res, next) => {
  const { params } = req;
  const { body } = req;
  const { query } = req;
  res.status(200).send('OK');
});

router.get('/success', (req, res, next) => {
  controllerAsync.success(req)
    .then((result) => {
      res.render('pages/success', { currentPageData: result || {}, user: req.user })
    })
    .catch(next);
});

router.get('/fail', (req, res, next) => {
  controllerAsync.fail(req)
    .then((result) => {
      res.render('pages/fail', { currentPageData: result || {}, user: req.user })
    })
    .catch(next);
});

router.post('/order', (req, res, next) => {
  if (!req.body.id) return next();
  controllerAsync.order(req)
    .then((url) => {
      if (!url) return next(new Error('Acquiring not url'));
      res.redirect(url);
    })
    .catch(next);
});

router.post('/certificate', (req, res, next) => {
  if (!req.body.id) return next();
  controllerAsync.certificate(req)
    .then((url) => {
      if (!url) return next(new Error('Acquiring not url'));
      res.redirect(url);
    })
    .catch(next);
});

router.post('/subscription', (req, res, next) => {
  if (!req.body.id) return next();
  controllerAsync.subscription(req)
    .then((url) => {
      if (!url) return next(new Error('Acquiring not url'));
      res.redirect(url);
    })
    .catch(next);
});

module.exports = router;
