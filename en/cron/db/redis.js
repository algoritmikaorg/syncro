const redis = require('redis');
const { promisify } = require('util');

const { REDIS: { HOST, PORT, PASS } } = require('./../config');

const client = redis.createClient({
  host: HOST,
  port: PORT,
  password: PASS,
});

const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const delAsync = promisify(client.del).bind(client);
const scanAsync = promisify(client.scan).bind(client);
const flushallAsync = promisify(client.flushall).bind(client);

redis.Multi.prototype.execAsync = promisify(redis.Multi.prototype.exec);

module.exports = client;
module.exports.getAsync = getAsync;
module.exports.setAsync = setAsync;
module.exports.delAsync = delAsync;
module.exports.scanAsync = scanAsync;
module.exports.flushallAsync = flushallAsync;
