const { pool } = require('./../cron');
const { graphQlFetch } = require('./../graphql');

const allEventsQueryAndPool = () => graphQlFetch({
  query: 'allEventsQuery',
  variables: { date: new Date() },
})
  .then(({ data, errors, error }) => {
    if (errors || error) return Promise.reject(errors || error);
    data.allEvents.map((event) => {
      pool.add(event);
    });
    return Promise.resolve({});
  });

module.exports = (app) => {
  Promise.all([
    allEventsQueryAndPool(),
  ])
    .then((results) => {
      const err = [];
      results.forEach(({ data, errors, error }) => {
        if (errors) err.push(errors);
        if (error) err.push(error);
      });
      if (err.length > 0) return Promise.reject(err);
      return Promise.resolve(results);
    })
    .catch((err) => {
      console.error('Initial fail', err);
      throw new Error(err);
    });
};
