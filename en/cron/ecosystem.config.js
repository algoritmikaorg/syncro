module.exports = {
  apps: [
    {
      name: 'cron',
      script: './bin/www',
      watch: false,
      env: {
        PORT: 60005,
        NODE_ENV: 'development',
        LOCATION_ENV: 'dev',
      },
      env_production: {
        PORT: 60005,
        NODE_ENV: 'production',
        LOCATION_ENV: 'prod',
      },
    },
  ],
};
