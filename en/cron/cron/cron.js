const { CronJob } = require('cron');

const reminderOfNonPaymentJob = require('./jobs/reminderOfNonPayment');
const reminderUpcomingEventJob = require('./jobs/reminderUpcomingEvent');

const timeZone = 'Europe/Moscow';
const start = true;
// const runOnInit = true;
const runOnInit = false;

const reminderUpcomingEven = new CronJob({
  // cronTime: '* * * * * * ',
  cronTime: '0 09 * * *',
  onTick: reminderUpcomingEventJob.onTick,
  onComplete: reminderUpcomingEventJob.onComplete,
  start,
  timeZone,
  runOnInit,
});

// const reminderOfNonPayment = new CronJob({
//   cronTime: '* * * * * *',
//   onTick: reminderOfNonPaymentJob.onTick,
//   onComplete: reminderOfNonPaymentJob.onComplete,
//   start,
//   timeZone,
//   runOnInit,
// });

module.exports = {
  reminderUpcomingEven,
  // reminderOfNonPayment,
};
