const { CronJob } = require('cron');

const afterEndOfEvent = require('./jobs/afterEndOfEvent');

const timeZone = 'Europe/Moscow';
const start = true;

// const endEventDate = (date, duration) => new Date(new Date(date).getTime() + (duration * 3600000));
const endEventDate = (date, duration) => new Date(date);

class CronOncePull {
  constructor() {
    this.pool = {};
    this.add = this.add.bind(this);
    this.update = this.update.bind(this);
    this.remove = this.remove.bind(this);
  }
  add(node) {
    const { pool } = this;
    const { id, date, lecture: { duration } } = node;

    pool[id] = new CronJob(
      endEventDate(date, duration),
      () => {
        afterEndOfEvent.onTick(id);
      },
      afterEndOfEvent.onComplete,
      start,
      timeZone,
    );

    return Object.keys(pool).length;
  }
  update(previousValues, node) {
    const { pool, add } = this;
    const { id } = node;
    if (pool[id]) {
      pool[id].stop();
      delete pool[id];
    }
    return add(node);
  }
  remove(data) {
    const { pool } = this;
    const { previousValues: { id } } = data;
    pool[id].stop();
    delete pool[id];
    return Object.keys(pool).length;
  }
  get length() {
    return Object.keys(this.pool).length;
  }
}

module.exports = new CronOncePull()
