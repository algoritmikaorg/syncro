const { DateTime } = require('luxon');

module.exports = (date) => {
  if (!(date instanceof Date)) return undefined;
  const dt = DateTime.fromJSDate(date).toUTC();
  const dtn = dt.plus({ days: 1 });
  return {
    start: dtn.startOf('days').toISO(),
    end: dtn.endOf('days').toISO(),
  };
};
